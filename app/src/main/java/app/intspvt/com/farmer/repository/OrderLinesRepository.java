package app.intspvt.com.farmer.repository;

import androidx.lifecycle.MutableLiveData;

import java.net.ConnectException;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.OrderDataHistory;
import app.intspvt.com.farmer.rest.response.SingleOrderHistory;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderLinesRepository {
    private static OrderLinesRepository mInstance;
    private OrderDataHistory orderDataHistory;

    public static OrderLinesRepository getInstance() {
        if (mInstance == null)
            mInstance = new OrderLinesRepository();
        return mInstance;
    }

    public MutableLiveData<OrderDataHistory> getOrderLines(Long id, String forFrgament) {
        final MutableLiveData<OrderDataHistory> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<SingleOrderHistory> call;
        if (forFrgament.equals("dehaati")) {
            call = client.getSingleDeHaatOrder(id);
        } else {
            call = client.getSingleOrder(id);
        }
        call.enqueue(new Callback<SingleOrderHistory>() {
            @Override
            public void onResponse(Call<SingleOrderHistory> call, Response<SingleOrderHistory> response) {
                if (response.body() == null)
                    return;
                orderDataHistory = null;
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        orderDataHistory = response.body().getData();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }

                data.setValue(orderDataHistory);
            }

            @Override
            public void onFailure(Call<SingleOrderHistory> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Farmer.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Farmer.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(null);
            }


        });
        return data;
    }
}

