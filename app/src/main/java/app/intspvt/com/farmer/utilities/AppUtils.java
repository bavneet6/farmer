package app.intspvt.com.farmer.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.amazonaws.HttpMethod;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;

import org.json.JSONException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.LoginActivity;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.FarmerInfo;
import app.intspvt.com.farmer.rest.response.GetIdNames;
import app.intspvt.com.farmer.rest.response.GetPersonalInfo;
import app.intspvt.com.farmer.rest.response.ProductFileTable;
import app.intspvt.com.farmer.rest.response.VersionName;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by DELL on 6/22/2017.
 */

public class AppUtils {

    private static ProgressDialog progressDialog;
    private static DatabaseHandler databaseHandler;
    private static String appPackageName, versionName;

    public static void showToast(String msg) {
        Context context = Farmer.getInstance();
        if (context != null && msg != null)
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void showProgressDialog(Context context) {
        if (context != null) {
            if (progressDialog != null)
                return;
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(context.getString(R.string.please_wait));
            progressDialog.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    public static void imageSelection(Fragment fragment, int count) {
        Options options = Options.init()
                .setRequestCode(100)
                .setCount(count)
                .setFrontfacing(false)
                .setImageQuality(ImageQuality.HIGH)
                .setImageResolution(1024, 800)
                .setScreenOrientation(Options.SCREEN_ORIENTATION_FULL_SENSOR)
                .setPath("/dehaat/new");
        Pix.start(fragment, options);
    }

    public static boolean haveNetworkConnection(Context activity) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public static void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        progressDialog = null;
    }

    public static void changeFragment(FragmentActivity activity, Fragment fragment) {
        if (activity == null)
            return;
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frag_container, fragment).addToBackStack("").commitAllowingStateLoss();
    }

    public static int getSelectedId(ArrayList<GetIdNames.GetData> names, Long sid) {
        int id = 0;
        for (int i = 0; i < names.size(); i++)
            if (names.get(i).getId() == sid)
                return i;
        return id;
    }

    public static void showSessionExpiredDialog(final Activity context) {
        AppPreference.getInstance().setAPP_LOGIN(false);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("कृप्या फिरसे लॉगिन करें");
        builder.setCancelable(false);

        builder.setPositiveButton("ठीक है", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                databaseHandler = new DatabaseHandler(context);
                AppPreference.getInstance().clearData();
                Intent i = new Intent(context, LoginActivity.class);
                i.putExtra("logout", "logout");
                databaseHandler.clearCartTable();
                databaseHandler.clearIssueStatusSeen();
                databaseHandler.clearURlsData();
                Farmer.getInstance().clearApplicationData();
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                context.finish();
                dialog.dismiss();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public static boolean isNullCase(String test) {
        return (test == null) || (test.equals("")) || (test.toLowerCase().equals("false")) ||
                (test.equals("0.0")) || (test.equals("0"));
    }

    public static void showCartCount(Activity activity, TextView cart_count, RelativeLayout cart_back) {
        if (activity == null)
            return;
        if (databaseHandler == null)
            databaseHandler = new DatabaseHandler(activity);
        if (databaseHandler != null) {
            cart_count.setText("" + databaseHandler.getAllCartData().size());
            if (databaseHandler.getAllCartData().size() != 0) {
                final Animation animation = AnimationUtils.loadAnimation(activity,
                        R.anim.scale_text);
                cart_back.setAnimation(animation);
            }
        }
    }

    public static void storeFarmerInfo(FarmerInfo farmerInfo) {
        // storing personal details
        AppPreference.getInstance().setName(farmerInfo.getName());
        if (AppUtils.isNullCase(farmerInfo.getZip().toString()))
            AppPreference.getInstance().setPINCODE(0L);
        else
            AppPreference.getInstance().setPINCODE(farmerInfo.getZip());
        int i = farmerInfo.getTitle();
        if (i == 6)
            AppPreference.getInstance().setGender(UrlConstant.MALE);
        else
            AppPreference.getInstance().setGender(UrlConstant.FEMALE);

        AppPreference.getInstance().setSTATE(farmerInfo.getState());
        AppPreference.getInstance().setDISTRICT(farmerInfo.getDistrict());
        AppPreference.getInstance().setBLOCK(farmerInfo.getBlock());
        AppPreference.getInstance().setPANCHAYAT(farmerInfo.getPanchayat());
        AppPreference.getInstance().setVILLAGE(farmerInfo.getVillage());

        AppPreference.getInstance().setIMAGE(farmerInfo.getImage_small());
    }

    public static void storeNodeData(GetPersonalInfo.GetInfo.NodeInfo node_info) {
        AppPreference.getInstance().setTOLL_FREE(node_info.getToll_free());
        AppPreference.getInstance().setNODE_NAME(node_info.getName());
        if (node_info.getLatitude() != null && node_info.getLatitude() > 0)
            AppPreference.getInstance().setLATITUDE("" + node_info.getLatitude());
        else
            AppPreference.getInstance().setLATITUDE(null);
        if (node_info.getLongitude() != null && node_info.getLongitude() > 0)
            AppPreference.getInstance().setLONGITUDE("" + node_info.getLongitude());
        else
            AppPreference.getInstance().setLONGITUDE(null);
    }

    public static Address getLatLongFromPin(Activity activity, String pincode) {
        if (activity == null)
            return null;
        final Geocoder geocoder = new Geocoder(activity);
        try {
            List<Address> addresses = geocoder.getFromLocationName(pincode, 1);
            if (addresses != null && !addresses.isEmpty()) {
                Address address = addresses.get(0);
                return address;
            } else {
            }
        } catch (IOException e) {
        }
        return null;
    }

    public static void generateAndCheckUrl(Context context, String picFilePath) {
        databaseHandler = new DatabaseHandler(context);
        ProductFileTable productFileTable = new ProductFileTable();
        productFileTable.setProd_file(picFilePath);
        URL url = null;
        try {
            url = new AppUtils.FetchPicture(picFilePath).execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        productFileTable.setProd_url("" + url);
        databaseHandler.inserPicturesFile(productFileTable);
    }

    public static void getUpdateApp(Activity context) {
        if (context == null)
            return;
        appPackageName = context.getPackageName();
        try {
            versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        getVersionName(context);
    }

    private static void getVersionName(final Activity activity) {
        AppRestClient client = AppRestClient.getInstance();
        Call<VersionName> call = client.getVersionName();
        call.enqueue(new ApiCallback<VersionName>() {
            @Override
            public void onResponse(Response<VersionName> response) {
                if (response.body() == null)
                    return;
                String version = response.body().getVersion();
                if (!version.equals(versionName)) {
                    openDialog(activity);
                }
            }

            @Override
            public void onResponse401(Response<VersionName> response) throws JSONException {

            }


        });
    }

    private static void openDialog(final Activity context) {
        final Dialog dialog = new Dialog(context);
        TextView okay;
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_update);
        okay = dialog.findViewById(R.id.okay);
        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                dialog.dismiss();
            }
        });
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }

    public static class FetchPicture extends android.os.AsyncTask<String, String, URL> {
        URL url = null;
        String photo;

        public FetchPicture(String photo1) {
            photo = photo1;
        }

        @Override
        protected URL doInBackground(String... strings) {
            if (haveNetworkConnection(Farmer.getInstance().getApplicationContext())) {
                AmazonS3 s3Client = new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider());
                GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(UrlConstant.TEMP_FNF_DB, photo);
                generatePresignedUrlRequest.setMethod(HttpMethod.GET);
                url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
            }
            return url;
        }
    }

}
