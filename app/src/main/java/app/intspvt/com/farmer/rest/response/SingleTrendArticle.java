package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

public class SingleTrendArticle {
    @SerializedName("data")
    private TrendingData trendingData;

    public TrendingData getTrendingData() {
        return trendingData;
    }
}
