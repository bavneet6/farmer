package app.intspvt.com.farmer.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.fragment.SingleTrendingArticleFragment;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.rest.response.TrendingData;
import app.intspvt.com.farmer.utilities.AppUtils;

import static app.intspvt.com.farmer.utilities.AppUtils.isNullCase;

/**
 * Created by DELL on 2/14/2018.
 */

public class TrendingRecyclerAdapter extends RecyclerView.Adapter<TrendingRecyclerAdapter.Articles> {
    private Context context;
    private ArrayList<TrendingData> articleList;
    private String getPic = null;
    private HashMap<Long, ArrayList<String>> trendingArticleMap = new HashMap<>();
    private DatabaseHandler databaseHandler;

    public TrendingRecyclerAdapter(Context context, ArrayList<TrendingData> articleList) {
        this.context = context;
        this.articleList = articleList;
        databaseHandler = new DatabaseHandler(context);
        trendingArticleMap.clear();
        for (int i = 0; i < articleList.size(); i++) {
            ArrayList<String> articleNameIdList = new ArrayList<>();
            articleNameIdList.add("" + articleList.get(i).getId());
            articleNameIdList.add("" + articleList.get(i).getName());
            trendingArticleMap.put(Long.valueOf(i), articleNameIdList);
        }

    }

    @Override
    public TrendingRecyclerAdapter.Articles onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_trending_articles, parent, false);
        context = parent.getContext();
        return new TrendingRecyclerAdapter.Articles(itemView);

    }

    @Override
    public void onBindViewHolder(final TrendingRecyclerAdapter.Articles holder, int position) {
        final int pos = holder.getAdapterPosition();
        holder.heading.setText(articleList.get(pos).getName());
        String input = articleList.get(pos).getContent();
        // the content of a particular article contains \n then its converted into empty spaces
        // to trim the spaces
        input = input.replace("\n", "");
        input = input.trim();
        holder.content.setText(input);
        showPicassoImage(pos, holder.image);

        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("ARTICLE_ID", articleList.get(pos).getId());
                bundle.putLong("ARTICLE_POS", pos);
                bundle.putSerializable("ARTICLE_MAP", trendingArticleMap);
                SingleTrendingArticleFragment fragment = SingleTrendingArticleFragment.newInstance();
                fragment.setArguments(bundle);
                AppUtils.changeFragment((FragmentActivity) context, fragment);
            }
        });

    }

    private void showPicassoImage(int pos, final ImageView imageView) {
        AppUtils.generateAndCheckUrl(context, articleList.get(pos).getImage());
        String photo = articleList.get(pos).getImage();
        getPic = null;
        if (!isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(imageView, new PicassoCallback(photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_crop_image).into(imageView);
                    }
                });

            } else {
                imageView.setImageResource(R.drawable.no_crop_image);
            }
        }
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    public class Articles extends RecyclerView.ViewHolder {
        private TextView heading, content;
        private ImageView image;
        private LinearLayout back;

        public Articles(View itemView) {
            super(itemView);
            back = itemView.findViewById(R.id.back);
            heading = itemView.findViewById(R.id.heading);
            content = itemView.findViewById(R.id.content);
            image = itemView.findViewById(R.id.trend_img);
        }

    }
}
