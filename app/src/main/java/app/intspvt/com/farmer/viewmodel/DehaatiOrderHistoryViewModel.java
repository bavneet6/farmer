package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import app.intspvt.com.farmer.repository.DehaatiOrderHistoryRepository;
import app.intspvt.com.farmer.rest.response.OrderDataHistory;

public class DehaatiOrderHistoryViewModel extends ViewModel {
    private MutableLiveData<List<OrderDataHistory>> orderList;
    private DehaatiOrderHistoryRepository orderHistoryRepository;

    public void init() {
        if (orderHistoryRepository == null)
            orderHistoryRepository = DehaatiOrderHistoryRepository.getInstance();
        orderList = orderHistoryRepository.getOrderData();
    }

    public LiveData<List<OrderDataHistory>> getHistoryData() {
        return orderList;
    }
}
