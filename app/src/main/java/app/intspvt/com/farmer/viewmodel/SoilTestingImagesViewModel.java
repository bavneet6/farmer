package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import app.intspvt.com.farmer.repository.SoilTestingImagesRepository;
import app.intspvt.com.farmer.rest.response.SoilTestingReport;

public class SoilTestingImagesViewModel extends ViewModel {

    private MutableLiveData<ArrayList<SoilTestingReport.Data>> soilImageslist;
    private SoilTestingImagesRepository soilTestingImagesRepository;

    public void init() {
        if (soilTestingImagesRepository == null)
            soilTestingImagesRepository = SoilTestingImagesRepository.getInstance();
        soilImageslist = soilTestingImagesRepository.getSoilImagesList();
    }

    public LiveData<ArrayList<SoilTestingReport.Data>> getSoilImagesList() {
        return soilImageslist;
    }
}
