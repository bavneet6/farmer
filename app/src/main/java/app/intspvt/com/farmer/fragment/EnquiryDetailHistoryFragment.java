package app.intspvt.com.farmer.fragment;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterEnquiryImageList;
import app.intspvt.com.farmer.adapter.RecyclerAdapterEnquiryReplies;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.EnquiryDataHistory;
import app.intspvt.com.farmer.rest.response.EnquiryReply;
import app.intspvt.com.farmer.rest.response.IssueDbStatus;
import app.intspvt.com.farmer.rest.response.SingleIssueHistory;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by DELL on 3/23/2018.
 */

public class EnquiryDetailHistoryFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = EnquiryDetailHistoryFragment.class.getSimpleName();
    private static MediaPlayer mediaPlayer = null;
    private static ImageView previous = null;
    private static int previousImg;
    private static DatabaseHandler databaseHandler;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ArrayList<EnquiryReply> enquiryReply = new ArrayList<>();
    private ArrayList<String> images = new ArrayList<>();
    private ImageView back, play;
    private LinearLayoutManager layoutManager;
    private TextView status, id, description, create_date, addMore, solution;
    private RecyclerView imagesRecy, repliesRecy;
    private LinearLayout no_reply, solutionBack;
    private Long issue_id = 0L;
    private String picUrlDb;
    private boolean checkPic;
    private EnquiryDataHistory enquiryDataHistory;

    public static EnquiryDetailHistoryFragment newInstance() {
        return new EnquiryDetailHistoryFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_single_enquiry, null);
        MainActivity.showCallAndHome(true);
        databaseHandler = new DatabaseHandler(getActivity());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Bundle bundle = getArguments();
        if (bundle != null) {
            issue_id = bundle.getLong("ISSUE_ID");
        }
        mediaPlayer = new MediaPlayer();

        back = v.findViewById(R.id.back);
        play = v.findViewById(R.id.play);
        status = v.findViewById(R.id.enStatus);
        id = v.findViewById(R.id.enId);
        description = v.findViewById(R.id.enHeading);
        create_date = v.findViewById(R.id.enDate);
        solution = v.findViewById(R.id.solution);
        no_reply = v.findViewById(R.id.no_reply);
        solutionBack = v.findViewById(R.id.solutionBack);
        imagesRecy = v.findViewById(R.id.images_rec);
        imagesRecy.setNestedScrollingEnabled(false);
        repliesRecy = v.findViewById(R.id.replies_rec);
        repliesRecy.setNestedScrollingEnabled(false);
        addMore = v.findViewById(R.id.addMore);
        back.setOnClickListener(this);
        play.setOnClickListener(this);
        addMore.setOnClickListener(this);
        getSingleEnquiryHistory(issue_id);

        // now a user opens a enquiry with a new reply change its state to false
        IssueDbStatus issueDbStatus = new IssueDbStatus();
        issueDbStatus.setIssue_id(issue_id);
        issueDbStatus.setIssue_unseen(0);
        databaseHandler.insertIssueStatusSeen(issueDbStatus);
        return v;
    }


    private void displayImages() {
        if (getActivity() != null && isAdded()) {
            if (images != null) {
                imagesRecy.setVisibility(View.VISIBLE);
                imagesRecy.setAdapter(new RecyclerAdapterEnquiryImageList(images));
                imagesRecy.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            } else {
                imagesRecy.setVisibility(View.GONE);
            }
        }
    }

    private void displayReplies() {
        if (getActivity() != null && isAdded()) {
            if (enquiryReply != null) {
                if (enquiryReply.size() != 0) {
                    no_reply.setVisibility(View.GONE);
                    repliesRecy.setVisibility(View.VISIBLE);
                    layoutManager = new LinearLayoutManager(getActivity());
                    layoutManager.setReverseLayout(true);
                    repliesRecy.setAdapter(new RecyclerAdapterEnquiryReplies(enquiryReply));
                    repliesRecy.setLayoutManager(layoutManager);
                }
            } else {
                repliesRecy.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.play:
                if (AppUtils.haveNetworkConnection(getActivity())) {
                    playAudio(play, picUrlDb, R.drawable.play, R.drawable.pause, enquiryDataHistory.getAudio(), getActivity());
                } else {
                    AppUtils.showToast(
                            getString(R.string.no_internet));
                }
                break;
            case R.id.addMore:
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                Bundle main = new Bundle();
                Bundle bundle = new Bundle();
                bundle.putString("NEWISSUE", "false");
                bundle.putString("ID", String.valueOf(enquiryDataHistory.getId()));
                bundle.putString("STATE", String.valueOf(enquiryDataHistory.getKanban_state()));
                main.putBundle("B1", bundle);
                EnquiryFormFragment fragment = EnquiryFormFragment.newInstance();
                fragment.setArguments(main);
                transaction.replace(R.id.frag_container, fragment).addToBackStack("").commitAllowingStateLoss();
                break;
        }
    }

    public void playAudio(final ImageView current, String url, final int playImg, final int pauseImg, String imagePath, Context context) {
        // if media player is not playing then set the media player object
        if (!mediaPlayer.isPlaying()) {
            try {
//
                mediaPlayer.reset();
                mediaPlayer.setDataSource(url);
                mediaPlayer.prepare();
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        current.setImageResource(pauseImg);
                        mediaPlayer.start();
                    }
                });
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        current.setImageResource(playImg);
                        mp.seekTo(0);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();

            }
            previousImg = playImg;

        } else {
            //if media player is playing then stop the media player , and set previous image to play
            // and check if previous and current image are not same make cureent play the souund
            mediaPlayer.stop();
            previous.setImageResource(previousImg);
            if (!previous.equals(current)) {
                playAudio(current, url, playImg, pauseImg, imagePath, getActivity());
            }
        }
        previous = current;
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("EnquirySingleHistory", Long.parseLong(AppPreference.getInstance().getMobile()));
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        mFirebaseAnalytics.logEvent("EnquirySingleHistory", params);
    }

    private void getSingleEnquiryHistory(Long issue_id) {
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<SingleIssueHistory> call = client.getSingleHistoryData(issue_id);
        call.enqueue(new ApiCallback<SingleIssueHistory>() {
            @Override
            public void onResponse(Response<SingleIssueHistory> response) {
                AppUtils.hideProgressDialog();

                if (response.body() == null)
                    return;
                if (response.body().getData() == null)
                    return;
                images = response.body().getData().getImages();
                enquiryReply = response.body().getData().getReplies();
                enquiryDataHistory = response.body().getData();
                addMore.setVisibility(View.VISIBLE);
                displayImages();
                displayReplies();
                displayData();
            }

            @Override
            public void onResponse401(Response<SingleIssueHistory> response) {
                AppUtils.showSessionExpiredDialog(getActivity());
            }


        });
    }

    private void displayData() {
        if (!AppUtils.isNullCase(enquiryDataHistory.getKanban_state())) {
            status.setText(enquiryDataHistory.getKanban_state());
        }
        id.setText(getString(R.string.issue_no) + ": " + enquiryDataHistory.getId());
        if (!AppUtils.isNullCase(enquiryDataHistory.getDescription())) {
            description.setVisibility(View.VISIBLE);
            description.setText(enquiryDataHistory.getDescription());
        }
        if (!AppUtils.isNullCase(enquiryDataHistory.getSolution())) {
            solutionBack.setVisibility(View.VISIBLE);
            solution.setText(enquiryDataHistory.getSolution());
        }
        create_date.setText(enquiryDataHistory.getCreate_date());
        if (!AppUtils.isNullCase(enquiryDataHistory.getAudio())) {
            play.setVisibility(View.VISIBLE);
            checkPic = databaseHandler.checkImage(enquiryDataHistory.getAudio());
            if (!checkPic)
                AppUtils.generateAndCheckUrl(getActivity(), enquiryDataHistory.getAudio());

            picUrlDb = databaseHandler.getFileUrl(enquiryDataHistory.getAudio());
        } else
            play.setVisibility(View.GONE);


    }
}