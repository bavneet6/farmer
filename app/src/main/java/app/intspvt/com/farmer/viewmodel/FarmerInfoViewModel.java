package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import app.intspvt.com.farmer.repository.FarmerInfoRepository;
import app.intspvt.com.farmer.rest.response.GetPersonalInfo;

public class FarmerInfoViewModel extends ViewModel {
    private MutableLiveData<GetPersonalInfo.GetInfo> getPersonalInfo;
    private FarmerInfoRepository farmerInfoRepository;

    public void init() {
        if (farmerInfoRepository == null)
            farmerInfoRepository = FarmerInfoRepository.getInstance();
        getPersonalInfo = farmerInfoRepository.getPersonalInfo();
    }

    public LiveData<GetPersonalInfo.GetInfo> getPersonalInfo() {
        return getPersonalInfo;
    }
}
