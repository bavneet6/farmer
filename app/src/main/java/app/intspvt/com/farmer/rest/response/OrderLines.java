package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by DELL on 9/26/2017.
 */

public class OrderLines implements Serializable {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("state")
    private String state;

    @SerializedName("price_total")
    private float price_total;

    @SerializedName("product_id")
    private int product_id;

    @SerializedName("product_uom_qty")
    private int product_uom_qty;

    @SerializedName("product_uom")
    private List uom;


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getState() {
        return state;
    }

    public float getPrice_total() {
        return price_total;
    }

    public int getProduct_id() {
        return product_id;
    }

    public int getProduct_uom_qty() {
        return product_uom_qty;
    }

    public List getUom() {
        return uom;
    }
}
