package app.intspvt.com.farmer.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.utilities.DeleteInputCart;

public class RecyclerAdapterWeatherDates extends RecyclerView.Adapter<RecyclerAdapterWeatherDates.WeatherDates> {
    private ArrayList<String> weatherDatesList;
    private Context context;
    private int selectPos;
    private String datePos;
    private DeleteInputCart deleteInputCart;


    public RecyclerAdapterWeatherDates(Activity activity, ArrayList<String> weatherDatesList, String datePos, DeleteInputCart deleteInputCart) {
        this.context = activity;
        this.weatherDatesList = weatherDatesList;
        this.datePos = datePos;
        this.deleteInputCart = deleteInputCart;
        this.selectPos = Integer.parseInt(datePos);
    }

    @Override
    public RecyclerAdapterWeatherDates.WeatherDates onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_weather_dates, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterWeatherDates.WeatherDates(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterWeatherDates.WeatherDates holder, final int position) {
        final int pos = holder.getAdapterPosition();

        SimpleDateFormat input = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat outputDate = new SimpleDateFormat("dd");
        SimpleDateFormat outputMonth = new SimpleDateFormat("MMM");
        try {
            holder.date.setText(outputDate.format(input.parse(weatherDatesList.get(pos))));    // format output
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            holder.month.setText(outputMonth.format(input.parse(weatherDatesList.get(pos))));    // format output
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (selectPos == pos)
            holder.date.setTextColor(Color.parseColor("#ffdc7b"));
        else
            holder.date.setTextColor(context.getResources().getColor(R.color.white));

        holder.dateBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteInputCart.onItemClick(weatherDatesList.get(pos));
                selectPos = pos;
                update(weatherDatesList);
            }
        });

    }

    private void update(ArrayList<String> weatherDatesList) {
        this.weatherDatesList = weatherDatesList;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return weatherDatesList.size();
    }

    public class WeatherDates extends RecyclerView.ViewHolder {
        TextView date, month;
        LinearLayout dateBack;

        public WeatherDates(View itemView) {
            super(itemView);
            dateBack = itemView.findViewById(R.id.dateBack);
            date = itemView.findViewById(R.id.date);
            month = itemView.findViewById(R.id.month);

        }


    }

}

