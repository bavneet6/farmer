package app.intspvt.com.farmer.rest.response;

/**
 * Created by DELL on 9/14/2017.
 */

public class CropsDbClass {
    private String landUnit, season, productName;
    private float cropArea;
    private int productId, crop_record_id;

    public float getCropArea() {
        return cropArea;
    }

    public void setCropArea(float cropArea) {
        this.cropArea = cropArea;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getCrop_record_id() {
        return crop_record_id;
    }

    public void setCrop_record_id(int crop_record_id) {
        this.crop_record_id = crop_record_id;
    }

    public String getLandUnit() {
        return landUnit;
    }

    public void setLandUnit(String landUnit) {
        this.landUnit = landUnit;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
