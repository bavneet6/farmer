package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DELL on 9/8/2017.
 */

public class Trending {
    @SerializedName("data")
    private ArrayList<TrendingData> datas;

    public ArrayList<TrendingData> getDatas() {
        return datas;
    }

}
