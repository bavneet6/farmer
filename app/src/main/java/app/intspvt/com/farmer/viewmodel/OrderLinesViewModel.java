package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import app.intspvt.com.farmer.repository.OrderLinesRepository;
import app.intspvt.com.farmer.rest.response.OrderDataHistory;

public class OrderLinesViewModel extends ViewModel {
    private MutableLiveData<OrderDataHistory> orderLines;
    private OrderLinesRepository orderLinesRepository;

    public void init(Long id, String forFrgament) {
        if (orderLinesRepository == null)
            orderLinesRepository = OrderLinesRepository.getInstance();
        orderLines = orderLinesRepository.getOrderLines(id,forFrgament);
    }

    public LiveData<OrderDataHistory> getOrderLines() {
        return orderLines;
    }
}
