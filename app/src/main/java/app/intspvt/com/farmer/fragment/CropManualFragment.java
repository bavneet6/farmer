package app.intspvt.com.farmer.fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.SearchView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;

import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterCropManuals;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.CropManualData;
import app.intspvt.com.farmer.rest.response.CropManuals;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Response;

public class CropManualFragment extends BaseFragment implements View.OnClickListener, SearchView.OnQueryTextListener {
    public static final String TAG = CropManualFragment.class.getSimpleName();
    private ImageView back;
    private RecyclerView crop_manuals_list;
    private RecyclerAdapterCropManuals recyclerAdapterCropManuals;
    private SearchView search;
    private FirebaseAnalytics firebaseAnalytics;
    private Long cropId;
    private ArrayList<CropManualData> cropManualData = new ArrayList<>();

    public static CropManualFragment newInstance() {
        return new CropManualFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MainActivity.showCallAndHome(true);
        View v = inflater.inflate(R.layout.fragment_crop_manual, null);
        back = v.findViewById(R.id.back);
        crop_manuals_list = v.findViewById(R.id.crop_manuals_list);
        search = v.findViewById(R.id.search);
        crop_manuals_list.setNestedScrollingEnabled(false);
        search.setOnQueryTextListener(this);
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Bundle bundle = getArguments();
        if (bundle != null)
            cropId = bundle.getLong("CROP_ID");
        getCropManuals();
        back.setOnClickListener(this);
        search.setOnClickListener(v1 -> search.setIconified(false));
        return v;
    }

    private void getCropManuals() {

        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<CropManuals> call = client.getCropManuals(cropId, null);
        call.enqueue(new ApiCallback<CropManuals>() {
            @Override
            public void onResponse(Response<CropManuals> response) {
                if (getActivity() != null && isAdded()) {
                    AppUtils.hideProgressDialog();

                    if (response.body() == null)
                        return;
                    if (response.body().getCropManual() == null)
                        return;
                    cropManualData = response.body().getCropManual();
                    displayManualData(cropManualData);
                }
            }

            @Override
            public void onResponse401(Response<CropManuals> response) throws
                    JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }


        });

    }

    private void displayManualData(ArrayList<CropManualData> cropManuals) {
        if (getActivity() != null && isAdded()) {
            recyclerAdapterCropManuals = new RecyclerAdapterCropManuals(getActivity(), cropManuals);
            crop_manuals_list.setAdapter(recyclerAdapterCropManuals);
            crop_manuals_list.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("CropManual", Long.parseLong(AppPreference.getInstance().getMobile()));
        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        firebaseAnalytics.logEvent("CropManual", params);
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (cropManualData.size() != 0) {
            recyclerAdapterCropManuals.getFilter().filter(newText);
        }
        return false;
    }

}
