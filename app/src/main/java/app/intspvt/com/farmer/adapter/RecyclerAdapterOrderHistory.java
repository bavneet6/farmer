package app.intspvt.com.farmer.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.fragment.OrderLinesFragment;
import app.intspvt.com.farmer.rest.response.OrderDataHistory;
import app.intspvt.com.farmer.utilities.AppUtils;

/**
 * Created by DELL on 10/5/2017.
 */

public class RecyclerAdapterOrderHistory extends RecyclerView.Adapter<RecyclerAdapterOrderHistory.Order> {
    private Context context;
    private List<OrderDataHistory> orderHistoryData;
    private String forFragment;

    public RecyclerAdapterOrderHistory(Context context, List<OrderDataHistory> orderHistoryData,
                                       String forFragment) {
        this.context = context;
        this.orderHistoryData = orderHistoryData;
        this.forFragment = forFragment;
    }

    @Override
    public RecyclerAdapterOrderHistory.Order onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_order_history_layout, parent, false);

        context = parent.getContext();
        return new RecyclerAdapterOrderHistory.Order(itemView, this.forFragment);
    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterOrderHistory.Order holder, int position) {
        final int pos = holder.getAdapterPosition();
        if (orderHistoryData != null) {
            holder.orderStatus.setText(orderHistoryData.get(pos).getState());
            holder.orderAmt.setText(context.getString(R.string.rs) + " " + orderHistoryData.get(pos).getAmount_total());
            holder.orderDate.setText(orderHistoryData.get(pos).getDate_order());
            holder.orderId.setText(context.getString(R.string.order_no) + ": " + orderHistoryData.get(pos).getName());
            holder.totalItem.setText("" + orderHistoryData.get(pos).getOrder_lines().size());
            holder.orderPersonName.setText("" + orderHistoryData.get(pos).getSeller_name());
            holder.orderFromPerson.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(
                            Uri.parse("tel:" + orderHistoryData.get(pos).getSeller_number()));
                    context.startActivity(intent);
                }
            });
            // view more details of a particular order
            holder.back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle main = new Bundle();
                    main.putLong("ORDER_ID", orderHistoryData.get(pos).getId());
                    main.putString("KEY", forFragment);
                    OrderLinesFragment fragment = OrderLinesFragment.newInstance();
                    fragment.setArguments(main);
                    AppUtils.changeFragment((FragmentActivity) context, fragment);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return orderHistoryData.size();
    }

    public class Order extends RecyclerView.ViewHolder {
        private TextView orderStatus, orderId, orderDate, orderAmt, totalItem, orderPersonName;
        private LinearLayout back, orderFromPerson;

        public Order(View itemView, String forFragment) {
            super(itemView);
            back = itemView.findViewById(R.id.back);
            orderStatus = itemView.findViewById(R.id.orderStatus);
            orderDate = itemView.findViewById(R.id.orderDate);
            orderId = itemView.findViewById(R.id.orderId);
            orderAmt = itemView.findViewById(R.id.orderAmt);
            totalItem = itemView.findViewById(R.id.totalItem);
            orderFromPerson = itemView.findViewById(R.id.orderFromPerson);
            orderPersonName = itemView.findViewById(R.id.orderPersonName);

            if (forFragment.equals("dehaati")) {
                orderFromPerson.setVisibility(View.VISIBLE);
                orderStatus.setVisibility(View.GONE);
            } else if (forFragment.equals("node")) {
                orderFromPerson.setVisibility(View.GONE);
                orderStatus.setVisibility(View.VISIBLE);
            }
        }
    }
}

