package app.intspvt.com.farmer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.response.CropsData;
import app.intspvt.com.farmer.utilities.CropShowMandi;

public class RecyclerAdapterUserCropsMandi extends RecyclerView.Adapter<RecyclerAdapterUserCropsMandi.MandiData> {
    private Context context;
    private ArrayList<CropsData.CropsInfo> cropsInfos;
    private Long cropSelectedId;
    private CropShowMandi cropShowMandi;

    public RecyclerAdapterUserCropsMandi(Context context, ArrayList<CropsData.CropsInfo> cropsInfos, Long cropSelectedId, CropShowMandi cropShowMandi) {
        this.context = context;
        this.cropsInfos = cropsInfos;
        this.cropSelectedId = cropSelectedId;
        this.cropShowMandi = cropShowMandi;

    }

    @Override
    public RecyclerAdapterUserCropsMandi.MandiData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_user_crop_mandi, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterUserCropsMandi.MandiData(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterUserCropsMandi.MandiData holder, int position) {
        final int pos = holder.getAdapterPosition();
        holder.cropName.setText(cropsInfos.get(pos).getCrop_name());
        if (cropSelectedId == cropsInfos.get(pos).getCrop_id()) {
            holder.back.setBackground(context.getResources().getDrawable(R.drawable.blue_back_box));
            holder.cropName.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.cropName.setTextColor(context.getResources().getColor(R.color.textColor));
            holder.back.setBackground(context.getResources().getDrawable(R.drawable.categ_box));
        }
        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cropSelectedId = cropsInfos.get(pos).getCrop_id();
                List<String> cropData = new ArrayList<>();
                cropData.add("" + cropSelectedId);
                cropData.add(cropsInfos.get(pos).getCrop_name());
                cropShowMandi.onSelect(cropData);
                updateInfo(cropSelectedId);

            }
        });

    }

    private void updateInfo(Long cropSelectedId) {
        this.cropSelectedId = cropSelectedId;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (cropsInfos.size() > 3)
            return 3;
        else
            return cropsInfos.size();
    }

    public class MandiData extends RecyclerView.ViewHolder {
        TextView cropName;
        LinearLayout back;

        public MandiData(View itemView) {
            super(itemView);
            back = itemView.findViewById(R.id.back);
            cropName = itemView.findViewById(R.id.cropName);
        }
    }
}
