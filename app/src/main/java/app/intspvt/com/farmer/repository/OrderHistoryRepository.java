package app.intspvt.com.farmer.repository;

import androidx.lifecycle.MutableLiveData;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.OrderDataHistory;
import app.intspvt.com.farmer.rest.response.OrderHistory;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderHistoryRepository {

    private static OrderHistoryRepository mInstance;
    private List<OrderDataHistory> orderDataHistories = new ArrayList<>();

    public static OrderHistoryRepository getInstance() {
        if (mInstance == null)
            mInstance = new OrderHistoryRepository();
        return mInstance;
    }

    public MutableLiveData<List<OrderDataHistory>> getOrderData() {
        MutableLiveData<List<OrderDataHistory>> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<OrderHistory> call = client.orderHistory();
        call.enqueue(new Callback<OrderHistory>() {
            @Override
            public void onResponse(Call<OrderHistory> call, Response<OrderHistory> response) {
                if (response.body() == null)
                    return;
                orderDataHistories = new ArrayList<>();
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        orderDataHistories = response.body().getData();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }
                data.setValue(orderDataHistories);

            }

            @Override
            public void onFailure(Call<OrderHistory> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Farmer.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Farmer.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(new ArrayList<>());
            }

        });
        return data;
    }
}
