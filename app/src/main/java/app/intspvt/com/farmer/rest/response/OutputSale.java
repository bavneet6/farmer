package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OutputSale {
    @SerializedName("data")
    private ArrayList<OutputSaleData> outputData;

    public ArrayList<OutputSaleData> getOutputData() {
        return outputData;
    }
}
