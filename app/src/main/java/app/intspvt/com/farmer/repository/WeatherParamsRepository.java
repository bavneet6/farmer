package app.intspvt.com.farmer.repository;

import androidx.lifecycle.MutableLiveData;

import java.net.ConnectException;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.WeatherParams;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherParamsRepository {

    private static WeatherParamsRepository mInstance;
    private WeatherParams.Data weatherParamList;

    public static WeatherParamsRepository getInstance() {
        if (mInstance == null)
            mInstance = new WeatherParamsRepository();
        return mInstance;
    }

    public MutableLiveData<WeatherParams.Data> getWeatherParamData() {
        final MutableLiveData<WeatherParams.Data> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<WeatherParams> call = client.getWeatherParams();
        call.enqueue(new Callback<WeatherParams>() {
            @Override
            public void onResponse(Call<WeatherParams> call, Response<WeatherParams> response) {
                if (response.body() == null)
                    return;
                weatherParamList = null;
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        weatherParamList = response.body().getData();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }

                data.setValue(weatherParamList);
            }

            @Override
            public void onFailure(Call<WeatherParams> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Farmer.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Farmer.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(null);
            }


        });
        return data;
    }
}
