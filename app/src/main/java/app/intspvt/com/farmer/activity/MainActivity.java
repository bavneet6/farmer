package app.intspvt.com.farmer.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.amazonaws.mobile.client.AWSMobileClient;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.fragment.EnquiryDetailHistoryFragment;
import app.intspvt.com.farmer.fragment.HomeFragment1;
import app.intspvt.com.farmer.fragment.OrderLinesFragment;
import app.intspvt.com.farmer.fragment.OutputSalesQueryFragment;
import app.intspvt.com.farmer.fragment.SingleDiseaseDetectionFragment;
import app.intspvt.com.farmer.fragment.SoilTestingFragment;
import app.intspvt.com.farmer.fragment.TrendingArticleListFragment;
import app.intspvt.com.farmer.fragment.WeatherAdvisoryFragment;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.UrlConstant;

public class MainActivity extends AppCompatActivity{
    private static LinearLayout home, call;
    private static TextView mTXtCall;
    private FrameLayout frameLayout;


    public static void showCallAndHome(boolean show) {
        if (show) {
            home.setVisibility(View.VISIBLE);
            call.setVisibility(View.VISIBLE);
        } else {
            home.setVisibility(View.GONE);
            call.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppUtils.getUpdateApp(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);


        frameLayout = findViewById(R.id.frag_container);
        home = findViewById(R.id.home);
        call = findViewById(R.id.call);
        mTXtCall = findViewById(R.id.main_txtCall);

        FragmentManager fragManager = getSupportFragmentManager();
        Fragment fragment = fragManager.findFragmentByTag(HomeFragment1.TAG);
        if (fragment != null) {
            fragManager.beginTransaction().remove(fragManager.findFragmentByTag(HomeFragment1.TAG)).commitAllowingStateLoss();
            getSupportFragmentManager().popBackStack();
        }
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frag_container, HomeFragment1.newInstance(),
                        HomeFragment1.TAG).commitAllowingStateLoss();

        if (AppPreference.getInstance().getLANGUAGE() != null && AppPreference.getInstance().getLANGUAGE().length() > 0) {
            Farmer.setLocale(MainActivity.this, AppPreference.getInstance().getLANGUAGE());
        } else {
            Farmer.setLocale(MainActivity.this, UrlConstant.HINDI);
            AppPreference.getInstance().setLANGUAGE(UrlConstant.HINDI);
        }
        mTXtCall.setText(getResources().getString(R.string.call_us));

        AWSMobileClient.getInstance().initialize(this).execute();
        onNewIntent(getIntent());

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppUtils.isNullCase(AppPreference.getInstance().getTOLL_FREE())) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + AppPreference.getInstance().getTOLL_FREE()));
                    startActivity(intent);
                }
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragManager = getSupportFragmentManager();
                Fragment fragment = fragManager.findFragmentByTag(HomeFragment1.TAG);
                if (fragment == null)
                    fragment = HomeFragment1.newInstance();
                getSupportFragmentManager().popBackStack(null, android.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frag_container, fragment,
                                HomeFragment1.TAG).commitAllowingStateLoss();
            }
        });
    }


    private void checkNotification(Intent intent) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Bundle getData = intent.getExtras();
        Fragment currentFrag = this.getSupportFragmentManager().findFragmentById(R.id.frag_container);
        Log.e("currentFrag", "" + currentFrag);
        if (getData == null)
            return;

        if (getData.getString("screen") == null)
            return;
        if (getData.getString("screen").equals(UrlConstant.AGR_IN_HI)) {
            String data = getData.getString("ORDER_ID");
            if (data == null)
                return;
            Bundle bundle = new Bundle();
            bundle.putLong("ORDER_ID", Long.parseLong(data));
            if (currentFrag instanceof OrderLinesFragment) {
                OrderLinesFragment fragment = OrderLinesFragment.newInstance();
                fragment.setArguments(bundle);
                transaction.replace(R.id.frag_container, fragment).commitAllowingStateLoss();
            } else {
                OrderLinesFragment fragment = OrderLinesFragment.newInstance();
                fragment.setArguments(bundle);
                transaction.replace(R.id.frag_container, fragment).addToBackStack("").commitAllowingStateLoss();
            }
        } else if (getData.getString("screen").equals(UrlConstant.TREN_ART)) {
            if (currentFrag instanceof TrendingArticleListFragment)
                transaction.replace(R.id.frag_container, TrendingArticleListFragment.newInstance()).commitAllowingStateLoss();
            else
                transaction.replace(R.id.frag_container, TrendingArticleListFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
        } else if (getData.getString("screen").equals(UrlConstant.WEATHER_DATA)) {
            if (currentFrag instanceof WeatherAdvisoryFragment)
                transaction.replace(R.id.frag_container, WeatherAdvisoryFragment.newInstance()).commitAllowingStateLoss();
            else
                transaction.replace(R.id.frag_container, WeatherAdvisoryFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
        } else if (getData.getString("screen").equals(UrlConstant.PROM_ART)) {
            if (currentFrag instanceof HomeFragment1) {
                transaction.replace(R.id.frag_container, HomeFragment1.newInstance()).commitAllowingStateLoss();
            } else if (currentFrag != null)
                transaction.replace(R.id.frag_container, HomeFragment1.newInstance()).addToBackStack("").commitAllowingStateLoss();
        } else if (getData.getString("screen").equals(UrlConstant.ENQ_HI)) {
            String data = getData.getString("ISSUE_ID");
            if (data == null)
                return;
            Bundle bundle = new Bundle();
            bundle.putLong("ISSUE_ID", Long.parseLong(data));
            if (currentFrag instanceof EnquiryDetailHistoryFragment) {
                EnquiryDetailHistoryFragment fragment = EnquiryDetailHistoryFragment.newInstance();
                fragment.setArguments(bundle);
                transaction.replace(R.id.frag_container, fragment).commitAllowingStateLoss();
            } else {
                EnquiryDetailHistoryFragment fragment = EnquiryDetailHistoryFragment.newInstance();
                fragment.setArguments(bundle);
                transaction.replace(R.id.frag_container, fragment).addToBackStack("").commitAllowingStateLoss();
            }
        } else if (getData.getString("screen").equals(UrlConstant.DISEASE_HI)) {
            String data = getData.getString("ISSUE_ID");
            if (data == null)
                return;
            Bundle bundle = new Bundle();
            bundle.putLong("ISSUE_ID", Long.parseLong(data));
            if (currentFrag instanceof SingleDiseaseDetectionFragment) {
                SingleDiseaseDetectionFragment fragment = SingleDiseaseDetectionFragment.newInstance();
                fragment.setArguments(bundle);
                transaction.replace(R.id.frag_container, fragment).commitAllowingStateLoss();
            } else {
                SingleDiseaseDetectionFragment fragment = SingleDiseaseDetectionFragment.newInstance();
                fragment.setArguments(bundle);
                transaction.replace(R.id.frag_container, fragment).addToBackStack("").commitAllowingStateLoss();
            }
        } else if (getData.getString("screen").equals(UrlConstant.SOIL_HI)) {
            if (currentFrag instanceof WeatherAdvisoryFragment)
                transaction.replace(R.id.frag_container, SoilTestingFragment.newInstance()).commitAllowingStateLoss();
            else
                transaction.replace(R.id.frag_container, SoilTestingFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
        } else if (getData.getString("screen").equals(UrlConstant.OUTPUT_HI)) {
            String data = getData.getString("ENTITY_ID");
            if (data == null)
                return;
            Bundle bundle = new Bundle();
            bundle.putLong("ENTITY_ID", Long.parseLong(data));
            if (currentFrag instanceof OutputSalesQueryFragment) {
                OutputSalesQueryFragment fragment = OutputSalesQueryFragment.newInstance();
                fragment.setArguments(bundle);
                transaction.replace(R.id.frag_container, fragment).commitAllowingStateLoss();
            } else {
                OutputSalesQueryFragment fragment = OutputSalesQueryFragment.newInstance();
                fragment.setArguments(bundle);
                transaction.replace(R.id.frag_container, fragment).addToBackStack("").commitAllowingStateLoss();
            }
        }
    }

    public void refreshUI() {
        mTXtCall.setText(getResources().getString(R.string.call_us));
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        checkNotification(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}


