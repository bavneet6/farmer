package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import app.intspvt.com.farmer.repository.ManualCropRepository;
import app.intspvt.com.farmer.rest.response.CropManualData;

public class ManualCropsViewModel extends ViewModel {
    private MutableLiveData<ArrayList<CropManualData>> manualCrops;
    private ManualCropRepository manualCropRepository;

    public void init() {
        if (manualCropRepository == null)
            manualCropRepository = ManualCropRepository.getInstance();
        manualCrops = manualCropRepository.getManualCrops();
    }

    public LiveData<ArrayList<CropManualData>> getManualCrops() {
        return manualCrops;
    }
}
