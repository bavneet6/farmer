package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by DELL on 9/26/2017.
 */

public class OrderHistory {
    @SerializedName("data")
    private List<OrderDataHistory> data;

    public List<OrderDataHistory> getData() {
        return data;
    }
}
