package app.intspvt.com.farmer.repository;

import androidx.lifecycle.MutableLiveData;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.EnquiryDataHistory;
import app.intspvt.com.farmer.rest.response.EnquiryHistory;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnquiryRepository {

    private static EnquiryRepository mInstance;
    private List<EnquiryDataHistory> enquiryDataHistories = new ArrayList<>();

    public static EnquiryRepository getInstance() {
        if (mInstance == null)
            mInstance = new EnquiryRepository();
        return mInstance;
    }

    public MutableLiveData<List<EnquiryDataHistory>> getEnquiryData(String type) {
        MutableLiveData<List<EnquiryDataHistory>> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<EnquiryHistory> call = client.getEnquiryHistory(type);
        call.enqueue(new Callback<EnquiryHistory>() {
            @Override
            public void onResponse(Call<EnquiryHistory> call, Response<EnquiryHistory> response) {
                if (response.body() == null)
                    return;
                enquiryDataHistories = new ArrayList<>();
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        enquiryDataHistories = response.body().getData();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }
                data.setValue(enquiryDataHistories);

            }

            @Override
            public void onFailure(Call<EnquiryHistory> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Farmer.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Farmer.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(new ArrayList<>());
            }

        });
        return data;
    }
}

