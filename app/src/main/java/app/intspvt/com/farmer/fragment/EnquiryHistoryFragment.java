package app.intspvt.com.farmer.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterEnquiryHistory;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.response.IssueDbStatus;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.UrlConstant;
import app.intspvt.com.farmer.viewmodel.EnquiryViewModel;

/**
 * Created by DELL on 7/24/2017.
 */

public class EnquiryHistoryFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = EnquiryHistoryFragment.class.getSimpleName();
    private FirebaseAnalytics mFirebaseAnalytics;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private TextView no_data_mssg, createNewQuery;
    private ImageView back;
    private DatabaseHandler databaseHandler;
    private EnquiryViewModel enquiryViewModel;

    public static EnquiryHistoryFragment newInstance() {
        return new EnquiryHistoryFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_enquriy_history, null);
        MainActivity.showCallAndHome(true);
        recyclerView = v.findViewById(R.id.recy_list);
        recyclerView.setNestedScrollingEnabled(false);
        no_data_mssg = v.findViewById(R.id.no_data_mssg);
        createNewQuery = v.findViewById(R.id.createNewQuery);
        back = v.findViewById(R.id.back);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        databaseHandler = new DatabaseHandler(getActivity());
        createNewQuery.setOnClickListener(this);
        back.setOnClickListener(this);
        AppUtils.showProgressDialog(getActivity());
        enquiryViewModel = ViewModelProviders.of(getActivity()).get(EnquiryViewModel.class);
        enquiryViewModel.init(UrlConstant.ADVISORY);
        observeViewModel(enquiryViewModel);
        return v;
    }

    private void observeViewModel(EnquiryViewModel viewModel) {
        viewModel.getEnquiryData()
                .observe(this, enquiryData -> {
                    AppUtils.hideProgressDialog();
                    if (enquiryData != null && enquiryData.size() > 0) {
                        for (int i = 0; i < enquiryData.size(); i++) {
                            if (enquiryData.get(i).isHas_new_replies()) {
                                IssueDbStatus issueDbStatus = new IssueDbStatus();
                                issueDbStatus.setIssue_id(enquiryData.get(i).getId());
                                // 1 for true
                                issueDbStatus.setIssue_unseen(1);
                                databaseHandler.insertIssueStatusSeen(issueDbStatus);
                            }

                        }
                        no_data_mssg.setVisibility(View.GONE);
                        recyclerView.setAdapter(new RecyclerAdapterEnquiryHistory(enquiryData));
                        layoutManager = new LinearLayoutManager(getActivity());
                        recyclerView.setLayoutManager(layoutManager);
                    } else
                        no_data_mssg.setVisibility(View.VISIBLE);

                });
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("EnquiryHistory", Long.parseLong(AppPreference.getInstance().getMobile()));
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        mFirebaseAnalytics.logEvent("EnquiryHistory", params);
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onClick(View view) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        int id = view.getId();
        switch (id) {
            case R.id.createNewQuery:
                Bundle main = new Bundle();
                Bundle bundle = new Bundle();
                bundle.putString("NEWISSUE", "true");
                bundle.putString("ID", "0");
                bundle.putString("STATE", null);
                main.putBundle("B1", bundle);
                EnquiryFormFragment fragment = EnquiryFormFragment.newInstance();
                fragment.setArguments(main);
                transaction.replace(R.id.frag_container, fragment).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.back:
                getActivity().onBackPressed();
                break;
        }
    }
}
