package app.intspvt.com.farmer.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.rest.response.AddToCart;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.OnItemClick;

/**
 * Created by DELL on 8/2/2017.
 */

public class RecyclerAdapterCart extends RecyclerView.Adapter<RecyclerAdapterCart.Products> {
    private ArrayList<AddToCart> products;
    private Context context;
    private float total_val;
    private int productQty;
    private HashMap<Integer, Float> hashMap = new HashMap<>();
    private DatabaseHandler databaseHandler;
    private AlertDialog.Builder builder;
    private OnItemClick onItemClick;
    private String getPic = null;
    private boolean clicked = false;

    public RecyclerAdapterCart(Context context, ArrayList<AddToCart> products,
                               OnItemClick callback) {
        this.context = context;
        this.products = products;
        this.onItemClick = callback;
        databaseHandler = new DatabaseHandler(context);

    }


    @Override
    public RecyclerAdapterCart.Products onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_cart_layout, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterCart.Products(itemView);

    }


    @Override
    public void onBindViewHolder(final RecyclerAdapterCart.Products holder, final int position) {
        clicked = false;
        final int pos = holder.getAdapterPosition();
        holder.rs.setText(context.getString(R.string.rs) + " ");
        if (!AppUtils.isNullCase(products.get(pos).getVariant()))
            holder.name.setText(products.get(pos).getProductName() + " / " +
                    products.get(pos).getVariant());
        else
            holder.name.setText(products.get(pos).getProductName());
        holder.pro_qty.setText("" + products.get(pos).getProductQty());
        holder.mrp.setText("" + products.get(pos).getProductMrp());
        int m = products.get(pos).getProductQty();
        final float total = m * products.get(pos).getProductMrp();
        total_val = total_val + total;
        holder.total_price.setText(context.getString(R.string.rs) + " " + total);
        final String photo = products.get(pos).getProductImage();
        getPic = null;

        if (!AppUtils.isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(holder.image, new PicassoCallback(photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_product_image).
                                into(holder.image);
                    }
                });
            }
        }
        holder.pro_qty.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                clicked = true;
            }
        });
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicked = true;
                if (holder.pro_qty.getText().toString().equals("")) {
                    holder.pro_qty.setText("0");
                } else {
                    productQty = Integer.parseInt(holder.pro_qty.getText().toString());
                    if (productQty == 1) {
                        deleteItem(products.get(pos).getProductId(), 0,
                                products.get(pos).getProductMrp(), pos);
                    } else {
                        --productQty;
                        holder.pro_qty.setText("" + productQty);
                    }
                }
            }
        });
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clicked = true;
                if (holder.pro_qty.getText().toString().equals("")) {
                    holder.pro_qty.setText("1");
                } else {
                    productQty = Integer.parseInt(holder.pro_qty.getText().toString());
                    ++productQty;
                    holder.pro_qty.setText("" + productQty);
                }

            }
        });
        holder.pro_qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!holder.pro_qty.getText().toString().equals("")) {
                    productQty = Integer.parseInt(holder.pro_qty.getText().toString());
                    float totalPrice = productQty * products.get(pos).getProductMrp();
                    holder.total_price.setText(context.getString(R.string.rs) + " " + totalPrice);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (clicked)
                    if (!holder.pro_qty.getText().toString().equals("")) {
                        AddToCart contact = new AddToCart();
                        contact.setProductId("" + products.get(pos).getProductId());
                        contact.setProductName(products.get(pos).getProductName());
                        contact.setProductMrp(products.get(pos).getProductMrp());
                        contact.setProductQty(Integer.parseInt(holder.pro_qty.getText().toString()));
                        float a = Integer.parseInt(holder.pro_qty.getText().toString())
                                * products.get(pos).getProductMrp();
                        contact.setTotalPrice(a);
                        contact.setProductImage(products.get(pos).getProductImage());
                        contact.setVariant(products.get(pos).getVariant());
                        databaseHandler.insertCartData(false, contact);
                        updateList();
                    }

            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteItem(products.get(pos).getProductId(), 0,
                        products.get(pos).getProductMrp(), pos);
            }
        });

    }

    public void updateList() {
        onItemClick.onClick(hashMap);
        this.products = databaseHandler.getAllCartData();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    private void deleteItem(final String id, final int value, final Float mrp, final int pos) {
        builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.delete)
                .setMessage(R.string.delete_an_item)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        databaseHandler.deleteProductData(id);
                        updateList();

                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_menu_delete)
                .show();
    }

    public class Products extends RecyclerView.ViewHolder {
        private TextView name, mrp, total_price, rs;
        private ImageView image, plus, minus, delete;
        private EditText pro_qty;

        public Products(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.productName);
            mrp = itemView.findViewById(R.id.productMrp);
            total_price = itemView.findViewById(R.id.total_price);
            image = itemView.findViewById(R.id.image);
            plus = itemView.findViewById(R.id.plus);
            delete = itemView.findViewById(R.id.delete);
            minus = itemView.findViewById(R.id.minus);
            pro_qty = itemView.findViewById(R.id.pro_qty);
            rs = itemView.findViewById(R.id.rs);

        }
    }
}
