package app.intspvt.com.farmer.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterImageList;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.body.Enquiry;
import app.intspvt.com.farmer.rest.body.Issue;
import app.intspvt.com.farmer.rest.response.IssueData;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.DeleteInputCart;
import app.intspvt.com.farmer.utilities.PermissionCheck;
import app.intspvt.com.farmer.utilities.UrlConstant;
import retrofit2.Call;
import retrofit2.Response;

import static android.Manifest.permission.RECORD_AUDIO;


/**
 * Created by DELL on 11/29/2017.
 */

public class EnquiryFormFragment extends BaseFragment implements View.OnClickListener, DeleteInputCart {
    public static final String TAG = EnquiryFormFragment.class.getSimpleName();
    private static final String TAGG = "SoundRecordingActivity";
    ArrayList<String> returnValue = new ArrayList<>();
    private RecyclerView image_list;
    private MediaRecorder recorder;
    private File audiofile = null;
    private TextView send, recordAudio;
    private ImageView playCross, play, recordImg, back;
    private Enquiry enquiry;
    private Animation animation;
    private MediaPlayer mediaPlayer;
    private Handler handler;
    private EditText description;
    private Runnable runnable;
    private LinearLayout record_back_layout, select_image;
    private RelativeLayout playBack;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String showCategory = "true", replyId = "0", kanbanState = null, recordStatus = "Record";
    private HashMap<Integer, File> imageFileList = new HashMap<>();
    private ArrayList<File> imageFileList1 = new ArrayList<>();

    public static EnquiryFormFragment newInstance() {
        return new EnquiryFormFragment();
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_enquiry_form, null);
        MainActivity.showCallAndHome(true);
        playBack = v.findViewById(R.id.playBack);
        description = v.findViewById(R.id.description);
        image_list = v.findViewById(R.id.image_list);
        select_image = v.findViewById(R.id.select_image);
        record_back_layout = v.findViewById(R.id.record_back_layout);
        recordAudio = v.findViewById(R.id.recordAudio);
        send = v.findViewById(R.id.send);
        play = v.findViewById(R.id.play);
        back = v.findViewById(R.id.back);
        playCross = v.findViewById(R.id.playCross);
        recordImg = v.findViewById(R.id.recordImg);

        select_image.setOnClickListener(this);
        recordAudio.setOnClickListener(this);
        play.setOnClickListener(this);
        back.setOnClickListener(this);
        playBack.setOnClickListener(this);
        send.setOnClickListener(this);
        record_back_layout.setOnClickListener(this);
        playCross.setOnClickListener(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        if (!AppUtils.haveNetworkConnection(getActivity())) {
            AppUtils.showToast(
                    getString(R.string.no_internet));
        }

        Bundle bundle = getArguments();
        if (bundle != null) {
            Bundle b = bundle.getBundle("B1");
            if (b != null) {
                showCategory = b.getString("NEWISSUE");
                replyId = b.getString("ID");
                kanbanState = b.getString("STATE");
            }
        }

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("EnquiryForm", Long.parseLong(AppPreference.getInstance().getMobile()));
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        mFirebaseAnalytics.logEvent("EnquiryForm", params);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.select_image:
                AppUtils.imageSelection(this, 5);
                break;
            case R.id.play:
                playAudio();
                break;
            case R.id.playCross:
                showDeleteDialog();
                break;
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.send:
                if (recordStatus.equals("Stop")) {
                    stopRecording();
                }
                sendIssueData();
                break;
            case R.id.record_back_layout:
                if (PermissionCheck.checkAudioPermissiom(getActivity())) {
                    if (recordStatus.equals("Record")) {
                        recordStatus = "Stop";
                        try {
                            startRecording();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        stopRecording();
                        recordStatus = "Record";
                    }
                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{RECORD_AUDIO}, 1);
                }
                break;


        }

    }

    private void sendIssueData() {
        imageFileList1.clear();
        for (Map.Entry<Integer, File> entry : imageFileList.entrySet()) {
            imageFileList1.add(entry.getValue());
        }

        if (audiofile == null) {
            enquiry = new Enquiry(null);
            enquiry.setAudio(null);

        } else {
            enquiry = new Enquiry(audiofile.getAbsolutePath());
            enquiry.setAudio(audiofile.getAbsolutePath());

        }
        IssueData issueData = new IssueData();
        issueData.setIssue_id(Long.valueOf(replyId));
        issueData.setKanban_state(kanbanState);
        issueData.setCategory(UrlConstant.ADVISORY);
        if (description.getText().toString().equals("")) {
            issueData.setDescription(null);

        } else {
            issueData.setDescription(description.getText().toString());
        }
        Issue issue = new Issue(issueData);
        if ((issue.getData().getDescription() != null) || (enquiry.getAudio() != null) || (imageFileList1.size() != 0)) {

            AppUtils.showProgressDialog(getActivity());
            Call<Void> call;
            AppRestClient client = AppRestClient.getInstance();
            if ((showCategory.equals("false")) && (!replyId.equals("0"))) {
                call = client.sendReply(issue, enquiry, imageFileList1);
            } else {
                call = client.sendEnquiryData(issue, enquiry, imageFileList1);
            }
            call.enqueue(new ApiCallback<Void>() {
                @Override
                public void onResponse(Response<Void> response) {

                    AppUtils.hideProgressDialog();
                    if (response.code() == 200) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(R.string.information);
                        builder.setMessage(R.string.info_receievd);
                        builder.setCancelable(false);
                        builder.setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                getActivity().onBackPressed();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                    Bundle params = new Bundle();
                    params.putLong("enquirysent", Long.parseLong(AppPreference.getInstance().getMobile()));
                    mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
                    mFirebaseAnalytics.logEvent("enquirysent", params);

                }

                @Override
                public void onResponse401(Response<Void> response) throws JSONException {

                }

            });
        } else {
            AppUtils.showToast(getString(R.string.no_info));
        }
    }

    private void showDeleteDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.delete)
                .setMessage(R.string.delete_recording)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        audiofile = null;
                        playBack.setVisibility(View.GONE);
                        record_back_layout.setVisibility(View.VISIBLE);

                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_menu_delete)
                .show();
    }

    private void playAudio() {
        if (mediaPlayer == null)
            mediaPlayer = new MediaPlayer();
        if (!mediaPlayer.isPlaying()) {
            try {
                mediaPlayer.reset();
                mediaPlayer.setDataSource(audiofile.getAbsolutePath());
                mediaPlayer.prepare();
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        play.setImageResource(R.drawable.pause);
                        mediaPlayer.start();
                    }
                });
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        play.setImageResource(R.drawable.play);
                        mp.seekTo(0);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            mediaPlayer.stop();
            play.setImageResource(R.drawable.play);

        }
    }

    public void startRecording() throws IOException {
        recorder = new MediaRecorder();


        File sampleDir = Environment.getExternalStorageDirectory();
        try {
            audiofile = File.createTempFile("sound", ".3gp", sampleDir);
        } catch (IOException e) {
            Log.e(TAGG, "sdcard access error");
            return;
        }
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(audiofile.getAbsolutePath());
        recorder.prepare();
        recorder.start();
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                stopRecording();
                recordStatus = "Record";
                play.setVisibility(View.VISIBLE);

            }
        };
        handler.postDelayed(runnable, 120000);
        animation = new AlphaAnimation(1, 0);
        animation.setDuration(1000);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        recordAudio.startAnimation(animation);
        recordImg.startAnimation(animation);

        recordAudio.setText(R.string.recording_on);

    }

    public void stopRecording() {
        record_back_layout.setVisibility(View.GONE);
        playBack.setVisibility(View.VISIBLE);
        handler.removeCallbacks(runnable);

        try {
            recordAudio.clearAnimation();
            recordImg.clearAnimation();
            recorder.stop();
            recorder.reset();
            recorder.release();
            addRecordingToMediaLibrary();
        } catch (IllegalStateException s) {
        } catch (RuntimeException stopException) {
        }

    }

    protected void addRecordingToMediaLibrary() {
        ContentValues values = new ContentValues(4);
        long current = System.currentTimeMillis();
        values.put(MediaStore.Audio.Media.TITLE, "audio" + audiofile.getName());
        values.put(MediaStore.Audio.Media.DATE_ADDED, (int) (current / 1000));
        values.put(MediaStore.Audio.Media.MIME_TYPE, "audio/3gpp");
        values.put(MediaStore.Audio.Media.DATA, audiofile.getAbsolutePath());
        ContentResolver contentResolver = getActivity().getContentResolver();

        Uri base = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Uri newUri = contentResolver.insert(base, values);

        getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, newUri));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("val", "requestCode ->  " + requestCode + "  resultCode " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (100): {
                if (resultCode == Activity.RESULT_OK) {
                    returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                    storeImagesInList(returnValue);
                }
            }
            break;
        }
    }

    private void storeImagesInList(ArrayList<String> returnValue) {
        for (int i = 0; i < returnValue.size(); i++) {
            File file = new File(returnValue.get(i));
            imageFileList1.add(file);
        }
        for (int i = 0; i < imageFileList1.size(); i++) {
            imageFileList.put(i, imageFileList1.get(i));
        }
        image_list.setAdapter(new RecyclerAdapterImageList(imageFileList1, this));
        image_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AppUtils.imageSelection(this, 5);
                } else {
                }
                return;
            }
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (recordStatus.equals("Stop")) {
            stopRecording();
        }
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

    }

    @Override
    public void onItemClick(String s) {
        imageFileList.remove(Integer.parseInt(s));
    }
}
