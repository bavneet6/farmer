package app.intspvt.com.farmer.utilities;

import java.util.HashSet;

public interface CropSelection {
    void onSelect(HashSet<Long> selectedCrops);
}
