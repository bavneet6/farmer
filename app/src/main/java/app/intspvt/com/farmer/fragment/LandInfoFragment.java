package app.intspvt.com.farmer.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;

import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.body.PostFarmerInfo;
import app.intspvt.com.farmer.rest.body.PostFarmerPersonalInfo;
import app.intspvt.com.farmer.rest.response.GetPersonalInfo;
import app.intspvt.com.farmer.rest.response.LandInfo;
import app.intspvt.com.farmer.rest.response.LandUnits;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by DELL on 8/24/2017.
 */

public class LandInfoFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    public static final String TAG = LandInfoFragment.class.getSimpleName();
    private TextView sendData;
    private EditText tLand, liLand, loLand;
    private ImageView back;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String cropsUnit;
    private Spinner landUnitsDropDown;
    private ArrayList<String> landUnitsNames = new ArrayList<>();

    public static LandInfoFragment newInstance() {
        return new LandInfoFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_land_info, null);
        MainActivity.showCallAndHome(true);
        sendData = v.findViewById(R.id.sendData);
        tLand = v.findViewById(R.id.tLand);
        liLand = v.findViewById(R.id.liLand);
        loLand = v.findViewById(R.id.loLand);
        back = v.findViewById(R.id.back);
        landUnitsDropDown = v.findViewById(R.id.landUnits);
        landUnitsDropDown.setOnItemSelectedListener(this);
        sendData.setOnClickListener(this);
        back.setOnClickListener(this);
        getLandUnits();
        getUserLandInfo(getActivity());

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        return v;
    }

    private void getLandUnits() {
        AppRestClient client = AppRestClient.getInstance();
        Call<LandUnits> call = client.getLandUnits();
        call.enqueue(new ApiCallback<LandUnits>() {
            @Override
            public void onResponse(Response<LandUnits> response) {
                if (response.body() == null)
                    return;
                landUnitsNames = response.body().getLandUnits();
                setLandUnits(landUnitsNames);
            }

            @Override
            public void onResponse401(Response<LandUnits> response) throws
                    JSONException {
                AppUtils.showSessionExpiredDialog(activity);

            }


        });
    }

    private void setLandUnits(ArrayList<String> landUnits) {
        if (getActivity() != null && isAdded()) {
            ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(getActivity(), R.layout.template_spinner_text, landUnits);
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            landUnitsDropDown.setAdapter(stateAdapter);
        }
    }

    private void getUserLandInfo(final Activity activity) {
        AppRestClient client = AppRestClient.getInstance();
        Call<GetPersonalInfo> call = client.getLandInfo();
        call.enqueue(new ApiCallback<GetPersonalInfo>() {
            @Override
            public void onResponse(Response<GetPersonalInfo> response) {
                if (response.body() == null)
                    return;
                setLandData(response.body().getData().getLand_info());
            }

            @Override
            public void onResponse401(Response<GetPersonalInfo> response) throws
                    JSONException {
                AppUtils.showSessionExpiredDialog(activity);

            }


        });
    }

    private void setLandData(LandInfo landData) {
        String landUnit = landData.getX_land_unit();
        if (landUnitsNames != null && landUnitsNames.size() != 0)
            for (int i = 0; i < landUnitsNames.size(); i++)
                if (landUnitsNames.get(i).equals(landUnit))
                    landUnitsDropDown.setSelection(i);


        if (!AppUtils.isNullCase(String.valueOf(landData.getX_total_land()))) {
            tLand.setText("" + landData.getX_total_land());
            tLand.setTextColor(Color.parseColor("#464646"));
        }
        if (!AppUtils.isNullCase(String.valueOf(landData.getX_leased_in()))) {
            liLand.setText("" + landData.getX_leased_in());
            liLand.setTextColor(Color.parseColor("#464646"));
        }
        if (!AppUtils.isNullCase(String.valueOf(landData.getX_leased_out()))) {
            loLand.setText("" + landData.getX_leased_out());
            loLand.setTextColor(Color.parseColor("#464646"));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("LandScreen", Long.parseLong(AppPreference.getInstance().getMobile()));
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        mFirebaseAnalytics.logEvent("LandScreen", params);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.sendData:
                if (AppUtils.haveNetworkConnection(getActivity()))
                    sendLandUpdateInfo();
                else
                    AppUtils.showToast(getString(R.string.no_internet));
                break;
            case R.id.back:
                getActivity().onBackPressed();
                break;

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void sendLandUpdateInfo() {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        final float total, leasedIn, leasedOut;

        if (tLand.getText().toString().equals("")) {
            total = 0.0f;
        } else {
            total = Float.parseFloat(tLand.getText().toString());
        }
        if (liLand.getText().toString().equals("")) {
            leasedIn = 0.0f;
        } else {
            leasedIn = Float.parseFloat(liLand.getText().toString());
        }
        if (loLand.getText().toString().equals("")) {
            leasedOut = 0.0f;
        } else {
            leasedOut = Float.parseFloat(loLand.getText().toString());
        }
        LandInfo landInfo = new LandInfo(cropsUnit, total, leasedIn, leasedOut);
        PostFarmerInfo.PostInfo getInfo = new PostFarmerInfo.PostInfo(landInfo);
        PostFarmerPersonalInfo postFarmerPersonalInfo = new PostFarmerPersonalInfo(getInfo);
        Call<PostFarmerPersonalInfo> call = client.updateLandInfo(postFarmerPersonalInfo);
        call.enqueue(new ApiCallback<PostFarmerPersonalInfo>() {
            @Override
            public void onResponse(Response<PostFarmerPersonalInfo> response) {
                AppUtils.hideProgressDialog();
                Bundle params = new Bundle();
                params.putLong("LandUpdate", Long.parseLong(AppPreference.getInstance().getMobile()));
                mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
                mFirebaseAnalytics.logEvent("LandUpdate", params);
                getActivity().onBackPressed();
            }

            @Override
            public void onResponse401(Response<PostFarmerPersonalInfo> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }


        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.landUnits:
                cropsUnit = landUnitsNames.get(position);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
