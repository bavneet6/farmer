package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import app.intspvt.com.farmer.repository.WeatherParamsRepository;
import app.intspvt.com.farmer.rest.response.WeatherParams;

public class WeatherParamsViewModel extends ViewModel {


    private MutableLiveData<WeatherParams.Data> weatherParamData;
    private WeatherParamsRepository weatherParamsRepository;

    public void init() {
        if (weatherParamsRepository == null)
            weatherParamsRepository = WeatherParamsRepository.getInstance();
        weatherParamData = weatherParamsRepository.getWeatherParamData();
    }

    public LiveData<WeatherParams.Data> getWeatherParamData() {
        return weatherParamData;
    }
}
