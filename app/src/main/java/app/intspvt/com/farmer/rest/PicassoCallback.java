package app.intspvt.com.farmer.rest;

import android.content.Context;

import com.squareup.picasso.Callback;

import java.net.URL;
import java.util.concurrent.ExecutionException;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.response.ProductFileTable;
import app.intspvt.com.farmer.utilities.AppUtils;

/**
 * Created by DELL on 3/23/2018.
 */

public abstract class PicassoCallback implements Callback {
    String imagePath;
    Context context = Farmer.getInstance();
    DatabaseHandler databaseHandler = new DatabaseHandler(context);

    public PicassoCallback(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public void onSuccess() {


    }

    @Override
    public void onError() {
        if (AppUtils.haveNetworkConnection(context)) {
            ProductFileTable productFileTable = new ProductFileTable();
            productFileTable.setProd_file(imagePath);
            URL url = null;
            try {
                url = new AppUtils.FetchPicture(imagePath).execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            productFileTable.setProd_url("" + url);

            databaseHandler.inserPicturesFile(productFileTable);
            onErrorPic(url);
        }
    }

    public abstract void onErrorPic(URL url);

}
