package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import app.intspvt.com.farmer.repository.CropRepository;
import app.intspvt.com.farmer.rest.response.CropsData;

public class CropViewModel extends ViewModel {
    private MutableLiveData<ArrayList<CropsData.CropsInfo>> enquiryList;
    private CropRepository cropRepository;

    public void init() {
        if (cropRepository == null)
            cropRepository = CropRepository.getInstance();
        enquiryList = cropRepository.getCropList();
    }

    public LiveData<ArrayList<CropsData.CropsInfo>> getCropList() {
        return enquiryList;
    }
}
