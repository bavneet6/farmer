package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import app.intspvt.com.farmer.repository.EnquiryRepository;
import app.intspvt.com.farmer.rest.response.EnquiryDataHistory;

public class EnquiryViewModel extends ViewModel {
    private MutableLiveData<List<EnquiryDataHistory>> enquiryList;
    private EnquiryRepository enquiryRepository;

    public void init(String type) {
        if (enquiryRepository == null)
            enquiryRepository = EnquiryRepository.getInstance();
        enquiryList = enquiryRepository.getEnquiryData(type);
    }

    public LiveData<List<EnquiryDataHistory>> getEnquiryData() {
        return enquiryList;
    }
}
