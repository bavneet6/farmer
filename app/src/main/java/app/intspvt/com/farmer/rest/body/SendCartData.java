package app.intspvt.com.farmer.rest.body;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import app.intspvt.com.farmer.rest.response.OrderData;

/**
 * Created by DELL on 9/1/2017.
 */

public class SendCartData {
    @SerializedName("data")
    private CartData data;

    public SendCartData(CartData data) {
        this.data = data;
    }

    public static class CartData {
        @SerializedName("order_lines")
        public List<OrderData> outputData;

        @SerializedName("discount")
        public Float discount;
        @SerializedName("promotion_id")
        public int promotion_id;

        public CartData(List<OrderData> outputData, Float discount, int promotion_id) {
            this.outputData = outputData;
            this.discount = discount;
            this.promotion_id = promotion_id;
        }
    }
}
