package app.intspvt.com.farmer.repository;

import androidx.lifecycle.MutableLiveData;

import java.net.ConnectException;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.Products;
import app.intspvt.com.farmer.rest.response.SingleProduct;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SingleProductRepository {
    private static SingleProductRepository mInstance;
    private Products products;

    public static SingleProductRepository getInstance() {
        if (mInstance == null)
            mInstance = new SingleProductRepository();
        return mInstance;
    }

    public MutableLiveData<Products> getSingleProduct(Long id) {
        final MutableLiveData<Products> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<SingleProduct> call = client.getSingleProduct(id);
        call.enqueue(new Callback<SingleProduct>() {
            @Override
            public void onResponse(Call<SingleProduct> call, Response<SingleProduct> response) {
                if (response.body() == null)
                    return;
                products = null;
                if (response.code() == 200) {
                    if (response.body().getProducts() != null) {
                        products = response.body().getProducts();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }
                data.setValue(products);
            }

            @Override
            public void onFailure(Call<SingleProduct> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Farmer.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Farmer.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(null);
            }


        });
        return data;
    }
}
