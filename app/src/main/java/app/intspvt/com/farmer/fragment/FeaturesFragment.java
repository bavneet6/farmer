package app.intspvt.com.farmer.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.analytics.FirebaseAnalytics;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;

public class FeaturesFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    public static final String TAG = FeaturesFragment.class.getSimpleName();
    private LinearLayout mandi_back;
    private FirebaseAnalytics mFirebaseAnalytics;

    public static FeaturesFragment newInstance() {
        return new FeaturesFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_features, null);
        MainActivity.showCallAndHome(false);
        mandi_back = v.findViewById(R.id.mandi_back);
        mandi_back.setOnClickListener(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }


    @Override
    public void onClick(View view) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        HomeFragment1.newInstance().dismissSheet();
        int id = view.getId();
        switch (id) {
            case R.id.mandi_back:
                transaction.replace(R.id.frag_container, MandiPricesFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
        }
    }

}
