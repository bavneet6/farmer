package app.intspvt.com.farmer.fragment;

import android.content.Intent;
import android.location.Address;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.adapter.RecyclerAdapterSingleMandi;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.MandiCropPrice;
import app.intspvt.com.farmer.rest.response.MandiPrices;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.SellProduce;
import retrofit2.Call;
import retrofit2.Response;

public class SingleMandiBottomFragment extends BaseFragment implements View.OnClickListener, SellProduce {
    private RecyclerView crop_list;
    private List<MandiCropPrice.Mandi> mandiPricesData;
    private TextView mandiName, navigate, cropName, minPrice, maxPrice, bestPrice, dehaatPrice, sell_produce;
    private ImageView back;
    private Long mandi_id = 0L, crop_id = 0L, node_id = 0L;
    private float dehaatPriceValue = 0f;
    private MandiPrices.Mandi mandi_data;
    private LinearLayout minPriceBack, maxPriceBack, bestPriceBack, dehaatPriceBack;

    public static SingleMandiBottomFragment getInstance() {
        return new SingleMandiBottomFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.single_mandi_bottom_sheet, container, false);
        mandiName = v.findViewById(R.id.mandiName);
        navigate = v.findViewById(R.id.navigate);
        cropName = v.findViewById(R.id.cropName);
        minPrice = v.findViewById(R.id.minPrice);
        maxPrice = v.findViewById(R.id.maxPrice);
        minPriceBack = v.findViewById(R.id.minPriceBack);
        maxPriceBack = v.findViewById(R.id.maxPriceBack);
        bestPriceBack = v.findViewById(R.id.bestPriceBack);
        dehaatPriceBack = v.findViewById(R.id.dehaatPriceBack);
        sell_produce = v.findViewById(R.id.sell_produce);
        bestPrice = v.findViewById(R.id.bestPrice);
        dehaatPrice = v.findViewById(R.id.dehaatPrice);
        back = v.findViewById(R.id.back);
        sell_produce = v.findViewById(R.id.sell_produce);
        crop_list = v.findViewById(R.id.cropList);
        crop_list.setNestedScrollingEnabled(false);

        sell_produce.setOnClickListener(this);
        back.setOnClickListener(this);
        navigate.setOnClickListener(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mandi_data = (MandiPrices.Mandi) bundle.getSerializable("MANDI_DATA");


            if (mandi_data != null) {
                crop_id = Long.valueOf(mandi_data.getProduct_id().get(0));
                if (mandi_data.getDehaat_price() != null) {
                    dehaatPriceValue = mandi_data.getDehaat_price();
                }
                if (mandi_data.getType().equals("dehaat")) {
                    node_id = Long.valueOf(mandi_data.getNode().get(0));
                    if (node_id != 0L)
                        getNodePriceList(node_id, mandi_data.getType());
                } else {
                    mandi_id = Long.valueOf(mandi_data.getMandi().get(0));
                    if (mandi_id != 0L)
                        getMandiPriceList(mandi_id, mandi_data.getType());
                }
            }

        }
        return v;
    }

    private void getNodePriceList(Long node_id, String type) {
        AppRestClient client = AppRestClient.getInstance();
        Call<MandiCropPrice> call = client.getNodePricesData(node_id);
        call.enqueue(new ApiCallback<MandiCropPrice>() {
            @Override
            public void onResponse(Response<MandiCropPrice> response) {
                if (response.body() == null)
                    return;
                if (response.body().getData() == null)
                    return;
                mandiPricesData = response.body().getData();
                displayNodeData(mandiPricesData, type);
            }

            @Override
            public void onResponse401(Response<MandiCropPrice> response) throws JSONException {

            }


        });
    }

    private void getMandiPriceList(Long mandi_id, String type) {
        AppRestClient client = AppRestClient.getInstance();
        Call<MandiCropPrice> call = client.getMandiPricesData(mandi_id);
        call.enqueue(new ApiCallback<MandiCropPrice>() {
            @Override
            public void onResponse(Response<MandiCropPrice> response) {
                if (response.body() == null)
                    return;
                if (response.body().getData() == null)
                    return;
                mandiPricesData = response.body().getData();
                displayData(mandiPricesData, type);
            }

            @Override
            public void onResponse401(Response<MandiCropPrice> response) throws JSONException {

            }


        });
    }

    private void displayData(List<MandiCropPrice.Mandi> mandiPricesData, String type) {
        dehaatPriceBack.setVisibility(View.GONE);
        sell_produce.setVisibility(View.GONE);
        mandiName.setText(getString(R.string.mandi) + " " + mandi_data.getMandi().get(1));

        for (int i = 0; i < mandiPricesData.size(); i++) {
            if (mandiPricesData.get(i).getProduct_id().get(0).equals("" + crop_id)) {
                displaySelectedCropData(mandiPricesData.get(i));
                mandiPricesData.remove(i);
                break;
            }
        }

        crop_list.setAdapter(new RecyclerAdapterSingleMandi(getActivity(), type, mandiPricesData, this));
        crop_list.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void displayNodeData(List<MandiCropPrice.Mandi> mandiPricesData, String type) {
        dehaatPriceBack.setVisibility(View.VISIBLE);
        sell_produce.setVisibility(View.VISIBLE);

        minPriceBack.setVisibility(View.GONE);
        maxPriceBack.setVisibility(View.GONE);
        bestPriceBack.setVisibility(View.GONE);

        mandiName.setText(getString(R.string.node) + " " + mandi_data.getNode().get(1));

        for (int i = 0; i < mandiPricesData.size(); i++) {
            if (mandiPricesData.get(i).getProduct_id().get(0).equals("" + crop_id)) {
                displaySelectedNodeCropData(mandiPricesData.get(i));
                mandiPricesData.remove(i);
                break;
            }
        }

        crop_list.setAdapter(new RecyclerAdapterSingleMandi(getActivity(), type, mandiPricesData, this));
        crop_list.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void displaySelectedCropData(MandiCropPrice.Mandi crop) {
        cropName.setText(getString(R.string.crop) + ": " + crop.getProduct_id().get(1));
        minPrice.setText(getString(R.string.rs) + crop.getMin_price());
        maxPrice.setText(getString(R.string.rs) + crop.getMax_price());
        bestPrice.setText(getString(R.string.rs) + crop.getBest_price());
    }

    private void displaySelectedNodeCropData(MandiCropPrice.Mandi crop) {
        cropName.setText(getString(R.string.crop) + ": " + crop.getProduct_id().get(1));
        dehaatPrice.setText(getString(R.string.rs) + crop.getDehaat_price());
    }

    private List<Double> getLocationFromType(MandiPrices.Mandi mandi) {
        List<Double> location = new ArrayList<>();
        if (mandi.getType().equals("dehaat")) {
            location.add(mandi.getLatitude());
            location.add(mandi.getLongitude());
            return location;
        } else {
            Address address = AppUtils.getLatLongFromPin(getActivity(), mandi.getPincode());
            if (address == null)
                return null;
            location.add(address.getLatitude());
            location.add(address.getLongitude());
            return location;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.navigate:
                List<Double> locationList = getLocationFromType(mandi_data);
                if (locationList == null)
                    return;
                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)",
                        locationList.get(0), locationList.get(1), "");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
                break;
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.sell_produce:
                OutputSalesQueryFragment outputSalesQueryFragment = OutputSalesQueryFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putSerializable("MANDI_CROP_ID", crop_id);

                bundle.putSerializable("DEHAAT_PRICE", dehaatPriceValue);
                outputSalesQueryFragment.setArguments(bundle);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frag_container, outputSalesQueryFragment).addToBackStack("").commitAllowingStateLoss();
                break;

        }
    }

    @Override
    public void onClick(Long id) {
        OutputSalesQueryFragment outputSalesQueryFragment = OutputSalesQueryFragment.newInstance();
        Bundle bundle = new Bundle();
        if (id != null)
            bundle.putSerializable("MANDI_CROP_ID", id);
        else
            bundle.putSerializable("MANDI_CROP_ID", 0L);
        outputSalesQueryFragment.setArguments(bundle);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frag_container, outputSalesQueryFragment).addToBackStack("").commitAllowingStateLoss();
    }
}
