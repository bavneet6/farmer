package app.intspvt.com.farmer.fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterCrops;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.body.SendCropData;
import app.intspvt.com.farmer.rest.response.CropsData;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.ItemClickListener;
import app.intspvt.com.farmer.viewmodel.CropViewModel;
import app.intspvt.com.farmer.viewmodel.FarmerCropViewModel;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by DELL on 8/25/2017.
 */

public class CropFragment extends BaseFragment implements View.OnClickListener, ItemClickListener {
    public static final String TAG = CropFragment.class.getSimpleName();
    private RecyclerView crops_rec;
    private ImageView back;
    private FirebaseAnalytics mFirebaseAnalytics;
    private TextView sendData;
    private ProgressBar progressBar;
    private ArrayList<CropsData.CropsInfo> cropsInfos = new ArrayList<>();
    private ArrayList<CropsData.CropsInfo> farmerCropsInfo = new ArrayList<>();
    private HashMap<Long, CropsData.CropsInfo> cropMapData = new HashMap<>();
    private List<CropsData.CropsInfo> dataSendCrops = new ArrayList<>();

    private CropViewModel cropViewModel;
    private FarmerCropViewModel farmerCropViewModel;

    public static CropFragment newInstance() {
        return new CropFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MainActivity.showCallAndHome(true);
        View v = inflater.inflate(R.layout.fragment_crops, null);
        crops_rec = v.findViewById(R.id.crops_rec);
        progressBar = v.findViewById(R.id.progress_bar);
        crops_rec.setNestedScrollingEnabled(false);
        back = v.findViewById(R.id.back);
        sendData = v.findViewById(R.id.sendData);
        back.setOnClickListener(this);
        sendData.setOnClickListener(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        progressBar.setVisibility(View.VISIBLE);
        cropViewModel = ViewModelProviders.of(getActivity()).get(CropViewModel.class);
        cropViewModel.init();
        observeViewModel(cropViewModel);

        return v;
    }

    private void observeViewModel(CropViewModel viewModel) {
        viewModel.getCropList()
                .observe(this, cropInfo -> {
                    if (cropInfo != null && cropInfo.size() > 0) {
                        cropsInfos = cropInfo;
                        farmerCropViewModel = ViewModelProviders.of(getActivity()).get(FarmerCropViewModel.class);
                        farmerCropViewModel.init();
                        observeFarmerCropViewModel(farmerCropViewModel);
                    }
                });
    }

    private void observeFarmerCropViewModel(FarmerCropViewModel viewModel) {
        viewModel.getFarmerCropList()
                .observe(this, cropInfo -> {
                    if (cropInfo != null && cropInfo.size() > 0) {
                        farmerCropsInfo = cropInfo;
                    }
                    displayCropList();

                });
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.sendData:
                if (cropMapData.isEmpty())
                    AppUtils.showToast(getString(R.string.please_select_crops));
                else
                    sendCropData();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("CropScreen", Long.parseLong(AppPreference.getInstance().getMobile()));
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        mFirebaseAnalytics.logEvent("CropScreen", params);
    }

    private void sendCropData() {
        for (Map.Entry<Long, CropsData.CropsInfo> entry : cropMapData.entrySet()) {
            CropsData.CropsInfo cropsInfo = entry.getValue();
            cropsInfo.setCrop_id(cropsInfo.getCrop_id());
            cropsInfo.setLand_unit(cropsInfo.getLand_unit());
            cropsInfo.setPartner_crop_rel_record_id(cropsInfo.getPartner_crop_rel_record_id());
            cropsInfo.setCultivated_area(cropsInfo.getCultivated_area());
            cropsInfo.setDeleted(cropsInfo.isDeleted());
            dataSendCrops.add(cropsInfo);
        }
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        SendCropData sendCropData = new SendCropData(dataSendCrops);
        Call<CropsData> call = client.sendCropData(sendCropData);
        call.enqueue(new ApiCallback<CropsData>() {
            @Override
            public void onResponse(Response<CropsData> response) {
                AppUtils.hideProgressDialog();
                getActivity().onBackPressed();
                Bundle params = new Bundle();
                params.putLong("CropUpdate", Long.parseLong(AppPreference.getInstance().getMobile()));
                mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
                mFirebaseAnalytics.logEvent("CropUpdate", params);

            }

            @Override
            public void onResponse401(Response<CropsData> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }


        });
    }

    private void displayCropList() {
        if (getActivity() != null && isAdded()) {
            crops_rec.setVisibility(View.VISIBLE);
            if (farmerCropsInfo != null && farmerCropsInfo.size() != 0)
                for (int i = 0; i < farmerCropsInfo.size(); i++)
                    cropMapData.put(farmerCropsInfo.get(i).getCrop_id(), farmerCropsInfo.get(i));
            crops_rec.setAdapter(new RecyclerAdapterCrops(getActivity(), cropsInfos, cropMapData, this));
            crops_rec.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(HashMap<Long, CropsData.CropsInfo> cropMapData) {
        cropMapData.putAll(cropMapData);
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

    }
}