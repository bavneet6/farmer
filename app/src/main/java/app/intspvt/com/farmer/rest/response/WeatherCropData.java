package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WeatherCropData {

    @SerializedName("data")
    private CropData data;

    public CropData getData() {
        return data;
    }

    public static class CropData {
        @SerializedName("crop_advisory")
        private ArrayList<CropAdvisory> crop_advisory;

        public ArrayList<CropAdvisory> getCrop_advisory() {
            return crop_advisory;
        }

        public static class CropAdvisory {
            @SerializedName("advisory_content")
            private String advisory_content;
            @SerializedName("crop_name")
            private String crop_name;
            @SerializedName("stage")
            private String stage;

            public String getCrop_name() {
                return crop_name;
            }

            public String getStage() {
                return stage;
            }

            public String getAdvisory_content() {
                return advisory_content;
            }
        }
    }
}
