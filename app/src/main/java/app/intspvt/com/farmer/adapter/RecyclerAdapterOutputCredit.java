package app.intspvt.com.farmer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.response.OutputCreditHome;

public class RecyclerAdapterOutputCredit extends RecyclerView.Adapter<RecyclerAdapterOutputCredit.Order> {
    private Context context;
    private ArrayList<OutputCreditHome> data;

    public RecyclerAdapterOutputCredit(Context context, ArrayList<OutputCreditHome> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public RecyclerAdapterOutputCredit.Order onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_output_credit_home, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterOutputCredit.Order(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterOutputCredit.Order holder, int position) {
        final int pos = holder.getAdapterPosition();
        holder.text1.setText(data.get(pos).getText1());
        holder.text2.setText(data.get(pos).getText2());
        holder.image.setImageResource(data.get(pos).getImageId());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Order extends RecyclerView.ViewHolder {
        private TextView text1, text2;
        private ImageView image;

        public Order(View itemView) {
            super(itemView);
            text1 = itemView.findViewById(R.id.text1);
            text2 = itemView.findViewById(R.id.text2);
            image = itemView.findViewById(R.id.image);

        }
    }


}
