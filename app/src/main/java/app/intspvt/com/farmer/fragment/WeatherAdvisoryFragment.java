package app.intspvt.com.farmer.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterWeatherCropData;
import app.intspvt.com.farmer.adapter.RecyclerAdapterWeatherDates;
import app.intspvt.com.farmer.rest.response.WeatherCropData;
import app.intspvt.com.farmer.rest.response.WeatherParams;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.DeleteInputCart;
import app.intspvt.com.farmer.viewmodel.WeatherCropsViewModel;
import app.intspvt.com.farmer.viewmodel.WeatherParamsViewModel;
import pl.droidsonroids.gif.GifImageView;

public class WeatherAdvisoryFragment extends BaseFragment implements View.OnClickListener, DeleteInputCart {
    public static final String TAG = WeatherAdvisoryFragment.class.getSimpleName();
    private ImageView back;
    private GifImageView rainImage;
    private TextView maxTemp, minTemp, maxHum, minHum, rainText, windDir, district;
    private RecyclerView weather_params_list, weather_crop_list;
    private ArrayList<String> datesList = new ArrayList<>();
    private ArrayList<WeatherParams.Data.WeatherData> weatherData;
    private String datePos = null;
    private FirebaseAnalytics firebaseAnalytics;
    private WeatherParamsViewModel weatherParamsViewModel;
    private WeatherCropsViewModel weatherCropsViewModel;

    public static WeatherAdvisoryFragment newInstance() {
        return new WeatherAdvisoryFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_weather_advisory, null);
        MainActivity.showCallAndHome(true);
        back = v.findViewById(R.id.back);
        rainImage = v.findViewById(R.id.rainImage);

        maxTemp = v.findViewById(R.id.maxTemp);
        minTemp = v.findViewById(R.id.minTemp);
        maxHum = v.findViewById(R.id.maxHum);
        minHum = v.findViewById(R.id.minHum);
        rainText = v.findViewById(R.id.rainText);
        windDir = v.findViewById(R.id.windDir);
        district = v.findViewById(R.id.district);

        weather_params_list = v.findViewById(R.id.weather_params_list);
        weather_crop_list = v.findViewById(R.id.weather_crop_list);

        weather_params_list.setNestedScrollingEnabled(false);
        weather_crop_list.setNestedScrollingEnabled(false);

        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        back.setOnClickListener(this);
        AppUtils.showProgressDialog(getActivity());
        weatherParamsViewModel = ViewModelProviders.of(getActivity()).get(WeatherParamsViewModel.class);
        weatherParamsViewModel.init();
        observeWeatherParamViewModel(weatherParamsViewModel);
        weatherCropsViewModel = ViewModelProviders.of(getActivity()).get(WeatherCropsViewModel.class);
        weatherCropsViewModel.init();
        observeWeatherCropViewModel(weatherCropsViewModel);
        return v;
    }

    private void observeWeatherParamViewModel(WeatherParamsViewModel viewModel) {
        viewModel.getWeatherParamData()
                .observe(this, weatherParamsData -> {
                    AppUtils.hideProgressDialog();
                    if (weatherParamsData != null) {
                        String date1 = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
                        district.setText(weatherParamsData.getDistrict());
                        weatherData = weatherParamsData.getWeatherData();
                        for (int i = 0; i < weatherData.size(); i++) {
                            if (date1.equals(weatherData.get(i).getDate())) {
                                displayWeatherParamsData(weatherData.get(i));
                                datePos = "" + i;
                            }
                            datesList.add(weatherData.get(i).getDate());
                        }
                        displayDatesList();
                    }
                });
    }

    private void observeWeatherCropViewModel(WeatherCropsViewModel viewModel) {
        viewModel.getWeatherCropData()
                .observe(this, weatherCropData -> {
                    AppUtils.hideProgressDialog();
                    if (weatherCropData != null && weatherCropData.size() > 0) {
                        displayWeatherCropData(weatherCropData);
                    }
                });
    }


    private void displayDatesList() {
        if (getActivity() != null && isAdded()) {
            weather_params_list.setAdapter(new RecyclerAdapterWeatherDates(getActivity(), datesList, datePos, this));
            weather_params_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        }
    }

    private void displayWeatherParamsData(WeatherParams.Data.WeatherData weatherData) {
        if (getActivity() != null && isAdded()) {
            maxTemp.setText("" + weatherData.getMax_temp() + "°C");
            minTemp.setText("" + weatherData.getMin_temp() + "°C");
            minHum.setText("" + weatherData.getMin_humidity() + "%");
            maxHum.setText("" + weatherData.getMax_humidity() + "%");
            windDir.setText(weatherData.getWind_direction());
            if (weatherData.getRainfall() > 0) {
                rainImage.setBackgroundResource(R.drawable.rain_gif);
                rainText.setText(getString(R.string.rain_text));
            } else {
                rainImage.setBackgroundResource(R.drawable.sun_gif);
                rainText.setText(getString(R.string.no_rain_text));
            }
        }
    }


    private void displayWeatherCropData(ArrayList<WeatherCropData.CropData.CropAdvisory> data) {
        if (getActivity() != null && isAdded()) {
            weather_crop_list.setAdapter(new RecyclerAdapterWeatherCropData(getActivity(), data));
            weather_crop_list.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("WeatherPage", Long.parseLong(AppPreference.getInstance().getMobile()));
        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        firebaseAnalytics.logEvent("WeatherPage", params);
    }

    @Override
    public void onPause() {
        super.onPause();

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onItemClick(String s) {
        for (int i = 0; i < weatherData.size(); i++)
            if (s.equals(weatherData.get(i).getDate()))
                displayWeatherParamsData(weatherData.get(i));

    }
}
