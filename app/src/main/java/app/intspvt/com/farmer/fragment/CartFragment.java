package app.intspvt.com.farmer.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterCart;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.body.SendCartData;
import app.intspvt.com.farmer.rest.response.AddToCart;
import app.intspvt.com.farmer.rest.response.GetOrderResponse;
import app.intspvt.com.farmer.rest.response.OrderData;
import app.intspvt.com.farmer.rest.response.PromotionId;
import app.intspvt.com.farmer.rest.response.Promotions;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.OnItemClick;
import io.supercharge.shimmerlayout.ShimmerLayout;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by DELL on 8/2/2017.
 */

public class CartFragment extends BaseFragment implements OnItemClick, View.OnClickListener {
    public static final String TAG = CartFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ImageView buy, back;
    private TextView total_price, totalItem, disocunt, price_after_discount;
    private float total_amt, discountedPrice, totalSavingPrice;
    private FirebaseAnalytics mFirebaseAnalytics;
    private LinearLayout bottomBuy, item, noCartItem;
    private String number;
    private DatabaseHandler databaseHandler;
    private List<OrderData> sendCartList = new ArrayList<>();
    private int promotion_id = 0;
    private ArrayList<AddToCart> cartDataList;
    private SwipeRevealLayout swipeRevealLayout;
    private HashMap<Float, PromotionId> promotionValues = new HashMap<Float, PromotionId>();

    public static CartFragment newInstance() {
        return new CartFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cart, null);
        MainActivity.showCallAndHome(true);
        bottomBuy = v.findViewById(R.id.bottom_buy);
        item = v.findViewById(R.id.item);
        noCartItem = v.findViewById(R.id.tree);
        buy = v.findViewById(R.id.buy);
        recyclerView = v.findViewById(R.id.list_recyl);
        recyclerView.setNestedScrollingEnabled(false);
        total_price = v.findViewById(R.id.price);
        totalItem = v.findViewById(R.id.totalItem);
        disocunt = v.findViewById(R.id.disocunt);
        price_after_discount = v.findViewById(R.id.price_after_discount);
        back = v.findViewById(R.id.back);

        databaseHandler = new DatabaseHandler(this.getActivity());
        buy.setOnClickListener(this);
        back.setOnClickListener(this);

        displayCartData();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        // list returns all the data that are in the cart

        return v;

    }

    private void displayCartData() {
        cartDataList = databaseHandler.getAllCartData();
        recyclerView.setAdapter(new RecyclerAdapterCart(getActivity(), cartDataList, this));
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        getPromotionList();
        showDehaatBag();
        printPriceAndItem();
    }

    private void getPromotionList() {
        AppRestClient client = AppRestClient.getInstance();
        Call<Promotions> call = client.getPromotions();
        call.enqueue(new ApiCallback<Promotions>() {
            @Override
            public void onResponse(Response<Promotions> response) {

                if (response.body() == null)
                    return;
                if (response.body().getPromotionsData() == null)
                    return;
                List<Promotions.PromotionsData> promotionsData = response.body().getPromotionsData();
                for (int i = 0; i < promotionsData.size(); i++) {
                    PromotionId promotionId = new PromotionId();
                    promotionId.setDiscount(promotionsData.get(i).getTotal_discount());
                    promotionId.setPromotion_id(promotionsData.get(i).getId());
                    promotionValues.put(promotionsData.get(i).getMin_order_amount(), promotionId);
                }
                printPriceAndItem();
            }

            @Override
            public void onResponse401(Response<Promotions> response) throws
                    JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }


        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("CartScreen", Long.parseLong(AppPreference.getInstance().getMobile()));
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        mFirebaseAnalytics.logEvent("CartScreen", params);
    }


    @Override
    public void onClick(HashMap<Integer, Float> hashMap) {
        cartDataList = databaseHandler.getAllCartData();
        printPriceAndItem();
        showDehaatBag();
    }

    private void printPriceAndItem() {
        promotion_id = 0;
        discountedPrice = 0;
        totalSavingPrice = 0;
        total_amt = 0;
        for (int i = 0; i < cartDataList.size(); i++) {
            total_amt = total_amt + cartDataList.get(i).getTotalPrice();
        }
        total_price.setText(getString(R.string.rs) + " " + total_amt);
        totalItem.setText(getString(R.string.total_item) + ": " + cartDataList.size());
        disocunt.setText(getString(R.string.rs) + " " + 0.0);
        price_after_discount.setText(getString(R.string.rs) + " " + total_amt);
        if (!promotionValues.isEmpty()) {
            TreeMap<Float, PromotionId> sorted = new TreeMap<>(promotionValues);
            Map<Float, PromotionId> reverseMap = new TreeMap(Collections.reverseOrder());
            reverseMap.putAll(sorted);
            for (Map.Entry<Float, PromotionId> entry : reverseMap.entrySet())
                if (total_amt > entry.getKey()) {
                    disocunt.setText(getString(R.string.rs) + " " + entry.getValue().getDiscount());
                    discountedPrice = entry.getValue().getDiscount();
                    totalSavingPrice = total_amt - entry.getValue().getDiscount();
                    promotion_id = entry.getValue().getPromotion_id();
                    price_after_discount.setText(getString(R.string.rs) + " " + totalSavingPrice);
                    break;
                }
        }
    }

    private void showDehaatBag() {
        if (cartDataList.size() == 0) {
            bottomBuy.setVisibility(View.GONE);
            item.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            noCartItem.setVisibility(View.VISIBLE);

        } else {
            bottomBuy.setVisibility(View.VISIBLE);
            item.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
            noCartItem.setVisibility(View.GONE);
        }
    }

    /**
     * method for sending the cart data i.e. order products
     */
    private void sendData() {
        AppUtils.showProgressDialog(activity);
        sendCartList.clear();
        cartDataList = databaseHandler.getAllCartData();
        if (!cartDataList.isEmpty()) {
            for (int i = 0; i < cartDataList.size(); i++) {
                OrderData orderData = new OrderData();
                orderData.setProduct_id(Integer.parseInt(cartDataList.get(i).getProductId()));
                orderData.setProduct_uom_qty(cartDataList.get(i).getProductQty());
                orderData.setPrice_unit(cartDataList.get(i).getProductMrp());
                sendCartList.add(orderData);
            }
        }
        AppRestClient client = AppRestClient.getInstance();
        SendCartData.CartData cartData = new SendCartData.CartData(sendCartList, discountedPrice, promotion_id);
        SendCartData sendCartData = new SendCartData(cartData);
        Call<GetOrderResponse> call = client.sendCartData(sendCartData);
        call.enqueue(new ApiCallback<GetOrderResponse>() {
            @Override
            public void onResponse(Response<GetOrderResponse> response) {
                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    showDialog();
                    Bundle params = new Bundle();
                    params.putLong("ProductPurchased", Long.parseLong(AppPreference.getInstance().getMobile()));
                    mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
                    mFirebaseAnalytics.logEvent("ProductPurchased", params);

                }

            }

            @Override
            public void onResponse401(Response<GetOrderResponse> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());

            }
        });
    }

    private void showReviewDialog() {
        final Dialog dialog = new Dialog(getActivity());
        final TextView textCount, textTotal, cancel;
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_review_order);
        final ShimmerLayout shimmerText = dialog.findViewById(R.id.shimmer_text);

        cancel = dialog.findViewById(R.id.cancel);
        textCount = dialog.findViewById(R.id.count);
        textTotal = dialog.findViewById(R.id.totalAmt);
        shimmerText.startShimmerAnimation();
        swipeRevealLayout = dialog.findViewById(R.id.swipe_layout_4);

        ((TextView) dialog.findViewById(R.id.textre5)).setText(AppPreference.getInstance().getTOLL_FREE());
        dialog.findViewById(R.id.textre5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!AppUtils.isNullCase(AppPreference.getInstance().getTOLL_FREE())) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + AppPreference.getInstance().getTOLL_FREE()));
                    getActivity().startActivity(intent);
                }

            }
        });

        cartDataList = databaseHandler.getAllCartData();
        textCount.setText("" + cartDataList.size());
        if (discountedPrice != 0)
            textTotal.setText(getString(R.string.rs) + " " + totalSavingPrice);
        else
            textTotal.setText(getString(R.string.rs) + " " + total_amt);

        swipeRevealLayout.setMinFlingVelocity(10000);
        swipeRevealLayout.setSwipeListener(new SwipeRevealLayout.SwipeListener() {
            @Override
            public void onClosed(SwipeRevealLayout view) {
            }

            @Override
            public void onOpened(SwipeRevealLayout view) {
                dialog.dismiss();
                shimmerText.stopShimmerAnimation();
                sendData();
            }

            @Override
            public void onSlide(SwipeRevealLayout view, float slideOffset) {
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        Drawable d = new ColorDrawable(getResources().getColor(R.color.black));
        d.setAlpha(100);
        dialog.getWindow().setBackgroundDrawable(d);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

    }


    /**
     * dialog to show that cart data is placed successfully...
     */
    private void showDialog() {
        final Dialog dialog = new Dialog(getActivity());
        final TextView num, textCount, textTotal, textData;
        LinearLayout ok;
        ImageView cross;
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confim);
        num = dialog.findViewById(R.id.num);
        textData = dialog.findViewById(R.id.textData);
        textCount = dialog.findViewById(R.id.count);
        cross = dialog.findViewById(R.id.cross);
        textTotal = dialog.findViewById(R.id.totalAmt);
        textCount.setText("" + cartDataList.size());
        textTotal.setText(getString(R.string.rs) + " " + total_amt);
        databaseHandler.clearCartTable();
        //if number of dehaati is not there , toll free number will be printed
        number = AppPreference.getInstance().getTOLL_FREE();
        num.setText(number);
        textData.setText(getString(R.string.order_placed_line));

        num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!AppUtils.isNullCase(AppPreference.getInstance().getTOLL_FREE())) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + number));
                    startActivity(intent);
                }

            }

        });
        ok = dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getFragmentManager().beginTransaction().replace(R.id.frag_container, HomeFragment1.newInstance()).commitAllowingStateLoss();
                databaseHandler.clearCartTable();

            }
        });
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
                databaseHandler.clearCartTable();
                dialog.dismiss();
            }
        });

        Drawable d = new ColorDrawable(getResources().getColor(R.color.white));
        dialog.getWindow().setBackgroundDrawable(d);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        int width = getResources().getDisplayMetrics().widthPixels;
        dialog.getWindow().setLayout(width, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();

    }


    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.buy:
                if (AppUtils.haveNetworkConnection(getActivity()))
                    showReviewDialog();
                else
                    AppUtils.showToast(
                            getString(R.string.no_internet));
                break;
            case R.id.back:
                getActivity().onBackPressed();
                break;
        }
    }
}
