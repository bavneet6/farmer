package app.intspvt.com.farmer.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterCropSelection;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.body.PostFarmerInfo;
import app.intspvt.com.farmer.rest.body.PostFarmerPersonalInfo;
import app.intspvt.com.farmer.rest.body.SendCropData;
import app.intspvt.com.farmer.rest.response.CropsData;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.CropSelection;
import app.intspvt.com.farmer.viewmodel.CropViewModel;
import retrofit2.Call;
import retrofit2.Response;

public class CropSelectionFragment extends BaseFragment implements View.OnClickListener, CropSelection, SearchView.OnQueryTextListener {
    public static final String TAG = CropSelectionFragment.class.getSimpleName();
    private ImageView back;
    private Button next;
    private SearchView search;
    private RecyclerView crop_list;
    private HashSet<Long> cropIdsSet = new HashSet<>();
    private ArrayList<Long> cropIdsList = new ArrayList<>();
    private RecyclerAdapterCropSelection recyclerAdapterCropSelection;
    private ArrayList<CropsData.CropsInfo> cropsInfos = new ArrayList<>();
    private Boolean has_crops = null;
    private FirebaseAnalytics firebaseAnalytics;
    private CropViewModel cropViewModel;

    public static CropSelectionFragment newInstance() {
        return new CropSelectionFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crop_selection, null);
        back = v.findViewById(R.id.back);
        next = v.findViewById(R.id.next);
        crop_list = v.findViewById(R.id.cropsList);
        search = v.findViewById(R.id.search);
        crop_list.setNestedScrollingEnabled(false);
        back.setOnClickListener(this);
        next.setOnClickListener(this);
        search.setOnQueryTextListener(this);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search.setIconified(false);
            }
        });
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Bundle bundle = getArguments();
        if (bundle != null)
            has_crops = bundle.getBoolean("HAS_CROPS");
        AppUtils.showProgressDialog(getActivity());
        cropViewModel = ViewModelProviders.of(getActivity()).get(CropViewModel.class);
        cropViewModel.init();
        observeViewModel(cropViewModel);

        return v;
    }

    private void observeViewModel(CropViewModel viewModel) {
        viewModel.getCropList()
                .observe(this, cropInfo -> {
                    AppUtils.hideProgressDialog();
                    if (cropInfo != null && cropInfo.size() > 0) {
                        cropsInfos = cropInfo;
                        displayCropList(cropsInfos);
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("CropRegisterSelection", Long.parseLong(AppPreference.getInstance().getMobile()));
        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        firebaseAnalytics.logEvent("CropRegisterSelection", params);
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        ArrayList<Long> idsList = new ArrayList<>();
        for (Long ids : cropIdsSet) {
            idsList.add(ids);
        }
        AppPreference.getInstance().setCropsIDS(idsList);
    }

    private void displayCropList(ArrayList<CropsData.CropsInfo> products) {
        cropIdsList.clear();
        if (AppPreference.getInstance().getCROPIDS() != null && AppPreference.getInstance().getCROPIDS().size() != 0) {
            for (int i = 0; i < AppPreference.getInstance().getCROPIDS().size(); i++) {
                cropIdsList.add(Long.parseLong(String.valueOf(AppPreference.getInstance().getCROPIDS().get(i))));
                cropIdsSet.add(Long.parseLong(String.valueOf(AppPreference.getInstance().getCROPIDS().get(i))));
            }
        }
        recyclerAdapterCropSelection = new RecyclerAdapterCropSelection(getActivity(), products, cropIdsList, this);
        crop_list.setAdapter(recyclerAdapterCropSelection);
        crop_list.setLayoutManager(new GridLayoutManager(getActivity(), 2));
    }

    @Override
    public void onClick(View view) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        int id = view.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.next:
                if (cropIdsSet.isEmpty()) {
                    AppUtils.showToast(getString(R.string.please_select_crops));
                } else {
                    ArrayList<Long> idsList = new ArrayList<>();
                    for (Long ids : cropIdsSet) {
                        idsList.add(ids);
                    }
                    if (has_crops == null) {
                        Bundle bundle = new Bundle();
                        AppPreference.getInstance().setCropsIDS(idsList);
                        bundle.putSerializable("CROP_IDS", idsList);
                        PicGenderFragment fragment = PicGenderFragment.newInstance();
                        fragment.setArguments(bundle);
                        transaction.replace(R.id.frag_cont, fragment).addToBackStack("").commitAllowingStateLoss();
                    } else if (!has_crops)
                        sendCropData(idsList);
                }
                break;
        }
    }

    private void sendUpdateInfo() {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        PostFarmerInfo.PostInfo.FarmerInfo farmerInfo = new PostFarmerInfo.PostInfo.FarmerInfo(0, 0, 0, 0, 0, null, AppPreference.getInstance().getPINCODE());
        PostFarmerInfo.PostInfo getInfo = new PostFarmerInfo.PostInfo(farmerInfo);
        Call<PostFarmerPersonalInfo> call = client.updateFarmerInfo(null, getInfo);
        call.enqueue(new ApiCallback<PostFarmerPersonalInfo>() {
            @Override
            public void onResponse(Response<PostFarmerPersonalInfo> response) {
                AppPreference.getInstance().setAPP_LOGIN(true);
                Intent intent = new Intent(getActivity(), MainActivity.class);
                getActivity().startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onResponse401(Response<PostFarmerPersonalInfo> response) throws JSONException {
            }


        });
    }

    private void sendCropData(ArrayList<Long> idsList) {
        HashMap<Long, CropsData.CropsInfo> cropMapData = new HashMap<>();
        List<CropsData.CropsInfo> dataSendCrops = new ArrayList<>();
        for (int i = 0; i < idsList.size(); i++) {
            CropsData.CropsInfo cropsInfo = new CropsData.CropsInfo();
            cropsInfo.setCrop_id(idsList.get(i));
            cropsInfo.setCultivated_area(0f);
            cropsInfo.setDeleted(false);
            cropsInfo.setPartner_crop_rel_record_id(0L);
            cropsInfo.setLand_unit(null);
            cropMapData.put(idsList.get(i), cropsInfo);
        }
        for (Map.Entry<Long, CropsData.CropsInfo> entry : cropMapData.entrySet()) {
            CropsData.CropsInfo cropsInfo = entry.getValue();
            cropsInfo.setCrop_id(cropsInfo.getCrop_id());
            cropsInfo.setLand_unit(cropsInfo.getLand_unit());
            cropsInfo.setPartner_crop_rel_record_id(cropsInfo.getPartner_crop_rel_record_id());
            cropsInfo.setCultivated_area(cropsInfo.getCultivated_area());
            cropsInfo.setDeleted(cropsInfo.isDeleted());
            dataSendCrops.add(cropsInfo);
        }
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        SendCropData sendCropData = new SendCropData(dataSendCrops);
        Call<CropsData> call = client.sendCropData(sendCropData);
        call.enqueue(new ApiCallback<CropsData>() {
            @Override
            public void onResponse(Response<CropsData> response) {
                sendUpdateInfo();
            }

            @Override
            public void onResponse401(Response<CropsData> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }


        });
    }

    @Override
    public void onSelect(HashSet<Long> selectedCrops) {
        cropIdsSet.clear();
        cropIdsSet.addAll(selectedCrops);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (cropsInfos.size() != 0) {
            recyclerAdapterCropSelection.getFilter().filter(newText);
        }
        return false;
    }
}
