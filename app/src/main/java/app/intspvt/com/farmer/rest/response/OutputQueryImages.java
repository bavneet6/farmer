package app.intspvt.com.farmer.rest.response;

public class OutputQueryImages {
    public String filePath;
    public boolean delete;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
}
