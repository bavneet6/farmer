package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DELL on 9/14/2017.
 */

public class CropsData {

    @SerializedName("data")
    private ArrayList<CropsInfo> cropsInfo;

    public ArrayList<CropsInfo> getCropsInfo() {
        return cropsInfo;
    }

    public static class CropsInfo {

        @SerializedName("id")
        private Long id;
        @SerializedName("crop_id")
        private Long crop_id;
        @SerializedName("unit")
        private Long unit;
        @SerializedName("cultivated_area")
        private float cultivated_area;
        @SerializedName("land_unit")
        private String land_unit;
        @SerializedName("crop_name")
        private String crop_name;
        @SerializedName("name")
        private String name;
        @SerializedName("image")
        private String image;
        @SerializedName("deleted")
        private boolean deleted;
        @SerializedName("partner_crop_rel_record_id")
        private Long partner_crop_rel_record_id;

        public Long getPartner_crop_rel_record_id() {
            return partner_crop_rel_record_id;
        }

        public void setPartner_crop_rel_record_id(Long partner_crop_rel_record_id) {
            this.partner_crop_rel_record_id = partner_crop_rel_record_id;
        }

        public String getCrop_name() {
            return crop_name;
        }

        public Long getUnit() {
            return unit;
        }

        public boolean isDeleted() {
            return deleted;
        }

        public void setDeleted(boolean deleted) {
            this.deleted = deleted;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public float getCultivated_area() {
            return cultivated_area;
        }

        public void setCultivated_area(float cultivated_area) {
            this.cultivated_area = cultivated_area;
        }

        public Long getID() {
            return id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Long getCrop_id() {
            return crop_id;
        }

        public void setCrop_id(Long crop_id) {
            this.crop_id = crop_id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getLand_unit() {
            return land_unit;
        }

        public void setLand_unit(String land_unit) {
            this.land_unit = land_unit;
        }
    }
}
