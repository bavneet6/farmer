package app.intspvt.com.farmer.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.fragment.app.FragmentTransaction;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.body.PostFarmerInfo;
import app.intspvt.com.farmer.rest.body.PostFarmerPersonalInfo;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.GPSTracker;
import retrofit2.Call;
import retrofit2.Response;

public class PincodeFragment extends BaseFragment implements TextWatcher, View.OnClickListener {
    public static final String TAG = PincodeFragment.class.getSimpleName();
    private Button next;
    private GPSTracker gps;
    private double lat, longi;
    private ImageView refresh;
    private Geocoder geocoder;
    private EditText pincode;
    private List<Address> addressList;
    private Boolean has_crops = null;

    public static PincodeFragment newInstance() {
        return new PincodeFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pincode, null);
        next = v.findViewById(R.id.next);
        next.setClickable(false);
        pincode = v.findViewById(R.id.pincode);
        refresh = v.findViewById(R.id.refresh);
        refresh.setOnClickListener(this);
        next.setOnClickListener(this);
        pincode.addTextChangedListener(this);
        Bundle bundle = getArguments();
        if (bundle != null)
            has_crops = bundle.getBoolean("HAS_CROPS");
        checkGPS();
        return v;

    }


    private void checkGPS() {
        gps = new GPSTracker(getActivity());
        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            if ((latitude != 0.0) && (longitude != 0.0)) {
                lat = latitude;
                longi = longitude;
                getAddressFromLatLong(lat, longi);
            }
        } else {
            if (gps.showSettingsAlert())
                checkGPS();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

    }

    private void getAddressFromLatLong(double lat, double longi) {
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            addressList = geocoder.getFromLocation(lat, longi, 1);
            if (addressList != null && addressList.get(0).getPostalCode() != null)
                pincode.setText(addressList.get(0).getPostalCode());// Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (pincode.getText().length() == 6) {
            next.setBackgroundColor(getActivity().getResources().getColor(R.color.baseColor));
            next.setClickable(true);
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        } else {
            next.setBackgroundColor(getActivity().getResources().getColor(R.color.grey));
        }
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();

        int id = v.getId();
        switch (id) {
            case R.id.refresh:
                checkGPS();
                break;
            case R.id.next:
                if (AppUtils.haveNetworkConnection(getActivity())) {
                    if (pincode.getText().toString().length() <= 0) {
                        pincode.setError(Html.fromHtml(getString(R.string.enter_pincode)), null);
                        pincode.requestFocus();
                    } else {
                        if (has_crops == null) {
                            transaction.replace(R.id.frag_cont, CropSelectionFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                            AppPreference.getInstance().setPINCODE(Long.valueOf(pincode.getText().toString()));
                        } else if (!has_crops) {
                            CropSelectionFragment fragment = new CropSelectionFragment();
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("HAS_CROPS", has_crops);
                            fragment.setArguments(bundle);
                            transaction.replace(R.id.frag_cont, fragment).addToBackStack("").commitAllowingStateLoss();
                            AppPreference.getInstance().setPINCODE(Long.valueOf(pincode.getText().toString()));
                        } else {
                            sendUpdateInfo();
                        }
                    }
                } else
                    AppUtils.showToast(getString(R.string.no_internet));

                break;
        }
    }

    private void sendUpdateInfo() {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        PostFarmerInfo.PostInfo.FarmerInfo farmerInfo = new PostFarmerInfo.PostInfo.FarmerInfo(0, 0, 0, 0, 0, null, Long.parseLong(pincode.getText().toString()));
        PostFarmerInfo.PostInfo getInfo = new PostFarmerInfo.PostInfo(farmerInfo);
        Call<PostFarmerPersonalInfo> call = client.updateFarmerInfo(null, getInfo);
        call.enqueue(new ApiCallback<PostFarmerPersonalInfo>() {
            @Override
            public void onResponse(Response<PostFarmerPersonalInfo> response) {
                AppPreference.getInstance().setAPP_LOGIN(true);
                Intent intent = new Intent(getActivity(), MainActivity.class);
                getActivity().startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onResponse401(Response<PostFarmerPersonalInfo> response) throws JSONException {
            }


        });
    }
}
