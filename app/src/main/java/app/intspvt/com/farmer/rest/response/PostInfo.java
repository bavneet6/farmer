package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 8/30/2017.
 */

public class PostInfo {
    @SerializedName("data")
    private GetInfo data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private String status;

    public GetInfo getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public static class GetInfo {

        @SerializedName("farmer_info")
        private FarmerInfo farmer_info;
        @SerializedName("node_info")
        private NodeInfo node_info;

        @SerializedName("land_info")
        private LandInfo land_info;

        public GetInfo(FarmerInfo farmerInfo, LandInfo landInfo) {
            this.farmer_info = farmerInfo;
            this.land_info = landInfo;
        }

        public FarmerInfo getFarmer_info() {
            return farmer_info;
        }

        public LandInfo getLand_info() {
            return land_info;
        }

        public NodeInfo getNode_info() {
            return node_info;
        }

        public static class FarmerInfo {
            @SerializedName("state_id")
            private int state_id;

            @SerializedName("district_id")
            private int district_id;
            @SerializedName("block_id")
            private int block_id;
            @SerializedName("panchayat_id")
            private int panchayat_id;
            @SerializedName("village_id")
            private int village_id;

            @SerializedName("title")
            private int title;
            @SerializedName("zip")
            private Long zip;
            @SerializedName("image_small")
            private String image_small;
            @SerializedName("name")
            private String name;

            public FarmerInfo(String name, int title, String image_small, Long zip) {
                this.name = name;
                this.title = title;
                this.image_small = image_small;
                this.zip = zip;

            }

            public FarmerInfo(int state_id, int district_id, int block_id, int panchayat_id, int village_id, String name, String image_small, Long zip) {
                this.state_id = state_id;
                this.district_id = district_id;
                this.block_id = block_id;
                this.panchayat_id = panchayat_id;
                this.village_id = village_id;
                this.name = name;
                this.image_small = image_small;
                this.zip = zip;

            }

        }

        public class NodeInfo {
            @SerializedName("toll_free")
            private String toll_free;


            public String getToll_free() {
                return toll_free;
            }
        }
    }

}
