package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by DELL on 7/31/2017.
 */

public class Products implements Serializable {
    @SerializedName("id")
    private int productId;

    @SerializedName("name")
    private String productName;
    @SerializedName("x_name_hindi")
    private String productHindiName;

    @SerializedName("description")
    private String description;

    @SerializedName("image")
    private String image;

    @SerializedName("categ_id")
    private String category;
    @SerializedName("active")
    private boolean status;
    @SerializedName("search_suggestion")
    private ArrayList<String> search_suggestion;

    @SerializedName("variants")
    private ArrayList<Variants> variants;


    public ArrayList<String> getSearch_suggestion() {
        return search_suggestion;
    }

    public String getImage() {
        return image;
    }

    public int getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public String getProductHindiName() {
        return productHindiName;
    }

    public ArrayList<Variants> getVariants() {
        return variants;
    }

    public String getDescription() {
        return description;
    }

    public String getCategory() {
        return category;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public class Variants {
        @SerializedName("attribute")
        private String attribute;
        @SerializedName("fixed_price")
        private float fixed_price;
        @SerializedName("id")
        private int id;

        public int getId() {
            return id;
        }

        public float getFixed_price() {
            return fixed_price;
        }

        public String getAttribute() {
            return attribute;
        }
    }
}
