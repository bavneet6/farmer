package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 9/1/2017.
 */

public class OrderData {
    @SerializedName("product_id")
    private int product_id;

    @SerializedName("product_uom_qty")
    private float product_uom_qty;
    @SerializedName("price_unit")
    private float price_unit;


    public void setPrice_unit(float price_unit) {
        this.price_unit = price_unit;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public void setProduct_uom_qty(float product_uom_qty) {
        this.product_uom_qty = product_uom_qty;
    }
}
