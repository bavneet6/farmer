package app.intspvt.com.farmer.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterOutputSaleImageList;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.body.OutputData;
import app.intspvt.com.farmer.rest.response.CropsData;
import app.intspvt.com.farmer.rest.response.GetIdNames;
import app.intspvt.com.farmer.rest.response.OutputQueryImages;
import app.intspvt.com.farmer.rest.response.OutputSaleData;
import app.intspvt.com.farmer.rest.response.SingleOutputSale;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.DeleteInputCart;
import retrofit2.Call;
import retrofit2.Response;

public class OutputSalesQueryFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, DeleteInputCart {
    public static final String TAG = OutputSalesQueryFragment.class.getSimpleName();
    ArrayList<String> returnValue = new ArrayList<>();
    private Spinner cropName, cropUnit;
    private EditText cropQty, cropPrice, days, notes, cropVarName;
    private TextView cropUnitLine, sendData, date;
    private LinearLayout date_back;
    private RecyclerView output_images;
    private Calendar myCalendar = Calendar.getInstance();
    private ArrayList<String> cropNamesList = new ArrayList<>();
    private ArrayList<String> cropUnitList = new ArrayList<>();
    private Long cropId, recordId = 0L, recCropId = 0L, crop_link_unit, mandiCropId = 0L;
    private ImageView back, addImages;
    private int cropUomId, recCropUOMId = 0;
    private ArrayList<CropsData.CropsInfo> cropNames = new ArrayList<>();
    private ArrayList<GetIdNames.GetData> cropUOMNames = new ArrayList<>();
    private ArrayList<OutputQueryImages> imageList = new ArrayList<>();
    private FirebaseAnalytics firebaseAnalytics;
    private OutputQueryImages outputQueryImages;
    private List<String> attachments = new ArrayList<>();
    private ArrayList<File> imageFileList1 = new ArrayList<>();
    private HashMap<Integer, OutputQueryImages> imageFileList = new HashMap<>();
    private float dehaatPriceValue = 0f;

    public static OutputSalesQueryFragment newInstance() {
        return new OutputSalesQueryFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_output_sales_query, null);
        MainActivity.showCallAndHome(true);
        cropName = v.findViewById(R.id.cropName);
        cropVarName = v.findViewById(R.id.cropVar);
        cropUnit = v.findViewById(R.id.cropUnit);
        cropQty = v.findViewById(R.id.cropQty);
        cropPrice = v.findViewById(R.id.cropPrice);
        date = v.findViewById(R.id.date);
        days = v.findViewById(R.id.days);
        notes = v.findViewById(R.id.notes);
        cropUnitLine = v.findViewById(R.id.cropUnitLine);
        sendData = v.findViewById(R.id.sendData);
        date_back = v.findViewById(R.id.date_back);
        output_images = v.findViewById(R.id.output_images);
        addImages = v.findViewById(R.id.addImages);
        back = v.findViewById(R.id.back);
        output_images.setNestedScrollingEnabled(false);
        date_back.setOnClickListener(this);
        sendData.setOnClickListener(this);
        days.setOnClickListener(this);
        date.setOnClickListener(this);
        back.setOnClickListener(this);
        addImages.setOnClickListener(this);

        cropUnit.setOnItemSelectedListener(this);
        cropName.setOnItemSelectedListener(this);
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Bundle bundle = getArguments();
        if (bundle != null) {
            recordId = bundle.getLong("ENTITY_ID");
            mandiCropId = bundle.getLong("MANDI_CROP_ID");
            dehaatPriceValue = bundle.getFloat("DEHAAT_PRICE");
        }
        if (mandiCropId != 0L) {
            recCropId = mandiCropId;
        }
        if (recordId != 0L)
            getSingleOutputSaleData();
        else {
            String dateText = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
            date.setText(dateText);
            if (dehaatPriceValue!=0f) {
                cropPrice.setText(String.format("%s", dehaatPriceValue));
            }
            getCropList();
            getUOMList();
        }
        return v;
    }

    private void getSingleOutputSaleData() {

        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        Call<SingleOutputSale> call = client.getSingleOutputSaleHistory(recordId);
        call.enqueue(new ApiCallback<SingleOutputSale>() {
            @Override
            public void onResponse(Response<SingleOutputSale> response) {
                if (getActivity() != null && isAdded()) {
                    AppUtils.hideProgressDialog();
                    if (response.body() == null)
                        return;
                    if (response.body().getOutputData() == null)
                        return;
                    getCropList();
                    getUOMList();
                    displayData(response.body().getOutputData());
                }
            }

            @Override
            public void onResponse401(Response<SingleOutputSale> response) throws JSONException {

            }


        });
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void displayData(OutputSaleData outputData) {

        cropQty.setText("" + outputData.getCrop_qty());
        date.setText("" + outputData.getExpected_date());
        days.setText("" + outputData.getAvailable_days());
        cropPrice.setText("" + outputData.getPrice_unit());
        notes.setText("" + outputData.getNotes());
        cropVarName.setText("" + outputData.getCrop_var());
        recCropId = Long.valueOf(outputData.getCrop_id().get(0));
        recCropUOMId = Integer.parseInt(outputData.getCrop_uom_id().get(0));

        attachments = outputData.getAttachments();
        if (attachments != null)
            if (attachments.size() != 0) {
                for (int i = 0; i < attachments.size(); i++) {
                    outputQueryImages = new OutputQueryImages();
                    outputQueryImages.setDelete(false);
                    outputQueryImages.setFilePath(attachments.get(i));
                    imageList.add(outputQueryImages);
                }
            }
        output_images.setAdapter(new RecyclerAdapterOutputSaleImageList(getActivity(), imageList, this));
        output_images.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.sendData:
                checkValidations();
                break;
            case R.id.date_back:
                openDate();
                break;
            case R.id.date:
                openDate();
                break;
            case R.id.addImages:
                AppUtils.imageSelection(this, 5);
                break;
        }
    }

    private void checkValidations() {
        if (cropQty.getText().toString().equals("")) {
            cropQty.requestFocus();
            cropQty.setError(Html.fromHtml(getString(R.string.enter_qty)), null);
        } else {
            if (days.getText().toString().equals(""))
                days.setText("" + 0);
            if (cropPrice.getText().toString().equals(""))
                cropPrice.setText("0");
            if (recordId == 0L)
                raiseSaleQuery();
            else
                updateRaiseQuery();
        }
    }

    private void openDate() {
        int mday, mmonth, myear;
        if (date.getText().toString().equals("")) {
            mday = myCalendar.get(Calendar.DAY_OF_MONTH);
            mmonth = myCalendar.get(Calendar.MONTH);
            myear = myCalendar.get(Calendar.YEAR);
        } else {
            ArrayList<String> list = new ArrayList<>(Arrays.asList(date.getText().toString().split("/")));
            mday = Integer.parseInt(list.get(0));
            mmonth = Integer.parseInt(list.get(1));
            myear = Integer.parseInt(list.get(2));
        }

        DatePickerDialog StartTime = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int month = monthOfYear + 1;
                date.setText(dayOfMonth + "/" + month + "/" + year);
            }
        }, myear, mmonth - 1, mday);
        StartTime.getDatePicker().setMinDate(System.currentTimeMillis());

        StartTime.show();

    }

    private void showConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.information);
        builder.setMessage(R.string.info_receievd);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getActivity().onBackPressed();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("OutputSale", Long.parseLong(AppPreference.getInstance().getMobile()));
        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        firebaseAnalytics.logEvent("OutputSale", params);
    }

    private void getCropList() {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        Call<CropsData> call = client.getOutputCropsList();
        call.enqueue(new ApiCallback<CropsData>() {
            @Override
            public void onResponse(Response<CropsData> response) {
                if (getActivity() != null && isAdded()) {
                    AppUtils.hideProgressDialog();
                    if (response.body() == null)
                        return;
                    if (response.body().getCropsInfo() == null)
                        return;
                    cropNames = response.body().getCropsInfo();
                    for (int i = 0; i < response.body().getCropsInfo().size(); i++) {
                        cropNamesList.add(response.body().getCropsInfo().get(i).getName());
                    }
                    if (getActivity() != null && isAdded()) {
                        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(getActivity(), R.layout.template_spinner_text, cropNamesList);
                        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        cropName.setAdapter(stateAdapter);
                    }

                    if (recCropId != 0L) {
                        int id = getSelectedCropId(cropNames, recCropId);
                        cropName.setSelection(id);
                    }
                }
            }

            @Override
            public void onResponse401(Response<CropsData> response) throws JSONException {

            }


        });
    }

    private int getSelectedCropId(ArrayList<CropsData.CropsInfo> names, Long sid) {
        int id = 0;
        for (int i = 0; i < names.size(); i++) {

            if (names.get(i).getID() == sid) {
                return i;
            }
        }
        return id;
    }

    private int getSelectedUOMId(ArrayList<GetIdNames.GetData> names, Long sid) {
        int id = 0;
        for (int i = 0; i < names.size(); i++) {

            if (names.get(i).getId() == sid) {
                return i;
            }
        }
        return id;
    }

    private void getUOMList() {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        Call<GetIdNames> call = client.getUOMList();
        call.enqueue(new ApiCallback<GetIdNames>() {
            @Override
            public void onResponse(Response<GetIdNames> response) {
                AppUtils.hideProgressDialog();
                if (response.body() == null)
                    return;
                if (response.body().getData() == null)
                    return;
                cropUOMNames = response.body().getData();
                for (int i = 0; i < response.body().getData().size(); i++) {
                    cropUnitList.add(response.body().getData().get(i).getName());
                }
                if (getActivity() != null && isAdded()) {
                    ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(getActivity(), R.layout.template_spinner_small_text, cropUnitList);
                    stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    cropUnit.setAdapter(stateAdapter);
                }
                if (recCropUOMId != 0) {
                    int id = getSelectedUOMId(cropUOMNames, Long.valueOf(recCropUOMId));
                    cropUnit.setSelection(id);
                }

            }

            @Override
            public void onResponse401(Response<GetIdNames> response) throws JSONException {

            }


        });
    }

    private Integer getUnitPos() {
        for (int i = 0; i < cropUOMNames.size(); i++)
            if (cropUOMNames.get(i).getId() == crop_link_unit)
                return i;

        return 0;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("val", "requestCode ->  " + requestCode + "  resultCode " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (100): {
                if (resultCode == Activity.RESULT_OK) {
                    returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                    storeImagesInList(returnValue);
                }
            }
            break;
        }
    }

    private void storeImagesInList(ArrayList<String> returnValue) {
        for (int i = 0; i < returnValue.size(); i++) {
            File file = new File(returnValue.get(i));
            outputQueryImages = new OutputQueryImages();
            outputQueryImages.setDelete(true);
            outputQueryImages.setFilePath("" + file);
            imageList.add(outputQueryImages);
        }
        for (int i = 0; i < imageList.size(); i++) {
            imageFileList.put(i, imageList.get(i));
        }

        output_images.setAdapter(new RecyclerAdapterOutputSaleImageList(getActivity(), imageList, this));
        output_images.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AppUtils.imageSelection(this, 5);
                } else {
                }
                return;
            }
        }
    }

    private void raiseSaleQuery() {
        imageFileList1.clear();
        for (Map.Entry<Integer, OutputQueryImages> entry : imageFileList.entrySet()) {
            if (entry.getValue().isDelete())
                imageFileList1.add(new File(entry.getValue().getFilePath()));
        }
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        OutputData outputData = new OutputData(recordId, cropId, cropVarName.getText().toString(),
                Float.parseFloat(cropQty.getText().toString()), cropUomId, Float.parseFloat(cropPrice.getText().toString()),
                date.getText().toString(), Integer.parseInt(days.getText().toString()), notes.getText().toString());
        Call<Void> call = client.createFarmerOutputSale(imageFileList1, outputData);
        call.enqueue(new ApiCallback<Void>() {
            @Override
            public void onResponse(Response<Void> response) {
                Bundle params = new Bundle();
                params.putLong("OutputSaleCreated", Long.parseLong(AppPreference.getInstance().getMobile()));
                firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
                firebaseAnalytics.logEvent("OutputSaleCreated", params);
                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    showConfirmationDialog();
                }
            }

            @Override
            public void onResponse401(Response<Void> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });
    }

    private void updateRaiseQuery() {
        imageFileList1.clear();
        for (Map.Entry<Integer, OutputQueryImages> entry : imageFileList.entrySet()) {
            if (entry.getValue().isDelete())
                imageFileList1.add(new File(entry.getValue().getFilePath()));
        }
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        OutputData outputData = new OutputData(recordId, cropId, cropVarName.getText().toString(),
                Float.parseFloat(cropQty.getText().toString()), cropUomId, Float.parseFloat(cropPrice.getText().toString()),
                date.getText().toString(), Integer.parseInt(days.getText().toString()), notes.getText().toString());
        Call<Void> call = client.updateFarmerOutputSale(imageFileList1, outputData);
        call.enqueue(new ApiCallback<Void>() {
            @Override
            public void onResponse(Response<Void> response) {
                Bundle params = new Bundle();
                params.putLong("OutputSaleUpdated", Long.parseLong(AppPreference.getInstance().getMobile()));
                firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
                firebaseAnalytics.logEvent("OutputSaleUpdated", params);
                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    showConfirmationDialog();
                }
            }

            @Override
            public void onResponse401(Response<Void> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String label = parent.getItemAtPosition(position).toString();
        switch (parent.getId()) {
            case R.id.cropName:
                cropId = Long.valueOf(cropNames.get(position).getID());
                if (!AppUtils.isNullCase(String.valueOf(cropNames.get(position).getUnit())))
                    crop_link_unit = cropNames.get(position).getUnit();

                if (crop_link_unit != 0L) {
                    int pos = getUnitPos();
                    cropUnit.setSelection(pos);
                }
                break;
            case R.id.cropUnit:
                cropUnit.setDropDownWidth(175);
                cropUomId = cropUOMNames.get(position).getId();
                cropUnitLine.setText(getString(R.string.per) + " " + label);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onItemClick(String s) {
        imageFileList.remove(Integer.parseInt(s));
    }
}
