package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL on 1/11/2018.
 */

public class EnquiryDataHistory implements Serializable {
    @SerializedName("create_date")
    private String create_date;
    @SerializedName("description")
    private String description;
    @SerializedName("solution")
    private String solution;
    @SerializedName("kanban_state")
    private String kanban_state;
    @SerializedName("x_audio")
    private String audio;
    @SerializedName("has_audio")
    private boolean has_audio;
    @SerializedName("has_new_replies")
    private boolean has_new_replies;
    @SerializedName("id")
    private Long id;
    @SerializedName("category")
    private String category;
    @SerializedName("has_images")
    private boolean has_images;
    @SerializedName("report_available")
    private boolean report_available;
    @SerializedName("images")
    private ArrayList<String> images;
    @SerializedName("replies")
    private ArrayList<EnquiryReply> replies;
    @SerializedName("soil_report")
    private SoilReport soil_report;
    @SerializedName("crop_manual_ids")
    private List<Long> crop_manual_ids;

    public List<Long> getCrop_manual_ids() {
        return crop_manual_ids;
    }

    public SoilReport getSoil_report() {
        return soil_report;
    }

    public boolean isReport_available() {
        return report_available;
    }

    public String getCategory() {
        return category;
    }


    public String getCreate_date() {
        return create_date;
    }

    public String getSolution() {
        return solution;
    }

    public String getDescription() {
        return description;
    }

    public String getKanban_state() {
        return kanban_state;
    }

    public Long getId() {
        return id;
    }

    public String getAudio() {
        return audio;
    }

    public boolean isHas_images() {
        return has_images;
    }

    public boolean isHas_audio() {
        return has_audio;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public ArrayList<EnquiryReply> getReplies() {
        return replies;
    }

    public boolean isHas_new_replies() {
        return has_new_replies;
    }

    public class SoilReport {
        @SerializedName("image")
        private String image;

        public String getImage() {
            return image;
        }
    }
}
