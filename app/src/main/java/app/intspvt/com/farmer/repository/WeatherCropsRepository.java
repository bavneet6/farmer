package app.intspvt.com.farmer.repository;

import android.app.Activity;

import androidx.lifecycle.MutableLiveData;

import java.net.ConnectException;
import java.util.ArrayList;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.WeatherCropData;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherCropsRepository {
    private static WeatherCropsRepository mInstance;
    private ArrayList<WeatherCropData.CropData.CropAdvisory> weatherCropList;

    public static WeatherCropsRepository getInstance() {
        if (mInstance == null)
            mInstance = new WeatherCropsRepository();
        return mInstance;
    }

    public MutableLiveData<ArrayList<WeatherCropData.CropData.CropAdvisory>> getWeatherCropData() {
        final MutableLiveData<ArrayList<WeatherCropData.CropData.CropAdvisory>> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<WeatherCropData> call = client.getWeatherCropData();
        call.enqueue(new Callback<WeatherCropData>() {
            @Override
            public void onResponse(Call<WeatherCropData> call, Response<WeatherCropData> response) {
                if (response.body() == null)
                    return;
                weatherCropList = new ArrayList<>();
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        weatherCropList = response.body().getData().getCrop_advisory();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.tech_prob));
                } else if (response.code() == 401) {
                    AppUtils.showSessionExpiredDialog((Activity) Farmer.getInstance().getApplicationContext());
                }
                data.setValue(weatherCropList);
            }

            @Override
            public void onFailure(Call<WeatherCropData> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Farmer.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Farmer.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(new ArrayList<>());
            }


        });
        return data;
    }
}

