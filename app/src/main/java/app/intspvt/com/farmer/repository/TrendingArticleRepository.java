package app.intspvt.com.farmer.repository;

import androidx.lifecycle.MutableLiveData;

import java.net.ConnectException;
import java.util.ArrayList;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.Trending;
import app.intspvt.com.farmer.rest.response.TrendingData;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrendingArticleRepository {


    private static TrendingArticleRepository mInstance;
    private ArrayList<TrendingData> trendingData = new ArrayList<>();

    public static TrendingArticleRepository getInstance() {
        if (mInstance == null)
            mInstance = new TrendingArticleRepository();
        return mInstance;
    }

    public MutableLiveData<ArrayList<TrendingData>> getTrendingData() {
        MutableLiveData<ArrayList<TrendingData>> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<Trending> call = client.trendingData();
        call.enqueue(new Callback<Trending>() {
            @Override
            public void onResponse(Call<Trending> call, Response<Trending> response) {
                if (response.body() == null)
                    return;
                trendingData = new ArrayList<>();
                if (response.code() == 200) {
                    if (response.body().getDatas() != null) {
                        trendingData = response.body().getDatas();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }
                data.setValue(trendingData);

            }

            @Override
            public void onFailure(Call<Trending> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Farmer.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Farmer.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(new ArrayList<>());
            }

        });
        return data;
    }
}
