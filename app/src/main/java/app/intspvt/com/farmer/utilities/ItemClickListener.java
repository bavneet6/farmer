package app.intspvt.com.farmer.utilities;

import java.util.HashMap;

import app.intspvt.com.farmer.rest.response.CropsData;

/**
 * Created by DELL on 8/23/2017.
 */

public interface ItemClickListener {
    void onItemClick(HashMap<Long, CropsData.CropsInfo> cropMapData);
}

