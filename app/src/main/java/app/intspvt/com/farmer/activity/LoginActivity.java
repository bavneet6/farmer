package app.intspvt.com.farmer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.amazonaws.mobile.client.AWSMobileClient;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.fragment.IntroFragment;
import app.intspvt.com.farmer.fragment.LanguageFragment;
import app.intspvt.com.farmer.utilities.AppPreference;

public class LoginActivity extends AppCompatActivity{
    private FrameLayout frameLayout;
    private FragmentManager fragManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        frameLayout = findViewById(R.id.frag_cont);
        fragManager = getSupportFragmentManager();
        AWSMobileClient.getInstance().initialize(this).execute();
        // if bundle is recievd from logout show the number screen
        // else show the into screen
        Bundle getData = getIntent().getExtras();
        if (getData != null) {
            if (getData.getString("logout") != null) {
                Fragment fragment = fragManager.findFragmentByTag(LanguageFragment.TAG);
                if (fragment == null)
                    fragment = LanguageFragment.newInstance();
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.frag_cont, fragment,
                                LanguageFragment.TAG).commitAllowingStateLoss();
            } else if (!AppPreference.getInstance().getAPP_LOGIN())
                launchIntro();
            else
                launchAct();

        } else {
            if (!AppPreference.getInstance().getAPP_LOGIN())
                launchIntro();
            else
                launchAct();
        }
    }

    private void launchIntro() {
        Fragment fragment = fragManager.findFragmentByTag(IntroFragment.TAG);
        if (fragment == null)
            fragment = IntroFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frag_cont, fragment,
                        IntroFragment.TAG).commitAllowingStateLoss();
    }

    private void launchAct() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
