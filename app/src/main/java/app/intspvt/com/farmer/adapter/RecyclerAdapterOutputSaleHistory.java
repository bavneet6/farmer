package app.intspvt.com.farmer.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.fragment.OutputSalesQueryFragment;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.rest.response.OutputSaleData;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.RoundImageView;

import static app.intspvt.com.farmer.utilities.AppUtils.isNullCase;

public class RecyclerAdapterOutputSaleHistory extends RecyclerView.Adapter<RecyclerAdapterOutputSaleHistory.OutputQuery> {
    private Context context;
    private ArrayList<OutputSaleData> outputData;
    private String getPic;
    private DatabaseHandler databaseHandler;


    public RecyclerAdapterOutputSaleHistory(Context activity, ArrayList<OutputSaleData> outputData) {
        this.context = activity;
        this.outputData = outputData;
        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public RecyclerAdapterOutputSaleHistory.OutputQuery onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_output_sale_query, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterOutputSaleHistory.OutputQuery(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterOutputSaleHistory.OutputQuery holder, int position) {
        final int pos = holder.getAdapterPosition();
        holder.cropName.setText(outputData.get(pos).getCrop_id().get(1));
        if (!AppUtils.isNullCase(outputData.get(pos).getCrop_var()))
            holder.cropVar.setText(context.getString(R.string.variety) + ": " + outputData.get(pos).getCrop_var());
        holder.crop_qty.setText("" + outputData.get(pos).getCrop_qty());
        holder.price_unit.setText(context.getString(R.string.rs) + " " + outputData.get(pos).getPrice_unit());
        holder.date.setText(outputData.get(pos).getExpected_date());
        holder.status.setText(outputData.get(pos).getStatus());
        holder.back_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OutputSalesQueryFragment fragment = OutputSalesQueryFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putLong("ENTITY_ID", outputData.get(pos).getId());
                fragment.setArguments(bundle);
                AppUtils.changeFragment((FragmentActivity) context, fragment);
            }
        });

        if (outputData.get(pos).getAttachments() != null && outputData.get(pos).getAttachments().size() != 0)
            showPicassoImage(pos, holder.attachment);
    }

    private void showPicassoImage(int pos, final RoundImageView imageView) {
        AppUtils.generateAndCheckUrl(context, outputData.get(pos).getAttachments().get(0));
        String photo = outputData.get(pos).getAttachments().get(0);
        getPic = null;
        if (!isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(imageView, new PicassoCallback(photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).into(imageView);
                    }
                });

            } else {
                imageView.setImageResource(0);
            }
        }
    }


    @Override
    public int getItemCount() {
        return outputData.size();
    }

    public class OutputQuery extends RecyclerView.ViewHolder {
        private TextView status, cropName, cropVar, crop_qty, price_unit, date;
        private RoundImageView attachment;
        private LinearLayout back_card;

        public OutputQuery(View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.status);
            cropName = itemView.findViewById(R.id.cropName);
            cropVar = itemView.findViewById(R.id.cropVar);
            back_card = itemView.findViewById(R.id.back_card);
            crop_qty = itemView.findViewById(R.id.crop_qty);
            price_unit = itemView.findViewById(R.id.price_unit);
            date = itemView.findViewById(R.id.date);
            attachment = itemView.findViewById(R.id.attachment);
        }

    }
}
