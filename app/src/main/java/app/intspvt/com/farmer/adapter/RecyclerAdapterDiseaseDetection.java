package app.intspvt.com.farmer.adapter;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.List;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.fragment.SingleDiseaseDetectionFragment;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.rest.response.EnquiryDataHistory;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.RoundImageView;

import static app.intspvt.com.farmer.utilities.AppUtils.isNullCase;

public class RecyclerAdapterDiseaseDetection extends RecyclerView.Adapter<RecyclerAdapterDiseaseDetection.Order> {
    private Context context;
    private List<EnquiryDataHistory> data;
    private String getPic;
    private DatabaseHandler databaseHandler;


    public RecyclerAdapterDiseaseDetection(Context activity, List<EnquiryDataHistory> data) {
        this.context = activity;
        this.data = data;
        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public RecyclerAdapterDiseaseDetection.Order onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_disease_history, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterDiseaseDetection.Order(itemView);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final RecyclerAdapterDiseaseDetection.Order holder, int position) {
        final int pos = holder.getAdapterPosition();
        if (!AppUtils.isNullCase(data.get(pos).getDescription())) {
            holder.notes.setText(data.get(pos).getDescription());
        }
        holder.date.setText(data.get(pos).getCreate_date());
        holder.query_num.setText(context.getString(R.string.issue_no) + ": " + data.get(pos).getId());

        if (!AppUtils.isNullCase(data.get(pos).getKanban_state()))
            holder.status.setText(data.get(pos).getKanban_state());

        if (data.get(pos).getImages() != null && data.get(pos).getImages().size() > 0) {
            showPicassoImage(pos, holder.image);

        }

        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("ISSUE_ID", data.get(pos).getId());
                SingleDiseaseDetectionFragment fragment = SingleDiseaseDetectionFragment.newInstance();
                fragment.setArguments(bundle);
                AppUtils.changeFragment((FragmentActivity) context, fragment);
            }
        });
    }

    private void showPicassoImage(int pos, final RoundImageView imageView) {
        AppUtils.generateAndCheckUrl(context, data.get(pos).getImages().get(0));
        String photo = data.get(pos).getImages().get(0);
        getPic = null;
        if (!isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(imageView, new PicassoCallback(photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).into(imageView);
                    }
                });

            } else
                imageView.setImageResource(0);

        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Order extends RecyclerView.ViewHolder {
        private TextView date, query_num, notes, status;
        private RoundImageView image;
        private LinearLayout back;

        public Order(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            query_num = itemView.findViewById(R.id.query_num);
            notes = itemView.findViewById(R.id.notes);
            status = itemView.findViewById(R.id.status);
            image = itemView.findViewById(R.id.image);
            back = itemView.findViewById(R.id.back);


        }
    }
}



