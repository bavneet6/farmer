package app.intspvt.com.farmer.fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.SearchView;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterCropManualList;
import app.intspvt.com.farmer.rest.response.CropManualData;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.viewmodel.ManualCropsViewModel;

public class CropManualListFragment extends BaseFragment implements View.OnClickListener, SearchView.OnQueryTextListener {
    public static final String TAG = CropManualListFragment.class.getSimpleName();
    private ImageView back;
    private RecyclerView crop_manuals_list;
    private RecyclerAdapterCropManualList recyclerAdapterCropManualList;
    private SearchView search;
    private FirebaseAnalytics firebaseAnalytics;
    private ArrayList<CropManualData> cropManualData = new ArrayList<>();
    private ManualCropsViewModel manualCropsViewModel;

    public static CropManualListFragment newInstance() {
        return new CropManualListFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MainActivity.showCallAndHome(true);
        View v = inflater.inflate(R.layout.fragment_crop_manual_list, null);
        back = v.findViewById(R.id.back);
        crop_manuals_list = v.findViewById(R.id.crop_manuals_list);
        search = v.findViewById(R.id.search);
        crop_manuals_list.setNestedScrollingEnabled(false);
        search.setOnQueryTextListener(this);
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        back.setOnClickListener(this);
        search.setOnClickListener(v1 -> search.setIconified(false));
        AppUtils.showProgressDialog(getActivity());
        manualCropsViewModel = ViewModelProviders.of(getActivity()).get(ManualCropsViewModel.class);
        manualCropsViewModel.init();
        observeViewModel(manualCropsViewModel);

        return v;
    }

    private void observeViewModel(ManualCropsViewModel viewModel) {
        viewModel.getManualCrops()
                .observe(this, manualInfo -> {
                    AppUtils.hideProgressDialog();
                    if (manualInfo != null && manualInfo.size() > 0) {
                        cropManualData = manualInfo;
                        displayManualData(cropManualData);
                    }
                });
    }

    private void displayManualData(ArrayList<CropManualData> cropManuals) {
        if (getActivity() != null && isAdded()) {
            recyclerAdapterCropManualList = new RecyclerAdapterCropManualList(getActivity(), cropManuals);
            crop_manuals_list.setAdapter(recyclerAdapterCropManualList);
            crop_manuals_list.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("CropManual", Long.parseLong(AppPreference.getInstance().getMobile()));
        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        firebaseAnalytics.logEvent("CropManual", params);
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (cropManualData.size() != 0) {
            recyclerAdapterCropManualList.getFilter().filter(newText);
        }
        return false;
    }

}
