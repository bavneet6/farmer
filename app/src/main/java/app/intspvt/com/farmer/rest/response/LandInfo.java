package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 9/14/2017.
 */

public class LandInfo {

    @SerializedName("x_land_unit")
    private String x_land_unit;

    @SerializedName("x_total_land")
    private float x_total_land;

    @SerializedName("x_leased_in")
    private float x_leased_in;

    @SerializedName("x_leased_out")
    private float x_leased_out;

    public LandInfo(String x_land_unit, float x_total_land, float x_leased_in, float x_leased_out) {
        this.x_land_unit = x_land_unit;
        this.x_total_land = x_total_land;
        this.x_leased_in = x_leased_in;
        this.x_leased_out = x_leased_out;
    }

    public String getX_land_unit() {
        return x_land_unit;
    }

    public float getX_leased_in() {
        return x_leased_in;
    }

    public float getX_leased_out() {
        return x_leased_out;
    }

    public float getX_total_land() {
        return x_total_land;
    }

}
