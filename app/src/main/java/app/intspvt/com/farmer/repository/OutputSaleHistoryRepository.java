package app.intspvt.com.farmer.repository;

import androidx.lifecycle.MutableLiveData;

import java.net.ConnectException;
import java.util.ArrayList;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.OutputSale;
import app.intspvt.com.farmer.rest.response.OutputSaleData;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OutputSaleHistoryRepository {
    private static OutputSaleHistoryRepository mInstance;
    private ArrayList<OutputSaleData> outputSaleHistoryList = new ArrayList<>();

    public static OutputSaleHistoryRepository getInstance() {
        if (mInstance == null)
            mInstance = new OutputSaleHistoryRepository();
        return mInstance;
    }

    public MutableLiveData<ArrayList<OutputSaleData>> getOutputSaleHistoryList() {
        MutableLiveData<ArrayList<OutputSaleData>> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<OutputSale> call = client.getOutputSaleHistory();
        call.enqueue(new Callback<OutputSale>() {
            @Override
            public void onResponse(Call<OutputSale> call, Response<OutputSale> response) {
                if (response.body() == null)
                    return;
                outputSaleHistoryList = new ArrayList<>();
                if (response.code() == 200) {
                    if (response.body().getOutputData() != null) {
                        outputSaleHistoryList = response.body().getOutputData();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }
                data.setValue(outputSaleHistoryList);

            }

            @Override
            public void onFailure(Call<OutputSale> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Farmer.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Farmer.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(new ArrayList<>());
            }

        });
        return data;
    }
}
