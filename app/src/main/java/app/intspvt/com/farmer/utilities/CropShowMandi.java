package app.intspvt.com.farmer.utilities;

import java.util.List;

public interface CropShowMandi {

    void onSelect(List<String> list);
}
