package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MandiPrices implements Serializable {

    @SerializedName("data")
    private List<Mandi> data;

    public List<Mandi> getData() {
        return data;
    }

    public void setData(List<Mandi> data) {
        this.data = data;
    }

    public class Mandi implements Serializable {
        @SerializedName("mandi_id")
        private List<String> mandi;
        @SerializedName("team_id")
        private List<String> node;
        @SerializedName("product_id")
        private List<String> product_id;
        @SerializedName("product_uom")
        private List<String> product_uom;
        @SerializedName("max_price")
        private Float max_price;
        @SerializedName("min_price")
        private Float min_price;
        @SerializedName("best_price")
        private Float best_price;
        @SerializedName("dehaat_price")
        private Float dehaat_price;
        @SerializedName("pincode")
        private String pincode;
        @SerializedName("type")
        private String type;
        @SerializedName("latitude")
        private Double latitude;
        @SerializedName("longitude")
        private Double longitude;

        public String getType() {
            return type;
        }

        public Double getLatitude() {
            return latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public List<String> getNode() {
            return node;
        }

        public String getPincode() {
            return pincode;
        }

        public List<String> getMandi() {
            return mandi;
        }

        public List<String> getProduct_id() {
            return product_id;
        }

        public Float getDehaat_price() {
            return dehaat_price;
        }

        public Float getMax_price() {
            return max_price;
        }

        public Float getBest_price() {
            return best_price;
        }

        public Float getMin_price() {
            return min_price;
        }

        public List<String> getProduct_uom() {
            return product_uom;
        }
    }
}
