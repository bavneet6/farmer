package app.intspvt.com.farmer.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import app.intspvt.com.farmer.rest.response.AddToCart;
import app.intspvt.com.farmer.rest.response.IssueDbStatus;
import app.intspvt.com.farmer.rest.response.ProductFileTable;

/**
 * Created by DELL on 8/17/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "cartData7";
    private static final String CART_TABLE = "cartTable";

    private static final String VARIANT_NAME = "variant_name";
    private static final String PRODUCT_ID = "product_id";
    private static final String PRODUCT_NAME = "name";
    private static final String PRODUCT_PRICE = "product_price";
    private static final String PRODUCT_QTY = "qty";
    private static final String TOTAL_PRICE = "total_price";
    private static final String CART_PIC = "cart_pic";
    private static final String PRODUCT_PIC_FILE = "product_pic_file";
    private static final String PRODUCT_PIC_URL = "product_pic_url";
    private static final String PRODUCT_PIC_TABLE = "product_pic_table";
    private static final String ISSUE_SEEN = "issue_seen";
    private static final String ISSUE_ID = "issue_id";
    private static final String ISSUE_TABLE = "issue_table";
    private Context context;

    private SQLiteDatabase database;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_CART_TABLE = "CREATE TABLE " + CART_TABLE + "("
                + PRODUCT_ID + " INTEGER PRIMARY KEY ," + PRODUCT_NAME + " TEXT,"
                + PRODUCT_PRICE + " FLOAT," + PRODUCT_QTY + " INTEGER,"
                + TOTAL_PRICE + " FLOAT," + CART_PIC + " TEXT," + VARIANT_NAME + " TEXT" + ")";
        String PRO_URL_TABLE = "CREATE TABLE " + PRODUCT_PIC_TABLE + "("
                + PRODUCT_PIC_FILE + " TEXT PRIMARY KEY ," + PRODUCT_PIC_URL + " TEXT " + ")";
        String ISSUE_SEEN_TABLE = "CREATE TABLE " + ISSUE_TABLE + "("
                + ISSUE_ID + " INTEGER PRIMARY KEY ," + ISSUE_SEEN + " INTEGER " + ")";
        db.execSQL(CREATE_CART_TABLE);
        db.execSQL(PRO_URL_TABLE);
        db.execSQL(ISSUE_SEEN_TABLE);


    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + CART_TABLE);
        String CREATE_CART_TABLE = "CREATE TABLE " + CART_TABLE + "("
                + PRODUCT_ID + " INTEGER PRIMARY KEY ," + PRODUCT_NAME + " TEXT,"
                + PRODUCT_PRICE + " FLOAT," + PRODUCT_QTY + " INTEGER,"
                + TOTAL_PRICE + " FLOAT," + CART_PIC + " TEXT," + VARIANT_NAME + " TEXT" + ")";
        db.execSQL(CREATE_CART_TABLE);
        // Create tables again
    }

    /**
     * function to insert the products into cart table
     *
     * @param contact
     */
    public void insertCartData(boolean add, AddToCart contact) {
        int productQty = 0;
        String selectQuery = "SELECT " + PRODUCT_QTY + " FROM " + CART_TABLE + " WHERE " + PRODUCT_ID + " =" + "'" +
                contact.getProductId() + "'";
        database = this.getWritableDatabase();

        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            productQty = Integer.parseInt(cursor.getString(0));
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(PRODUCT_ID, contact.getProductId());
        contentValues.put(PRODUCT_NAME, contact.getProductName());
        contentValues.put(PRODUCT_PRICE, contact.getProductMrp());
        if (add) {
            contentValues.put(PRODUCT_QTY, contact.getProductQty() + productQty);
            float total = ((contact.getProductQty() + productQty) * contact.getProductMrp());
            contentValues.put(TOTAL_PRICE, total);

        } else {
            contentValues.put(PRODUCT_QTY, contact.getProductQty());
            contentValues.put(TOTAL_PRICE, contact.getTotalPrice());
        }
        contentValues.put(CART_PIC, contact.getProductImage());
        contentValues.put(VARIANT_NAME, contact.getVariant());
        // if quantity is 0 then product is deleted from the table
        if (contact.getProductQty() == 0) {
            database.delete(CART_TABLE, PRODUCT_ID + " = ?", new String[]{contact.getProductId()});
        } else {
            database.insertWithOnConflict(CART_TABLE, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        }
        if (cursor != null)
            cursor.close();
        database.close();
    }

    public void deleteProductData(String id) {
        database = this.getWritableDatabase();
        database.delete(CART_TABLE, PRODUCT_ID + " = ?", new String[]{id});
    }

    public void inserPicturesFile(ProductFileTable productFileTable) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PRODUCT_PIC_FILE, productFileTable.getProd_file());
        contentValues.put(PRODUCT_PIC_URL, productFileTable.getProd_url());
        database.insertWithOnConflict(PRODUCT_PIC_TABLE, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        database.close();
    }

    public boolean checkImage(String picFile) {
        String strR = null;
        String selectQuery = "SELECT " + PRODUCT_PIC_URL + " FROM " + PRODUCT_PIC_TABLE + " WHERE " + PRODUCT_PIC_FILE + " =" + "'" +
                picFile + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            strR = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        return strR != null;
    }

    public String getFileUrl(String picFile) {
        String strR = null;
        String selectQuery = "SELECT " + PRODUCT_PIC_URL + " FROM " + PRODUCT_PIC_TABLE + " WHERE " + PRODUCT_PIC_FILE + " =" + "'" +
                picFile + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            strR = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        return strR;
    }


    public void insertIssueStatusSeen(IssueDbStatus contact) {
        database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ISSUE_ID, contact.getIssue_id());
        contentValues.put(ISSUE_SEEN, contact.getIssue_unseen());
        database.insertWithOnConflict(ISSUE_TABLE, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);

        database.close();
    }

    /**
     * function to return all the rows of the cart table
     *
     * @return
     */
    public ArrayList<AddToCart> getAllCartData() {
        ArrayList<AddToCart> contactList = new ArrayList<AddToCart>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + CART_TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                AddToCart contactModel = new AddToCart();
                contactModel.setProductId(cursor.getString(0));
                contactModel.setProductName(cursor.getString(1));
                contactModel.setProductMrp(Float.valueOf(cursor.getString(2)));
                contactModel.setProductQty(Integer.parseInt(cursor.getString(3)));
                contactModel.setTotalPrice(Float.valueOf(cursor.getString(4)));
                contactModel.setProductImage(cursor.getString(5));
                contactModel.setVariant(cursor.getString(6));
                // Adding contact to list
                contactList.add(contactModel);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();

        // return contact list
        return contactList;
    }

    public ArrayList<IssueDbStatus> getIssueSeenData() {
        ArrayList<IssueDbStatus> contactList = new ArrayList<IssueDbStatus>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + ISSUE_TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                IssueDbStatus contactModel = new IssueDbStatus();
                contactModel.setIssue_id(cursor.getLong(0));

                contactModel.setIssue_unseen(cursor.getInt(1));
                // Adding contact to list
                contactList.add(contactModel);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();

        // return contact list
        return contactList;
    }

    /**
     * function to delete the cart table
     */
    public void clearCartTable() {
        database = this.getWritableDatabase();
        database.delete(CART_TABLE, null, null);
    }


    public void clearURlsData() {
        database = this.getWritableDatabase();
        database.delete(PRODUCT_PIC_TABLE, null, null);
    }

    public void clearIssueStatusSeen() {
        database = this.getWritableDatabase();
        database.delete(ISSUE_TABLE, null, null);
    }


}

