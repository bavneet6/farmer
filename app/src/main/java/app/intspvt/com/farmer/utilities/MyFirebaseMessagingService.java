package app.intspvt.com.farmer.utilities;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;

/**
 * Created by DELL on 1/3/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN", s);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        Log.e(TAG, "From: " + remoteMessage);
        Log.e(TAG, "Notification Message Body: " + remoteMessage.getData().get("screen"));
        createNotification(remoteMessage);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createNotification(RemoteMessage remoteMessage) {
        Intent intent = new Intent(this, MainActivity.class);

        if (remoteMessage.getData().get("screen").equals(UrlConstant.ENQ_HI)) {
            intent.putExtra("screen", UrlConstant.ENQ_HI);
            intent.putExtra("ISSUE_ID", remoteMessage.getData().get("id"));

        } else if (remoteMessage.getData().get("screen").equals(UrlConstant.DISEASE_HI)) {

            intent.putExtra("screen", UrlConstant.DISEASE_HI);
            intent.putExtra("ISSUE_ID", remoteMessage.getData().get("id"));

        } else if (remoteMessage.getData().get("screen").equals(UrlConstant.SOIL_HI)) {

            intent.putExtra("screen", UrlConstant.SOIL_HI);
            intent.putExtra("ISSUE_ID", remoteMessage.getData().get("id"));

        } else if (remoteMessage.getData().get("screen").equals(UrlConstant.AGR_IN_HI)) {
            intent.putExtra("screen", UrlConstant.AGR_IN_HI);
            intent.putExtra("ORDER_ID", remoteMessage.getData().get("id"));
        } else if (remoteMessage.getData().get("screen").equals(UrlConstant.OUTPUT_HI)) {
            intent.putExtra("screen", UrlConstant.OUTPUT_HI);
            intent.putExtra("ENTITY_ID", remoteMessage.getData().get("id"));
        } else if (remoteMessage.getData().get("screen").equals(UrlConstant.TREN_ART)) {
            intent.putExtra("screen", UrlConstant.TREN_ART);
        } else if (remoteMessage.getData().get("screen").equals(UrlConstant.PROM_ART)) {
            intent.putExtra("screen", UrlConstant.PROM_ART);
        } else if (remoteMessage.getData().get("screen").equals(UrlConstant.WEATHER_DATA)) {
            intent.putExtra("screen", UrlConstant.WEATHER_DATA);
        }
        PendingIntent resultIntent = PendingIntent.getActivity(this, 0, intent,
                -PendingIntent.FLAG_ONE_SHOT);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification_layout);
        contentView.setImageViewResource(R.id.image, R.drawable.dehaat_icon);
        contentView.setTextViewText(R.id.title, remoteMessage.getData().get("title"));
        contentView.setTextViewText(R.id.text, remoteMessage.getData().get("content"));


        String CHANNEL_ID = "DeHaat";// The id of the channel.
        CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.createNotificationChannel(mChannel);
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.iconleaf)
                .setContent(contentView)
                .setShowWhen(true)
                .setChannelId(CHANNEL_ID)
                .setAutoCancel(true)
                .setContentIntent(resultIntent)
                .setCustomBigContentView(contentView);
        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notification);

    }

}