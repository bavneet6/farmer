package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

public class SingleOutputSale {
    @SerializedName("data")
    private OutputSaleData outputData;

    public OutputSaleData getOutputData() {
        return outputData;
    }
}
