package app.intspvt.com.farmer.rest.body;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import app.intspvt.com.farmer.rest.response.LandInfo;

public class PostFarmerInfo {
    @SerializedName("data")
    private PostInfo data;

    public static class PostInfo {
        @SerializedName("farmer_info")
        private FarmerInfo farmer_info;
        @SerializedName("crop_info")
        private ArrayList<Long> crop_info;

        @SerializedName("land_info")
        private LandInfo land_info;

        public PostInfo(FarmerInfo farmerInfo, LandInfo landInfo, ArrayList<Long> cropInfo) {
            this.farmer_info = farmerInfo;
            this.land_info = landInfo;
            this.crop_info = cropInfo;
        }

        public PostInfo(FarmerInfo farmerInfo) {
            this.farmer_info = farmerInfo;
        }

        public PostInfo(LandInfo landInfo) {
            this.land_info = landInfo;
        }

        public static class FarmerInfo {
            @SerializedName("state_id")
            private int state_id;

            @SerializedName("district_id")
            private int district_id;
            @SerializedName("block_id")
            private int block_id;
            @SerializedName("panchayat_id")
            private int panchayat_id;
            @SerializedName("village_id")
            private int village_id;

            @SerializedName("title")
            private int title;
            @SerializedName("zip")
            private Long zip;
            @SerializedName("image_small")
            private String image_small;
            @SerializedName("name")
            private String name;

            public FarmerInfo(String name, int title, Long zip) {
                this.name = name;
                this.title = title;
                this.zip = zip;

            }

            public FarmerInfo(int state_id, int district_id, int block_id, int panchayat_id, int village_id, String name, Long zip) {
                this.state_id = state_id;
                this.district_id = district_id;
                this.block_id = block_id;
                this.panchayat_id = panchayat_id;
                this.village_id = village_id;
                this.name = name;
                this.zip = zip;

            }
        }
    }
}
