package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Features {
    @SerializedName("data")

    public ArrayList<Data> data;

    public ArrayList<Data> getData() {
        return data;
    }

    public class Data {
        @SerializedName("name")
        String name;
        @SerializedName("image")
        String image;
        @SerializedName("screen")
        String screen;

        public String getName() {
            return name;
        }

        public String getImage() {
            return image;
        }

        public String getScreen() {
            return screen;
        }
    }
}
