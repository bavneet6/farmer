package app.intspvt.com.farmer.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.EnquiryDataHistory;
import app.intspvt.com.farmer.rest.response.SingleIssueHistory;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Response;

public class SingleDiseaseDetectionFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = SingleDiseaseDetectionFragment.class.getSimpleName();
    private FirebaseAnalytics mFirebaseAnalytics;
    private ImageView back;
    private Long issue_id = 0L;
    private List<Long> crop_manual_ids;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private EnquiryDataHistory enquiryDataHistory;

    public static SingleDiseaseDetectionFragment newInstance() {
        return new SingleDiseaseDetectionFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_single_disease_detection, null);
        MainActivity.showCallAndHome(true);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Bundle bundle = getArguments();
        if (bundle != null) {
            issue_id = bundle.getLong("ISSUE_ID");
        }
        viewPager = v.findViewById(R.id.viewpager);
        tabLayout = v.findViewById(R.id.tabs);
        back = v.findViewById(R.id.back);
        getSingleEnquiryHistory(issue_id);
        back.setOnClickListener(this);
        return v;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("DiseaseSingleHistory", Long.parseLong(AppPreference.getInstance().getMobile()));
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        mFirebaseAnalytics.logEvent("DiseaseSingleHistory", params);
    }

    private void getSingleEnquiryHistory(Long issue_id) {
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<SingleIssueHistory> call = client.getSingleHistoryData(issue_id);
        call.enqueue(new ApiCallback<SingleIssueHistory>() {
            @Override
            public void onResponse(Response<SingleIssueHistory> response) {
                if (getActivity() != null && isAdded()) {
                    AppUtils.hideProgressDialog();

                    if (response.body() == null)
                        return;
                    if (response.body().getData() == null)
                        return;
                    enquiryDataHistory = response.body().getData();
                    crop_manual_ids = enquiryDataHistory.getCrop_manual_ids();
                    setupViewPager(enquiryDataHistory);
                    tabLayout.setupWithViewPager(viewPager);
                }
            }

            @Override
            public void onResponse401(Response<SingleIssueHistory> response) throws
                    JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }


        });
    }

    private void setupViewPager(EnquiryDataHistory enquiryDataHistory) {
        SingleDiseaseDetectionFragment.ViewPagerAdapter adapter = new SingleDiseaseDetectionFragment.ViewPagerAdapter(getChildFragmentManager());
        Fragment fragment = new SingleDiseaseMessagesFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("ISSUE_DATA", enquiryDataHistory);
        bundle.putLong("ISSUE_ID", issue_id);
        fragment.setArguments(bundle);
        adapter.addFragment(fragment, getString(R.string.issue));
        if (crop_manual_ids != null && crop_manual_ids.size() != 0) {
            Fragment fragment1 = new DiseaseCropManualFragment();
            Bundle bundle1 = new Bundle();
            bundle1.putSerializable("MANUAL_IDS", (Serializable) crop_manual_ids);
            fragment1.setArguments(bundle1);
            adapter.addFragment(fragment1, getString(R.string.suggested_crop_manuals));
        }
        this.viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}