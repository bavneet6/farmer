package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 9/1/2017.
 */

public class GetOrderResponse {
    @SerializedName("message")
    private String message;

    @SerializedName("orderID")
    private int orderID;

    @SerializedName("status")
    private String status;

    public int getOrderID() {
        return orderID;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }
}
