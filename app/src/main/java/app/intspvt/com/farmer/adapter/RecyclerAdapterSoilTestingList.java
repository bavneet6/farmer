package app.intspvt.com.farmer.adapter;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.List;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.rest.response.EnquiryDataHistory;
import app.intspvt.com.farmer.utilities.AppUtils;

import static app.intspvt.com.farmer.utilities.AppUtils.isNullCase;

public class RecyclerAdapterSoilTestingList extends RecyclerView.Adapter<RecyclerAdapterSoilTestingList.Order> {
    private Context context;
    private List<EnquiryDataHistory> data;
    private DatabaseHandler databaseHandler;
    private String getPic;


    public RecyclerAdapterSoilTestingList(Context activity, List<EnquiryDataHistory> data) {
        this.context = activity;
        this.data = data;
        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public RecyclerAdapterSoilTestingList.Order onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_soil_testing_list, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterSoilTestingList.Order(itemView);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final RecyclerAdapterSoilTestingList.Order holder, int position) {
        final int pos = holder.getAdapterPosition();
        if (!AppUtils.isNullCase(data.get(pos).getDescription())) {
            holder.notes.setText(data.get(pos).getDescription());
        }
        holder.date.setText(data.get(pos).getCreate_date());
        holder.query_num.setText(context.getString(R.string.issue_no) + ": " + data.get(pos).getId());
        if (!AppUtils.isNullCase(data.get(pos).getKanban_state()))
            holder.status.setText(data.get(pos).getKanban_state());

        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.get(pos).isReport_available()) {
                    final Dialog dialog = new Dialog(context);
                    dialog.setCanceledOnTouchOutside(true);
                    final ImageView imageView;
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.image_zoom);
                    imageView = dialog.findViewById(R.id.imageView);
                    showPicassoImage(pos, imageView);
                    dialog.getWindow().setGravity(Gravity.CENTER);
                    dialog.show();

                }

            }
        });
    }

    private void showPicassoImage(int pos, final ImageView imageView) {
        AppUtils.generateAndCheckUrl(context, data.get(pos).getSoil_report().getImage());
        String photo = data.get(pos).getSoil_report().getImage();
        getPic = null;
        if (!isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(imageView, new PicassoCallback(photo) {
                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).into(imageView);
                    }
                });

            } else {
                imageView.setImageResource(0);
            }
        }
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Order extends RecyclerView.ViewHolder {
        private TextView date, query_num, notes, status;
        private LinearLayout back;

        public Order(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            query_num = itemView.findViewById(R.id.query_num);
            notes = itemView.findViewById(R.id.notes);
            status = itemView.findViewById(R.id.status);
            back = itemView.findViewById(R.id.back);


        }
    }
}



