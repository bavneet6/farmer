package app.intspvt.com.farmer.repository;

import androidx.lifecycle.MutableLiveData;

import java.net.ConnectException;
import java.util.ArrayList;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.CropsData;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CropRepository {
    private static CropRepository mInstance;
    private ArrayList<CropsData.CropsInfo> cropInfos = new ArrayList<>();

    public static CropRepository getInstance() {
        if (mInstance == null)
            mInstance = new CropRepository();
        return mInstance;
    }

    public MutableLiveData<ArrayList<CropsData.CropsInfo>> getCropList() {
        MutableLiveData<ArrayList<CropsData.CropsInfo>> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<CropsData> call = client.getCropList();
        call.enqueue(new Callback<CropsData>() {
            @Override
            public void onResponse(Call<CropsData> call, Response<CropsData> response) {
                if (response.body() == null)
                    return;
                cropInfos = new ArrayList<>();
                if (response.code() == 200) {
                    if (response.body().getCropsInfo() != null) {
                        cropInfos = response.body().getCropsInfo();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }
                data.setValue(cropInfos);

            }

            @Override
            public void onFailure(Call<CropsData> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Farmer.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Farmer.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(new ArrayList<>());
            }

        });
        return data;
    }
}

