package app.intspvt.com.farmer.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.rest.response.CropsData;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.CropSelection;
import app.intspvt.com.farmer.utilities.RoundImageView;

import static app.intspvt.com.farmer.utilities.AppUtils.isNullCase;

public class RecyclerAdapterCropSelection extends RecyclerView.Adapter<RecyclerAdapterCropSelection.Crops> implements Filterable {
    private ArrayList<CropsData.CropsInfo> cropList;
    private ArrayList<CropsData.CropsInfo> cropFilterList;
    private Context context;
    private HashSet<Long> cropIds = new HashSet<>();
    private CropSelection cropSelection;
    private String getPic;
    private ArrayList<Long> cropIdsList;
    private DatabaseHandler databaseHandler;


    public RecyclerAdapterCropSelection(Activity activity, ArrayList<CropsData.CropsInfo> products, ArrayList<Long> cropIdsList, CropSelection cropSelection) {
        this.context = activity;
        this.cropList = products;
        this.cropIdsList = cropIdsList;
        this.cropSelection = cropSelection;
        this.cropFilterList = products;

        for (int i = 0; i < cropIdsList.size(); i++)
            cropIds.add(cropIdsList.get(i));

        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public RecyclerAdapterCropSelection.Crops onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_crop_selection, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterCropSelection.Crops(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterCropSelection.Crops holder, final int position) {
        final int pos = holder.getAdapterPosition();
        holder.cropName.setText("" + cropList.get(pos).getName());
        if (!AppUtils.isNullCase(cropList.get(pos).getImage()))
            showPicassoImage(pos, holder.cropImage);

        if (cropIds.contains(Long.parseLong(String.valueOf(cropList.get(pos).getID())))) {
            holder.back.setBackground(context.getResources().getDrawable(R.drawable.blue_back_box));
            holder.cropName.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.back.setBackground(context.getResources().getDrawable(R.drawable.categ_box));
            holder.cropName.setTextColor(context.getResources().getColor(R.color.textColor));
        }

        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cropIds.contains(Long.parseLong(String.valueOf(cropList.get(pos).getID())))) {
                    cropIds.remove(Long.parseLong(String.valueOf(cropList.get(pos).getID())));
                    holder.back.setBackground(context.getResources().getDrawable(R.drawable.categ_box));
                    holder.cropName.setTextColor(context.getResources().getColor(R.color.textColor));
                } else {
                    cropIds.add(Long.parseLong(String.valueOf(cropList.get(pos).getID())));
                    holder.back.setBackground(context.getResources().getDrawable(R.drawable.blue_back_box));
                    holder.cropName.setTextColor(context.getResources().getColor(R.color.white));
                }
                cropSelection.onSelect(cropIds);
            }
        });
    }

    private void showPicassoImage(int pos, final RoundImageView imageView) {
        AppUtils.generateAndCheckUrl(context, cropList.get(pos).getImage());
        String photo = cropList.get(pos).getImage();
        getPic = null;
        if (!isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(imageView, new PicassoCallback(photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_crop_image).into(imageView);
                    }
                });

            } else
                imageView.setImageResource(R.drawable.no_crop_image);

        }
    }


    @Override
    public int getItemCount() {
        return cropList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    cropList = cropFilterList;
                } else {
                    ArrayList<CropsData.CropsInfo> filteredList = new ArrayList<>();
                    for (CropsData.CropsInfo list : cropFilterList) {

                        if (list.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(list);
                        }
                    }
                    cropList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = cropList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                cropList = (ArrayList<CropsData.CropsInfo>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class Crops extends RecyclerView.ViewHolder {
        TextView cropName;
        RoundImageView cropImage;
        LinearLayout back;

        public Crops(View itemView) {
            super(itemView);
            back = itemView.findViewById(R.id.back);
            cropName = itemView.findViewById(R.id.cropName);
            cropImage = itemView.findViewById(R.id.cropImage);

        }
    }

}

