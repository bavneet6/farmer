package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OutputSaleData {
    @SerializedName("crop_id")
    private List<String> crop_id;
    @SerializedName("attachments")
    private List<String> attachments;

    @SerializedName("crop_var")
    private String crop_var;

    @SerializedName("crop_qty")
    private float crop_qty;

    @SerializedName("crop_uom_id")
    private List<String> crop_uom_id;

    @SerializedName("price_unit")
    private float price_unit;

    @SerializedName("expected_date")
    private String expected_date;
    @SerializedName("available_days")
    private int available_days;
    @SerializedName("id")
    private int id;
    @SerializedName("notes")
    private String notes;
    @SerializedName("status")
    private String status;
    @SerializedName("date_from")
    private String date_from;
    @SerializedName("date_to")
    private String date_to;

    public String getDate_from() {
        return date_from;
    }

    public void setDate_from(String date_from) {
        this.date_from = date_from;
    }

    public String getDate_to() {
        return date_to;
    }

    public void setDate_to(String date_to) {
        this.date_to = date_to;
    }

    public float getPrice_unit() {
        return price_unit;
    }

    public float getCrop_qty() {
        return crop_qty;
    }

    public int getAvailable_days() {
        return available_days;
    }

    public int getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public List<String> getAttachments() {
        return attachments;
    }

    public List<String> getCrop_id() {
        return crop_id;
    }

    public List<String> getCrop_uom_id() {
        return crop_uom_id;
    }

    public String getCrop_var() {
        return crop_var;
    }

    public String getExpected_date() {
        return expected_date;
    }

    public String getNotes() {
        return notes;
    }
}
