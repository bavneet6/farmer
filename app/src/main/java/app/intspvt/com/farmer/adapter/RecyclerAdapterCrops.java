package app.intspvt.com.farmer.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.rest.response.CropsData;
import app.intspvt.com.farmer.rest.response.LandUnits;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.ItemClickListener;
import app.intspvt.com.farmer.utilities.RoundImageView;
import retrofit2.Call;
import retrofit2.Response;

import static app.intspvt.com.farmer.utilities.AppUtils.isNullCase;

/**
 * Created by DELL on 8/23/2017.
 */

public class RecyclerAdapterCrops extends RecyclerView.Adapter<RecyclerAdapterCrops.Crops> {
    View itemView;
    private Context context;
    private DatabaseHandler databaseHandler;
    private ArrayList<CropsData.CropsInfo> cropsInfos;
    private String getPic, selectedLandUnit;
    private HashMap<Long, CropsData.CropsInfo> cropMapData;
    private ItemClickListener itemClickListener;
    private ArrayList<String> landUnitsNames = new ArrayList<>();

    public RecyclerAdapterCrops(Activity activity, ArrayList<CropsData.CropsInfo> cropsInfos, HashMap<Long, CropsData.CropsInfo> cropMapData, ItemClickListener itemClickListener) {
        this.context = activity;
        this.cropsInfos = cropsInfos;
        this.cropMapData = cropMapData;
        this.itemClickListener = itemClickListener;
        databaseHandler = new DatabaseHandler(context);
        getLandUnits();

    }

    @Override
    public RecyclerAdapterCrops.Crops onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_select_crops, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterCrops.Crops(itemView);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final RecyclerAdapterCrops.Crops holder, int position) {
        holder.areaBack.setVisibility(View.INVISIBLE);
        final int pos = holder.getAdapterPosition();
        if (!cropMapData.isEmpty())
            if (cropMapData.containsKey(cropsInfos.get(pos).getID())) {
                if (!cropMapData.get(cropsInfos.get(pos).getID()).isDeleted()) {
                    holder.selected_box.setBackground(context.getResources().getDrawable(R.drawable.categ_box_outline_blue));
                    holder.areaBack.setVisibility(View.VISIBLE);
                    CropsData.CropsInfo cropsInfo = cropMapData.get(cropsInfos.get(pos).getID());
                    holder.cropArea.setText("" + cropsInfo.getCultivated_area());
                    if (!AppUtils.isNullCase(cropsInfo.getLand_unit()))
                        holder.landUnit.setText(cropsInfo.getLand_unit());
                } else {
                    holder.selected_box.setBackground(context.getResources().getDrawable(R.drawable.categ_box));
                    holder.areaBack.setVisibility(View.INVISIBLE);
                }
            } else {
                holder.selected_box.setBackground(context.getResources().getDrawable(R.drawable.categ_box));
                holder.areaBack.setVisibility(View.INVISIBLE);
            }

        holder.cropName.setText(cropsInfos.get(pos).getName());
        if (!AppUtils.isNullCase(cropsInfos.get(pos).getImage()))
            showPicassoImage(pos, holder.cropImage);

        holder.areaBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAreaDialog(pos, holder.cropArea.getText().toString(), holder.landUnit.getText().toString()
                        , holder.cropArea, holder.landUnit);
            }
        });

        holder.selected_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cropMapData.containsKey(cropsInfos.get(pos).getID())) {
                    holder.selected_box.setBackground(context.getResources().getDrawable(R.drawable.categ_box));
                    CropsData.CropsInfo cropsInfo = cropMapData.get(cropsInfos.get(pos).getID());
                    if (cropsInfo != null && cropsInfo.getPartner_crop_rel_record_id() == 0L) {
                        cropMapData.remove(cropsInfos.get(pos).getID());
                        holder.areaBack.setVisibility(View.INVISIBLE);
                    } else {
                        cropsInfo.setCrop_id(cropsInfo.getCrop_id());
                        cropsInfo.setCultivated_area(cropsInfo.getCultivated_area());
                        cropsInfo.setPartner_crop_rel_record_id(cropsInfo.getPartner_crop_rel_record_id());
                        cropsInfo.setLand_unit(cropsInfo.getLand_unit());
                        if (cropsInfo.isDeleted()) {
                            holder.areaBack.setVisibility(View.VISIBLE);
                            holder.selected_box.setBackground(context.getResources().getDrawable(R.drawable.categ_box_outline_blue));
                            cropsInfo.setDeleted(false);
                        } else {
                            holder.areaBack.setVisibility(View.INVISIBLE);
                            cropsInfo.setDeleted(true);
                        }
                        cropMapData.put(cropsInfos.get(pos).getID(), cropsInfo);
                    }
                } else {
                    holder.selected_box.setBackground(context.getResources().getDrawable(R.drawable.categ_box_outline_blue));
                    CropsData.CropsInfo cropsInfo = new CropsData.CropsInfo();
                    cropsInfo.setCrop_id(cropsInfos.get(pos).getID());
                    cropsInfo.setCultivated_area(0f);
                    cropsInfo.setPartner_crop_rel_record_id(0L);
                    cropsInfo.setLand_unit(landUnitsNames.get(0));
                    cropsInfo.setDeleted(false);
                    cropMapData.put(cropsInfos.get(pos).getID(), cropsInfo);
                    holder.areaBack.setVisibility(View.VISIBLE);
                    holder.cropArea.setText("" + cropsInfo.getCultivated_area());
                    holder.landUnit.setText(cropsInfo.getLand_unit());
                }
                itemClickListener.onItemClick(cropMapData);
                updateList(cropMapData);
            }
        });
    }

    private void showAreaDialog(final int pos, final String area, String land, final TextView cropArea, final TextView landUnit) {
        final Dialog dialog = new Dialog(context);
        final EditText value;
        final Spinner unit;
        final ImageView cross;
        TextView saveData;
        dialog.setCanceledOnTouchOutside(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.crop_area_dialog);
        unit = dialog.findViewById(R.id.unit);
        value = dialog.findViewById(R.id.value);
        value.requestFocus();
        value.postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(value, 0);
            }
        }, 50);
        saveData = dialog.findViewById(R.id.saveData);
        cross = dialog.findViewById(R.id.cross);
        saveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropsData.CropsInfo cropsInfo = cropMapData.get(cropsInfos.get(pos).getID());
                cropsInfo.setCrop_id(cropsInfo.getCrop_id());
                if (value.getText().toString().equals(""))
                    cropsInfo.setCultivated_area(0f);
                else
                    cropsInfo.setCultivated_area(Float.parseFloat(value.getText().toString()));
                cropsInfo.setPartner_crop_rel_record_id(cropsInfo.getPartner_crop_rel_record_id());
                cropsInfo.setLand_unit(selectedLandUnit);
                cropsInfo.setDeleted(false);
                cropMapData.put(cropsInfos.get(pos).getID(), cropsInfo);
                itemClickListener.onItemClick(cropMapData);
                updateList(cropMapData);
                cropArea.setText("" + cropsInfo.getCultivated_area());
                landUnit.setText(cropsInfo.getLand_unit());
                dialog.dismiss();
            }
        });

        setLandUnits(land, unit, landUnitsNames);
        if (!AppUtils.isNullCase(area)) {
            value.setText("" + area);
        }

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedLandUnit = landUnitsNames.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Drawable d = new ColorDrawable(context.getResources().getColor(R.color.black));

        d.setAlpha(100);
        dialog.getWindow().setBackgroundDrawable(d);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();

    }

    private void updateList(HashMap<Long, CropsData.CropsInfo> cropMapData) {
        this.cropMapData = cropMapData;
//        notifyDataSetChanged();
    }

    private void getLandUnits() {
        AppRestClient client = AppRestClient.getInstance();
        Call<LandUnits> call = client.getLandUnits();
        call.enqueue(new ApiCallback<LandUnits>() {
            @Override
            public void onResponse(Response<LandUnits> response) {
                if (response.body() == null)
                    return;
                landUnitsNames = response.body().getLandUnits();
            }

            @Override
            public void onResponse401(Response<LandUnits> response) throws
                    JSONException {

            }


        });
    }

    private void setLandUnits(String land, Spinner unit, ArrayList<String> landUnitsNames) {
        try {
            ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(context, R.layout.template_spinner_text, landUnitsNames);
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            unit.setAdapter(stateAdapter);
        } catch (ActivityNotFoundException ac) {
        }
        if (!AppUtils.isNullCase(land))
            unit.setSelection(getLandUnitPosition(land));

    }

    private int getLandUnitPosition(String land) {
        for (int i = 0; i < landUnitsNames.size(); i++)
            if (landUnitsNames.get(i).equals(land))
                return i;
        return 0;
    }

    private void showPicassoImage(int pos, final RoundImageView imageView) {
        AppUtils.generateAndCheckUrl(context, cropsInfos.get(pos).getImage());
        String photo = cropsInfos.get(pos).getImage();
        getPic = null;
        if (!isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(imageView, new PicassoCallback(photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_crop_image).into(imageView);
                    }
                });

            } else {
                imageView.setImageResource(R.drawable.no_crop_image);
            }
        }
    }


    @Override
    public int getItemCount() {
        return cropsInfos.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class Crops extends RecyclerView.ViewHolder {
        TextView cropName, cropArea, landUnit;
        EditText value;
        LinearLayout areaBack, back, selected_box;
        Spinner unit;
        private RoundImageView cropImage;

        public Crops(View itemView) {
            super(itemView);
            back = itemView.findViewById(R.id.back);
            selected_box = itemView.findViewById(R.id.selected_box);
            areaBack = itemView.findViewById(R.id.areaBack);
            cropName = itemView.findViewById(R.id.cropName);
            cropArea = itemView.findViewById(R.id.cropArea);
            landUnit = itemView.findViewById(R.id.landUnit);
            unit = itemView.findViewById(R.id.unit);
            value = itemView.findViewById(R.id.value);
            cropImage = itemView.findViewById(R.id.cropImage);

        }

    }
}
