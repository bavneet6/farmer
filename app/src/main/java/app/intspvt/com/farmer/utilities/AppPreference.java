package app.intspvt.com.farmer.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import app.intspvt.com.farmer.Farmer;

/**
 * Created by DELL on 6/22/2017.
 */

public class AppPreference {

    private static AppPreference mInstance;
    private final String PREF_NAME = "pref";
    private final String MOBILE = "mobile";
    private final String NAME = "name";
    private final String STATE = "state";
    private final String DISTRICT = "distrcit";
    private final String BLOCK = "block";
    private final String PANCHAYAT = "panchayat";
    private final String VILLAGE = "village";
    private final String PINCODE = "pincode";
    private final String GENDER = "gender";
    private final String IMAGE = "image";
    private final String AUTH_TOKEN = "auth_token";
    private final String FCM_TOKEN = "fcm_token";
    private final String ROTATEIMG = "rotate_img";
    private final String TOLL_FREE = "toll_free";
    private final String LATITUDE = "latitude";
    private final String LONGITUDE = "longitude";
    private final String APP_LOGIN = "app_login";
    private final String CROP_IDS = "crop_ids";
    private final String NODE_NAME = "node_name";
    private final String LANGUAGE = "language";
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor mEditor;

    public AppPreference() {
        sharedPref = Farmer.getInstance().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static AppPreference getInstance() {
        if (mInstance == null)
            mInstance = new AppPreference();

        return mInstance;
    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap bitmap_image = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap_image.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        return imageEncoded;
    }

    public void clearData() {
        SharedPreferences.Editor mEditor = sharedPref.edit();
        mEditor.clear();
        mEditor.commit();
    }

    private void saveData(String key, String val) {
        SharedPreferences.Editor mEditor = sharedPref.edit();
        mEditor.putString(key, val);
        mEditor.commit();
    }

    private void saveData(String key, float val) {
        SharedPreferences.Editor mEditor = sharedPref.edit();
        mEditor.putFloat(key, val);
        mEditor.commit();
    }

    private void saveData(String key, int val) {
        SharedPreferences.Editor mEditor = sharedPref.edit();
        mEditor.putInt(key, val);
        mEditor.commit();
    }

    private void saveData(String key, Long val) {
        SharedPreferences.Editor mEditor = sharedPref.edit();
        mEditor.putLong(key, val);
        mEditor.commit();
    }

    private void saveData(String key, boolean val) {
        SharedPreferences.Editor mEditor = sharedPref.edit();
        mEditor.putBoolean(key, val);
        mEditor.commit();
    }


    private String getStringData(String key, String defValue) {
        return sharedPref.getString(key, defValue);
    }

    private Long getLongData(String key, Long defValue) {
        return sharedPref.getLong(key, defValue);
    }

    private int getIntData(String key, int defValue) {
        return sharedPref.getInt(key, defValue);
    }


    private boolean getBooleanData(String key, boolean defValue) {
        return sharedPref.getBoolean(key, defValue);
    }


    private float getFloatData(String key, float defValue) {
        return sharedPref.getFloat(key, defValue);
    }

    public String getAuthToken() {
        return getStringData(AUTH_TOKEN, null);
    }

    public void setAuthToken(String token) {
        saveData(AUTH_TOKEN, token);
    }
    //****************************************************************888

    public String getFCM_TOKEN() {
        return getStringData(FCM_TOKEN, null);
    }

    public void setFCM_TOKEN(String token) {
        saveData(FCM_TOKEN, token);
    }

    public String getMobile() {
        return getStringData(MOBILE, null);
    }

    public void setMobile(String mobile) {
        saveData(MOBILE, mobile);
    }

    public void setRImage(boolean insuranceFlag) {
        saveData(ROTATEIMG, insuranceFlag);
    }

    public String getName() {
        return getStringData(NAME, null);
    }

    public void setName(String name) {
        saveData(NAME, name);
    }

    public Long getPINCODE() {
        return getLongData(PINCODE, 0L);
    }

    public void setPINCODE(Long address) {
        saveData(PINCODE, address);
    }

    public List<String> getSTATE() {
        Gson gson = new Gson();
        String signInJson = getStringData(STATE, null);
        if (signInJson != null) {
            Type type = new TypeToken<List<String>>() {
            }.getType();
            List<String> allCardResponse = gson.fromJson(signInJson, type);
            return allCardResponse;
        }
        return null;
    }

    public void setSTATE(List<String> token) {
        Gson gson = new Gson();
        String signInJson = null;
        if (token != null) {
            signInJson = gson.toJson(token);
        }
        saveData(STATE, signInJson);
    }

    public ArrayList<Long> getCROPIDS() {
        Gson gson = new Gson();
        String signInJson = getStringData(CROP_IDS, null);
        if (signInJson != null) {
            Type type = new TypeToken<List<String>>() {
            }.getType();
            ArrayList<Long> allCardResponse = gson.fromJson(signInJson, type);
            return allCardResponse;
        }
        return null;
    }

    public List<String> getDISTRICT() {
        Gson gson = new Gson();
        String signInJson = getStringData(DISTRICT, null);
        if (signInJson != null) {
            Type type = new TypeToken<List<String>>() {
            }.getType();
            List<String> allCardResponse = gson.fromJson(signInJson, type);
            return allCardResponse;
        }
        return null;
    }

    public void setDISTRICT(List<String> token) {
        Gson gson = new Gson();
        String signInJson = null;
        if (token != null) {
            signInJson = gson.toJson(token);
        }
        saveData(DISTRICT, signInJson);
    }

    public List<String> getBLOCK() {
        Gson gson = new Gson();
        String signInJson = getStringData(BLOCK, null);
        if (signInJson != null) {
            Type type = new TypeToken<List<String>>() {
            }.getType();
            List<String> allCardResponse = gson.fromJson(signInJson, type);
            return allCardResponse;
        }
        return null;
    }

    public void setBLOCK(List<String> token) {
        Gson gson = new Gson();
        String signInJson = null;
        if (token != null) {
            signInJson = gson.toJson(token);
        }
        saveData(BLOCK, signInJson);
    }

    public List<String> getPANCHAYAT() {
        Gson gson = new Gson();
        String signInJson = getStringData(PANCHAYAT, null);
        if (signInJson != null) {
            Type type = new TypeToken<List<String>>() {
            }.getType();
            List<String> allCardResponse = gson.fromJson(signInJson, type);
            return allCardResponse;
        }
        return null;
    }

    public void setPANCHAYAT(List<String> token) {
        Gson gson = new Gson();
        String signInJson = null;
        if (token != null) {
            signInJson = gson.toJson(token);
        }
        saveData(PANCHAYAT, signInJson);
    }

    public List<String> getVILLAGE() {
        Gson gson = new Gson();
        String signInJson = getStringData(VILLAGE, null);
        if (signInJson != null) {
            Type type = new TypeToken<List<String>>() {
            }.getType();
            List<String> allCardResponse = gson.fromJson(signInJson, type);
            return allCardResponse;
        }
        return null;
    }

    public void setVILLAGE(List<String> token) {
        Gson gson = new Gson();
        String signInJson = null;
        if (token != null) {
            signInJson = gson.toJson(token);
        }
        saveData(VILLAGE, signInJson);
    }

    public void setCropsIDS(ArrayList<Long> token) {
        Gson gson = new Gson();
        String signInJson = null;
        if (token != null) {
            signInJson = gson.toJson(token);
        }
        saveData(CROP_IDS, signInJson);
    }

    public String getGender() {
        return getStringData(GENDER, null);
    }

    public void setGender(String gender) {
        saveData(GENDER, gender);
    }

    public String getTOLL_FREE() {
        return getStringData(TOLL_FREE, null);
    }

    public void setTOLL_FREE(String mobile) {
        saveData(TOLL_FREE, mobile);
    }

    public String getLATITUDE() {
        return getStringData(LATITUDE, null);
    }

    public void setLATITUDE(String mobile) {
        saveData(LATITUDE, mobile);
    }

    public String getLONGITUDE() {
        return getStringData(LONGITUDE, null);
    }

    public void setLONGITUDE(String mobile) {
        saveData(LONGITUDE, mobile);
    }

    public String getIMAGE() {
        return getStringData(IMAGE, null);
    }

    public void setIMAGE(String mobile) {
        saveData(IMAGE, mobile);
    }

    public String getNODE_NAME() {
        return getStringData(NODE_NAME, null);
    }

    public void setNODE_NAME(String node_name) {
        saveData(NODE_NAME, node_name);
    }

    public String getLANGUAGE() {
        return getStringData(LANGUAGE, null);
    }

    public void setLANGUAGE(String node_name) {
        saveData(LANGUAGE, node_name);
    }


    public boolean getAPP_LOGIN() {
        return getBooleanData(APP_LOGIN, false);
    }

    public void setAPP_LOGIN(boolean b) {
        saveData(APP_LOGIN, b);
    }

}


