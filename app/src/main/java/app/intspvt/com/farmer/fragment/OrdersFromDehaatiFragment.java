package app.intspvt.com.farmer.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.adapter.RecyclerAdapterOrderHistory;
import app.intspvt.com.farmer.rest.response.OrderDataHistory;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.viewmodel.DehaatiOrderHistoryViewModel;


public class OrdersFromDehaatiFragment extends BaseFragment {
    private TextView no_data_msg;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerAdapterOrderHistory recyclerAdapterOrderHistory;

    private List<OrderDataHistory> orderHistoryList = new ArrayList<>();
    private DehaatiOrderHistoryViewModel orderHistoryViewModel;

    private FirebaseAnalytics mFirebaseAnalytics;

    public OrdersFromDehaatiFragment() {
        // Required empty public constructor
    }

    public static OrdersFromDehaatiFragment newInstance() {
        return new OrdersFromDehaatiFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_orders_from_dehaati, null);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        orderHistoryViewModel = ViewModelProviders.of(
                getActivity()).get(DehaatiOrderHistoryViewModel.class);
        orderHistoryViewModel.init();
        AppUtils.showProgressDialog(getActivity());
        recyclerView = v.findViewById(R.id.order_history_recycler_view);
        no_data_msg = v.findViewById(R.id.no_data_msg);
        observeViewModel(orderHistoryViewModel);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());

        return v;
    }

    private void observeViewModel(DehaatiOrderHistoryViewModel viewModel) {
        viewModel.getHistoryData().observe(this, orderDataHistories -> {
            AppUtils.hideProgressDialog();
            if (orderDataHistories != null && orderDataHistories.size() > 0) {
                orderHistoryList.clear();
                no_data_msg.setVisibility(View.GONE);
                orderHistoryList.addAll(orderDataHistories);
                printData();
            } else {
                no_data_msg.setVisibility(View.VISIBLE);
            }
        });
    }

    private void printData() {
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerAdapterOrderHistory = new RecyclerAdapterOrderHistory(
                getActivity(), orderHistoryList, "dehaati");
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerAdapterOrderHistory);
    }
}
