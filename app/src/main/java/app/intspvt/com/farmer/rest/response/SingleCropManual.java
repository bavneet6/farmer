package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

public class SingleCropManual {
    @SerializedName("data")
    private CropManualData cropManual;

    public CropManualData getCropManual() {
        return cropManual;
    }
}
