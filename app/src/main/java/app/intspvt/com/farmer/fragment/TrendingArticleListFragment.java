package app.intspvt.com.farmer.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterTrendingArticleList;
import app.intspvt.com.farmer.rest.response.TrendingData;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.viewmodel.TrendingArticleViewModel;

public class TrendingArticleListFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = TrendingArticleListFragment.class.getSimpleName();
    private ImageView back;
    private FirebaseAnalytics firebaseAnalytics;
    private RecyclerView trending_articles_list;
    private TrendingArticleViewModel trendingArticleViewModel;

    public static TrendingArticleListFragment newInstance() {
        return new TrendingArticleListFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_trending_article_list, null);
        MainActivity.showCallAndHome(true);
        back = v.findViewById(R.id.back);
        trending_articles_list = v.findViewById(R.id.trending_articles_list);
        trending_articles_list.setNestedScrollingEnabled(false);
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        back.setOnClickListener(this);
        AppUtils.showProgressDialog(getActivity());
        trendingArticleViewModel = ViewModelProviders.of(getActivity()).get(TrendingArticleViewModel.class);
        trendingArticleViewModel.init();
        observeViewModel(trendingArticleViewModel);
        return v;
    }

    private void observeViewModel(TrendingArticleViewModel viewModel) {
        viewModel.getTrendingData()
                .observe(this, trendingData -> {
                    AppUtils.hideProgressDialog();
                    if (trendingData != null && trendingData.size() > 0) {
                        displayTrendingArticle(trendingData);
                    }
                });
    }

    private void displayTrendingArticle(ArrayList<TrendingData> datas) {
        if (getActivity() != null && isAdded()) {
            trending_articles_list.setAdapter(new RecyclerAdapterTrendingArticleList(getActivity(), datas));
            trending_articles_list.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("TrendingArticleList", Long.parseLong(AppPreference.getInstance().getMobile()));
        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        firebaseAnalytics.logEvent("TrendingArticleList", params);
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
        }
    }
}
