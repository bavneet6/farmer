package app.intspvt.com.farmer.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.response.WeatherCropData;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;

public class RecyclerAdapterWeatherCropData extends RecyclerView.Adapter<RecyclerAdapterWeatherCropData.WeatherCrop> {
    private ArrayList<WeatherCropData.CropData.CropAdvisory> weatherCropData;
    private Context context;
    private String getPic;


    public RecyclerAdapterWeatherCropData(Activity activity, ArrayList<WeatherCropData.CropData.CropAdvisory> weatherCropData) {
        this.context = activity;
        this.weatherCropData = weatherCropData;
    }

    @Override
    public RecyclerAdapterWeatherCropData.WeatherCrop onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_weather_crop_data, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterWeatherCropData.WeatherCrop(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterWeatherCropData.WeatherCrop holder, final int position) {
        final int pos = holder.getAdapterPosition();
        if (!AppUtils.isNullCase(weatherCropData.get(pos).getCrop_name()))
            holder.cropName.setText(weatherCropData.get(pos).getCrop_name());
        if (!AppUtils.isNullCase(weatherCropData.get(pos).getStage()))
            holder.cropStage.setText(":" + weatherCropData.get(pos).getStage());
        Map<String, String> extraHeaders;
        extraHeaders = new HashMap<String, String>();
        extraHeaders.put("Authorization", "Bearer " + AppPreference.getInstance().getAuthToken());
        WebSettings settings = holder.cropContent.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        holder.cropContent.loadDataWithBaseURL(null, weatherCropData.get(pos).getAdvisory_content(), "text/html", "UTF-8", null);

    }


    @Override
    public int getItemCount() {
        return weatherCropData.size();
    }

    public class WeatherCrop extends RecyclerView.ViewHolder {
        TextView cropName, cropStage;
        WebView cropContent;

        public WeatherCrop(View itemView) {
            super(itemView);
            cropName = itemView.findViewById(R.id.cropName);
            cropStage = itemView.findViewById(R.id.cropStage);
            cropContent = itemView.findViewById(R.id.cropContent);

        }


    }

}

