package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Promotions {

    @SerializedName("data")
    private List<PromotionsData> promotionsData;

    public List<PromotionsData> getPromotionsData() {
        return promotionsData;
    }

    public static class PromotionsData {
        @SerializedName("id")
        private int id;
        @SerializedName("min_order_amount")
        private float min_order_amount;
        @SerializedName("total_discount")
        private float total_discount;
        @SerializedName("name")
        private String name;
        @SerializedName("image")
        private String promo_image;

        public float getMin_order_amount() {
            return min_order_amount;
        }

        public float getTotal_discount() {
            return total_discount;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getPromo_image() {
            return promo_image;
        }
    }
}
