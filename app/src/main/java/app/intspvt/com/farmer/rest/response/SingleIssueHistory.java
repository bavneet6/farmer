package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 3/28/2018.
 */

public class SingleIssueHistory {
    @SerializedName("data")
    private EnquiryDataHistory data;
    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private String status;

    public EnquiryDataHistory getData() {
        return data;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }


}
