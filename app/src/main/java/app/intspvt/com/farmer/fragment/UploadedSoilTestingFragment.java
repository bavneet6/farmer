package app.intspvt.com.farmer.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterSoilImages;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.SoilTestingReport;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.PermissionCheck;
import app.intspvt.com.farmer.viewmodel.SoilTestingImagesViewModel;
import retrofit2.Call;
import retrofit2.Response;

import static app.intspvt.com.farmer.utilities.UrlConstant.REQUEST_CAMERA;
import static app.intspvt.com.farmer.utilities.UrlConstant.SELECT_FILE;

public class UploadedSoilTestingFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = RaiseReqSoilTestingFragment.class.getSimpleName();
    private RecyclerView raiseReqHistoryList;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Dialog imageDialog;
    private TextView upload;
    private Uri imageUri;
    private ArrayList<String> fileList = new ArrayList<>();
    private SoilTestingImagesViewModel soilTestingImagesViewModel;


    public static RaiseReqSoilTestingFragment newInstance() {
        return new RaiseReqSoilTestingFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_uploaded_soil_testing, null);
        MainActivity.showCallAndHome(true);
        raiseReqHistoryList = v.findViewById(R.id.raiseReqHistoryList);
        raiseReqHistoryList.setNestedScrollingEnabled(false);
        upload = v.findViewById(R.id.upload);
        raiseReqHistoryList.setNestedScrollingEnabled(false);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        upload.setOnClickListener(this);
        AppUtils.showProgressDialog(getActivity());
        soilTestingImagesViewModel = ViewModelProviders.of(getActivity()).get(SoilTestingImagesViewModel.class);
        soilTestingImagesViewModel.init();
        observeViewModel(soilTestingImagesViewModel);

        return v;
    }

    private void observeViewModel(SoilTestingImagesViewModel viewModel) {
        viewModel.getSoilImagesList()
                .observe(this, soilImages -> {
                    AppUtils.hideProgressDialog();
                    if (soilImages != null && soilImages.size() > 0) {
                        displayData(soilImages);
                    }
                });
    }


    private void displayData(ArrayList<SoilTestingReport.Data> imagesList) {
        if (getActivity() != null && isAdded()) {
            raiseReqHistoryList.setAdapter(new RecyclerAdapterSoilImages(getActivity(), imagesList));
            raiseReqHistoryList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void openCameraDialog() {
        if (PermissionCheck.checkCameraPermission(getActivity())) {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "Attachment");
            values.put(MediaStore.Images.Media.DESCRIPTION, "Photo taken on " + System.currentTimeMillis());
            imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, REQUEST_CAMERA);
        } else {
            requestPermission(REQUEST_CAMERA);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestPermission(int requestCode) {
        if (requestCode == REQUEST_CAMERA)
            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK)
            if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);

    }

    private void onCaptureImageResult(Intent data) {
        AppUtils.showProgressDialog(getActivity());
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        int options = 50;
        while (bytes.toByteArray().length / 1024 > 300) {  //Loop if compressed picture is greater than 400kb, than to compression
            bytes.reset();//Reset baos is empty baos
            bitmap.compress(Bitmap.CompressFormat.JPEG, options, bytes);//The compression options%, storing the compressed data to the baos
            options -= 10;//Every time reduced by 10
        }

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
//
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        AppUtils.hideProgressDialog();
        fileList.add("" + destination);
        openConfirmationDialog(bitmap);
    }

    private void openConfirmationDialog(Bitmap destination) {
        imageDialog = new Dialog(getActivity());
        final ImageView imageView;
        final EditText text;
        TextView save;
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog.setContentView(R.layout.dialog_disease_detection);
        imageDialog.setCancelable(true);
        imageDialog.setCanceledOnTouchOutside(true);
        imageView = imageDialog.findViewById(R.id.image);
        save = imageDialog.findViewById(R.id.save);
        text = imageDialog.findViewById(R.id.disease_text);
        text.setVisibility(View.GONE);
        imageView.setImageBitmap(destination);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadSoilTest();
            }
        });

        Drawable d = new ColorDrawable(getActivity().getResources().getColor(R.color.black));
        d.setAlpha(100);
        imageDialog.getWindow().setBackgroundDrawable(d);
        imageDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        imageDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imageDialog.getWindow().setGravity(Gravity.CENTER);
        imageDialog.show();
    }

    private void uploadSoilTest() {
        AppUtils.showProgressDialog(getActivity());
        Call<Void> call;
        AppRestClient client = AppRestClient.getInstance();
        call = client.postSoilTesting("File", fileList);
        call.enqueue(new ApiCallback<Void>() {
            @Override
            public void onResponse(Response<Void> response) {

                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.information);
                    builder.setMessage(R.string.info_receievd);
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            imageDialog.dismiss();
                            soilTestingImagesViewModel.init();
                            observeViewModel(soilTestingImagesViewModel);
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                Bundle params = new Bundle();
                params.putLong("UploadSoilTesting", Long.parseLong(AppPreference.getInstance().getMobile()));
                mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
                mFirebaseAnalytics.logEvent("UploadSoilTesting", params);

            }

            @Override
            public void onResponse401(Response<Void> response) throws JSONException {

            }


        });

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.upload:
                if (PermissionCheck.checkCameraPermission(getActivity())) {
                    openCameraDialog();
                } else {
                    requestPermission(SELECT_FILE);
                }
                break;
        }
    }
}
