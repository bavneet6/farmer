package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by DELL on 10/26/2017.
 */

public class Variants implements Serializable {
    @SerializedName("price")
    private float price;

    @SerializedName("name")
    private String vname;
    @SerializedName("id")
    private int id;

    public float getPrice() {
        return price;
    }

    public int getVId() {
        return id;
    }

    public String getVName() {
        return vname;
    }
}
