package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by DELL on 8/29/2017.
 */

public class FarmerInfo {
    @SerializedName("image_small")
    private String image_small;
    @SerializedName("name")
    private String name;
    @SerializedName("state_id")
    private List<String> state;

    @SerializedName("district_id")
    private List<String> district;

    @SerializedName("block_id")
    private List<String> block;

    @SerializedName("panchayat_id")
    private List<String> panchayat;

    @SerializedName("village_id")
    private List<String> village;

    @SerializedName("title")
    private int title;
    @SerializedName("zip")
    private Long zip;


    public int getTitle() {
        return title;
    }

    public Long getZip() {
        return zip;
    }

    public String getImage_small() {
        return image_small;
    }

    public String getName() {
        return name;
    }

    public List<String> getState() {
        return state;
    }

    public List<String> getDistrict() {
        return district;
    }

    public List<String> getBlock() {
        return block;
    }

    public List<String> getPanchayat() {
        return panchayat;
    }

    public List<String> getVillage() {
        return village;
    }
}
