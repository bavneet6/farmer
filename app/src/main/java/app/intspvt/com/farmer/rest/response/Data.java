package app.intspvt.com.farmer.rest.response;


import java.util.ArrayList;

public class Data {
    private String order_date;

    private ArrayList<String> product_id;

    private String product_qty;

    private String date_to;

    private String id;

    private String date_from;

    private ArrayList<String> product_uom;


    private String state;

    private String unit_rent;

    private ArrayList<String> warehouse_id;


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUnit_rent() {
        return unit_rent;
    }

    public void setUnit_rent(String unit_rent) {
        this.unit_rent = unit_rent;
    }

    public ArrayList<String> getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(ArrayList<String> warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public ArrayList<String> getProduct_id() {
        return product_id;
    }

    public void setProduct_id(ArrayList<String> product_id) {
        this.product_id = product_id;
    }

    public String getProduct_qty() {
        return product_qty;
    }

    public void setProduct_qty(String product_qty) {
        this.product_qty = product_qty;
    }

    public String getDate_to() {
        return date_to;
    }

    public void setDate_to(String date_to) {
        this.date_to = date_to;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate_from() {
        return date_from;
    }

    public void setDate_from(String date_from) {
        this.date_from = date_from;
    }

    public ArrayList<String> getProduct_uom() {
        return product_uom;
    }

    public void setProduct_uom(ArrayList<String> product_uom) {
        this.product_uom = product_uom;
    }

    @Override
    public String toString() {
        return "ClassPojo [order_date = " + order_date + ", product_id = " + product_id + ", product_qty = " + product_qty + ", date_to = " + date_to + ", id = " + id + ", date_from = " + date_from + ", product_uom = " + product_uom + "]";
    }
}

