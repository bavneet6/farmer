package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WeatherParams {

    @SerializedName("data")
    private Data data;

    public Data getData() {
        return data;
    }

    public class Data {
        @SerializedName("district")
        private String district;
        @SerializedName("parameters")
        private ArrayList<WeatherData> weatherData;

        public String getDistrict() {
            return district;
        }

        public ArrayList<WeatherData> getWeatherData() {
            return weatherData;
        }


        public class WeatherData {
            @SerializedName("date")
            private String date;
            @SerializedName("rainfall")
            private int rainfall;
            @SerializedName("min_temp")
            private int min_temp;
            @SerializedName("max_temp")
            private int max_temp;
            @SerializedName("min_humidity")
            private int min_humidity;
            @SerializedName("max_humidity")
            private int max_humidity;
            @SerializedName("wind_speed")
            private int wind_speed;
            @SerializedName("wind_direction")
            private String wind_direction;

            public int getMax_humidity() {
                return max_humidity;
            }

            public int getMax_temp() {
                return max_temp;
            }

            public int getMin_humidity() {
                return min_humidity;
            }

            public int getMin_temp() {
                return min_temp;
            }

            public int getRainfall() {
                return rainfall;
            }

            public int getWind_speed() {
                return wind_speed;
            }

            public String getWind_direction() {
                return wind_direction;
            }

            public String getDate() {
                return date;
            }
        }
    }
}
