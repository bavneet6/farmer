package app.intspvt.com.farmer.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;

import java.util.List;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;

/**
 * Created by DELL on 7/3/2017.
 */

public class NumberFragment extends BaseFragment implements TextWatcher {
    public static final String TAG = NumberFragment.class.getSimpleName();
    private Button next;
    private EditText mobNum;
    private String number;
    private Dialog dialog;

    public static NumberFragment newInstance() {
        return new NumberFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_number, null);
        next = v.findViewById(R.id.next);
        next.setClickable(false);
        mobNum = v.findViewById(R.id.mobileNo);

        mobNum.addTextChangedListener(this);
        if (!AppUtils.haveNetworkConnection(getActivity())) {
            openDialog();
        }
        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                if (mobNum.getText().toString().length() != 10) {
                    mobNum.requestFocus();
                } else {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }
            }
        };
        handler.postDelayed(r, 1);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = mobNum.getText().toString();
                if (number.length() != 10) {
                    mobNum.setError(Html.fromHtml(getString(R.string.enter_correct_number)));
                    mobNum.requestFocus();
                } else {
                    FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    transaction.replace(R.id.frag_cont, OTPFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                    AppPreference.getInstance().setMobile(number);
                }
            }
        });

        return v;
    }

    private View.OnClickListener getNumberInputClickListener(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() != R.id.other_number_choice){
                    String mobileNumber = ((TextView) view).getText().toString();
                    if(mobileNumber.length()>10) {
                        mobileNumber = mobileNumber.substring(mobileNumber.length() - 10);
                    }
                    mobNum.setText(mobileNumber);
                    dialog.hide();
                } else{
                    dialog.hide();
                    mobNum.requestFocus();
                }
            }
        };
    }

    /**
     * internet connection dialog
     */
    private void openDialog() {
        final Dialog dialog = new Dialog(getActivity());
        final LinearLayout linearLayout;
        dialog.setCanceledOnTouchOutside(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_internet);
        linearLayout = dialog.findViewById(R.id.ok);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        int width = getResources().getDisplayMetrics().widthPixels;
        dialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }



    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onResume() {
        super.onResume();
        AppUtils.getUpdateApp(getActivity());

        if(ContextCompat.checkSelfPermission(
                getContext(),
                Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                SubscriptionManager subManager = SubscriptionManager.from(getContext());
                List<SubscriptionInfo> subPlans = subManager.getActiveSubscriptionInfoList();
                if(subPlans != null) {
                    dialog = new Dialog(getContext());
                    dialog.setCancelable(true);
                    View dialogView = dialog.getLayoutInflater().inflate(
                            R.layout.dialog_number_selection, null);
                    dialog.setContentView(dialogView);
                    TextView firstNumber =  dialogView.findViewById(R.id.mobile_option_one);
                    TextView secondNumber = dialogView.findViewById(R.id.mobile_option_two);

                    TextView[] mobileNumbers = new TextView[]{firstNumber, secondNumber};
                    int mobIndex = 0;
                    for(SubscriptionInfo subInfo : subPlans){
                        if(subInfo.getNumber() != null
                                && subInfo.getNumber().length() >= 10
                                && mobIndex < 2 ) {
                            mobileNumbers[mobIndex].setVisibility(View.VISIBLE);
                            mobileNumbers[mobIndex].setOnClickListener(getNumberInputClickListener());
                            mobileNumbers[mobIndex].setText(subInfo.getNumber());
                            mobIndex++;
                        }
                    }

                    dialogView.findViewById(
                            R.id.other_number_choice).setOnClickListener(getNumberInputClickListener());
                    if(mobIndex>0) {
                        dialog.show();
                    }
                }
            } else {
                TelephonyManager tMgr = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
                String mPhoneNumber = tMgr.getLine1Number();
                if(mPhoneNumber != null && mPhoneNumber.length() >= 10) {
                    if(mPhoneNumber.length() > 10){
                        mPhoneNumber = mPhoneNumber.substring(mPhoneNumber.length() -10 );
                    }
                    mobNum.setText(mPhoneNumber);
                }
            }
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (mobNum.getText().length() == 10) {
            next.setBackgroundColor(getActivity().getResources().getColor(R.color.baseColor));
            next.setClickable(true);
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        } else {
            next.setBackgroundColor(getActivity().getResources().getColor(R.color.grey));
        }
    }
}
