package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

public class TrendingData {
    @SerializedName("content")
    public String content;

    @SerializedName("image")
    public String image;
    @SerializedName("name")
    public String name;
    @SerializedName("id")
    public Long id;

    public Long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }
}