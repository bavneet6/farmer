package app.intspvt.com.farmer.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterOrderLines;
import app.intspvt.com.farmer.rest.response.OrderDataHistory;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.viewmodel.OrderLinesViewModel;

/**
 * Created by DELL on 10/5/2017.
 */

public class OrderLinesFragment extends BaseFragment {
    public static final String TAG = OrderLinesFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private TextView orderStatus, orderId, orderDate, totalItem, viewAll, amtUntaxed, discount,
            orderPersonName, tax, price_after_discount;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Long order_id;
    private ImageView back;
    private LinearLayout amount_back, orderFromPerson;
    private String forFrgament, phone_number;
    private OrderLinesViewModel orderLinesViewModel;

    public static OrderLinesFragment newInstance() {
        return new OrderLinesFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order_lines, null);
        MainActivity.showCallAndHome(true);
        amount_back = v.findViewById(R.id.amount_back);
        recyclerView = v.findViewById(R.id.order_rec1);
        recyclerView.setNestedScrollingEnabled(false);
        orderStatus = v.findViewById(R.id.orderStatus);
        orderDate = v.findViewById(R.id.orderDate);
        orderId = v.findViewById(R.id.orderId);
        totalItem = v.findViewById(R.id.totalItem);
        viewAll = v.findViewById(R.id.viewAll);
        amtUntaxed = v.findViewById(R.id.amtUntaxed);
        discount = v.findViewById(R.id.discount);
        tax = v.findViewById(R.id.tax);
        back = v.findViewById(R.id.back);
        orderFromPerson = v.findViewById(R.id.orderFromPerson);
        orderPersonName = v.findViewById(R.id.orderPersonName);

        price_after_discount = v.findViewById(R.id.price_after_discount);
        Bundle bundle = getArguments();
        if (bundle != null) {
            order_id = bundle.getLong("ORDER_ID");
            forFrgament = bundle.getString("KEY");
        }
        if (forFrgament.equals("dehaati")) {
            orderFromPerson.setVisibility(View.VISIBLE);
            orderStatus.setVisibility(View.GONE);
            amount_back.setVisibility(View.GONE);
        } else {
            orderStatus.setVisibility(View.VISIBLE);
            orderFromPerson.setVisibility(View.GONE);
            amount_back.setVisibility(View.VISIBLE);
        }
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        AppUtils.showProgressDialog(getActivity());
        orderLinesViewModel = ViewModelProviders.of(getActivity()).get(OrderLinesViewModel.class);
        orderLinesViewModel.init(order_id, forFrgament);
        observeViewModel(orderLinesViewModel);

        orderFromPerson.setOnClickListener(click -> {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(
                            Uri.parse("tel:" + phone_number));
                    getActivity().startActivity(intent);
                }
        );


        return v;
    }

    private void observeViewModel(OrderLinesViewModel viewModel) {
        viewModel.getOrderLines()
                .observe(this, orderLines -> {
                    AppUtils.hideProgressDialog();
                    if (orderLines != null) {
                        displayOrderLinesData(orderLines);
                    }
                });
    }

    private void displayOrderLinesData(OrderDataHistory orderDataHistory) {
        if (getActivity() != null && isAdded()) {
            recyclerView.setAdapter(new RecyclerAdapterOrderLines(getActivity(), orderDataHistory.getOrder_lines()));
            layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);

            if (forFrgament.equals("dehaati")) {
                orderPersonName.setText(orderDataHistory.getSeller_name());
                phone_number = orderDataHistory.getSeller_number();
            } else
                orderStatus.setText(orderDataHistory.getState());

            // printing order details of a particular order
            orderId.setText(getString(R.string.order_no) + ": " + orderDataHistory.getName());
            amtUntaxed.setText(getString(R.string.rs) + " " + orderDataHistory.getAmount_untaxed());
            tax.setText(getString(R.string.rs) + " " + orderDataHistory.getAmount_tax());
            discount.setText("-" + getString(R.string.rs) + " " + orderDataHistory.getDiscount());
            price_after_discount.setText(getString(R.string.rs) + " " + orderDataHistory.getAmount_total());
            totalItem.setText(getString(R.string.total_item) + ": " + orderDataHistory.getOrder_lines().size());
            orderDate.setText(orderDataHistory.getDate_order());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("SingleOrderHistory", Long.parseLong(AppPreference.getInstance().getMobile()));
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        mFirebaseAnalytics.logEvent("SingleOrderHistory", params);
    }

    @Override
    public void onPause() {
        super.onPause();

    }
}
