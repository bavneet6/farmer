package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 3/27/2018.
 */

public class IssueData {
    @SerializedName("description")
    private String description;

    @SerializedName("issue_id")
    private Long issue_id;
    @SerializedName("kanban_state")
    private String kanban_state;
    @SerializedName("category")
    private String category;

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getIssue_id() {
        return issue_id;
    }

    public void setIssue_id(Long issue_id) {
        this.issue_id = issue_id;
    }

    public String getKanban_state() {
        return kanban_state;
    }

    public void setKanban_state(String kanban_state) {
        this.kanban_state = kanban_state;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
