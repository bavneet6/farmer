package app.intspvt.com.farmer.rest.body;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 11/30/2017.
 */

public class Enquiry {
    @SerializedName("data")
    private String audio;

    public Enquiry(String audio) {
        this.audio = audio;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }
}
