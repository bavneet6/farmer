package app.intspvt.com.farmer.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterDiseaseDetection;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.body.Issue;
import app.intspvt.com.farmer.rest.response.IssueData;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.UrlConstant;
import app.intspvt.com.farmer.viewmodel.EnquiryViewModel;
import retrofit2.Call;
import retrofit2.Response;

public class DiseaseDetectionFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = DiseaseDetectionFragment.class.getSimpleName();
    ArrayList<String> returnValue = new ArrayList<>();
    private ImageView back;
    private TextView createNew, no_data_mssg;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ArrayList<File> imageFileList1 = new ArrayList<>();
    private Dialog imageDialog;
    private RecyclerView disease_list;
    private EnquiryViewModel enquiryViewModel;

    public static DiseaseDetectionFragment newInstance() {
        return new DiseaseDetectionFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_disease_detection, null);
        MainActivity.showCallAndHome(true);
        back = v.findViewById(R.id.back);
        createNew = v.findViewById(R.id.createNew);
        no_data_mssg = v.findViewById(R.id.no_data_mssg);
        disease_list = v.findViewById(R.id.disease_list);
        disease_list.setNestedScrollingEnabled(false);
        back.setOnClickListener(this);
        createNew.setOnClickListener(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        AppUtils.showProgressDialog(getActivity());
        enquiryViewModel = ViewModelProviders.of(getActivity()).get(EnquiryViewModel.class);
        enquiryViewModel.init(UrlConstant.DISEASE_DETECTION);
        observeViewModel(enquiryViewModel);

        return v;
    }

    private void observeViewModel(EnquiryViewModel viewModel) {
        viewModel.getEnquiryData()
                .observe(this, enquiryData -> {
                    AppUtils.hideProgressDialog();
                    if (enquiryData != null && enquiryData.size() > 0) {
                        no_data_mssg.setVisibility(View.GONE);
                        disease_list.setAdapter(new RecyclerAdapterDiseaseDetection(getActivity(),
                                enquiryData));
                        disease_list.setLayoutManager(new LinearLayoutManager(getActivity()));
                    } else
                        no_data_mssg.setVisibility(View.VISIBLE);

                });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.createNew:
                AppUtils.imageSelection(this, 1);
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("val", "requestCode ->  " + requestCode + "  resultCode " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (100): {
                if (resultCode == Activity.RESULT_OK) {
                    returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                    storeImagesInList(returnValue);
                }
            }
            break;
        }
    }

    private void storeImagesInList(ArrayList<String> returnValue) {
        imageFileList1.clear();
        for (int i = 0; i < returnValue.size(); i++) {
            File file = new File(returnValue.get(i));
            imageFileList1.add(file);
        }
        Bitmap bitmap = new BitmapDrawable(getActivity().getResources(), imageFileList1.get(0).getAbsolutePath()).getBitmap();
        openConfirmationDialog(bitmap);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AppUtils.imageSelection(this, 1);
                } else {
                }
                return;
            }
        }

    }

    private void openConfirmationDialog(Bitmap destination) {
        imageDialog = new Dialog(getActivity());
        imageDialog.setCanceledOnTouchOutside(true);
        imageDialog.setCancelable(true);
        final ImageView imageView;
        final EditText text;
        TextView save;
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog.setContentView(R.layout.dialog_disease_detection);
        imageView = imageDialog.findViewById(R.id.image);
        save = imageDialog.findViewById(R.id.save);
        text = imageDialog.findViewById(R.id.disease_text);
        text.setSelection(text.getText().length());
        imageView.setImageBitmap(destination);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData(text.getText().toString());
            }
        });

        Drawable d = new ColorDrawable(getActivity().getResources().getColor(R.color.black));
        d.setAlpha(100);
        imageDialog.getWindow().setBackgroundDrawable(d);
        imageDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        imageDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imageDialog.getWindow().setGravity(Gravity.CENTER);
        imageDialog.show();
    }

    private void sendData(String text) {
        IssueData issueData = new IssueData();
        if (AppUtils.isNullCase(text)) {
            issueData.setDescription(null);
        } else {
            issueData.setDescription(text);
        }
        issueData.setCategory(UrlConstant.DISEASE_DETECTION);
        Issue issue = new Issue(issueData);
        AppUtils.showProgressDialog(activity);
        Call<Void> call;
        AppRestClient client = AppRestClient.getInstance();
        call = client.sendEnquiryData(issue, null, imageFileList1);
        call.enqueue(new ApiCallback<Void>() {
            @Override
            public void onResponse(Response<Void> response) {

                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.information);
                    builder.setMessage(R.string.info_receievd);
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            imageDialog.dismiss();
                            dialog.dismiss();
                            enquiryViewModel.init(UrlConstant.DISEASE_DETECTION);
                            observeViewModel(enquiryViewModel);
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                Bundle params = new Bundle();
                params.putLong("RaiseDiseaseDetection", Long.parseLong(AppPreference.getInstance().getMobile()));
                mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
                mFirebaseAnalytics.logEvent("RaiseDiseaseDetection", params);

            }

            @Override
            public void onResponse401(Response<Void> response) throws JSONException {

            }


        });
    }

}
