package app.intspvt.com.farmer.rest.body;

import java.util.List;

import app.intspvt.com.farmer.rest.response.CropsData;

/**
 * Created by DELL on 9/15/2017.
 */

public class SendCropData {
    private List<CropsData.CropsInfo> data;

    public SendCropData(List<CropsData.CropsInfo> data) {
        this.data = data;
    }

    public List<CropsData.CropsInfo> getData() {
        return data;
    }

    public void setData(List<CropsData.CropsInfo> data) {
        this.data = data;
    }
}
