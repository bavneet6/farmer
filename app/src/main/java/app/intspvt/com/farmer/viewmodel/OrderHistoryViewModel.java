package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import app.intspvt.com.farmer.repository.OrderHistoryRepository;
import app.intspvt.com.farmer.rest.response.OrderDataHistory;

public class OrderHistoryViewModel extends ViewModel {
    private MutableLiveData<List<OrderDataHistory>> orderList;
    private OrderHistoryRepository orderHistoryRepository;

    public void init() {
        if (orderHistoryRepository == null)
            orderHistoryRepository = OrderHistoryRepository.getInstance();
        orderList = orderHistoryRepository.getOrderData();
    }

    public LiveData<List<OrderDataHistory>> getHistoryData() {
        return orderList;
    }
}
