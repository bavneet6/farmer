package app.intspvt.com.farmer.rest;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.rest.body.Enquiry;
import app.intspvt.com.farmer.rest.body.Issue;
import app.intspvt.com.farmer.rest.body.OutputData;
import app.intspvt.com.farmer.rest.body.PostFarmerInfo;
import app.intspvt.com.farmer.rest.body.PostFarmerPersonalInfo;
import app.intspvt.com.farmer.rest.body.SendCartData;
import app.intspvt.com.farmer.rest.body.SendCropData;
import app.intspvt.com.farmer.rest.body.SendNumber;
import app.intspvt.com.farmer.rest.response.CropManuals;
import app.intspvt.com.farmer.rest.response.CropsData;
import app.intspvt.com.farmer.rest.response.EnquiryHistory;
import app.intspvt.com.farmer.rest.response.FcmToken;
import app.intspvt.com.farmer.rest.response.GetIdNames;
import app.intspvt.com.farmer.rest.response.GetOrderResponse;
import app.intspvt.com.farmer.rest.response.GetPersonalInfo;
import app.intspvt.com.farmer.rest.response.IssueData;
import app.intspvt.com.farmer.rest.response.LandUnits;
import app.intspvt.com.farmer.rest.response.LoginResponse;
import app.intspvt.com.farmer.rest.response.MandiCropPrice;
import app.intspvt.com.farmer.rest.response.MandiPrices;
import app.intspvt.com.farmer.rest.response.OrderHistory;
import app.intspvt.com.farmer.rest.response.OutputSale;
import app.intspvt.com.farmer.rest.response.ProductsList;
import app.intspvt.com.farmer.rest.response.Promotions;
import app.intspvt.com.farmer.rest.response.SingleCropManual;
import app.intspvt.com.farmer.rest.response.SingleIssueHistory;
import app.intspvt.com.farmer.rest.response.SingleOrderHistory;
import app.intspvt.com.farmer.rest.response.SingleOutputSale;
import app.intspvt.com.farmer.rest.response.SingleProduct;
import app.intspvt.com.farmer.rest.response.SingleTrendArticle;
import app.intspvt.com.farmer.rest.response.SoilTestingReport;
import app.intspvt.com.farmer.rest.response.Trending;
import app.intspvt.com.farmer.rest.response.VersionName;
import app.intspvt.com.farmer.rest.response.WeatherCropData;
import app.intspvt.com.farmer.rest.response.WeatherParams;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.UrlConstant;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by DELL on 6/22/2017.
 */

public class AppRestClient {

    private static AppRestClient mInstance;
    int cacheSize = 10 * 1024 * 1024; // 10 MB
    Cache cache = new Cache(Farmer.getInstance().getCacheDir(), cacheSize);
    private AppRestClientService appRestClientService;

    private AppRestClient() {
        setRestClient();
    }

    public static AppRestClient getInstance() {
        if (mInstance == null) {
            synchronized (AppRestClient.class) {
                if (mInstance == null)
                    mInstance = new AppRestClient();
            }
        }
        return mInstance;
    }

    private void setRestClient() {
        Gson gson = new GsonBuilder()
//                .setExclusionStrategies(new SurveyExclusionStrategy())
//                .serializeNulls()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .cache(cache)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .addInterceptor(httpLoggingInterceptor) //TODO: debug
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        String authToken = AppPreference.getInstance().getAuthToken();
                        if (TextUtils.isEmpty(authToken))
                            authToken = "";
                        Request request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("Authorization", "Bearer " + authToken)
                                .method(original.method(), original.body())
                                .build();

                        if (Farmer.getInstance().isConnectedToNetwork()) {
                            request = request.newBuilder().header("Cache-Control", "public, max-age=" + 60).build();
                            okhttp3.Response response = chain.proceed(request);
                            Log.e("response", new Gson().toJson(response));
                            return response;
                        } else {
                            request = request.newBuilder()
                                    .header("Cache-Control", "public, , max-stale=" + 60 * 60 * 24 * 7).build();
                            okhttp3.Response response = chain.proceed(request);
                            Log.e("response", new Gson().toJson(response));

                            return response;

                        }


                    }
                })
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlConstant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();

        appRestClientService = retrofit.create(AppRestClientService.class);
    }

    /*
    New api
     */

    public Call<Void> sendNumber(SendNumber body) {
        return appRestClientService.sendNumber(body);
    }

    public Call<LoginResponse> verifyNumber(SendNumber body) {
        return appRestClientService.verifyNumber(body);
    }

    public Call<PostFarmerPersonalInfo> updateFarmerInfo(File imageList, PostFarmerInfo.PostInfo postFarmerPersonalInfo) {
        MultipartBody.Part part = null;
        List<MultipartBody.Part> parts = new ArrayList();
        if (imageList != null) {
            part = MultipartBody.Part.createFormData("image" + 0, "image" + 0 + ".jpeg", RequestBody.create(MediaType.parse("image/*"), imageList));
            parts.add(part);
        }
        Gson gson = new Gson();
        HashMap<String, RequestBody> mList = new HashMap<>();
        String entity_type = "";
        entity_type = gson.toJson(postFarmerPersonalInfo);
        RequestBody entity = RequestBody.create(MediaType.parse
                ("application/json"), entity_type);
        mList.put("data", entity);
        return appRestClientService.updateFarmerInfo(parts, mList);
    }

    public Call<PostFarmerPersonalInfo> updateLandInfo(PostFarmerPersonalInfo postFarmerPersonalInfo) {
        return appRestClientService.updateLandInfo(postFarmerPersonalInfo);
    }

    public Call<GetPersonalInfo> getFarmerInfo() {
        return appRestClientService.getFarmerInfo();
    }

    public Call<GetPersonalInfo> getLandInfo() {
        return appRestClientService.getLandInfo();
    }

    public Call<LandUnits> getLandUnits() {
        return appRestClientService.getLandUnits();
    }

    public Call<CropsData> getCropList() {
        return appRestClientService.getCropList();
    }

    public Call<CropsData> getFarmerCropList() {
        return appRestClientService.getFarmerCropList();
    }

    public Call<MandiPrices> getCropMandiData(Long id) {
        return appRestClientService.getCropMandiData(id);
    }

    public Call<MandiCropPrice> getMandiPricesData(Long id) {
        return appRestClientService.getMandiPricesData(id);
    }

    public Call<MandiCropPrice> getNodePricesData(Long id) {
        return appRestClientService.getNodePricesData(id);
    }

    public Call<CropsData> getOutputCropsList() {
        return appRestClientService.getOutputCropsList();
    }

    public Call<GetIdNames> getUOMList() {
        return appRestClientService.getUOMList();
    }

    public Call<Void> getUserLoginInfo() {
        return appRestClientService.getUserLoginInfo();
    }

    public Call<ProductsList> productList(String product_ids) {
        return appRestClientService.productList(product_ids);
    }

    public Call<OrderHistory> orderHistory() {
        return appRestClientService.orderHistory();
    }

    public Call<OrderHistory> dehaatiOrderHistory() {
        return appRestClientService.dehaatiOrderHistory();
    }

    public Call<Trending> trendingData() {
        return appRestClientService.trendingData();
    }

    public Call<GetOrderResponse> sendCartData(SendCartData sendCartData) {
        return appRestClientService.sendCartData(sendCartData);
    }

    public Call<PostFarmerPersonalInfo> postInfo(File imageList, PostFarmerInfo.PostInfo postFarmerPersonalInfo) {
        MultipartBody.Part part = null;
        List<MultipartBody.Part> parts = new ArrayList();
        if (imageList != null) {
            part = MultipartBody.Part.createFormData("image" + 0, "image" + 0 + ".jpeg", RequestBody.create(MediaType.parse("image/*"), imageList));
            parts.add(part);
        }
        Gson gson = new Gson();
        HashMap<String, RequestBody> mList = new HashMap<>();
        String entity_type = "";
        entity_type = gson.toJson(postFarmerPersonalInfo);
        RequestBody entity = RequestBody.create(MediaType.parse
                ("application/json"), entity_type);
        mList.put("data", entity);
        return appRestClientService.postInfo(parts, mList);
    }

    public Call<Void> createFarmerOutputSale(List<File> imageList, OutputData outputData) {
        MultipartBody.Part part = null;
        List<MultipartBody.Part> parts = new ArrayList();
        for (int i = 0; i < imageList.size(); i++) {
            part = MultipartBody.Part.createFormData("image" + i, "image" + i + ".jpeg", RequestBody.create(MediaType.parse("image/*"), imageList.get(i)));
            parts.add(part);
        }
        Gson gson = new Gson();
        HashMap<String, RequestBody> mList = new HashMap<>();
        String entity_type = "";
        entity_type = gson.toJson(outputData);
        RequestBody entity = RequestBody.create(MediaType.parse
                ("application/json"), entity_type);
        mList.put("data", entity);
        return appRestClientService.createFarmerOutputSale(parts, mList);
    }

    public Call<Void> updateFarmerOutputSale(List<File> imageList, OutputData outputData) {
        MultipartBody.Part part = null;
        List<MultipartBody.Part> parts = new ArrayList();
        if (imageList != null)
            for (int i = 0; i < imageList.size(); i++) {
                part = MultipartBody.Part.createFormData("image" + i, "image" + i + ".jpeg", RequestBody.create(MediaType.parse("image/*"), imageList.get(i)));
                parts.add(part);
            }
        Gson gson = new Gson();
        HashMap<String, RequestBody> mList = new HashMap<>();
        String entity_type = "";
        entity_type = gson.toJson(outputData);
        RequestBody entity = RequestBody.create(MediaType.parse
                ("application/json"), entity_type);
        mList.put("data", entity);
        return appRestClientService.updateFarmerOutputSale(parts, mList);
    }


    public Call<CropsData> sendCropData(SendCropData sendCropData) {
        return appRestClientService.sendCropData(sendCropData);
    }

    public Call<Void> sendEnquiryData(Issue issue, Enquiry enquiry, List<File> stringList) {
        RequestBody audio;
        MultipartBody.Part audioPath = null;
        MultipartBody.Part part = null;
        List<MultipartBody.Part> parts = new ArrayList();
        if (stringList != null)
            for (int i = 0; i < stringList.size(); i++) {
                part = MultipartBody.Part.createFormData("image" + i, "image" + i + ".jpeg", RequestBody.create(MediaType.parse("image/*"), stringList.get(i)));
                parts.add(part);
            }
        if (enquiry != null)
            if (enquiry.getAudio() != null) {
                audio = RequestBody.create(MediaType.parse("audio/*"), new File(Uri.parse(enquiry.getAudio()).getPath()));
                audioPath = MultipartBody.Part.createFormData("audio", "audio.3gp", audio);
            }
        IssueData issueData = issue.getData();
        Gson gson = new Gson();
        HashMap<String, RequestBody> mList = new HashMap<>();
        String arrayJson = "";
        arrayJson = gson.toJson(issueData);
        RequestBody des = RequestBody.create(MediaType.parse("application/json"), arrayJson);
        mList.put("data", des);
        return appRestClientService.sendEnquiryData(mList, audioPath, parts);
    }

    public Call<Void> postSoilTesting(String data, List<String> stringList) {
        MultipartBody.Part part = null;
        List<MultipartBody.Part> parts = new ArrayList();
        if (stringList != null)
            for (int i = 0; i < stringList.size(); i++) {
                File file = new File(stringList.get(i));
                part = MultipartBody.Part.createFormData("image" + i, "image" + i + ".jpeg", RequestBody.create(MediaType.parse("image/*"), file));
                parts.add(part);
            }

        HashMap<String, RequestBody> mList = new HashMap<>();
        RequestBody des = RequestBody.create(MediaType.parse("application/json"), data);
        mList.put("data", des);
        return appRestClientService.postSoilTesting(parts, mList);
    }

    public Call<Void> sendReply(Issue issue, Enquiry enquiry, List<File> stringList) {
        RequestBody audio;
        MultipartBody.Part audioPath = null;
        MultipartBody.Part part = null;
        List<MultipartBody.Part> parts = new ArrayList();
        if (stringList != null)
            for (int i = 0; i < stringList.size(); i++) {
                part = MultipartBody.Part.createFormData("image" + i, "image" + i + ".jpeg", RequestBody.create(MediaType.parse("image/*"), stringList.get(i)));
                parts.add(part);
            }
        if (enquiry != null)
            if (enquiry.getAudio() != null) {
                audio = RequestBody.create(MediaType.parse("audio/*"), new File(Uri.parse(enquiry.getAudio()).getPath()));
                audioPath = MultipartBody.Part.createFormData("audio", "audio.3gp", audio);
            }
        IssueData issueData = issue.getData();
        Gson gson = new Gson();
        HashMap<String, RequestBody> mList = new HashMap<>();
        String arrayJson = "";
        arrayJson = gson.toJson(issueData);
        RequestBody des = RequestBody.create(MediaType.parse("application/json"), arrayJson);
        mList.put("data", des);
        return appRestClientService.sendReply(mList, audioPath, parts);
    }


    public Call<EnquiryHistory> getEnquiryHistory(String type) {
        return appRestClientService.getEnquiryHistory(type);
    }

    public Call<SingleIssueHistory> getSingleHistoryData(Long id) {
        return appRestClientService.getSingleHistoryData(id);
    }

    public Call<GetIdNames> getStateRegister() {
        return appRestClientService.getStateRegister();
    }

    public Call<GetIdNames> getDistrictRegister(int id) {
        return appRestClientService.getDistrictRegister(id);
    }

    public Call<GetIdNames> getBlockRegister(int id) {
        return appRestClientService.getBlockRegister(id);
    }

    public Call<GetIdNames> getPanchayatRegister(int id) {
        return appRestClientService.getPanchayatRegister(id);
    }

    public Call<GetIdNames> getVillageRegister(int id) {
        return appRestClientService.getVillageRegister(id);
    }

    public Call<OutputSale> getOutputSaleHistory() {
        return appRestClientService.getOutputSaleHistory();
    }


    public Call<SingleOutputSale> getSingleOutputSaleHistory(Long id) {
        return appRestClientService.getSingleOutputSaleHistory(id);
    }

    public Call<Promotions> getPromotions() {
        return appRestClientService.getPromotions();
    }

    public Call<CropManuals> getCropManuals(Long id, String manualIds) {
        return appRestClientService.getCropManuals(id, manualIds);
    }

    public Call<SingleCropManual> getSingleCropManual(Long id) {
        return appRestClientService.getSingleCropManual(id);
    }

    public Call<WeatherParams> getWeatherParams() {
        return appRestClientService.getWeatherParams();
    }

    public Call<WeatherCropData> getWeatherCropData() {
        return appRestClientService.getWeatherCropData();
    }

    public Call<SingleProduct> getSingleProduct(Long id) {
        return appRestClientService.getSingleProduct(id);
    }

    public Call<SingleOrderHistory> getSingleOrder(Long id) {
        return appRestClientService.getSingleOrder(id);
    }

    public Call<SingleOrderHistory> getSingleDeHaatOrder(Long id) {
        return appRestClientService.getSingleDeHaatOrder(id);
    }

    public Call<CropManuals> getCropManualList() {
        return appRestClientService.getCropManualList();
    }

    public Call<VersionName> getVersionName() {
        return appRestClientService.getVersionName();
    }

    public Call<Void> fcmToken(FcmToken fcmToken) {
        return appRestClientService.fcmToken(fcmToken);
    }

    public Call<SoilTestingReport> getSoilTesting() {
        return appRestClientService.getSoilTesting();
    }

    public Call<SingleTrendArticle> geSingleTrendArticle(Long id) {
        return appRestClientService.geSingleTrendArticle(id);
    }

}

