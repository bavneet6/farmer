package app.intspvt.com.farmer.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.FragmentTransaction;

import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;

import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.body.PostFarmerInfo;
import app.intspvt.com.farmer.rest.body.PostFarmerPersonalInfo;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.RoundImageView;
import app.intspvt.com.farmer.utilities.UrlConstant;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by DELL on 7/4/2017.
 */

public class PicGenderFragment extends BaseFragment implements View.OnClickListener, TextWatcher {
    public static final String TAG = PicGenderFragment.class.getSimpleName();
    ArrayList<String> returnValue = new ArrayList<>();
    private TextView number;
    private Button male, female, next;
    private String gender = UrlConstant.MALE;
    private int gen;
    private EditText name;
    private RoundImageView pic;
    private ArrayList<Long> idsList = new ArrayList<>();
    private String imageUrl = null;
    private ArrayList<File> imageFileList1 = new ArrayList<>();

    public static PicGenderFragment newInstance() {
        return new PicGenderFragment();
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pic_gender, null);
        next = v.findViewById(R.id.next);
        male = v.findViewById(R.id.male);
        female = v.findViewById(R.id.female);
        number = v.findViewById(R.id.number);
        name = v.findViewById(R.id.name);
        pic = v.findViewById(R.id.pic);
        next.setOnClickListener(this);
        male.setOnClickListener(this);
        female.setOnClickListener(this);
        pic.setOnClickListener(this);
        name.addTextChangedListener(this);
        number.setText(getString(R.string.mobile_number) + ": " + AppPreference.getInstance().getMobile());

        if (!AppUtils.isNullCase(AppPreference.getInstance().getName()))
            name.setText(AppPreference.getInstance().getName());

        if (!AppUtils.isNullCase(AppPreference.getInstance().getIMAGE())) {
            byte[] decodedString = Base64.decode(AppPreference.getInstance().getIMAGE(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            pic.setImageBitmap(decodedByte);
        }

        if (!AppUtils.isNullCase(AppPreference.getInstance().getGender())) {
            if (AppPreference.getInstance().getGender().equals(UrlConstant.MALE)) {
                male.setBackgroundResource(R.drawable.blue_back_box);
                female.setBackgroundResource(R.drawable.gender_back_unselect);
            } else {
                female.setBackgroundResource(R.drawable.blue_back_box);
                male.setBackgroundResource(R.drawable.gender_back_unselect);
            }
        }
        Bundle bundle = getArguments();
        if (bundle != null)
            idsList = (ArrayList<Long>) bundle.getSerializable("CROP_IDS");

        return v;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.next:
                AppPreference.getInstance().setGender(gender);
                if (name.getText().toString().equals(""))
                    AppPreference.getInstance().setName("");
                else
                    AppPreference.getInstance().setName(name.getText().toString());

                postInfo();
                break;
            case R.id.male:
                male.setBackgroundResource(R.drawable.blue_back_box);
                female.setBackgroundResource(R.drawable.gender_back_unselect);
                gender = UrlConstant.MALE;
                AppPreference.getInstance().setGender(gender);
                break;
            case R.id.female:
                female.setBackgroundResource(R.drawable.blue_back_box);
                male.setBackgroundResource(R.drawable.gender_back_unselect);
                gender = UrlConstant.FEMALE;
                AppPreference.getInstance().setGender(gender);
                break;
            case R.id.pic:
                AppUtils.imageSelection(this, 1);
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    /**
     * information of a new user is post to the server that contains name, address if location is on , gender , pic
     * 6 for male
     * 7 for female
     */
    private void postInfo() {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        if (AppPreference.getInstance().getGender().equals(UrlConstant.MALE)) {
            gen = 6;
        } else {
            gen = 7;
        }
        PostFarmerInfo.PostInfo.FarmerInfo farmerInfo = new PostFarmerInfo.PostInfo.FarmerInfo(name.getText().toString(),
                gen, AppPreference.getInstance().getPINCODE());
        PostFarmerInfo.PostInfo getInfo = new PostFarmerInfo.PostInfo(farmerInfo, null, idsList);
        Call<PostFarmerPersonalInfo> call = null;
        if (imageFileList1.size() != 0)
            call = client.postInfo(imageFileList1.get(0), getInfo);
        else
            call = client.postInfo(null, getInfo);
        call.enqueue(new ApiCallback<PostFarmerPersonalInfo>() {
            @Override
            public void onResponse(Response<PostFarmerPersonalInfo> response) {
                AppUtils.hideProgressDialog();
                AppPreference.getInstance().setAPP_LOGIN(true);
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction.replace(R.id.frag_cont, HomeIntroFragment.newInstance()).commitAllowingStateLoss();
            }

            @Override
            public void onResponse401(Response<PostFarmerPersonalInfo> response) throws JSONException {

            }


        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("val", "requestCode ->  " + requestCode + "  resultCode " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (100): {
                if (resultCode == Activity.RESULT_OK) {
                    returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                    storeImagesInList(returnValue);
                }
            }
            break;
        }
    }

    private void storeImagesInList(ArrayList<String> returnValue) {
        imageFileList1.clear();
        for (int i = 0; i < returnValue.size(); i++) {
            File file = new File(returnValue.get(i));
            imageFileList1.add(file);
        }
        Bitmap bitmap = new BitmapDrawable(getActivity().getResources(), imageFileList1.get(0).getAbsolutePath()).getBitmap();
        pic.setImageBitmap(bitmap);
        imageUrl = AppPreference.encodeTobase64(bitmap);
        AppPreference.getInstance().setIMAGE(imageUrl);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AppUtils.imageSelection(this, 1);
                } else {
                }
                return;
            }
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (name.getText().toString().equals(""))
            AppPreference.getInstance().setName("");
        else
            AppPreference.getInstance().setName(name.getText().toString());
    }
}