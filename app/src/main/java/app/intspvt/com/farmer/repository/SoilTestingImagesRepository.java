package app.intspvt.com.farmer.repository;

import androidx.lifecycle.MutableLiveData;

import java.net.ConnectException;
import java.util.ArrayList;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.SoilTestingReport;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SoilTestingImagesRepository {
    private static SoilTestingImagesRepository mInstance;
    private ArrayList<SoilTestingReport.Data> soilImagesList = new ArrayList<>();

    public static SoilTestingImagesRepository getInstance() {
        if (mInstance == null)
            mInstance = new SoilTestingImagesRepository();
        return mInstance;
    }

    public MutableLiveData<ArrayList<SoilTestingReport.Data>> getSoilImagesList() {
        MutableLiveData<ArrayList<SoilTestingReport.Data>> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<SoilTestingReport> call = client.getSoilTesting();
        call.enqueue(new Callback<SoilTestingReport>() {
            @Override
            public void onResponse(Call<SoilTestingReport> call, Response<SoilTestingReport> response) {
                if (response.body() == null)
                    return;
                soilImagesList = new ArrayList<>();
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        soilImagesList = response.body().getData();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }
                data.setValue(soilImagesList);

            }

            @Override
            public void onFailure(Call<SoilTestingReport> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Farmer.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Farmer.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(new ArrayList<>());
            }

        });
        return data;
    }
}
