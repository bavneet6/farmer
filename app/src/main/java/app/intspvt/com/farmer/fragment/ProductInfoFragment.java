package app.intspvt.com.farmer.fragment;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.rest.response.AddToCart;
import app.intspvt.com.farmer.rest.response.Products;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.viewmodel.SingleProductViewModel;

import static app.intspvt.com.farmer.utilities.AppUtils.isNullCase;

/**
 * Created by DELL on 11/16/2017.
 */

public class ProductInfoFragment extends BaseFragment implements View.OnClickListener, TextWatcher, AdapterView.OnItemSelectedListener {
    public static final String TAG = ProductInfoFragment.class.getSimpleName();
    private ImageView productImg, plus, minus, back;
    private TextView productName, pro_mrp, about, add_to_cart, cart_count;
    private EditText pro_qty;
    private RelativeLayout cart_back;
    private int productQty, variantSelectedPos;
    private LinearLayout variantBack;
    private DatabaseHandler databaseHandler;
    private Spinner variants;
    private ArrayList<Products.Variants> variantList;
    private ArrayList<String> variantsNames = new ArrayList<>();
    private String getPic = null;
    private Long productId = 0L;
    private ProgressBar progressBar;
    private FirebaseAnalytics firebaseAnalytics;
    private Products productsData;
    private SingleProductViewModel singleProductViewModel;

    public static ProductInfoFragment newInstance() {
        return new ProductInfoFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_product_details, null);
        databaseHandler = new DatabaseHandler(getActivity());
        MainActivity.showCallAndHome(true);
        variantBack = v.findViewById(R.id.variantBack);
        cart_back = v.findViewById(R.id.cart_back);
        productImg = v.findViewById(R.id.productImg);
        plus = v.findViewById(R.id.plus);
        minus = v.findViewById(R.id.minus);
        productName = v.findViewById(R.id.productName);
        pro_mrp = v.findViewById(R.id.pro_mrp);
        cart_count = v.findViewById(R.id.cart_count);
        about = v.findViewById(R.id.aboutProduct);
        add_to_cart = v.findViewById(R.id.add_to_cart);
        pro_qty = v.findViewById(R.id.pro_qty);
        variants = v.findViewById(R.id.variants);
        back = v.findViewById(R.id.back);
        progressBar = v.findViewById(R.id.progress_bar);

        variants.setOnItemSelectedListener(this);
        cart_back.setOnClickListener(this);
        back.setOnClickListener(this);
        plus.setOnClickListener(this);
        minus.setOnClickListener(this);
        add_to_cart.setOnClickListener(this);
        pro_qty.addTextChangedListener(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            productId = bundle.getLong("PRODUCT_ID");
        }
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        showCartCount();
        AppUtils.showProgressDialog(getActivity());
        singleProductViewModel = ViewModelProviders.of(getActivity()).get(SingleProductViewModel.class);
        singleProductViewModel.init(productId);
        observeViewModel(singleProductViewModel);

        return v;
    }

    private void observeViewModel(SingleProductViewModel viewModel) {
        viewModel.getSingleProduct()
                .observe(this, productData -> {
                    AppUtils.hideProgressDialog();
                    if (productData != null) {
                        productsData = productData;
                        displayData(productsData);
                    }
                });
    }

    private void displayData(Products products) {
        if (AppUtils.isNullCase(products.getProductHindiName()))
            productName.setText(products.getProductName());
        else
            productName.setText(products.getProductHindiName());

        if (!AppUtils.isNullCase(products.getImage()))
            showPicassoImage(products.getImage());

        //display Variants
        variantList = new ArrayList<>();
        variantsNames = new ArrayList<>();
        if (products.getVariants() != null && products.getVariants().size() != 0)
            variantList = products.getVariants();
        for (int i = 0; i < variantList.size(); i++) {
            if (!AppUtils.isNullCase(variantList.get(i).getAttribute()))
                variantsNames.add(variantList.get(i).getAttribute());
        }
        if (variantsNames.size() != 0) {
            variantBack.setVisibility(View.VISIBLE);
            try {
                ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(getContext(), R.layout.template_spinner_text, variantsNames);
                stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                variants.setAdapter(stateAdapter);
            } catch (ActivityNotFoundException ac) {
            }
        } else {
            variantBack.setVisibility(View.INVISIBLE);
            pro_mrp.setText(getString(R.string.rs) + " " + products.getVariants().get(0).getFixed_price());
        }
        if (!AppUtils.isNullCase(products.getDescription()))
            about.setText(products.getDescription());

    }

    private void showPicassoImage(String image) {
        AppUtils.generateAndCheckUrl(getContext(), image);
        getPic = null;
        if (!isNullCase(image)) {
            getPic = databaseHandler.getFileUrl(image);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(getActivity().getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(productImg, new PicassoCallback(image) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_product_image).into(productImg);
                    }
                });

            } else {
                productImg.setImageResource(R.drawable.no_product_image);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("SingleProductPage", Long.parseLong(AppPreference.getInstance().getMobile()));
        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        firebaseAnalytics.logEvent("SingleProductPage", params);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.plus:
                if (pro_qty.getText().toString().equals("")) {
                    pro_qty.setText("1");
                } else {
                    productQty = Integer.parseInt(pro_qty.getText().toString());
                    ++productQty;
                    pro_qty.setText("" + productQty);
                }
                break;
            case R.id.minus:
                if (pro_qty.getText().toString().equals("")) {
                    pro_qty.setText("0");
                } else {
                    productQty = Integer.parseInt(pro_qty.getText().toString());
                    if (productQty == 0) {
                    } else {
                        --productQty;
                    }
                    pro_qty.setText("" + productQty);
                }
                break;
            case R.id.buy:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, CartFragment.newInstance()).commitAllowingStateLoss();
                break;
            case R.id.cart_back:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, CartFragment.newInstance()).commitAllowingStateLoss();
                break;
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.add_to_cart:
                if (AppUtils.haveNetworkConnection(getActivity())) {
                    if (AppUtils.isNullCase(pro_qty.getText().toString())) {
                        AppUtils.showToast(getString(R.string.enter_qty));
                    } else {
                        progressBar.setVisibility(View.VISIBLE);
                        AddToCart contact = new AddToCart();
                        contact.setProductId("" + productsData.getVariants().get(variantSelectedPos).getId());
                        contact.setProductName(productName.getText().toString());
                        contact.setProductMrp(Float.valueOf(productsData.getVariants().get(variantSelectedPos).getFixed_price()));
                        contact.setProductQty(Integer.parseInt(pro_qty.getText().toString()));
                        float a = Integer.parseInt(pro_qty.getText().toString()) * Float.valueOf(productsData.getVariants().get(variantSelectedPos).getFixed_price());
                        contact.setTotalPrice(a);
                        contact.setProductImage(productsData.getImage());
                        contact.setVariant(productsData.getVariants().get(variantSelectedPos).getAttribute());
                        databaseHandler.insertCartData(true, contact);
                        showCartCount();
                    }
                } else
                    AppUtils.showToast(getString(R.string.no_internet));

                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (AppUtils.isNullCase(pro_qty.getText().toString()))
            add_to_cart.setBackgroundColor(getActivity().getResources().getColor(R.color.grey));
        else
            add_to_cart.setBackgroundColor(getActivity().getResources().getColor(R.color.baseColor));
    }

    public void onPause() {
        super.onPause();
        ((InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(pro_qty.getWindowToken(), 0);


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        variantSelectedPos = position;
        pro_mrp.setText(getString(R.string.rs) + " " + productsData.getVariants().get(position).getFixed_price());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void showCartCount() {
        AppUtils.showCartCount(getActivity(), cart_count, cart_back);

        if (progressBar.getVisibility() == View.VISIBLE) {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AppUtils.showToast(getString(R.string.added_to_cart));
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                }
            };
            thread.start();
        }
    }
}
