package app.intspvt.com.farmer.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterPromotions;
import app.intspvt.com.farmer.adapter.TrendingRecyclerAdapter;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.FcmToken;
import app.intspvt.com.farmer.rest.response.Promotions;
import app.intspvt.com.farmer.rest.response.Trending;
import app.intspvt.com.farmer.rest.response.TrendingData;
import app.intspvt.com.farmer.rest.response.WeatherParams;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.CirclePagerIndicatorDecoration;
import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment1 extends BaseFragment implements View.OnClickListener {
    public static final String TAG = HomeFragment1.class.getSimpleName();
    private static FeaturesFragment fragment;
    private ImageView menu;
    private LinearLayout helpline, buy_input, sell_produce, weather_card, cropManualBack, trend_back, promo_back, disease_back, soilTestingBack;
    private RecyclerView promotionsList, trending_articles;
    private FirebaseAnalytics firebaseAnalytics;
    private TextView maxTemp, minTemp, rainText, windDir, minHum, maxHum, district, all_articles, suggestion;
    private LinearLayoutManager linearLayoutManager;
    private GifImageView rainImage, moreFeatures;
    private Timer timer;
    private TimerTask timerTask;

    public static HomeFragment1 newInstance() {
        return new HomeFragment1();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home1, null);
        MainActivity.showCallAndHome(false);
        menu = v.findViewById(R.id.menu);
        moreFeatures = v.findViewById(R.id.moreFeatures);
        helpline = v.findViewById(R.id.helpline);
        buy_input = v.findViewById(R.id.buy_input);
        sell_produce = v.findViewById(R.id.sell_produce);
        weather_card = v.findViewById(R.id.weather_card);
        trend_back = v.findViewById(R.id.trend_back);
        promo_back = v.findViewById(R.id.promo_back);
        cropManualBack = v.findViewById(R.id.cropManualBack);
        soilTestingBack = v.findViewById(R.id.soilTestingBack);
        disease_back = v.findViewById(R.id.disease_back);
        promotionsList = v.findViewById(R.id.promotions);
        trending_articles = v.findViewById(R.id.trending_articles);

        maxTemp = v.findViewById(R.id.maxTemp);
        minTemp = v.findViewById(R.id.minTemp);
        minHum = v.findViewById(R.id.minHum);
        maxHum = v.findViewById(R.id.maxHum);
        rainText = v.findViewById(R.id.rainText);
        windDir = v.findViewById(R.id.windDir);
        district = v.findViewById(R.id.district);
        all_articles = v.findViewById(R.id.all_articles);
        suggestion = v.findViewById(R.id.suggestion);
        rainImage = v.findViewById(R.id.rainImage);

        trending_articles.setNestedScrollingEnabled(false);
        promotionsList.setNestedScrollingEnabled(false);
        menu.setOnClickListener(this);
        helpline.setOnClickListener(this);
        buy_input.setOnClickListener(this);
        weather_card.setOnClickListener(this);
        menu.setOnClickListener(this);
        sell_produce.setOnClickListener(this);
        cropManualBack.setOnClickListener(this);
        disease_back.setOnClickListener(this);
        soilTestingBack.setOnClickListener(this);
        moreFeatures.setOnClickListener(this);
        all_articles.setOnClickListener(this);
        suggestion.setOnClickListener(this);
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        sendFCMToken();
        getTrendingArticles();
        getWeatherParams();
        getPromotions();
        return v;
    }

    private void getPromotions() {
        AppRestClient client = AppRestClient.getInstance();
        Call<Promotions> call = client.getPromotions();
        call.enqueue(new ApiCallback<Promotions>() {
            @Override
            public void onResponse(Response<Promotions> response) {
                if (getActivity() != null && isAdded()) {
                    if (response.body() == null)
                        return;
                    if (response.body().getPromotionsData() == null)
                        return;
                    promo_back.setVisibility(View.VISIBLE);
                    displayPromotionsData(response.body().getPromotionsData());
                }
            }

            @Override
            public void onResponse401(Response<Promotions> response) throws
                    JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }


        });

    }

    private void displayPromotionsData(final List<Promotions.PromotionsData> promotionsData) {
        if (getActivity() != null && isAdded()) {
            promotionsList.addItemDecoration(new CirclePagerIndicatorDecoration());
            SnapHelper snapHelper = new PagerSnapHelper();
            snapHelper.attachToRecyclerView(promotionsList);
            promotionsList.setAdapter(new RecyclerAdapterPromotions(getActivity(), promotionsData));
            linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            promotionsList.setLayoutManager(linearLayoutManager);


            timerTask = new TimerTask() {
                @Override
                public void run() {
                    promotionsList.post(() -> {
                        try {
                            if (linearLayoutManager.findLastVisibleItemPosition() + 1 == promotionsData.size())
                                promotionsList.getLayoutManager().scrollToPosition(0);
                            else
                                promotionsList.getLayoutManager().
                                        scrollToPosition(linearLayoutManager.findLastVisibleItemPosition() + 1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                }
            };
            timer = new Timer();
            timer.schedule(timerTask, 5000, 5000);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getView() == null) {
            return;
        }
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                getActivity().finish();
                return false;
            }
        });
        Bundle params = new Bundle();
        params.putLong("HomePage", Long.parseLong(AppPreference.getInstance().getMobile()));
        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        firebaseAnalytics.logEvent("HomePage", params);
    }

    private void sendFCMToken() {
        final String token = FirebaseInstanceId.getInstance().getToken();
        if (token == null)
            return;
        if (!token.equals(AppPreference.getInstance().getFCM_TOKEN())) {
            FcmToken.Token token1 = new FcmToken.Token();
            token1.setFcm_token(token);
            AppRestClient client = AppRestClient.getInstance();
            FcmToken fcmToken = new FcmToken(token1);
            Call<Void> call = client.fcmToken(fcmToken);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.code() == 200) {
                        AppPreference.getInstance().setFCM_TOKEN(token);
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {

                }

            });

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (timerTask != null)
            timerTask.cancel();
    }


    @Override
    public void onClick(View view) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        int id = view.getId();
        switch (id) {
            case R.id.menu:
                transaction.replace(R.id.frag_container, EditFarmerInfoFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.helpline:
                transaction.replace(R.id.frag_container, EnquiryHistoryFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.sell_produce:
                transaction.replace(R.id.frag_container, OutputSalesQueryHistoryFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.buy_input:
                transaction.replace(R.id.frag_container, ProductListFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.cropManualBack:
                transaction.replace(R.id.frag_container, CropManualListFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.weather_card:
                transaction.replace(R.id.frag_container, WeatherAdvisoryFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.all_articles:
                transaction.replace(R.id.frag_container, TrendingArticleListFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.soilTestingBack:
                transaction.replace(R.id.frag_container, SoilTestingFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.disease_back:
                transaction.replace(R.id.frag_container, DiseaseDetectionFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.suggestion:
                transaction.replace(R.id.frag_container, SuggestionFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.moreFeatures:
                fragment = FeaturesFragment.newInstance();
                fragment.show(getActivity().getSupportFragmentManager(), "");
                break;
        }
    }

    public void dismissSheet() {
        fragment.dismiss();
    }

    private void getTrendingArticles() {
        AppRestClient client = AppRestClient.getInstance();
        Call<Trending> call = client.trendingData();
        call.enqueue(new ApiCallback<Trending>() {
            @Override
            public void onResponse(final Response<Trending> response) {
                if (getActivity() != null && isAdded()) {
                    if (response.body().getDatas() == null)
                        return;
                    if (getActivity() == null)
                        return;
                    trend_back.setVisibility(View.VISIBLE);
                    displayTrendingArticle(response.body().getDatas());
                }
            }

            @Override
            public void onResponse401(Response<Trending> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());

            }

        });

    }

    private void displayTrendingArticle(ArrayList<TrendingData> datas) {
        if (getActivity() != null && isAdded()) {
            trending_articles.addItemDecoration(new CirclePagerIndicatorDecoration());
            SnapHelper snapHelper = new PagerSnapHelper();
            trending_articles.setOnFlingListener(null);
            snapHelper.attachToRecyclerView(trending_articles);
            trending_articles.setAdapter(new TrendingRecyclerAdapter(getActivity(), datas));
            trending_articles.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        }
    }


    private void getWeatherParams() {
        AppRestClient client = AppRestClient.getInstance();
        Call<WeatherParams> call = client.getWeatherParams();
        call.enqueue(new ApiCallback<WeatherParams>() {
            @Override
            public void onResponse(Response<WeatherParams> response) {
                if (getActivity() != null && isAdded()) {
                    if (response.body() == null)
                        return;
                    if (response.body().getData() == null)
                        return;
                    weather_card.setVisibility(View.VISIBLE);
                    displayWeatherData(response.body().getData());
                }
            }

            @Override
            public void onResponse401(Response<WeatherParams> response) throws
                    JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }


        });

    }

    private void displayWeatherData(WeatherParams.Data data) {
        district.setText(data.getDistrict());

        String date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
        ArrayList<WeatherParams.Data.WeatherData> weatherData = data.getWeatherData();
        for (int i = 0; i < weatherData.size(); i++) {
            if (weatherData.get(i).getDate().equals(date)) {
                maxTemp.setText("" + weatherData.get(i).getMax_temp() + "°C");
                minTemp.setText("" + weatherData.get(i).getMin_temp() + "°C");
                minHum.setText("" + weatherData.get(i).getMin_humidity() + "%");
                maxHum.setText("" + weatherData.get(i).getMax_humidity() + "%");
                windDir.setText("" + weatherData.get(i).getWind_direction());
                try {
                    if (getActivity() != null && isAdded()) {
                        if (weatherData.get(i).getRainfall() > 0) {
                            rainImage.setBackgroundResource(R.drawable.rain_gif);
                            rainText.setText(getString(R.string.rain_text));
                        } else {
                            rainImage.setBackgroundResource(R.drawable.sun_gif);
                            rainText.setText(getString(R.string.no_rain_text));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }

}
