package app.intspvt.com.farmer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.response.OrderLines;

/**
 * Created by DELL on 10/5/2017.
 */

public class RecyclerAdapterOrderLines extends RecyclerView.Adapter<RecyclerAdapterOrderLines.Order> {
    private Context context;
    private ArrayList<OrderLines> data;

    public RecyclerAdapterOrderLines(Context context, ArrayList<OrderLines> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public RecyclerAdapterOrderLines.Order onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_order_lines_layout, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterOrderLines.Order(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterOrderLines.Order holder, int position) {
        // print information recieved from the bundle
        final int pos = holder.getAdapterPosition();
        holder.productName.setText(data.get(pos).getName());
        holder.product_qty.setText("x" + data.get(pos).getProduct_uom_qty());
        holder.product_total.setText(context.getString(R.string.rs) + " " + data.get(pos).getPrice_total());
        float i = data.get(pos).getPrice_total() / data.get(pos).getProduct_uom_qty();
        holder.product_mrp.setText(context.getString(R.string.rs) + " " + i);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Order extends RecyclerView.ViewHolder {
        private TextView productName, product_mrp, product_qty, product_total;

        public Order(View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.productName);
            product_mrp = itemView.findViewById(R.id.product_mrp);
            product_qty = itemView.findViewById(R.id.product_qty);
            product_total = itemView.findViewById(R.id.product_total);

        }
    }


}
