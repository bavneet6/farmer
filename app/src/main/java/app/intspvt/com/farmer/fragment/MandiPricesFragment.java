package app.intspvt.com.farmer.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.adapter.RecyclerAdapterMandiAllCrops;
import app.intspvt.com.farmer.adapter.RecyclerAdapterUserCropsMandi;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.CropsData;
import app.intspvt.com.farmer.rest.response.MandiPrices;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.CropShowMandi;
import app.intspvt.com.farmer.utilities.PermissionCheck;
import app.intspvt.com.farmer.utilities.UrlConstant;
import retrofit2.Call;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MandiPricesFragment extends BaseFragment implements View.OnClickListener, OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, CropShowMandi, SearchView.OnQueryTextListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    public static final String TAG = MandiPricesFragment.class.getSimpleName();
    private ImageView back, search;
    private RecyclerView cropList, cropsAllList;
    private MapView mapView;
    private RecyclerAdapterMandiAllCrops recyclerAdapterMandiAllCrops;
    private SearchView searchView;
    private float highestPrice = 0f;
    private Integer highestPricePosition;
    private Dialog dialog;
    private GoogleMap googleMap;
    private List<Long> userCropsIds = new ArrayList<>();
    private List<String> userCropsNames = new ArrayList<>();
    private int cropPos = 0;
    private ArrayList<CropsData.CropsInfo> cropsInfos = new ArrayList<>();

    private String cropSelectedName = null;
    private Long cropSelectedId = 0L;
    private ArrayList<CropsData.CropsInfo> farmerCropsInfo = new ArrayList<>();
    private List<MandiPrices.Mandi> mandiPricesData;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Marker mCurrLocationMarker;


    public static MandiPricesFragment newInstance() {
        return new MandiPricesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_mandi_price, container, false);
        // Gets the MapView from the XML layout and creates it
        back = v.findViewById(R.id.back);
        cropList = v.findViewById(R.id.cropList);
        cropList.setNestedScrollingEnabled(false);
        search = v.findViewById(R.id.search);

        back.setOnClickListener(this);
        search.setOnClickListener(this);
        MapsInitializer.initialize(getActivity());
        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity())) {
            case ConnectionResult.SUCCESS:
                mapView = v.findViewById(R.id.mapview);
                mapView.onCreate(savedInstanceState);
                // Gets to GoogleMap from the MapView and does initialization stuff
                if (mapView != null) {
                    mapView.getMapAsync(this);
                }
                break;
            case ConnectionResult.SERVICE_MISSING:
                Toast.makeText(getActivity(), "SERVICE MISSING", Toast.LENGTH_SHORT).show();
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                Toast.makeText(getActivity(), "UPDATE REQUIRED", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(getActivity(), GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()), Toast.LENGTH_SHORT).show();
        }

        final Handler handler = new Handler();
        if (cropSelectedId == 0L) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    getUserCrops();
                    Log.d("Handler", "Running Handler");
                }
            }, 5000);
        } else {
            getUserCrops();
        }
        return v;
    }

    @Override
    public void onResume() {
        if (mapView != null)
            mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        if (mapView != null)
            mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null)
            mapView.onLowMemory();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.search:
                getAllCrops();
                break;
        }
    }

    private Float getPriceFromType(MandiPrices.Mandi mandi) {
        if (mandi.getType().equals("dehaat"))
            return mandi.getDehaat_price();
        else
            return mandi.getBest_price();
    }

    private String getNameFromType(MandiPrices.Mandi mandi) {
        if (mandi.getType().equals("dehaat"))
            return mandi.getNode().get(1);
        else if (!AppUtils.isNullCase(mandi.getMandi().get(1)))
            return mandi.getMandi().get(1);
        else
            return "no mandi found";
    }

    private List<Double> getLocationFromType(MandiPrices.Mandi mandi) {
        List<Double> location = new ArrayList<>();
        if (mandi.getType().equals("dehaat")) {
            location.add(mandi.getLatitude());
            location.add(mandi.getLongitude());
            return location;
        } else {
            Address address = AppUtils.getLatLongFromPin(getActivity(), mandi.getPincode());
            if (address == null)
                return null;
            location.add(address.getLatitude());
            location.add(address.getLongitude());
            return location;
        }
    }

    private void showMandiOnMap() {
        highestPrice = 0f;
        if (googleMap != null)
            googleMap.clear();
        showNodeLocation();

        if (cropSelectedId != 0L) {
            if (mandiPricesData != null && mandiPricesData.size() > 0) {
                for (int i = 0; i < mandiPricesData.size(); i++) {
                    MandiPrices.Mandi mandi = mandiPricesData.get(i);
                    Float data = getPriceFromType(mandi);
                    if (highestPrice < data) {
                        highestPrice = data;
                        highestPricePosition = i;
                    }
                }
                for (int i = 0; i < mandiPricesData.size(); i++) {
                    Float data = getPriceFromType(mandiPricesData.get(i));
                    String name = getNameFromType(mandiPricesData.get(i));

                    List<Double> locationList = getLocationFromType(mandiPricesData.get(i));
                    if (locationList == null)
                        return;

                    if (highestPricePosition != null && highestPricePosition.equals(i)) {
                        googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(locationList.get(0),
                                        locationList.get(1)))
                                .icon(BitmapDescriptorFactory.
                                        fromBitmap(createStoreMarker(R.layout.highest_map_marker,
                                                mandiPricesData.get(i).getType(),
                                                name, data,
                                                mandiPricesData.get(i).getProduct_id().get(1)))).title("")).
                                setTag(mandiPricesData.get(i));
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom
                                (new LatLng(locationList.get(0),
                                        locationList.get(1)), 10));
                    } else {
                        googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(locationList.get(0),
                                        locationList.get(1)))
                                .icon(BitmapDescriptorFactory.
                                        fromBitmap(createStoreMarker(R.layout.map_marker,
                                                mandiPricesData.get(i).getType(), name, data,
                                                mandiPricesData.get(i).getProduct_id().get(1)))).title("")).
                                setTag(mandiPricesData.get(i));
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom
                                (new LatLng(locationList.get(0),
                                        locationList.get(1)), 10));
                    }
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.information);
                if (AppPreference.getInstance().getLANGUAGE().equals(UrlConstant.ENGLISH))
                    builder.setMessage(getContext().getString(R.string.no_mandis) + " " + cropSelectedName);
                else
                    builder.setMessage(cropSelectedName + " " + getContext().getString(R.string.no_mandis));
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.okay, (dialog, which) -> dialog.dismiss());
                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private void showNodeLocation() {
        if (AppPreference.getInstance().getLATITUDE() != null && AppPreference.getInstance().getLONGITUDE() != null) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(AppPreference.getInstance().getLATITUDE()), Double.parseDouble(AppPreference.getInstance().getLONGITUDE())), 3));
            googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(AppPreference.getInstance().getLATITUDE()), Double.parseDouble(AppPreference.getInstance().getLONGITUDE())))).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        }
    }

    private Bitmap createStoreMarker(int resource, String type, String mandi_name, Float best_price, String productName) {
        View markerLayout = getLayoutInflater().inflate(resource, null);

        TextView mandiName = markerLayout.findViewById(R.id.mandiName);
        TextView bestPrice = markerLayout.findViewById(R.id.best_price);
        TextView product = markerLayout.findViewById(R.id.product);
        if (type.equals("dehaat")) {
            mandiName.setText(getString(R.string.node) + " " + mandi_name);

        } else {
            mandiName.setText(getString(R.string.mandi) + " " + mandi_name);

        }
        bestPrice.setText(getString(R.string.rs) + best_price);
        product.setText(productName);

        markerLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        markerLayout.layout(0, 0, markerLayout.getMeasuredWidth(), markerLayout.getMeasuredHeight());

        final Bitmap bitmap = Bitmap.createBitmap(markerLayout.getMeasuredWidth(), markerLayout.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        markerLayout.draw(canvas);
        return bitmap;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (PermissionCheck.checkFLocationPermission(getActivity())) {
            if (googleMap == null)
                return;
            this.googleMap = googleMap;
            if (cropSelectedId == 0L) {
                buildGoogleApiClient();
            }
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            showNodeLocation();
            googleMap.setOnMarkerClickListener(this);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, 1);
        }
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (!marker.getTitle().equals("Current Position")) {
            SingleMandiBottomFragment singleMandiBottomFragment = SingleMandiBottomFragment.getInstance();
            Bundle bundle = new Bundle();
            bundle.putSerializable("MANDI_DATA", (Serializable) marker.getTag());
            bundle.putLong("CROP_ID", cropSelectedId);
            singleMandiBottomFragment.setArguments(bundle);
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frag_container, singleMandiBottomFragment).addToBackStack("").commitAllowingStateLoss();
        }
        return false;
    }

    private void getUserCrops() {
        AppRestClient client = AppRestClient.getInstance();
        Call<CropsData> call = client.getFarmerCropList();
        call.enqueue(new ApiCallback<CropsData>() {
            @Override
            public void onResponse(Response<CropsData> response) {
                if (response.body() == null)
                    return;
                if (response.body().getCropsInfo() == null)
                    return;
                farmerCropsInfo = response.body().getCropsInfo();
                if (farmerCropsInfo != null && farmerCropsInfo.size() > 0) {
                    for (int i = 0; i < farmerCropsInfo.size(); i++) {
                        userCropsIds.add(farmerCropsInfo.get(i).getCrop_id());
                        userCropsNames.add(farmerCropsInfo.get(i).getCrop_name());
                    }
                    if (cropSelectedId == 0L) {
                        cropSelectedId = farmerCropsInfo.get(0).getCrop_id();
                        cropSelectedName = farmerCropsInfo.get(0).getCrop_name();
                    }
                    getCropMandiData(cropSelectedId, false);
                }
                displayCropList();
            }

            @Override
            public void onResponse401(Response<CropsData> response) throws JSONException {

            }


        });
    }

    private void displayCropList() {
        if (farmerCropsInfo != null && farmerCropsInfo.size() != 0) {
            cropList.setAdapter(new RecyclerAdapterUserCropsMandi(getActivity(), farmerCropsInfo, cropSelectedId, this));
            cropList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        }
    }

    @Override
    public void onSelect(List<String> list) {
        cropSelectedId = Long.valueOf(list.get(0));
        cropSelectedName = list.get(1);
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();

        getCropMandiData(cropSelectedId, true);
        displayCropList();
    }

    private void getCropMandiData(Long id, boolean flag) {
        AppRestClient client = AppRestClient.getInstance();
        Call<MandiPrices> call = client.getCropMandiData(id);
        call.enqueue(new ApiCallback<MandiPrices>() {
            @Override
            public void onResponse(Response<MandiPrices> response) {
                if (response.body() == null)
                    return;
                if (response.body().getData() == null)
                    return;
                mandiPricesData = response.body().getData();
                if (flag) {
                    showMandiOnMap();
                } else {
                    if (mandiPricesData.size() == 0 && cropPos < userCropsIds.size() - 1) {
                        int position = ++cropPos;
                        cropSelectedId = userCropsIds.get(position);
                        cropSelectedName = userCropsNames.get(position);
                        displayCropList();
                        getCropMandiData(cropSelectedId, flag);
                    } else if (mandiPricesData.size() == 0 && cropPos == userCropsIds.size() - 1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(R.string.information);
                        if (AppPreference.getInstance().getLANGUAGE().equals(UrlConstant.ENGLISH))
                            builder.setMessage(getContext().getString(R.string.no_mandis) + " " + cropSelectedName);
                        else
                            builder.setMessage(cropSelectedName + " " + getContext().getString(R.string.no_mandis));
                        builder.setCancelable(false);
                        builder.setPositiveButton(R.string.okay, (dialog, which) -> dialog.dismiss());
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        showMandiOnMap();
                    }
                }

            }

            @Override
            public void onResponse401(Response<MandiPrices> response) throws JSONException {

            }
        });

    }

    private void openAllCropsDialog() {
        dialog = new Dialog(getActivity());
        dialog.setCanceledOnTouchOutside(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_mandi_all_crops);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        searchView = dialog.findViewById(R.id.search);
        cropsAllList = dialog.findViewById(R.id.cropsAllList);
        cropsAllList.setNestedScrollingEnabled(false);

        searchView.setOnQueryTextListener(this);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });
        Drawable d = new ColorDrawable(getActivity().getResources().getColor(R.color.black));
        d.setAlpha(100);
        dialog.getWindow().setBackgroundDrawable(d);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }

    private void getAllCrops() {
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<CropsData> call = client.getCropList();
        call.enqueue(new ApiCallback<CropsData>() {
            @Override
            public void onResponse(Response<CropsData> response) {
                AppUtils.hideProgressDialog();
                if (response.body() == null)
                    return;
                if (response.body().getCropsInfo() == null)
                    return;
                openAllCropsDialog();
                cropsInfos = response.body().getCropsInfo();
                displayAllCropsData(response.body().getCropsInfo());
            }

            @Override
            public void onResponse401(Response<CropsData> response) throws JSONException {

            }

        });
    }

    private void displayAllCropsData(ArrayList<CropsData.CropsInfo> cropsInfo) {
        recyclerAdapterMandiAllCrops = new RecyclerAdapterMandiAllCrops(getActivity(), cropsInfo, this);
        cropsAllList.setAdapter(recyclerAdapterMandiAllCrops);
        cropsAllList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (cropsInfos.size() != 0) {
            recyclerAdapterMandiAllCrops.getFilter().filter(newText);
        }
        return false;
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (cropSelectedId == 0L) {
            if (mCurrLocationMarker != null) {
                mCurrLocationMarker.remove();
            }

            LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Current Position");
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            mCurrLocationMarker = googleMap.addMarker(markerOptions);
            mCurrLocationMarker.setTitle("Current Position");

            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 7));
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (cropSelectedId == 0L) {
            mLocationRequest = new LocationRequest();
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this::onLocationChanged);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
