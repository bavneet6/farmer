package app.intspvt.com.farmer.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.FragmentTransaction;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.UrlConstant;

public class LanguageFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = LanguageFragment.class.getSimpleName();
    private TextView english, hindi, languageLine;
    private Button next;

    public static LanguageFragment newInstance() {
        return new LanguageFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_language, null);
        english = v.findViewById(R.id.english);
        hindi = v.findViewById(R.id.hindi);
        languageLine = v.findViewById(R.id.languageLine);
        next = v.findViewById(R.id.next);
        english.setOnClickListener(this);
        hindi.setOnClickListener(this);
        next.setOnClickListener(this);
        AppPreference.getInstance().clearData();
        AppPreference.getInstance().setLANGUAGE(UrlConstant.HINDI);
        Farmer.setLocale(getActivity(), UrlConstant.HINDI);
        return v;

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.english:
                setselection(UrlConstant.ENGLISH);
                AppPreference.getInstance().setLANGUAGE(UrlConstant.ENGLISH);
                Farmer.setLocale(getActivity(), UrlConstant.ENGLISH);
                next.setText("Next");
                languageLine.setText("Your language is set in English, you can choose from these following options :");
                break;
            case R.id.hindi:
                setselection(UrlConstant.HINDI);
                AppPreference.getInstance().setLANGUAGE(UrlConstant.HINDI);
                Farmer.setLocale(getActivity(), UrlConstant.HINDI);
                next.setText("आगे");
                languageLine.setText("आपकी भाषा हिंदी में सेट है, आप इन निम्नलिखित विकल्पों में से चुन सकते हैं :");
                break;
            case R.id.next:
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frag_cont, NumberFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
        }
    }

    private void setselection(String language) {
        if (language.equals(UrlConstant.HINDI)) {
            hindi.setTextColor(getActivity().getResources().getColor(R.color.white));
            english.setTextColor(getActivity().getResources().getColor(R.color.baseColor));
            hindi.setBackground(getActivity().getResources().getDrawable(R.drawable.blue_back_box));
            english.setBackground(getActivity().getResources().getDrawable(R.drawable.categ_box));
        } else {
            english.setTextColor(getActivity().getResources().getColor(R.color.white));
            hindi.setTextColor(getActivity().getResources().getColor(R.color.baseColor));
            english.setBackground(getActivity().getResources().getDrawable(R.drawable.blue_back_box));
            hindi.setBackground(getActivity().getResources().getDrawable(R.drawable.categ_box));
        }

    }
}
