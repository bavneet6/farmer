package app.intspvt.com.farmer.repository;

import androidx.lifecycle.MutableLiveData;

import java.net.ConnectException;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.SingleTrendArticle;
import app.intspvt.com.farmer.rest.response.TrendingData;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SingleTrendArticleRepository {


    private static SingleTrendArticleRepository mInstance;
    private TrendingData trendingData;

    public static SingleTrendArticleRepository getInstance() {
        if (mInstance == null)
            mInstance = new SingleTrendArticleRepository();
        return mInstance;
    }

    public MutableLiveData<TrendingData> getSingleTrendData(Long id) {
        final MutableLiveData<TrendingData> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<SingleTrendArticle> call = client.geSingleTrendArticle(id);
        call.enqueue(new Callback<SingleTrendArticle>() {
            @Override
            public void onResponse(Call<SingleTrendArticle> call, Response<SingleTrendArticle> response) {
                if (response.body() == null)
                    return;
                trendingData = null;
                if (response.code() == 200) {
                    if (response.body().getTrendingData() != null) {
                        trendingData = response.body().getTrendingData();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }
                data.setValue(trendingData);
            }

            @Override
            public void onFailure(Call<SingleTrendArticle> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Farmer.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Farmer.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(null);
            }


        });
        return data;
    }
}
