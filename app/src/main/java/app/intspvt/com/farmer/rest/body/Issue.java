package app.intspvt.com.farmer.rest.body;

import com.google.gson.annotations.SerializedName;

import app.intspvt.com.farmer.rest.response.IssueData;

/**
 * Created by DELL on 12/1/2017.
 */

public class Issue {
    @SerializedName("data")
    private IssueData data;

    public Issue(IssueData issueData) {

        this.data = issueData;
    }


    public IssueData getData() {
        return data;
    }

    public void setData(IssueData data) {
        this.data = data;
    }
}
