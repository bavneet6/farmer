package app.intspvt.com.farmer.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterEnquiryImageList;
import app.intspvt.com.farmer.adapter.RecyclerAdapterEnquiryReplies;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.body.Issue;
import app.intspvt.com.farmer.rest.response.EnquiryDataHistory;
import app.intspvt.com.farmer.rest.response.EnquiryReply;
import app.intspvt.com.farmer.rest.response.IssueData;
import app.intspvt.com.farmer.rest.response.SingleIssueHistory;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Response;

public class SingleDiseaseMessagesFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = SingleDiseaseMessagesFragment.class.getSimpleName();
    ArrayList<String> returnValue = new ArrayList<>();
    private FirebaseAnalytics mFirebaseAnalytics;
    private ArrayList<EnquiryReply> enquiryReply = new ArrayList<>();
    private ArrayList<String> images = new ArrayList<>();
    private Dialog imageDialog;
    private LinearLayoutManager layoutManager;
    private TextView description, create_date, addMore, solution, status, query_num;
    private RecyclerView imagesRecy, repliesRecy;
    private LinearLayout no_reply, solutionBack;
    private Long issue_id = 0L;
    private EnquiryDataHistory enquiryDataHistory;
    private ArrayList<File> imageFileList1 = new ArrayList<>();

    public static SingleDiseaseMessagesFragment newInstance() {
        return new SingleDiseaseMessagesFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_single_disease_messages, null);
        MainActivity.showCallAndHome(true);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Bundle bundle = getArguments();
        if (bundle != null) {
            enquiryDataHistory = (EnquiryDataHistory) bundle.getSerializable("ISSUE_DATA");
            issue_id = bundle.getLong("ISSUE_ID");
        }
        description = v.findViewById(R.id.enHeading);
        create_date = v.findViewById(R.id.enDate);
        solution = v.findViewById(R.id.solution);
        no_reply = v.findViewById(R.id.no_reply);
        solutionBack = v.findViewById(R.id.solutionBack);
        imagesRecy = v.findViewById(R.id.images_rec);
        imagesRecy.setNestedScrollingEnabled(false);
        repliesRecy = v.findViewById(R.id.replies_rec);
        repliesRecy.setNestedScrollingEnabled(false);
        addMore = v.findViewById(R.id.addMore);
        status = v.findViewById(R.id.enStatus);
        query_num = v.findViewById(R.id.query_num);
        addMore.setOnClickListener(this);
        if (enquiryDataHistory != null) {
            images = enquiryDataHistory.getImages();
            enquiryReply = enquiryDataHistory.getReplies();
            displayImages();
            displayReplies();
            displayData();
        }
        return v;
    }


    private void displayImages() {
        if (getActivity() != null && isAdded()) {
            if (images != null) {
                imagesRecy.setVisibility(View.VISIBLE);
                imagesRecy.setAdapter(new RecyclerAdapterEnquiryImageList(images));
                imagesRecy.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            } else {
                imagesRecy.setVisibility(View.GONE);
            }
        }
    }

    private void displayReplies() {
        if (getActivity() != null && isAdded()) {
            if (enquiryReply != null) {
                if (enquiryReply.size() != 0) {
                    no_reply.setVisibility(View.GONE);
                    repliesRecy.setVisibility(View.VISIBLE);
                    layoutManager = new LinearLayoutManager(getActivity());
                    layoutManager.setReverseLayout(true);
                    repliesRecy.setAdapter(new RecyclerAdapterEnquiryReplies(enquiryReply));
                    repliesRecy.setLayoutManager(layoutManager);
                }
            } else {
                repliesRecy.setVisibility(View.GONE);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.addMore:
                AppUtils.imageSelection(this, 1);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("val", "requestCode ->  " + requestCode + "  resultCode " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (100): {
                if (resultCode == Activity.RESULT_OK) {
                    returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                    storeImagesInList(returnValue);
                }
            }
            break;
        }
    }

    private void storeImagesInList(ArrayList<String> returnValue) {
        imageFileList1.clear();
        for (int i = 0; i < returnValue.size(); i++) {
            File file = new File(returnValue.get(i));
            imageFileList1.add(file);
        }
        Bitmap bitmap = new BitmapDrawable(getActivity().getResources(), imageFileList1.get(0).getAbsolutePath()).getBitmap();
        openConfirmationDialog(bitmap);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AppUtils.imageSelection(this, 1);
                } else {
                }
                return;
            }
        }

    }


    private void openConfirmationDialog(Bitmap destination) {
        imageDialog = new Dialog(getActivity());
        imageDialog.setCanceledOnTouchOutside(true);
        final ImageView imageView;
        final EditText text;
        TextView save;
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog.setContentView(R.layout.dialog_disease_detection);
        imageView = imageDialog.findViewById(R.id.image);
        save = imageDialog.findViewById(R.id.save);
        text = imageDialog.findViewById(R.id.disease_text);
        text.setText(getString(R.string.disease_detect_text));
        imageView.setImageBitmap(destination);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData(text.getText().toString());
            }
        });

        Drawable d = new ColorDrawable(getActivity().getResources().getColor(R.color.black));
        d.setAlpha(100);
        imageDialog.getWindow().setBackgroundDrawable(d);
        imageDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        imageDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imageDialog.getWindow().setGravity(Gravity.CENTER);
        imageDialog.show();
    }

    private void sendData(String text) {
        IssueData issueData = new IssueData();
        if (AppUtils.isNullCase(text)) {
            issueData.setDescription(null);
        } else {
            issueData.setDescription(text);
        }
        issueData.setIssue_id(issue_id);
        issueData.setCategory("disease_detection");
        Issue issue = new Issue(issueData);
        AppUtils.showProgressDialog(activity);
        Call<Void> call;
        AppRestClient client = AppRestClient.getInstance();
        call = client.sendReply(issue, null, imageFileList1);
        call.enqueue(new ApiCallback<Void>() {
            @Override
            public void onResponse(Response<Void> response) {

                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.information);
                    builder.setMessage(R.string.info_receievd);
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            imageDialog.dismiss();
                            dialog.dismiss();
                            getSingleEnquiryHistory(issue_id);

                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                Bundle params = new Bundle();
                params.putLong("RaiseDiseaseDetection", Long.parseLong(AppPreference.getInstance().getMobile()));
                mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
                mFirebaseAnalytics.logEvent("RaiseDiseaseDetection", params);

            }

            @Override
            public void onResponse401(Response<Void> response) throws JSONException {

            }


        });
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("DiseaseSingleHistory", Long.parseLong(AppPreference.getInstance().getMobile()));
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        mFirebaseAnalytics.logEvent("DiseaseSingleHistory", params);
    }

    private void getSingleEnquiryHistory(Long issue_id) {
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<SingleIssueHistory> call = client.getSingleHistoryData(issue_id);
        call.enqueue(new ApiCallback<SingleIssueHistory>() {
            @Override
            public void onResponse(Response<SingleIssueHistory> response) {
                if (getActivity() != null && isAdded()) {
                    AppUtils.hideProgressDialog();

                    if (response.body() == null)
                        return;
                    if (response.body().getData() == null)
                        return;

                }
            }

            @Override
            public void onResponse401(Response<SingleIssueHistory> response) throws
                    JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }


        });
    }

    private void displayData() {
        if (getActivity() != null && isAdded()) {

            if (!AppUtils.isNullCase(enquiryDataHistory.getDescription())) {
                description.setVisibility(View.VISIBLE);
                description.setText(enquiryDataHistory.getDescription());
            }
            if (!AppUtils.isNullCase(enquiryDataHistory.getSolution())) {
                solutionBack.setVisibility(View.VISIBLE);
                solution.setText(enquiryDataHistory.getSolution());
            }
            if (!AppUtils.isNullCase(enquiryDataHistory.getKanban_state()))
                status.setText(enquiryDataHistory.getKanban_state());
            query_num.setText(getString(R.string.issue_no) + ": " + enquiryDataHistory.getId());
            create_date.setText(enquiryDataHistory.getCreate_date());
        }
    }
}