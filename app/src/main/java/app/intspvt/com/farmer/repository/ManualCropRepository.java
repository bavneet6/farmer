package app.intspvt.com.farmer.repository;

import androidx.lifecycle.MutableLiveData;

import java.net.ConnectException;
import java.util.ArrayList;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.CropManualData;
import app.intspvt.com.farmer.rest.response.CropManuals;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManualCropRepository {
    private static ManualCropRepository mInstance;
    private ArrayList<CropManualData> manualCrops = new ArrayList<>();

    public static ManualCropRepository getInstance() {
        if (mInstance == null)
            mInstance = new ManualCropRepository();
        return mInstance;
    }

    public MutableLiveData<ArrayList<CropManualData>> getManualCrops() {
        MutableLiveData<ArrayList<CropManualData>> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<CropManuals> call = client.getCropManualList();
        call.enqueue(new Callback<CropManuals>() {
            @Override
            public void onResponse(Call<CropManuals> call, Response<CropManuals> response) {
                if (response.body() == null)
                    return;
                manualCrops = new ArrayList<>();
                if (response.code() == 200) {
                    if (response.body().getCropManual() != null) {
                        manualCrops = response.body().getCropManual();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }
                data.setValue(manualCrops);

            }

            @Override
            public void onFailure(Call<CropManuals> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Farmer.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Farmer.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(new ArrayList<>());
            }

        });
        return data;
    }
}


