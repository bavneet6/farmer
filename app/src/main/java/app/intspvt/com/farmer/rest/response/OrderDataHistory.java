package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


/**
 * Created by DELL on 9/26/2017.
 */

public class OrderDataHistory {

    @SerializedName("amount_total")
    private float amount_total;
    @SerializedName("amount_tax")
    private float amount_tax;
    @SerializedName("amount_untaxed")
    private float amount_untaxed;
    @SerializedName("discount")
    private float discount;

    @SerializedName("date_order")
    private String date_order;
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;
    @SerializedName("order_lines")
    private ArrayList<OrderLines> order_lines;

    @SerializedName("state")
    private String state;

    @SerializedName("seller_name")
    private String seller_name;

    @SerializedName("seller_number")
    private String seller_number;


    public String getState() {
        return state;
    }

    public int getId() {
        return id;
    }

    public float getAmount_total() {
        return amount_total;
    }

    public ArrayList<OrderLines> getOrder_lines() {
        return order_lines;
    }

    public float getAmount_tax() {
        return amount_tax;
    }

    public float getAmount_untaxed() {
        return amount_untaxed;
    }

    public float getDiscount() {
        return discount;
    }

    public String getDate_order() {
        return date_order;
    }

    public String getName() {
        return name;
    }

    public String getSeller_name() {
        return seller_name;
    }

    public String getSeller_number() {
        return seller_number;
    }
}
