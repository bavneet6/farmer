package app.intspvt.com.farmer.adapter;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.fragment.EnquiryDetailHistoryFragment;
import app.intspvt.com.farmer.rest.response.EnquiryDataHistory;
import app.intspvt.com.farmer.rest.response.IssueDbStatus;
import app.intspvt.com.farmer.utilities.AppUtils;

/**
 * Created by DELL on 1/11/2018.
 */

public class RecyclerAdapterEnquiryHistory extends RecyclerView.Adapter<RecyclerAdapterEnquiryHistory.Order> {
    private Context context;
    private List<EnquiryDataHistory> data;
    private DatabaseHandler databaseHandler;
    private ArrayList<IssueDbStatus> issueArrayList = new ArrayList<>();

    public RecyclerAdapterEnquiryHistory(List<EnquiryDataHistory> data) {
        this.data = data;
    }

    @Override
    public RecyclerAdapterEnquiryHistory.Order onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_enquiry_history, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterEnquiryHistory.Order(itemView);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final RecyclerAdapterEnquiryHistory.Order holder, int position) {
        final int pos = holder.getAdapterPosition();
        databaseHandler = new DatabaseHandler(context);
        issueArrayList = databaseHandler.getIssueSeenData();
        for (int i = 0; i < issueArrayList.size(); i++) {
            if (issueArrayList.get(i).getIssue_id() == data.get(pos).getId()) {
                if (issueArrayList.get(i).getIssue_unseen() == 1) {
                    holder.new_reply.setVisibility(View.VISIBLE);
                    holder.back.setBackground(context.getResources().getDrawable(R.drawable.card_back_selected));
                } else {
                    holder.new_reply.setVisibility(View.GONE);
                    holder.back.setBackground(context.getResources().getDrawable(R.drawable.history_card_back));
                }
            }
        }
        if (!AppUtils.isNullCase(data.get(pos).getDescription()))
            holder.enHeading.setText(data.get(pos).getDescription());
        if (!AppUtils.isNullCase(data.get(pos).getCategory()))
            holder.enCate.setText(data.get(pos).getCategory());

        if (!AppUtils.isNullCase(data.get(pos).getKanban_state()))
            holder.enStatus.setText(data.get(pos).getKanban_state());

        holder.enDate.setText(data.get(pos).getCreate_date());
        holder.enId.setText(context.getString(R.string.issue_no) + ": " + data.get(pos).getId());

        if ((!data.get(pos).isHas_images()) && (!data.get(pos).isHas_audio())) {
            holder.image_no.setText(R.string.no_pic_aud_attach);
            holder.image_no.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        } else if (data.get(pos).isHas_images()) {
            if (data.get(pos).isHas_audio()) {
                holder.image_no.setText(R.string.pic_and_aud_attach);
                holder.image_no.setCompoundDrawablesWithIntrinsicBounds(R.drawable.attachment, 0, 0, 0);
            } else {
                holder.image_no.setText(R.string.pic_attach);
                holder.image_no.setCompoundDrawablesWithIntrinsicBounds(R.drawable.attachment, 0, 0, 0);
            }
        } else if (data.get(pos).isHas_audio()) {
            holder.image_no.setText(R.string.audio_attach);
            holder.image_no.setCompoundDrawablesWithIntrinsicBounds(R.drawable.attachment, 0, 0, 0);

        }
        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("ISSUE_ID", data.get(pos).getId());
                EnquiryDetailHistoryFragment fragment = EnquiryDetailHistoryFragment.newInstance();
                fragment.setArguments(bundle);
                AppUtils.changeFragment((FragmentActivity) context, fragment);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Order extends RecyclerView.ViewHolder {
        private TextView enHeading, enId, enDate, image_no, enStatus, enCate;
        private LinearLayout back, new_reply;

        public Order(View itemView) {
            super(itemView);
            enHeading = itemView.findViewById(R.id.enHeading);
            enStatus = itemView.findViewById(R.id.enStatus);
            enId = itemView.findViewById(R.id.enId);
            enDate = itemView.findViewById(R.id.enDate);
            image_no = itemView.findViewById(R.id.images_no);
            enCate = itemView.findViewById(R.id.enCate);
            back = itemView.findViewById(R.id.back);
            new_reply = itemView.findViewById(R.id.new_reply);
        }
    }
}



