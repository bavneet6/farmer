package app.intspvt.com.farmer.adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.RoundImageView;

/**
 * Created by DELL on 1/12/2018.
 */

public class RecyclerAdapterEnquiryImageList extends RecyclerView.Adapter<RecyclerAdapterEnquiryImageList.ImageList> {
    Context context;
    private ArrayList<String> arrayList = new ArrayList<>();
    private String photo = null, picUrlDb;
    private DatabaseHandler databaseHandler;
    private boolean checkPic;

    public RecyclerAdapterEnquiryImageList(ArrayList<String> images) {
        this.arrayList = images;
    }

    @Override
    public RecyclerAdapterEnquiryImageList.ImageList onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_enq_image_list, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterEnquiryImageList.ImageList(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterEnquiryImageList.ImageList holder, final int position) {
        databaseHandler = new DatabaseHandler(context);
        photo = arrayList.get(holder.getAdapterPosition());
        if (!AppUtils.isNullCase(photo)) {

            checkPic = databaseHandler.checkImage(photo);

            if (!checkPic) {
                AppUtils.generateAndCheckUrl(context, photo);

            }
            picUrlDb = databaseHandler.getFileUrl(photo);
            if (picUrlDb != null) {
                if (context != null) {
                    final Picasso picasso = Picasso.with(context);
                    picasso.setLoggingEnabled(true);
                    picasso.load(picUrlDb).into(holder.image, new PicassoCallback(photo) {

                        @Override
                        public void onErrorPic(URL url) {
                            picasso.load("" + url).into(holder.image);
                        }
                    });
                }

            }
        }
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photo = arrayList.get(holder.getAdapterPosition());
                if (!AppUtils.isNullCase(photo)) {
                    checkPic = databaseHandler.checkImage(photo);

                    if (!checkPic) {
                        AppUtils.generateAndCheckUrl(context, photo);

                    }
                    final Dialog dialog = new Dialog(context);
                    dialog.setCanceledOnTouchOutside(true);
                    final ImageView imageView;
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.image_zoom);
                    imageView = dialog.findViewById(R.id.imageView);
                    dialog.getWindow().setGravity(Gravity.CENTER);
                    dialog.show();
                    picUrlDb = databaseHandler.getFileUrl(photo);
                    if (picUrlDb != null) {
                        if (context != null) {
                            final Picasso picasso = Picasso.with(context);
                            picasso.setLoggingEnabled(true);
                            picasso.load(picUrlDb).into(imageView, new PicassoCallback(photo) {

                                @Override
                                public void onErrorPic(URL url) {
                                    picasso.load("" + url).into(imageView);
                                }
                            });
                        }

                    }
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ImageList extends RecyclerView.ViewHolder {
        private RoundImageView image;

        public ImageList(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image_list);
        }


    }

}
