package app.intspvt.com.farmer.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterSoilTestingList;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.body.Issue;
import app.intspvt.com.farmer.rest.response.IssueData;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.UrlConstant;
import app.intspvt.com.farmer.viewmodel.EnquiryViewModel;
import retrofit2.Call;
import retrofit2.Response;

public class RaiseReqSoilTestingFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = RaiseReqSoilTestingFragment.class.getSimpleName();
    private RecyclerView raiseReqHistoryList;
    private TextView raise_enq;
    private LinearLayout alert_back;
    private Dialog imageDialog;
    private FirebaseAnalytics mFirebaseAnalytics;
    private EnquiryViewModel enquiryViewModel;

    public static RaiseReqSoilTestingFragment newInstance() {
        return new RaiseReqSoilTestingFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_raise_req_soil_testing, null);
        MainActivity.showCallAndHome(true);
        raiseReqHistoryList = v.findViewById(R.id.raiseReqHistoryList);
        raise_enq = v.findViewById(R.id.raiseReq);
        alert_back = v.findViewById(R.id.alert_back);
        raiseReqHistoryList.setNestedScrollingEnabled(false);
        raise_enq.setOnClickListener(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        AppUtils.showProgressDialog(getActivity());
        enquiryViewModel = ViewModelProviders.of(getActivity()).get(EnquiryViewModel.class);
        enquiryViewModel.init(UrlConstant.SOIL_TESTING);
        observeViewModel(enquiryViewModel);

        return v;
    }

    private void observeViewModel(EnquiryViewModel viewModel) {
        viewModel.getEnquiryData()
                .observe(this, enquiryData -> {
                    AppUtils.hideProgressDialog();
                    if (enquiryData != null && enquiryData.size() > 0) {
                        alert_back.setVisibility(View.GONE);
                        raiseReqHistoryList.setAdapter(new RecyclerAdapterSoilTestingList
                                (getActivity(), enquiryData));
                        raiseReqHistoryList.setLayoutManager(new LinearLayoutManager(getActivity()));

                    } else {
                        alert_back.setVisibility(View.VISIBLE);
                    }

                });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.raiseReq:
                openTextDialog();
                break;
        }
    }

    private void openTextDialog() {
        imageDialog = new Dialog(getActivity());
        imageDialog.setCanceledOnTouchOutside(true);
        final ImageView imageView;
        final EditText text;
        TextView save;
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog.setContentView(R.layout.dialog_disease_detection);
        imageDialog.setCancelable(true);
        imageDialog.setCanceledOnTouchOutside(true);
        imageView = imageDialog.findViewById(R.id.image);
        save = imageDialog.findViewById(R.id.save);
        imageView.setVisibility(View.GONE);
        text = imageDialog.findViewById(R.id.disease_text);
        text.setText(R.string.want_soil_test);
        text.setSelection(text.getText().length());
        save.setOnClickListener(v -> sendData(text.getText().toString()));

        Drawable d = new ColorDrawable(getActivity().getResources().getColor(R.color.black));
        d.setAlpha(100);
        imageDialog.getWindow().setBackgroundDrawable(d);
        imageDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        imageDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imageDialog.getWindow().setGravity(Gravity.CENTER);
        imageDialog.show();
    }

    private void sendData(String text) {
        AppUtils.showProgressDialog(getActivity());
        IssueData issueData = new IssueData();
        if (AppUtils.isNullCase(text)) {
            issueData.setDescription(null);

        } else {
            issueData.setDescription(text);
        }
        issueData.setCategory(UrlConstant.SOIL_TESTING);
        Issue issue = new Issue(issueData);
        Call<Void> call;
        AppRestClient client = AppRestClient.getInstance();
        call = client.sendEnquiryData(issue, null, null);
        call.enqueue(new ApiCallback<Void>() {
            @Override
            public void onResponse(Response<Void> response) {

                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.information);
                    builder.setMessage(R.string.info_receievd);
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.okay, (dialog, which) -> {
                        imageDialog.dismiss();
                        dialog.dismiss();
                        enquiryViewModel.init(UrlConstant.SOIL_TESTING);
                        observeViewModel(enquiryViewModel);
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                Bundle params = new Bundle();
                params.putLong("RequestSoilTesting", Long.parseLong(AppPreference.getInstance().getMobile()));
                mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
                mFirebaseAnalytics.logEvent("RequestSoilTesting", params);

            }

            @Override
            public void onResponse401(Response<Void> response) {

            }


        });
    }
}
