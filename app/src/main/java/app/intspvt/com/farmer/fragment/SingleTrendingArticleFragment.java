package app.intspvt.com.farmer.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.rest.response.TrendingData;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.UrlConstant;
import app.intspvt.com.farmer.viewmodel.SingleTrendArticleViewModel;

import static app.intspvt.com.farmer.utilities.AppUtils.isNullCase;

public class SingleTrendingArticleFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = SingleTrendingArticleFragment.class.getSimpleName();
    private ImageView back, trend_img, share;
    private TextView heading, description, nextTopicHead;
    private LinearLayout nextTopicBack;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Long trending_article_id, article_pos;
    private DatabaseHandler databaseHandler;
    private String getPic, trimContent;
    private TrendingData trendingSingleData;
    private Bundle nextBundle = null;
    private HashMap<Long, ArrayList<String>> articleListMap = new HashMap<>();
    private SingleTrendArticleViewModel singleTrendArticleViewModel;

    public static SingleTrendingArticleFragment newInstance() {
        return new SingleTrendingArticleFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_single_trending_article, null);
        MainActivity.showCallAndHome(true);
        back = v.findViewById(R.id.back);
        trend_img = v.findViewById(R.id.trend_img);
        share = v.findViewById(R.id.share);
        heading = v.findViewById(R.id.heading);
        description = v.findViewById(R.id.description);
        nextTopicHead = v.findViewById(R.id.nextTopicHead);
        nextTopicBack = v.findViewById(R.id.nextTopicBack);
        nextTopicBack.setOnClickListener(this);
        back.setOnClickListener(this);
        share.setOnClickListener(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        databaseHandler = new DatabaseHandler(getActivity());
        Bundle bundle = getArguments();
        if (bundle != null) {
            trending_article_id = bundle.getLong("ARTICLE_ID");
            article_pos = bundle.getLong("ARTICLE_POS");
            articleListMap = (HashMap<Long, ArrayList<String>>) bundle.getSerializable("ARTICLE_MAP");
        }
        AppUtils.showProgressDialog(getActivity());
        singleTrendArticleViewModel = ViewModelProviders.of(getActivity()).get(SingleTrendArticleViewModel.class);
        singleTrendArticleViewModel.init(trending_article_id);
        observeViewModel(singleTrendArticleViewModel);
        return v;
    }

    private void observeViewModel(SingleTrendArticleViewModel viewModel) {
        viewModel.getSingleTrendData()
                .observe(this, trendingData -> {
                    AppUtils.hideProgressDialog();
                    if (trendingData != null) {
                        trendingSingleData = trendingData;
                        displayData(trendingSingleData);
                    }
                });
    }


    private void displayData(TrendingData trendingData) {
        if (getActivity() != null && isAdded()) {
            Long nextpos;
            if (articleListMap.size() - 1 == article_pos) {
                nextpos = 0L;
            } else
                nextpos = article_pos + 1;
            heading.setText(trendingData.getName());
            trimContent = trendingData.getContent();
            // the content of a particular article contains \n then its converted into empty spaces
            // to trim the spaces
            trimContent = trimContent.replace("\n", "");
            trimContent = trimContent.trim();
            description.setText(trimContent);
            if (!AppUtils.isNullCase(trendingData.getImage()))
                showPicassoImage(trendingData.getImage());
            if (!articleListMap.isEmpty())
                for (Map.Entry<Long, ArrayList<String>> entry : articleListMap.entrySet()) {
                    if (nextpos.equals(entry.getKey())) {
                        nextBundle = new Bundle();
                        nextBundle.putLong("ARTICLE_ID", Long.parseLong(entry.getValue().get(0)));
                        nextBundle.putLong("ARTICLE_POS", nextpos);
                        nextBundle.putSerializable("ARTICLE_MAP", articleListMap);
                        nextTopicHead.setText(getString(R.string.next_info) + entry.getValue().get(1));
                    }
                }
        }
    }

    private void showPicassoImage(final String image) {
        AppUtils.generateAndCheckUrl(getActivity(), image);
        getPic = null;
        if (!isNullCase(image)) {
            getPic = databaseHandler.getFileUrl(image);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(getActivity());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(trend_img, new PicassoCallback(image) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_crop_image).into(trend_img);
                    }
                });

            } else
                trend_img.setImageResource(R.drawable.no_crop_image);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }


    @Override
    public void onClick(View view) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();

        int id = view.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.share:
                shareData();
                break;
            case R.id.nextTopicBack:
                SingleTrendingArticleFragment fragment = SingleTrendingArticleFragment.newInstance();
                fragment.setArguments(nextBundle);
                transaction.replace(R.id.frag_container, fragment).commitAllowingStateLoss();
                break;
        }
    }

    private void shareData() {
        Bundle params = new Bundle();
        params.putLong("ShareTrend", Long.parseLong(AppPreference.getInstance().getMobile()));
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        mFirebaseAnalytics.logEvent("ShareTrend", params);
        String fileData;
        fileData = trendingSingleData.getName() + "\n \n" + trimContent +
                "\n \n" + UrlConstant.CALL_TOLL_FREE_SHARE_CONTENT;
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, fileData);
        try {
            getActivity().startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
        }
    }
}
