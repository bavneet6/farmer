package app.intspvt.com.farmer.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;

import com.google.firebase.analytics.FirebaseAnalytics;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.utilities.AppPreference;

/**
 * Created by DELL on 7/19/2017.
 */

public class YourDehaatFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = YourDehaatFragment.class.getSimpleName();
    private ImageView cross;
    private FirebaseAnalytics mFirebaseAnalytics;

    public static YourDehaatFragment newInstance() {
        return new YourDehaatFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_your_dehaat, null);
        MainActivity.showCallAndHome(true);
        cross = v.findViewById(R.id.cross);
        cross.setOnClickListener(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("YourDeHaat", Long.parseLong(AppPreference.getInstance().getMobile()));
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        mFirebaseAnalytics.logEvent("YourDeHaat", params);
    }

    @Override
    public void onPause() {
        super.onPause();

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.cross:
                getActivity().onBackPressed();
                break;
        }
    }
}
