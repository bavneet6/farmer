package app.intspvt.com.farmer.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.rest.response.SoilTestingReport;
import app.intspvt.com.farmer.utilities.AppUtils;

import static app.intspvt.com.farmer.utilities.AppUtils.isNullCase;

public class RecyclerAdapterSoilImages extends RecyclerView.Adapter<RecyclerAdapterSoilImages.ImageList> {
    Context context;
    ArrayList<SoilTestingReport.Data> imagesList = new ArrayList<>();
    private String getPic;
    private DatabaseHandler databaseHandler;


    public RecyclerAdapterSoilImages(Activity context, ArrayList<SoilTestingReport.Data> imagesList) {
        this.context = context;
        this.imagesList = imagesList;
        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public RecyclerAdapterSoilImages.ImageList onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_image_soil_reports, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterSoilImages.ImageList(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterSoilImages.ImageList holder, final int position) {
        holder.setIsRecyclable(false);
        final int pos = holder.getAdapterPosition();
        showPicassoImage(pos, holder.image);
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setCanceledOnTouchOutside(true);
                final ImageView imageView;
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.image_zoom);
                imageView = dialog.findViewById(R.id.imageView);
                showPicassoImage(pos, imageView);
                dialog.getWindow().setGravity(Gravity.CENTER);
                dialog.show();

            }

        });

    }

    private void showPicassoImage(int pos, final ImageView imageView) {
        AppUtils.generateAndCheckUrl(context, imagesList.get(pos).getImage());
        String photo = imagesList.get(pos).getImage();
        getPic = null;
        if (!isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(imageView, new PicassoCallback(photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).into(imageView);
                    }
                });

            } else {
                imageView.setImageResource(0);
            }
        }
    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }

    public class ImageList extends RecyclerView.ViewHolder {
        private ImageView image;

        public ImageList(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
        }


    }


}
