package app.intspvt.com.farmer.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.fragment.CropManualFragment;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.rest.response.CropManualData;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.RoundImageView;

import static app.intspvt.com.farmer.utilities.AppUtils.isNullCase;

public class RecyclerAdapterCropManualList extends
        RecyclerView.Adapter<RecyclerAdapterCropManualList.CropManual> implements Filterable {
    private Context context;
    private ArrayList<CropManualData> cropManuals = new ArrayList<>();
    private ArrayList<CropManualData> cropFilterManuals = new ArrayList<>();
    private String getPic;
    private DatabaseHandler databaseHandler;


    public RecyclerAdapterCropManualList(Context activity, ArrayList<CropManualData> cropManuals) {
        this.context = activity;
        this.cropManuals = cropManuals;
        this.cropFilterManuals = cropManuals;
        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public RecyclerAdapterCropManualList.CropManual onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_crop_manual_list, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterCropManualList.CropManual(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterCropManualList.CropManual holder, int position) {
        final int pos = holder.getAdapterPosition();
        holder.crop_name.setText(cropManuals.get(pos).getName());
        if (!AppUtils.isNullCase(cropManuals.get(pos).getAttachments())) {
            showPicassoImage(pos, holder.crop_image);
        }

        holder.crop_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putLong("CROP_ID", cropManuals.get(pos).getId());
                CropManualFragment fragment = CropManualFragment.newInstance();
                fragment.setArguments(bundle);
                AppUtils.changeFragment((FragmentActivity) context, fragment);
            }
        });
    }

    private void showPicassoImage(int pos, final ImageView imageView) {
        AppUtils.generateAndCheckUrl(context, cropManuals.get(pos).getAttachments());
        String photo = cropManuals.get(pos).getAttachments();
        getPic = null;
        if (!isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(imageView, new PicassoCallback(photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_crop_image).into(imageView);
                    }
                });

            } else {
                imageView.setImageResource(R.drawable.no_crop_image);
            }
        }
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    cropManuals = cropFilterManuals;
                } else {

                    ArrayList<CropManualData> filteredList = new ArrayList<>();

                    for (CropManualData list : cropFilterManuals)
                        if (list.getName().toLowerCase().contains(charString.toLowerCase()))
                            filteredList.add(list);

                    cropManuals = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = cropManuals;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                cropManuals = (ArrayList<CropManualData>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        return cropManuals.size();
    }

    public class CropManual extends RecyclerView.ViewHolder {
        private TextView crop_name;
        private LinearLayout crop_back;
        private RoundImageView crop_image;

        public CropManual(View itemView) {
            super(itemView);
            crop_name = itemView.findViewById(R.id.crop_name);
            crop_back = itemView.findViewById(R.id.crop_back);
            crop_image = itemView.findViewById(R.id.crop_image);
        }

    }
}
