package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MandiCropPrice implements Serializable{
    @SerializedName("data")
    private List<Mandi> data;

    public List<Mandi> getData() {
        return data;
    }

    public class Mandi implements Serializable {

        @SerializedName("product_id")
        private List<String> product_id;
        @SerializedName("product_uom")
        private List<String> product_uom;
        @SerializedName("min_price")
        private Float min_price;
        @SerializedName("max_price")
        private Float max_price;
        @SerializedName("best_price")
        private Float best_price;
        @SerializedName("dehaat_price")
        private Float dehaat_price;
        @SerializedName("id")
        private Long id;
        @SerializedName("crop_id")
        private List<String> crop_id;

        public Float getDehaat_price() {
            return dehaat_price;
        }

        public List<String> getCrop_id() {
            return crop_id;
        }

        public List<String> getProduct_uom() {
            return product_uom;
        }

        public List<String> getProduct_id() {
            return product_id;
        }

        public Float getMin_price() {
            return min_price;
        }

        public Float getMax_price() {
            return max_price;
        }

        public Float getBest_price() {
            return best_price;
        }

        public Long getId() {
            return id;
        }
    }

}
