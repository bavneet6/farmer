package app.intspvt.com.farmer.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterProductList;
import app.intspvt.com.farmer.rest.response.Products;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.ShowCartCount;
import app.intspvt.com.farmer.viewmodel.ProductListViewModel;

public class ManualProductsFragment extends BaseFragment implements ShowCartCount {
    public static final String TAG = ManualProductsFragment.class.getSimpleName();
    private RecyclerView productsView;
    private List<Long> productIds;
    private ProductListViewModel productListViewModel;

    public static ManualProductsFragment newInstance() {
        return new ManualProductsFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_manual_products, null);
        productsView = v.findViewById(R.id.productsView);
        MainActivity.showCallAndHome(true);
        Bundle bundle = getArguments();
        if (bundle != null) {
            productIds = (List<Long>) bundle.getSerializable("PRODUCT_IDS");
        }
        String iDs = "";
        for (int i = 0; i < productIds.size(); i++) {
            if (i == productIds.size() - 1)
                iDs = iDs + productIds.get(i);
            else
                iDs = iDs + productIds.get(i) + ",";
        }
        AppUtils.showProgressDialog(getActivity());
        productListViewModel = ViewModelProviders.of(getActivity()).get(ProductListViewModel.class);
        productListViewModel.init(iDs);
        observeViewModel(productListViewModel);
        return v;
    }

    private void observeViewModel(ProductListViewModel viewModel) {
        viewModel.getProductData()
                .observe(this, productsArrayList -> {
                    displayProductList(productsArrayList);
                });
    }

    private void displayProductList(ArrayList<Products> products) {
        if (getActivity() != null && isAdded()) {
            productsView.setLayoutManager(new LinearLayoutManager(getActivity()));
            productsView.setAdapter(new RecyclerAdapterProductList(getActivity(), products, this));
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void showCart(boolean showCart) {
        SingleCropManualFragment.newInstance().showCartManual(getActivity());
    }
}
