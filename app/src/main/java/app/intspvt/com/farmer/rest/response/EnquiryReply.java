package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by DELL on 3/23/2018.
 */

public class EnquiryReply implements Serializable {
    @SerializedName("body")
    private String body;
    @SerializedName("write_date")
    private String write_date;
    @SerializedName("author")
    private String author;

    @SerializedName("attachments")
    private Attachments attachments;

    public String getAuthor() {
        return author;
    }

    public String getBody() {
        return body;
    }

    public String getWrite_date() {
        return write_date;
    }


    public Attachments getAttachments() {
        return attachments;
    }

    public class Attachments implements Serializable {
        @SerializedName("audio")
        private String voice;
        @SerializedName("images")
        private ArrayList<String> images;


        public String getVoice() {
            return voice;
        }

        public ArrayList<String> getImages() {
            return images;
        }
    }

}
