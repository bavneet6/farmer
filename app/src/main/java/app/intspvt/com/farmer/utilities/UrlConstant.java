package app.intspvt.com.farmer.utilities;

import app.intspvt.com.farmer.BuildConfig;

/**
 * Created by DELL on 7/3/2017.
 */

public interface UrlConstant {
    int REQUEST_CAMERA = 0;
    int SELECT_FILE = 1;
    //    String BASE_URL = "http://35.154.53.102:5000";
    //        String BASE_URL = "https://impedio.serveo.net";
//    String BASE_URL = "http://13.126.85.106:5000";
    String BASE_URL = BuildConfig.BASE_URL;
    String ADVISORY = "advisory";
    String SUGGESTION = "suggestion";
    String SEED = "Seeds";
    String FERT = "Fertilizers";
    String CROP = "Crop Protection";
    String ENQ_HI = "farmer/enquiry/history";
    String DISEASE_HI = "farmer/enquiry/disease_detection";
    String SOIL_HI = "farmer/enquiry/soil_testing";
    String OUTPUT_HI = "farmer/output/sale/history";
    String AGR_IN_HI = "farmer/agri_input/history";
    String TREN_ART = "farmer/trending/article";
    String PROM_ART = "farmer/promotion";
    String WEATHER_DATA = "farmer/weather/advisory";
//    String FNF_DB = "fnfprodfnfdb";

    //    String TEMP_FNF_DB = "fnftestec2odoo";
    String TEMP_FNF_DB = BuildConfig.TEMP_FNF_DB;
    String SELECT = "Select";
    String MALE = "Male";
    String FEMALE = "Female";
    String SOIL_TESTING = "soil_testing";
    String DISEASE_DETECTION = "disease_detection";
    String ENGLISH = "en";
    String HINDI = "hi";
    String CALL_TOLL_FREE_SHARE_CONTENT = "देहात टोल फ्री - " + AppPreference.getInstance().getTOLL_FREE() + "\n \n" + "ऐप डाउनलोड करें - " + "https://play.google.com/store/apps/details?id=app.intspvt.com.farmer";

}
