package app.intspvt.com.farmer.repository;

import androidx.lifecycle.MutableLiveData;

import java.net.ConnectException;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.GetPersonalInfo;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FarmerInfoRepository {
    private static FarmerInfoRepository mInstance;
    private GetPersonalInfo.GetInfo getPersonalInfo;

    public static FarmerInfoRepository getInstance() {
        if (mInstance == null)
            mInstance = new FarmerInfoRepository();
        return mInstance;
    }

    public MutableLiveData<GetPersonalInfo.GetInfo> getPersonalInfo() {
        final MutableLiveData<GetPersonalInfo.GetInfo> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<GetPersonalInfo> call = client.getFarmerInfo();
        call.enqueue(new Callback<GetPersonalInfo>() {
            @Override
            public void onResponse(Call<GetPersonalInfo> call, Response<GetPersonalInfo> response) {
                if (response.body() == null)
                    return;
                getPersonalInfo = null;
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        getPersonalInfo = response.body().getData();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }
                data.setValue(getPersonalInfo);
            }

            @Override
            public void onFailure(Call<GetPersonalInfo> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Farmer.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Farmer.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.server_no_respond));
                    }
                }
                data.setValue(null);
            }
        });
        return data;
    }
}
