package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 8/30/2017.
 */

public class GetPersonalInfo {
    @SerializedName("data")
    private GetInfo data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private String status;

    public GetInfo getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public static class GetInfo {

        @SerializedName("farmer_info")
        private FarmerInfo farmer_info;
        @SerializedName("node_info")
        private NodeInfo node_info;

        @SerializedName("land_info")
        private LandInfo land_info;

        public GetInfo(FarmerInfo farmerInfo, LandInfo landInfo) {
            this.farmer_info = farmerInfo;
            this.land_info = landInfo;
        }

        public FarmerInfo getFarmer_info() {
            return farmer_info;
        }

        public LandInfo getLand_info() {
            return land_info;
        }

        public NodeInfo getNode_info() {
            return node_info;
        }

        public class NodeInfo {
            @SerializedName("toll_free")
            private String toll_free;
            @SerializedName("latitude")
            private Double latitude;
            @SerializedName("longitude")
            private Double longitude;
            @SerializedName("name")
            private String name;

            public Double getLongitude() {
                return longitude;
            }

            public Double getLatitude() {
                return latitude;
            }

            public String getName() {
                return name;
            }

            public String getToll_free() {
                return toll_free;
            }
        }
    }

}
