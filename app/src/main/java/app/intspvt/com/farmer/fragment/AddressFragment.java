package app.intspvt.com.farmer.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.body.PostFarmerInfo;
import app.intspvt.com.farmer.rest.body.PostFarmerPersonalInfo;
import app.intspvt.com.farmer.rest.response.GetIdNames;
import app.intspvt.com.farmer.rest.response.GetPersonalInfo;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.UrlConstant;
import retrofit2.Call;
import retrofit2.Response;

import static app.intspvt.com.farmer.utilities.AppUtils.getSelectedId;

public class AddressFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    public static final String TAG = AddressFragment.class.getSimpleName();
    private ImageView back;
    private TextView saveAdd;
    private Spinner ddl_state, ddl_village, ddl_district, ddl_block, ddl_panchayat;
    private EditText pincode;
    private int stateId, districtId, blockId, villageId, panchayatId;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ArrayList<String> distrcitList = new ArrayList<>();
    private ArrayList<String> blockList = new ArrayList<>();
    private ArrayList<String> villageList = new ArrayList<>();
    private ArrayList<String> stateList = new ArrayList<>();
    private ArrayList<String> panchayatList = new ArrayList<>();
    private ArrayList<GetIdNames.GetData> districtNames = new ArrayList<>();
    private ArrayList<GetIdNames.GetData> blockNames = new ArrayList<>();
    private ArrayList<GetIdNames.GetData> stateNames = new ArrayList<>();
    private ArrayList<GetIdNames.GetData> villageNames = new ArrayList<>();
    private ArrayList<GetIdNames.GetData> panchayatNames = new ArrayList<>();

    public static AddressFragment newInstance() {
        return new AddressFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_address, null);
        MainActivity.showCallAndHome(true);
        back = v.findViewById(R.id.back);
        saveAdd = v.findViewById(R.id.saveAdd);
        ddl_state = v.findViewById(R.id.ddl_state);
        ddl_village = v.findViewById(R.id.ddl_village);
        ddl_district = v.findViewById(R.id.ddl_district);
        ddl_block = v.findViewById(R.id.ddl_block);
        ddl_panchayat = v.findViewById(R.id.ddl_panchayat);
        pincode = v.findViewById(R.id.pincode);
        back.setOnClickListener(this);
        saveAdd.setOnClickListener(this);
        ddl_state.setOnItemSelectedListener(this);
        ddl_district.setOnItemSelectedListener(this);
        ddl_block.setOnItemSelectedListener(this);
        ddl_panchayat.setOnItemSelectedListener(this);
        ddl_village.setOnItemSelectedListener(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        if (AppUtils.haveNetworkConnection(getActivity()))
            getUserInfo(getActivity());
        else {
            if (!AppUtils.isNullCase(String.valueOf(AppPreference.getInstance().getPINCODE())))
                pincode.setText("" + AppPreference.getInstance().getPINCODE());
        }
        return v;
    }

    private void getUserInfo(final Activity activity) {
        AppRestClient client = AppRestClient.getInstance();
        Call<GetPersonalInfo> call = client.getFarmerInfo();
        call.enqueue(new ApiCallback<GetPersonalInfo>() {
            @Override
            public void onResponse(Response<GetPersonalInfo> response) {
                if (response.body() == null)
                    return;
                if (response.body().getData().getFarmer_info() != null)
                    AppUtils.storeFarmerInfo(response.body().getData().getFarmer_info());

                if (response.body().getData().getNode_info() != null) {
                    AppUtils.storeNodeData(response.body().getData().getNode_info());
                }

                getStateList();
                if (!AppUtils.isNullCase(String.valueOf(AppPreference.getInstance().getPINCODE())))
                    pincode.setText("" + AppPreference.getInstance().getPINCODE());

            }

            @Override
            public void onResponse401(Response<GetPersonalInfo> response) throws
                    JSONException {
                AppUtils.showSessionExpiredDialog(activity);

            }


        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("AddressScreen", Long.parseLong(AppPreference.getInstance().getMobile()));
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        mFirebaseAnalytics.logEvent("AddressScreen", params);
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.saveAdd:
                if (stateId == 0) {
                    AppUtils.showToast(getString(R.string.select_state));
                } else if (districtId == 0) {
                    AppUtils.showToast(getString(R.string.select_district));
                } else if (blockId == 0) {
                    AppUtils.showToast(getString(R.string.select_block));
                } else if (pincode.getText().toString().length() != 6) {
                    pincode.setError(Html.fromHtml(getString(R.string.enter_pincode)), null);
                    pincode.requestFocus();
                } else {
                    Bundle params = new Bundle();
                    SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
                    params.putLong("AddressChange", Long.parseLong(AppPreference.getInstance().getMobile()));
                    mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
                    mFirebaseAnalytics.logEvent("AddressChange", params);
                    sendUpdateInfo();
                }
                break;
        }
    }

    private void sendUpdateInfo() {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        PostFarmerInfo.PostInfo.FarmerInfo farmerInfo = new PostFarmerInfo.PostInfo.FarmerInfo(stateId, districtId, blockId, panchayatId, villageId, null, Long.parseLong(pincode.getText().toString()));
        PostFarmerInfo.PostInfo getInfo = new PostFarmerInfo.PostInfo(farmerInfo);
        Call<PostFarmerPersonalInfo> call = client.updateFarmerInfo(null, getInfo);
        call.enqueue(new ApiCallback<PostFarmerPersonalInfo>() {
            @Override
            public void onResponse(Response<PostFarmerPersonalInfo> response) {
                getActivity().onBackPressed();
            }

            @Override
            public void onResponse401(Response<PostFarmerPersonalInfo> response) throws JSONException {
            }


        });
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {

            case R.id.ddl_state:
                stateId = stateNames.get(position).getId();
                if (stateId != 0)
                    getDistrictList(stateId);
                break;
            case R.id.ddl_district:
                districtId = districtNames.get(position).getId();
                if (districtId != 0)
                    getBlockList(districtId);
                break;
            case R.id.ddl_block:
                blockId = blockNames.get(position).getId();
                if (blockId != 0)
                    getPanchayatList(blockId);
                break;
            case R.id.ddl_panchayat:
                panchayatId = panchayatNames.get(position).getId();
                if (panchayatId != 0)
                    getVillageList(panchayatId);
                break;
            case R.id.ddl_village:
                villageId = villageNames.get(position).getId();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void getStateList() {
        stateList.clear();
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<GetIdNames> call = client.getStateRegister();
        call.enqueue(new ApiCallback<GetIdNames>() {

            @Override
            public void onResponse(Response<GetIdNames> response) throws JSONException {
                AppUtils.hideProgressDialog();
                if (response.body() == null)
                    return;
                if (response.body().getData() == null)
                    return;
                GetIdNames.GetData getData = new GetIdNames.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                stateNames = response.body().getData();
                stateNames.add(0, getData);
                for (int i = 0; i < stateNames.size(); i++) {
                    stateList.add(stateNames.get(i).getName());
                }
                if (getActivity() != null && isAdded()) {
                    ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(getActivity(), R.layout.template_spinner_text, stateList);
                    stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    ddl_state.setAdapter(stateAdapter);
                }
                if (AppPreference.getInstance().getSTATE().size() != 0) {
                    int id = getSelectedId(stateNames, Long.valueOf(AppPreference.getInstance().getSTATE().get(0)));
                    ddl_state.setSelection(id);
                }
            }

            @Override
            public void onResponse401(Response<GetIdNames> response) throws JSONException {

            }

        });
    }


    private void getDistrictList(final int stateId) {
        distrcitList.clear();
        AppRestClient client = AppRestClient.getInstance();
        Call<GetIdNames> call = client.getDistrictRegister(stateId);
        call.enqueue(new ApiCallback<GetIdNames>() {

            @Override
            public void onResponse(Response<GetIdNames> response) throws JSONException {
                if (response.body() == null)
                    return;
                if (response.body().getData() == null)
                    return;
                GetIdNames.GetData getData = new GetIdNames.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                districtNames = response.body().getData();
                districtNames.add(0, getData);
                for (int i = 0; i < districtNames.size(); i++) {
                    distrcitList.add(districtNames.get(i).getName());
                }
                if (getActivity() != null && isAdded()) {
                    ArrayAdapter<String> districtAdapter = new ArrayAdapter<String>(getActivity(), R.layout.template_spinner_text, distrcitList);
                    districtAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    ddl_district.setAdapter(districtAdapter);
                }
                if (AppPreference.getInstance().getDISTRICT().size() != 0) {
                    int id = getSelectedId(districtNames, Long.valueOf(AppPreference.getInstance().getDISTRICT().get(0)));
                    ddl_district.setSelection(id);
                }
            }

            @Override
            public void onResponse401(Response<GetIdNames> response) throws JSONException {

            }


        });
    }

    private void getBlockList(final int districtId) {
        blockList.clear();
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<GetIdNames> call = client.getBlockRegister(districtId);
        call.enqueue(new ApiCallback<GetIdNames>() {

            @Override
            public void onResponse(Response<GetIdNames> response) throws JSONException {
                AppUtils.hideProgressDialog();
                if (response.body() == null)
                    return;
                if (response.body().getData() == null)
                    return;
                GetIdNames.GetData getData = new GetIdNames.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                blockNames = response.body().getData();
                blockNames.add(0, getData);
                for (int i = 0; i < blockNames.size(); i++) {
                    blockList.add(blockNames.get(i).getName());
                }
                if (getActivity() != null && isAdded()) {
                    ArrayAdapter<String> blockAdapter = new ArrayAdapter<String>(getActivity(), R.layout.template_spinner_text, blockList);
                    blockAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    ddl_block.setAdapter(blockAdapter);
                }
                if (AppPreference.getInstance().getBLOCK().size() != 0) {
                    if (AppPreference.getInstance().getDISTRICT().size() != 0)
                        if (AppPreference.getInstance().getDISTRICT().get(0).equals("" + districtId)) {
                            int id = getSelectedId(blockNames, Long.valueOf(AppPreference.getInstance().getBLOCK().get(0)));
                            ddl_block.setSelection(id);
                        }
                }
            }

            @Override
            public void onResponse401(Response<GetIdNames> response) throws
                    JSONException {

            }


        });
    }

    private void getPanchayatList(int panchayatId) {
        panchayatList.clear();
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<GetIdNames> call = client.getPanchayatRegister(panchayatId);
        call.enqueue(new ApiCallback<GetIdNames>() {

            @Override
            public void onResponse(Response<GetIdNames> response) throws JSONException {
                AppUtils.hideProgressDialog();
                if (response.body() == null)
                    return;
                if (response.body().getData() == null)
                    return;
                GetIdNames.GetData getData = new GetIdNames.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                panchayatNames = response.body().getData();
                panchayatNames.add(0, getData);
                for (int i = 0; i < panchayatNames.size(); i++) {
                    panchayatList.add(panchayatNames.get(i).getName());
                }
                if (getActivity() != null && isAdded()) {
                    ArrayAdapter<String> panchayatAdapter = new ArrayAdapter<String>(getActivity(), R.layout.template_spinner_text, panchayatList);
                    panchayatAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    ddl_panchayat.setAdapter(panchayatAdapter);
                }
                if (AppPreference.getInstance().getPANCHAYAT().size() != 0) {
                    if (AppPreference.getInstance().getBLOCK().size() != 0)
                        if (AppPreference.getInstance().getBLOCK().get(0).equals("" + blockId)) {
                            int id = getSelectedId(panchayatNames,
                                    Long.valueOf(AppPreference.getInstance().getPANCHAYAT().get(0)));
                            ddl_panchayat.setSelection(id);
                        }
                }
            }

            @Override
            public void onResponse401(Response<GetIdNames> response) throws JSONException {

            }


        });
    }

    private void getVillageList(int blockId) {
        villageList.clear();
        AppRestClient client = AppRestClient.getInstance();
        Call<GetIdNames> call = client.getVillageRegister(blockId);
        call.enqueue(new ApiCallback<GetIdNames>() {

            @Override
            public void onResponse(Response<GetIdNames> response) throws JSONException {
                if (response.body() == null)
                    return;
                if (response.body().getData() == null)
                    return;
                GetIdNames.GetData getData = new GetIdNames.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                villageNames = response.body().getData();
                villageNames.add(0, getData);
                for (int i = 0; i < villageNames.size(); i++) {
                    villageList.add(villageNames.get(i).getName());
                }

                if (getActivity() != null && isAdded()) {
                    ArrayAdapter<String> villageAdapter = new ArrayAdapter<String>
                            (getActivity(), R.layout.template_spinner_text, villageList);
                    villageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    ddl_village.setAdapter(villageAdapter);
                }
                if (AppPreference.getInstance().getVILLAGE().size() != 0) {
                    if (AppPreference.getInstance().getPANCHAYAT().size() != 0)
                        if (AppPreference.getInstance().getPANCHAYAT().get(0).equals("" + panchayatId)) {
                            int id = getSelectedId(villageNames, Long.valueOf(AppPreference.getInstance().getVILLAGE().get(0)));
                            ddl_village.setSelection(id);
                        }
                }
            }

            @Override
            public void onResponse401(Response<GetIdNames> response) throws JSONException {

            }


        });
    }
}
