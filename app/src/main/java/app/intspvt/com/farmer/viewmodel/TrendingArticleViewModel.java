package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import app.intspvt.com.farmer.repository.TrendingArticleRepository;
import app.intspvt.com.farmer.rest.response.TrendingData;

public class TrendingArticleViewModel extends ViewModel {

    private MutableLiveData<ArrayList<TrendingData>> trendingList;
    private TrendingArticleRepository trendingArticleRepository;

    public void init() {
        if (trendingArticleRepository == null)
            trendingArticleRepository = TrendingArticleRepository.getInstance();
        trendingList = trendingArticleRepository.getTrendingData();
    }

    public LiveData<ArrayList<TrendingData>> getTrendingData() {
        return trendingList;
    }
}
