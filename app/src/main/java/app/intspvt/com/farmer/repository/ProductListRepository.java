package app.intspvt.com.farmer.repository;

import androidx.lifecycle.MutableLiveData;

import java.net.ConnectException;
import java.util.ArrayList;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.response.Products;
import app.intspvt.com.farmer.rest.response.ProductsList;
import app.intspvt.com.farmer.utilities.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListRepository {
    private static ProductListRepository mInstance;
    private ArrayList<Products> productsArrayList = new ArrayList<>();

    public static ProductListRepository getInstance() {
        if (mInstance == null)
            mInstance = new ProductListRepository();
        return mInstance;
    }

    public MutableLiveData<ArrayList<Products>> getProductData(String ids) {
        final MutableLiveData<ArrayList<Products>> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<ProductsList> call = client.productList(ids);
        call.enqueue(new Callback<ProductsList>() {
            @Override
            public void onResponse(Call<ProductsList> call, Response<ProductsList> response) {
                if (response.body() == null)
                    return;
                productsArrayList = new ArrayList<>();
                if (response.code() == 200) {
                    if (response.body().getProducts() != null) {
                        productsArrayList = response.body().getProducts();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }
                data.setValue(productsArrayList);
            }

            @Override
            public void onFailure(Call<ProductsList> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Farmer.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Farmer.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Farmer.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(new ArrayList<>());
            }


        });
        return data;
    }
}
