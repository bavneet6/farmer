package app.intspvt.com.farmer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.response.CropsData;
import app.intspvt.com.farmer.utilities.CropShowMandi;

public class RecyclerAdapterMandiAllCrops extends RecyclerView.Adapter<RecyclerAdapterMandiAllCrops.MandiData> implements Filterable {
    private Context context;
    private ArrayList<CropsData.CropsInfo> cropsInfos;
    private ArrayList<CropsData.CropsInfo> filterCropsInfos;
    private CropShowMandi cropShowMandi;

    public RecyclerAdapterMandiAllCrops(Context context, ArrayList<CropsData.CropsInfo> cropsInfos, CropShowMandi cropShowMandi) {
        this.context = context;
        this.cropsInfos = cropsInfos;
        this.filterCropsInfos = cropsInfos;
        this.cropShowMandi = cropShowMandi;

    }

    @Override
    public RecyclerAdapterMandiAllCrops.MandiData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_mandi_all_crops, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterMandiAllCrops.MandiData(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterMandiAllCrops.MandiData holder, int position) {
        final int pos = holder.getAdapterPosition();
        holder.cropName.setText(cropsInfos.get(pos).getName());
        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> cropData = new ArrayList<>();
                cropData.add("" + cropsInfos.get(pos).getID());
                cropData.add(cropsInfos.get(pos).getName());
                cropShowMandi.onSelect(cropData);
            }
        });
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    cropsInfos = filterCropsInfos;
                } else {
                    ArrayList<CropsData.CropsInfo> filteredList = new ArrayList<>();
                    for (CropsData.CropsInfo list : filterCropsInfos) {

                        if (list.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(list);
                        }
                    }
                    cropsInfos = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = cropsInfos;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                cropsInfos = (ArrayList<CropsData.CropsInfo>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    @Override
    public int getItemCount() {
        return cropsInfos.size();
    }

    public class MandiData extends RecyclerView.ViewHolder {
        TextView cropName;
        LinearLayout back;

        public MandiData(View itemView) {
            super(itemView);
            back = itemView.findViewById(R.id.back);
            cropName = itemView.findViewById(R.id.cropName);
        }
    }
}
