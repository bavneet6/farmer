package app.intspvt.com.farmer.utilities;

import android.app.Activity;

public interface ShowCartManual {
    void showCartManual(Activity activity);
}
