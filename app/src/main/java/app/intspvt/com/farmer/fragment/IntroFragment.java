package app.intspvt.com.farmer.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import app.intspvt.com.farmer.R;

public class IntroFragment extends BaseFragment {
    public static final String TAG = IntroFragment.class.getSimpleName();
    private static final int PERMISSION_REQUEST_CODE = 1;
    private Button image;
    private ImageView next;

    public static IntroFragment newInstance() {
        return new IntroFragment();
    }

    public static boolean checkPermission(Activity context) {
        boolean result = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)
                    + ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
                    + ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                    + ContextCompat.checkSelfPermission(context, Manifest.permission.RECEIVE_SMS)
                    + ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO)
                    + ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    + ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale
                        (context, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale
                                (context, Manifest.permission.INTERNET) ||
                        ActivityCompat.shouldShowRequestPermissionRationale
                                (context, Manifest.permission.ACCESS_FINE_LOCATION) ||
                        ActivityCompat.shouldShowRequestPermissionRationale
                                (context, Manifest.permission.RECORD_AUDIO) ||
                        ActivityCompat.shouldShowRequestPermissionRationale
                                (context, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(
                                context, Manifest.permission.READ_PHONE_STATE)) {
                    Toast.makeText(context, "Please Grant Permissions", Toast.LENGTH_LONG).show();
                } else {
                    ActivityCompat.requestPermissions(context, new String[] {
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                            Manifest.permission.INTERNET,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.READ_PHONE_STATE,
                        }, PERMISSION_REQUEST_CODE);
                }
                result = false;
            }

        }
        return result;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_intro, null);
        next = v.findViewById(R.id.next);
        image = v.findViewById(R.id.image);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next.setVisibility(View.GONE);
                image.setVisibility(View.VISIBLE);
                final Animation animation = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.scale);
                Handler handler = new Handler();
                Runnable r = new Runnable() {
                    public void run() {

                        image.startAnimation(animation);
                        animation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                getFragmentManager().beginTransaction().replace(R.id.frag_cont, LanguageFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    }
                };
                handler.postDelayed(r, 300);
            }
        });
        checkPermission(getActivity());
        return v;
    }
}
