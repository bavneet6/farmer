package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import app.intspvt.com.farmer.repository.SingleTrendArticleRepository;
import app.intspvt.com.farmer.rest.response.TrendingData;

public class SingleTrendArticleViewModel extends ViewModel {
    private MutableLiveData<TrendingData> singleTrendData;
    private SingleTrendArticleRepository singleTrendArticleRepository;

    public void init(Long id) {
        if (singleTrendArticleRepository == null)
            singleTrendArticleRepository = SingleTrendArticleRepository.getInstance();
        singleTrendData = singleTrendArticleRepository.getSingleTrendData(id);
    }

    public LiveData<TrendingData> getSingleTrendData() {
        return singleTrendData;
    }
}
