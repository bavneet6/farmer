package app.intspvt.com.farmer.rest.response;

/**
 * Created by DELL on 3/29/2018.
 */

public class IssueDbStatus {

    private Long issue_id;
    private int issue_unseen;

    public Long getIssue_id() {
        return issue_id;
    }

    public void setIssue_id(Long issue_id) {
        this.issue_id = issue_id;
    }


    public int getIssue_unseen() {
        return issue_unseen;
    }

    public void setIssue_unseen(int issue_unseen) {
        this.issue_unseen = issue_unseen;
    }
}
