package app.intspvt.com.farmer.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.List;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.fragment.CartFragment;
import app.intspvt.com.farmer.fragment.ProductListFragment;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.rest.response.Promotions;
import app.intspvt.com.farmer.utilities.AppUtils;

import static app.intspvt.com.farmer.utilities.AppUtils.isNullCase;

public class RecyclerAdapterPromotions extends RecyclerView.Adapter<RecyclerAdapterPromotions.Promotion> {
    private Context context;
    private List<Promotions.PromotionsData> promotionsData;
    private String getPic;
    private DatabaseHandler databaseHandler;

    public RecyclerAdapterPromotions(Activity activity, List<Promotions.PromotionsData> promotionsData) {
        this.context = activity;
        this.promotionsData = promotionsData;
        databaseHandler = new DatabaseHandler(context);

    }

    @Override
    public RecyclerAdapterPromotions.Promotion onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_promotions, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterPromotions.Promotion(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterPromotions.Promotion holder, final int position) {
        final int pos = holder.getAdapterPosition();
        if (!AppUtils.isNullCase(promotionsData.get(pos).getPromo_image()))
            showPicassoImage(pos, holder.promoImage);


        holder.promoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (databaseHandler.getAllCartData().size() != 0) {
                    CartFragment fragment = CartFragment.newInstance();
                    AppUtils.changeFragment((FragmentActivity) context, fragment);
                } else {
                    ProductListFragment fragment = ProductListFragment.newInstance();
                    AppUtils.changeFragment((FragmentActivity) context, fragment);
                }

            }
        });
    }

    private void showPicassoImage(int pos, final ImageView imageView) {
        AppUtils.generateAndCheckUrl(context, promotionsData.get(pos).getPromo_image());
        String photo = promotionsData.get(pos).getPromo_image();
        getPic = null;
        if (!isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(imageView, new PicassoCallback(photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).into(imageView);
                    }
                });

            } else {
                imageView.setImageResource(0);
            }
        }
    }


    @Override
    public int getItemCount() {
        return promotionsData.size();
    }

    public class Promotion extends RecyclerView.ViewHolder {
        ImageView promoImage;

        public Promotion(View itemView) {
            super(itemView);
            promoImage = itemView.findViewById(R.id.promoImage);

        }

    }

}

