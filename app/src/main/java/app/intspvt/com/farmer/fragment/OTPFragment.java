package app.intspvt.com.farmer.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;

import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.body.SendNumber;
import app.intspvt.com.farmer.rest.response.CropsData;
import app.intspvt.com.farmer.rest.response.FarmerInfo;
import app.intspvt.com.farmer.rest.response.GetPersonalInfo;
import app.intspvt.com.farmer.rest.response.LoginResponse;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.MySMSBroadcastReceiver;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DELL on 7/10/2017.
 */

public class OTPFragment extends BaseFragment implements View.OnFocusChangeListener, TextWatcher, MySMSBroadcastReceiver.OTPReceiveListener, View.OnClickListener {
    public static final String TAG = OTPFragment.class.getSimpleName();

    private EditText otp1, otp2, otp3, otp4;
    private int focus = 0;
    private Button next;
    private MySMSBroadcastReceiver broadcastReceiver = new MySMSBroadcastReceiver();
    private String otp = "";
    private TextView resend;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ArrayList<CropsData.CropsInfo> farmerCropsInfo = new ArrayList<>();
    private MySMSBroadcastReceiver.OTPReceiveListener otpReceiveListener = this;

    public static OTPFragment newInstance() {
        return new OTPFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_otp, null);
        otp1 = v.findViewById(R.id.otp1);
        otp2 = v.findViewById(R.id.otp2);
        otp3 = v.findViewById(R.id.otp3);
        otp4 = v.findViewById(R.id.otp4);
        next = v.findViewById(R.id.next);
        resend = v.findViewById(R.id.resend);
        otp1.setOnFocusChangeListener(this);
        otp1.addTextChangedListener(this);
        otp2.setOnFocusChangeListener(this);
        otp2.addTextChangedListener(this);
        otp3.setOnFocusChangeListener(this);
        otp3.addTextChangedListener(this);
        otp4.setOnFocusChangeListener(this);
        otp4.addTextChangedListener(this);

        resend.setOnClickListener(this);
        next.setOnClickListener(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Bundle params = new Bundle();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String format = s.format(new Date());
        params.putString("OTP", format + "   " + AppPreference.getInstance().getMobile());
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        mFirebaseAnalytics.logEvent("OTP", params);
        initiateSmsRetriever();
        broadcastReceiver.initOTPListener(otpReceiveListener);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
        sendMobileNumber(AppPreference.getInstance().getMobile());
        return v;
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * check if length of otp is correct or not
     */
    private void verifyOTP() {
        otp = otp1.getText().toString() + otp2.getText().toString() + otp3.getText().toString() + otp4.getText().toString();
        AppUtils.showProgressDialog(getActivity());
        SendNumber sendNumber = new SendNumber(AppPreference.getInstance().getMobile(), otp);
        AppRestClient client = AppRestClient.getInstance();
        Call<LoginResponse> call = client.verifyNumber(sendNumber);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    AppPreference.getInstance().setAuthToken(response.body().getAuth_token());
                    if (getActivity() == null)
                        return;
                    getUserLoginInfo();
                } else if (response.code() == 401)
                    AppUtils.showToast(getString(R.string.correct_otp));
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
            }

        });

    }

    /**
     * get info of a user and then check whether user already exist or created and then show the fragments
     * if new user registration form is shown
     * existing user taken to the home page
     */
    private void getUserLoginInfo() {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        Call<Void> call = client.getUserLoginInfo();
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                AppUtils.hideProgressDialog();
                if (getActivity() == null)
                    return;
                if (response.code() == 403) {
                    final Dialog dialog = new Dialog(getActivity());
                    final TextView link;
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_dehaati_login);
                    link = dialog.findViewById(R.id.dehaati_link);

                    link.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.intspvt.app.dehaat2&amp;hl=en")));
                            dialog.dismiss();
                        }
                    });

                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    dialog.getWindow().setGravity(Gravity.CENTER);
                    dialog.show();


                } else if (response.code() == 404) {
                    FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    transaction.replace(R.id.frag_cont, PincodeFragment.newInstance()).commitAllowingStateLoss();
                } else if (response.code() == 200) {
                    getFarmerCropList();

                } else {
                    AppUtils.showToast(getString(R.string.tech_prob));
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                onFailureMethod(t);
            }
        });

    }

    private void getFarmerCropList() {
        AppRestClient client = AppRestClient.getInstance();
        Call<CropsData> call = client.getFarmerCropList();
        call.enqueue(new ApiCallback<CropsData>() {
            @Override
            public void onResponse(Response<CropsData> response) {
                AppUtils.showProgressDialog(getActivity());
                if (response.body() == null)
                    return;
                if (response.body().getCropsInfo() != null)
                    farmerCropsInfo = response.body().getCropsInfo();
                getFarmerInfo();

            }

            @Override
            public void onResponse401(Response<CropsData> response) throws JSONException {

            }


        });
    }

    private void getFarmerInfo() {
        AppRestClient client = AppRestClient.getInstance();
        Call<GetPersonalInfo> call = client.getFarmerInfo();
        call.enqueue(new ApiCallback<GetPersonalInfo>() {
            @Override
            public void onResponse(Response<GetPersonalInfo> response) {
                if (response.body() == null)
                    return;
                if (response.body().getData().getNode_info() != null) {
                    AppUtils.storeNodeData(response.body().getData().getNode_info());
                }
                if (response.body().getData().getFarmer_info() != null) {
                    FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                    FarmerInfo farmer_info;

                    farmer_info = response.body().getData().getFarmer_info();
                    if (AppUtils.isNullCase(String.valueOf(farmer_info.getZip()))) {
                        PincodeFragment fragment = new PincodeFragment();
                        Bundle bundle = new Bundle();
                        if (farmerCropsInfo != null && farmerCropsInfo.size() == 0)
                            bundle.putBoolean("HAS_CROPS", false);
                        else
                            bundle.putBoolean("HAS_CROPS", true);
                        fragment.setArguments(bundle);
                        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                        transaction.replace(R.id.frag_cont, fragment).commitAllowingStateLoss();
                    } else if (farmerCropsInfo != null && farmerCropsInfo.size() == 0) {
                        CropSelectionFragment fragment = new CropSelectionFragment();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("HAS_CROPS", false);
                        fragment.setArguments(bundle);
                        transaction.replace(R.id.frag_cont, fragment).commitAllowingStateLoss();
                    } else {
                        AppPreference.getInstance().setAPP_LOGIN(true);
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                    }
                }
            }

            @Override
            public void onResponse401(Response<GetPersonalInfo> response) throws
                    JSONException {
                AppUtils.showSessionExpiredDialog(activity);

            }


        });

    }

    private void sendMobileNumber(final String mobile) {
        AppUtils.showProgressDialog(getActivity());
        SendNumber sendNumber = new SendNumber(mobile, null);
        AppRestClient client = AppRestClient.getInstance();
        Call<Void> call = client.sendNumber(sendNumber);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                AppUtils.hideProgressDialog();

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }

        });
    }

    private void initiateSmsRetriever() {
        SmsRetrieverClient client = SmsRetriever.getClient(getActivity());
        Task<Void> task = client.startSmsRetriever();
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Successfully started retriever, expect broadcast intent
                // ...
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Failed to start retriever, inspect Exception for more details
                // ...
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        switch (v.getId()) {
            case R.id.otp1:
                focus = 1;
                break;
            case R.id.otp2:
                focus = 2;
                break;
            case R.id.otp3:
                focus = 3;
                break;
            case R.id.otp4:
                focus = 4;
                break;

        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        switch (focus) {
            case 1:
                if (otp1.getText().toString().length() == 1)
                    otp2.requestFocus();
                else
                    otp1.requestFocus();

                checkOTP();
                break;
            case 2:
                if (otp2.getText().toString().length() == 1)
                    otp3.requestFocus();
                else
                    otp2.requestFocus();

                checkOTP();
                break;

            case 3:
                if (otp3.getText().toString().length() == 1)
                    otp4.requestFocus();
                else
                    otp3.requestFocus();
                checkOTP();
                break;

            case 4:
                if (otp4.getText().toString().length() == 1) {
                    otp4.requestFocus();
                    verifyOTP();
                }
                checkOTP();
                break;
        }

    }

    private void checkOTP() {
        otp = otp1.getText().toString() + otp2.getText().toString() + otp3.getText().toString() + otp4.getText().toString();
        if (getActivity() == null)
            return;
        if (otp.length() == 4) {
            next.setBackgroundColor(getActivity().getResources().getColor(R.color.baseColor));
        } else {
            next.setBackgroundColor(getActivity().getResources().getColor(R.color.grey));
        }
    }

    private void onFailureMethod(Throwable t) {
        AppUtils.hideProgressDialog();
        if (t instanceof ConnectException) {
            if (!AppUtils.haveNetworkConnection(getActivity()))
                AppUtils.showToast(getString(R.string.no_internet));
            else
                AppUtils.showToast(getString(R.string.server_no_respond));
        }
    }

    @Override
    public void onOTPReceived(String otp) {
        if (otp == null)
            return;
        String otpString = otp.substring(18, 22);
        if (!AppUtils.isNullCase(otpString)) {
            otp1.setText(otpString.substring(0, 1));
            otp2.setText(otpString.substring(1, 2));
            otp3.setText(otpString.substring(2, 3));
            otp4.setText(otpString.substring(3, 4));
            verifyOTP();
        }
        if (broadcastReceiver != null && getActivity() != null) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    public void onOTPTimeOut() {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.resend:
                sendMobileNumber(AppPreference.getInstance().getMobile());
                break;
            case R.id.next:
                if (otp.length() != 4)
                    AppUtils.showToast(getString(R.string.enter_otp));
                else
                    verifyOTP();
                break;
        }
    }
}