package app.intspvt.com.farmer.fragment;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.rest.response.CropManualData;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;

import static app.intspvt.com.farmer.utilities.AppUtils.isNullCase;

public class SingleManualContentFragment extends BaseFragment {
    public static final String TAG = SingleManualContentFragment.class.getSimpleName();
    private ImageView cropImage;
    private TextView cropTitle, cropStage, cropInfoType, interCropping, diseaseNames;
    private WebView cropContent, cropActionStep, cropActionBenefit, cropGovnScheme;
    private LinearLayout action_step_back, action_benefit_back, govn_schemes_back, intercropping_back, disease_back;
    private String getPic;
    private DatabaseHandler databaseHandler;
    private CropManualData cropManualData;

    public static SingleManualContentFragment newInstance() {
        return new SingleManualContentFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_single_manual_content, null);
        MainActivity.showCallAndHome(true);
        action_step_back = v.findViewById(R.id.action_step_back);
        action_benefit_back = v.findViewById(R.id.action_benefit_back);
        govn_schemes_back = v.findViewById(R.id.govn_schemes_back);
        intercropping_back = v.findViewById(R.id.intercropping_back);
        disease_back = v.findViewById(R.id.disease_back);
        cropTitle = v.findViewById(R.id.cropTitle);
        interCropping = v.findViewById(R.id.interCropping);
        diseaseNames = v.findViewById(R.id.diseaseNames);
        cropStage = v.findViewById(R.id.cropStage);
        cropInfoType = v.findViewById(R.id.cropInfoType);
        cropContent = v.findViewById(R.id.cropContent);
        cropActionStep = v.findViewById(R.id.cropActionStep);
        cropActionBenefit = v.findViewById(R.id.cropActionBenefit);
        cropGovnScheme = v.findViewById(R.id.cropGovnScheme);
        cropImage = v.findViewById(R.id.cropImage);
        cropContent.getSettings().setJavaScriptEnabled(true); // enable javascript
        databaseHandler = new DatabaseHandler(getActivity());
        Bundle bundle = getArguments();
        if (bundle != null) {
            cropManualData = (CropManualData) bundle.getSerializable("MANUAL_DATA");
            displayManualData(cropManualData);
        }

        return v;
    }

    private void displayManualData(CropManualData cropManual) {
        cropInfoType.setText(cropManual.getInfo_type());
        cropStage.setText(cropManual.getStage());
        cropTitle.setText(cropManual.getName());
        loadWebsite(cropContent, cropManual.getContent());

        if (!AppUtils.isNullCase(cropManual.getAction_step())) {
            action_step_back.setVisibility(View.VISIBLE);
            loadWebsite(cropActionStep, cropManual.getAction_step());
        }
        if (!AppUtils.isNullCase(cropManual.getAction_benefit())) {
            action_benefit_back.setVisibility(View.VISIBLE);
            loadWebsite(cropActionBenefit, cropManual.getAction_benefit());
        }
        if (!AppUtils.isNullCase(cropManual.getGovernment_scheme())) {
            govn_schemes_back.setVisibility(View.VISIBLE);
            loadWebsite(cropGovnScheme, cropManual.getGovernment_scheme());
        }
        if (cropManual.getIntercropping().size() != 0) {
            intercropping_back.setVisibility(View.VISIBLE);
            String crops = "";
            for (int i = 0; i < cropManual.getIntercropping().size(); i++)
                crops = crops + cropManual.getIntercropping().get(i) + "\n";

            interCropping.setText(crops);
        }
        if (cropManual.getDisease().size() != 0) {
            disease_back.setVisibility(View.VISIBLE);
            String crops = "";
            for (int i = 0; i < cropManual.getDisease().size(); i++)
                if (i != cropManual.getDisease().size() - 1)
                    crops = crops + cropManual.getDisease().get(i) + "\n";
                else
                    crops = crops + cropManual.getDisease().get(i);

            diseaseNames.setText(crops);
        }

        if (!AppUtils.isNullCase(cropManual.getAttachments())) {
            cropImage.setVisibility(View.VISIBLE);
            showPicassoImage(getActivity(), cropManual.getAttachments());
        } else
            cropImage.setVisibility(View.GONE);

    }

    private void loadWebsite(WebView webView, String data) {
        webView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getActivity(), description, Toast.LENGTH_SHORT).show();
            }
        });
        Map<String, String> extraHeaders;
        extraHeaders = new HashMap<String, String>();
        extraHeaders.put("Authorization", "Bearer " + AppPreference.getInstance().getAuthToken());
        WebSettings settings = webView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        webView.loadDataWithBaseURL(null, data, "text/html", "UTF-8", null);
    }

    private void showPicassoImage(Activity context, String attachments) {
        AppUtils.generateAndCheckUrl(context, attachments);
        getPic = null;
        if (!isNullCase(attachments)) {
            getPic = databaseHandler.getFileUrl(attachments);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(cropImage, new PicassoCallback(attachments) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).into(cropImage);
                    }
                });

            } else
                cropImage.setImageResource(0);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

}
