package app.intspvt.com.farmer.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterOutputSaleHistory;
import app.intspvt.com.farmer.rest.response.OutputSaleData;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.viewmodel.OutputSaleHistoryViewModel;

public class OutputSalesQueryHistoryFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = OutputSalesQueryHistoryFragment.class.getSimpleName();
    private FirebaseAnalytics mFirebaseAnalytics;
    private ImageView back;
    private TextView createNew, no_data_mssg;
    private RecyclerView output_history;
    private OutputSaleHistoryViewModel outputSaleHistoryViewModel;

    public static OutputSalesQueryHistoryFragment newInstance() {
        return new OutputSalesQueryHistoryFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_output_query_history, null);
        MainActivity.showCallAndHome(true);
        back = v.findViewById(R.id.back);
        createNew = v.findViewById(R.id.createNew);
        no_data_mssg = v.findViewById(R.id.no_data_mssg);
        output_history = v.findViewById(R.id.output_history);
        output_history.setNestedScrollingEnabled(false);
        back.setOnClickListener(this);
        createNew.setOnClickListener(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());

        AppUtils.showProgressDialog(getActivity());
        outputSaleHistoryViewModel = ViewModelProviders.of(getActivity()).get(OutputSaleHistoryViewModel.class);
        outputSaleHistoryViewModel.init();
        observeViewModel(outputSaleHistoryViewModel);

        return v;
    }

    private void observeViewModel(OutputSaleHistoryViewModel viewModel) {
        viewModel.getOutputSaleHistoryList()
                .observe(this, outputSaleHistoryList -> {
                    AppUtils.hideProgressDialog();
                    if (outputSaleHistoryList != null && outputSaleHistoryList.size() > 0) {
                        no_data_mssg.setVisibility(View.GONE);
                        displayHistoryData(outputSaleHistoryList);
                    } else
                        no_data_mssg.setVisibility(View.VISIBLE);
                });
    }

    private void displayHistoryData(ArrayList<OutputSaleData> outputData) {
        if (getActivity() != null && isAdded()) {
            output_history.setAdapter(new RecyclerAdapterOutputSaleHistory(getActivity(), outputData));
            output_history.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("OutputSaleHistory", Long.parseLong(AppPreference.getInstance().getMobile()));
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        mFirebaseAnalytics.logEvent("OutputSaleHistory", params);
    }

    @Override
    public void onPause() {
        super.onPause();

    }


    @Override
    public void onClick(View view) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        int id = view.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.createNew:
                OutputSalesQueryFragment fragment = OutputSalesQueryFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putLong("ENTITY_ID", 0);
                fragment.setArguments(bundle);
                transaction.replace(R.id.frag_container, fragment).addToBackStack("").commitAllowingStateLoss();
                break;
        }
    }
}
