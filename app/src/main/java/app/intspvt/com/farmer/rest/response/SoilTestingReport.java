package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SoilTestingReport {
    @SerializedName("data")
    public ArrayList<Data> data;

    public ArrayList<Data> getData() {
        return data;
    }

    public class Data {
        @SerializedName("image")
        public String image;

        public String getImage() {
            return image;
        }
    }
}
