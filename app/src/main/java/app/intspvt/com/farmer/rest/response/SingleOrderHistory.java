package app.intspvt.com.farmer.rest.response;

import com.google.gson.annotations.SerializedName;

public class SingleOrderHistory {
    @SerializedName("data")
    private OrderDataHistory data;

    public OrderDataHistory getData() {
        return data;
    }
}
