package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import app.intspvt.com.farmer.repository.WeatherCropsRepository;
import app.intspvt.com.farmer.rest.response.WeatherCropData;

public class WeatherCropsViewModel extends ViewModel {

    private MutableLiveData<ArrayList<WeatherCropData.CropData.CropAdvisory>> weatherCropList;
    private WeatherCropsRepository weatherCropsRepository;

    public void init() {
        if (weatherCropsRepository == null)
            weatherCropsRepository = WeatherCropsRepository.getInstance();
        weatherCropList = weatherCropsRepository.getWeatherCropData();
    }

    public LiveData<ArrayList<WeatherCropData.CropData.CropAdvisory>> getWeatherCropData() {
        return weatherCropList;
    }
}
