package app.intspvt.com.farmer.rest.body;

import com.google.gson.annotations.SerializedName;

public class OutputData {
    @SerializedName("crop_id")
    private Long crop_id;

    @SerializedName("crop_var")
    private String crop_var;

    @SerializedName("crop_qty")
    private float crop_qty;

    @SerializedName("crop_uom_id")
    private int crop_uom_id;

    @SerializedName("price_unit")
    private float price_unit;

    @SerializedName("expected_date")
    private String expected_date;
    @SerializedName("available_days")
    private int available_days;
    @SerializedName("notes")
    private String notes;
    @SerializedName("id")
    private Long id;

    public OutputData(Long id, Long crop_id, String crop_var, float crop_qty, int crop_uom_id, float price_unit, String expected_date
            , int available_days, String notes) {
        this.id = id;
        this.crop_id = crop_id;
        this.crop_var = crop_var;
        this.crop_qty = crop_qty;
        this.crop_uom_id = crop_uom_id;
        this.price_unit = price_unit;
        this.expected_date = expected_date;
        this.available_days = available_days;
        this.notes = notes;

    }
}
