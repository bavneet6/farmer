package app.intspvt.com.farmer.rest;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.intspvt.com.farmer.rest.body.PostFarmerPersonalInfo;
import app.intspvt.com.farmer.rest.body.SendCartData;
import app.intspvt.com.farmer.rest.body.SendCropData;
import app.intspvt.com.farmer.rest.body.SendNumber;
import app.intspvt.com.farmer.rest.response.CropManuals;
import app.intspvt.com.farmer.rest.response.CropsData;
import app.intspvt.com.farmer.rest.response.EnquiryHistory;
import app.intspvt.com.farmer.rest.response.FcmToken;
import app.intspvt.com.farmer.rest.response.GetIdNames;
import app.intspvt.com.farmer.rest.response.GetOrderResponse;
import app.intspvt.com.farmer.rest.response.GetPersonalInfo;
import app.intspvt.com.farmer.rest.response.LandUnits;
import app.intspvt.com.farmer.rest.response.LoginResponse;
import app.intspvt.com.farmer.rest.response.MandiCropPrice;
import app.intspvt.com.farmer.rest.response.MandiPrices;
import app.intspvt.com.farmer.rest.response.OrderHistory;
import app.intspvt.com.farmer.rest.response.OutputSale;
import app.intspvt.com.farmer.rest.response.ProductsList;
import app.intspvt.com.farmer.rest.response.Promotions;
import app.intspvt.com.farmer.rest.response.SingleCropManual;
import app.intspvt.com.farmer.rest.response.SingleIssueHistory;
import app.intspvt.com.farmer.rest.response.SingleOrderHistory;
import app.intspvt.com.farmer.rest.response.SingleOutputSale;
import app.intspvt.com.farmer.rest.response.SingleProduct;
import app.intspvt.com.farmer.rest.response.SingleTrendArticle;
import app.intspvt.com.farmer.rest.response.SoilTestingReport;
import app.intspvt.com.farmer.rest.response.Trending;
import app.intspvt.com.farmer.rest.response.VersionName;
import app.intspvt.com.farmer.rest.response.WeatherCropData;
import app.intspvt.com.farmer.rest.response.WeatherParams;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by DELL on 6/22/2017.
 */

public interface AppRestClientService {
    //1
    @POST("/farmer/generate/otp")
    Call<Void> sendNumber(@Body SendNumber body);

    //2
    @GET("/farmer/user/personal_info")
    Call<GetPersonalInfo> getFarmerInfo();

    //3
    @GET("/farmer/user/land_info")
    Call<GetPersonalInfo> getLandInfo();

    //4
    @GET("/farmer/crop/list")
    Call<CropsData> getCropList();

    //5
    @GET("/farmer/user/crop/list")
    Call<CropsData> getFarmerCropList();


    //6
    @POST("/farmer/update/FCM_token")
    Call<Void> fcmToken(@Body FcmToken fcmToken);

    //7
    @Multipart
    @POST("/farmer/user/info")
    Call<PostFarmerPersonalInfo> postInfo(@Part List<MultipartBody.Part>
                                                  files, @PartMap HashMap<String, RequestBody> body);

    //8
    @Multipart
    @POST("/farmer/user/personal_info/update")
    Call<PostFarmerPersonalInfo> updateFarmerInfo(@Part List<MultipartBody.Part>
                                                          files, @PartMap HashMap<String, RequestBody> body);

    //9
    @POST("/farmer/user/land_info/update")
    Call<PostFarmerPersonalInfo> updateLandInfo(@Body PostFarmerPersonalInfo postFarmerPersonalInfo);

    //10
    @GET("/farmer/product/list")
    Call<ProductsList> productList(@Header("product_ids") String product_ids);

    //12
    @GET("farmer/order/list")
    Call<OrderHistory> orderHistory();

    //13
    @GET("/farmer/trend/article/list")
    Call<Trending> trendingData();

    //14
    @POST("/farmer/order")
    Call<GetOrderResponse> sendCartData(@Body SendCartData body);

    //15
    @POST("/farmer/user/crop")
    Call<CropsData> sendCropData(@Body SendCropData body);

    //16
    @Multipart
    @POST("/farmer/issue")
    Call<Void> sendEnquiryData(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part audio, @Part List<MultipartBody.Part> files);

    //17
    @GET("/farmer/issue/list")
    Call<EnquiryHistory> getEnquiryHistory(@Query("category") String type);

    //18
    @GET("/farmer/version")
    Call<VersionName> getVersionName();

    //19
    @Multipart
    @POST("/farmer/issue/reply")
    Call<Void> sendReply(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part audio, @Part List<MultipartBody.Part> files);

    //20
    @GET("/farmer/issue/{issue_id}")
    Call<SingleIssueHistory> getSingleHistoryData(@Path("issue_id") Long id);


    //21
    @GET("/farmer/user/login_info")
    Call<Void> getUserLoginInfo();


    //22
    @GET("/farmer/state")
    Call<GetIdNames> getStateRegister();
    //23

    @GET("/farmer/district/{state_id}")
    Call<GetIdNames> getDistrictRegister(@Path("state_id") int id);
    //24

    @GET("/farmer/block/{district_id}")
    Call<GetIdNames> getBlockRegister(@Path("district_id") int id);
    //25

    @GET("/farmer/village/{panchayat_id}")
    Call<GetIdNames> getVillageRegister(@Path("panchayat_id") int id);

    //26
    @GET("/farmer/panchayat/{block_id}")
    Call<GetIdNames> getPanchayatRegister(@Path("block_id") int id);

    //27
    @GET("/farmer/output/crop/list")
    Call<CropsData> getOutputCropsList();

    //28
    @GET("/farmer/uom/list")
    Call<GetIdNames> getUOMList();

    //29
    @Multipart
    @POST("/farmer/output/query/create")
    Call<Void> createFarmerOutputSale(@Part List<MultipartBody.Part>
                                              files, @PartMap HashMap<String, RequestBody> body);

    //30
    @Multipart
    @POST("/farmer/output/query/update")
    Call<Void> updateFarmerOutputSale(@Part List<MultipartBody.Part>
                                              files, @PartMap HashMap<String, RequestBody> body);


    //31
    @GET("/farmer/output/query/list")
    Call<OutputSale> getOutputSaleHistory();


    //33
    @GET("/farmer/crop/manual/{id}")
    Call<SingleCropManual> getSingleCropManual(@Path("id") Long id);


    //34
    @GET("/farmer/output/query/{id}")
    Call<SingleOutputSale> getSingleOutputSaleHistory(@Path("id") Long id);

    //35
    @GET("/farmer/weather/parameters")
    Call<WeatherParams> getWeatherParams();

    //36
    @GET("/farmer/weather/crop/advisory")
    Call<WeatherCropData> getWeatherCropData();

    //37
    @GET("/farmer/land/uom/list")
    Call<LandUnits> getLandUnits();

    //38
    @GET("/farmer/promotion/list")
    Call<Promotions> getPromotions();

    //40
    @GET("/farmer/order/{id}")
    Call<SingleOrderHistory> getSingleOrder(@Path("id") Long id); //40

    @GET("/farmer/product/{id}")
    Call<SingleProduct> getSingleProduct(@Path("id") Long id);

    @GET("/farmer/crop/manual/crops")
    Call<CropManuals> getCropManualList();

    //32
    @GET("/farmer/crop/manual/list")
    Call<CropManuals> getCropManuals(@Query("crop_id") Long id, @Header("crop_manual_ids") String manualIds);

    @POST("/farmer/verify/otp")
    Call<LoginResponse> verifyNumber(@Body SendNumber body);

    @Multipart
    @POST("/farmer/soil/testing")
    Call<Void> postSoilTesting(@Part List<MultipartBody.Part>
                                       files, @PartMap HashMap<String, RequestBody> body);

    @GET("/farmer/soil/testing")
    Call<SoilTestingReport> getSoilTesting();

    //33
    @GET("/farmer/trend/article/{id}")
    Call<SingleTrendArticle> geSingleTrendArticle(@Path("id") Long id); //33

    @GET("/farmer/crop/mandi/list/{crop_id}")
    Call<MandiPrices> getCropMandiData(@Path("crop_id") Long id);

    @GET("/farmer/mandi/list/{mandi_id}")
    Call<MandiCropPrice> getMandiPricesData(@Path("mandi_id") Long id);

    @GET("/farmer/node/list/{team_id}")
    Call<MandiCropPrice> getNodePricesData(@Path("team_id") Long id);


    @GET("/farmer/dehaat/order/list")
    Call<OrderHistory> dehaatiOrderHistory();

    //40
    @GET("/farmer/dehaat/order/{id}")
    Call<SingleOrderHistory> getSingleDeHaatOrder(@Path("id") Long id); //40
}
