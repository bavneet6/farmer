package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import app.intspvt.com.farmer.repository.OutputSaleHistoryRepository;
import app.intspvt.com.farmer.rest.response.OutputSaleData;

public class OutputSaleHistoryViewModel extends ViewModel {
    private MutableLiveData<ArrayList<OutputSaleData>> outputSaleHistoryList;
    private OutputSaleHistoryRepository outputSaleHistoryRepository;

    public void init() {
        if (outputSaleHistoryRepository == null)
            outputSaleHistoryRepository = OutputSaleHistoryRepository.getInstance();
        outputSaleHistoryList = outputSaleHistoryRepository.getOutputSaleHistoryList();
    }

    public LiveData<ArrayList<OutputSaleData>> getOutputSaleHistoryList() {
        return outputSaleHistoryList;
    }
}
