package app.intspvt.com.farmer.rest.body;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 6/29/2017.
 */


public class SendNumber {

    @SerializedName("phone_number")
    private String phone_num;
    @SerializedName("otp")
    private String otp;

    public SendNumber(String phone_num, String otp) {
        this.phone_num = phone_num;
        this.otp = otp;

    }


}
