package app.intspvt.com.farmer.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.rest.response.OutputQueryImages;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.DeleteInputCart;

import static app.intspvt.com.farmer.utilities.AppUtils.isNullCase;

/**
 * Created by DELL on 11/29/2017.
 */

public class RecyclerAdapterOutputSaleImageList extends RecyclerView.Adapter<RecyclerAdapterOutputSaleImageList.ImageList> {
    Context context;
    ArrayList<OutputQueryImages> imagesInfoList = new ArrayList<>();
    private DeleteInputCart deleteInputCart;
    private String getPic;
    private DatabaseHandler databaseHandler;


    public RecyclerAdapterOutputSaleImageList(Activity context, ArrayList<OutputQueryImages> imagesInfoList, DeleteInputCart deleteInputCart) {
        this.context = context;
        this.imagesInfoList = imagesInfoList;
        this.deleteInputCart = deleteInputCart;
        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public RecyclerAdapterOutputSaleImageList.ImageList onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_imagelist, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterOutputSaleImageList.ImageList(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterOutputSaleImageList.ImageList holder, final int position) {
        holder.setIsRecyclable(false);
        final int pos = holder.getAdapterPosition();
        if (imagesInfoList.get(pos).isDelete()) {
            holder.cross.setVisibility(View.VISIBLE);
            showBitmapImage(pos, holder.image);
        } else {
            holder.cross.setVisibility(View.GONE);
            showPicassoImage(pos, holder.image);
        }
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setCanceledOnTouchOutside(true);
                final ImageView imageView;
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.image_zoom);
                imageView = dialog.findViewById(R.id.imageView);
                if (imagesInfoList.get(pos).isDelete()) {
                    showBitmapImage(pos, imageView);
                } else {
                    showPicassoImage(pos, imageView);
                }
                dialog.getWindow().setGravity(Gravity.CENTER);
                dialog.show();

            }

        });
        holder.cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagesInfoList.remove(pos);
                deleteInputCart.onItemClick(String.valueOf(holder.getAdapterPosition()));
                notifyDataSetChanged();
            }
        });

    }

    private void showBitmapImage(int pos, ImageView imageView) {

        File file = new File(imagesInfoList.get(pos).getFilePath());
        Bitmap bitmap = new BitmapDrawable(context.getResources(), file.getAbsolutePath()).getBitmap();
        imageView.setImageBitmap(bitmap);
    }

    private void showPicassoImage(int pos, final ImageView imageView) {
        AppUtils.generateAndCheckUrl(context, imagesInfoList.get(pos).getFilePath());
        String photo = imagesInfoList.get(pos).getFilePath();
        getPic = null;
        if (!isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(imageView, new PicassoCallback(photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).into(imageView);
                    }
                });

            } else {
                imageView.setImageResource(0);
            }
        }
    }

    @Override
    public int getItemCount() {
        return imagesInfoList.size();
    }

    public class ImageList extends RecyclerView.ViewHolder {
        private TextView cross;
        private ImageView image;

        public ImageList(View itemView) {
            super(itemView);
            cross = itemView.findViewById(R.id.cross);
            image = itemView.findViewById(R.id.image);
        }


    }


}
