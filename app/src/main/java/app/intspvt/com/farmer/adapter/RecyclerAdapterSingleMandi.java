package app.intspvt.com.farmer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.rest.response.MandiCropPrice;
import app.intspvt.com.farmer.utilities.SellProduce;

public class RecyclerAdapterSingleMandi extends
        RecyclerView.Adapter<RecyclerAdapterSingleMandi.MandiData> {
    private Context context;
    private List<MandiCropPrice.Mandi> cropList;
    private SellProduce sellProduce;
    private String type;

    public RecyclerAdapterSingleMandi(Context context, String type, List<MandiCropPrice.Mandi> cropList,
                                      SellProduce sellProduce) {
        this.context = context;
        this.type = type;
        this.cropList = cropList;
        this.sellProduce = sellProduce;

    }

    @Override
    public RecyclerAdapterSingleMandi.MandiData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_single_mandi_crops, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterSingleMandi.MandiData(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterSingleMandi.MandiData holder, int position) {
        final int pos = holder.getAdapterPosition();
        holder.cropName.setText(context.getString(R.string.crop) + ": " + cropList.get(pos).getProduct_id().get(1));

        if (type.equals("dehaat")) {
            holder.dehaatPriceBack.setVisibility(View.VISIBLE);
            holder.sell_produce.setVisibility(View.VISIBLE);
            holder.minPriceBack.setVisibility(View.GONE);
            holder.maxPriceBack.setVisibility(View.GONE);
            holder.bestPriceBack.setVisibility(View.GONE);

            holder.dehaatPrice.setText(context.getString(R.string.rs) + "" + cropList.get(pos).getDehaat_price());
        } else {
            holder.dehaatPriceBack.setVisibility(View.GONE);
            holder.sell_produce.setVisibility(View.GONE);
            if (cropList.get(pos).getMin_price() != null)
                holder.minPrice.setText(context.getString(R.string.rs) + "" + cropList.get(pos).getMin_price());
            if (cropList.get(pos).getMax_price() != null)
                holder.maxPrice.setText(context.getString(R.string.rs) + "" + cropList.get(pos).getMax_price());
            if (cropList.get(pos).getBest_price() != null)
                holder.bestPrice.setText(context.getString(R.string.rs) + "" + cropList.get(pos).getBest_price());

        }
        holder.sell_produce.setOnClickListener(view -> {
            if (cropList.get(pos).getCrop_id() == null)
                sellProduce.onClick(null);
            else
                sellProduce.onClick(Long.valueOf(cropList.get(pos).getCrop_id().get(0)));
        });
    }

    @Override
    public int getItemCount() {
        return cropList.size();
    }

    public class MandiData extends RecyclerView.ViewHolder {
        TextView cropName, minPrice, maxPrice, bestPrice, dehaatPrice, sell_produce;
        LinearLayout back, minPriceBack, maxPriceBack, bestPriceBack, dehaatPriceBack;


        public MandiData(View itemView) {
            super(itemView);

            back = itemView.findViewById(R.id.back);
            cropName = itemView.findViewById(R.id.cropName);
            minPrice = itemView.findViewById(R.id.minPrice);
            maxPrice = itemView.findViewById(R.id.maxPrice);
            bestPrice = itemView.findViewById(R.id.bestPrice);
            dehaatPrice = itemView.findViewById(R.id.dehaatPrice);
            sell_produce = itemView.findViewById(R.id.sell_produce);
            minPriceBack = itemView.findViewById(R.id.minPriceBack);
            maxPriceBack = itemView.findViewById(R.id.maxPriceBack);
            bestPriceBack = itemView.findViewById(R.id.bestPriceBack);
            dehaatPriceBack = itemView.findViewById(R.id.dehaatPriceBack);
        }

    }
}
