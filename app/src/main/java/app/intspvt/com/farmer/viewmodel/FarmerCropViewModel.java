package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import app.intspvt.com.farmer.repository.FarmerCropRepository;
import app.intspvt.com.farmer.rest.response.CropsData;

public class FarmerCropViewModel extends ViewModel {
    private MutableLiveData<ArrayList<CropsData.CropsInfo>> enquiryList;
    private FarmerCropRepository cropRepository;

    public void init() {
        if (cropRepository == null)
            cropRepository = FarmerCropRepository.getInstance();
        enquiryList = cropRepository.getFarmerCropList();
    }

    public LiveData<ArrayList<CropsData.CropsInfo>> getFarmerCropList() {
        return enquiryList;
    }
}

