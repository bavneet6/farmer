package app.intspvt.com.farmer.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;

import app.intspvt.com.farmer.Farmer;
import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.LoginActivity;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.database.DatabaseHandler;
import app.intspvt.com.farmer.rest.ApiCallback;
import app.intspvt.com.farmer.rest.AppRestClient;
import app.intspvt.com.farmer.rest.PicassoCallback;
import app.intspvt.com.farmer.rest.body.PostFarmerInfo;
import app.intspvt.com.farmer.rest.body.PostFarmerPersonalInfo;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.RoundImageView;
import app.intspvt.com.farmer.utilities.UrlConstant;
import app.intspvt.com.farmer.viewmodel.FarmerInfoViewModel;
import retrofit2.Call;
import retrofit2.Response;


/**
 * Created by DELL on 8/14/2017.
 */

public class EditFarmerInfoFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = EditFarmerInfoFragment.class.getSimpleName();
    ArrayList<String> returnValue = new ArrayList<>();
    private TextView name, editName, num, add, editAdd, logout, node;
    private Button addCrops, addLand, dehaati, changelanguage;
    private ImageView backbutton;
    private RoundImageView imageP;
    private ViewSwitcher viewSwitcher;
    private EditText ename;
    private String getPic = null, picUrlDb, fullAddress;
    private DatabaseHandler databaseHandler;
    private boolean checkPic;
    private FirebaseAnalytics firebaseAnalytics;
    private ArrayList<File> imageFileList1 = new ArrayList<>();
    private FarmerInfoViewModel farmerInfoViewModel;
    private LinearLayout seeOrdersBack;

    public static EditFarmerInfoFragment newInstance() {
        return new EditFarmerInfoFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_edit_farmer_info, null);
        MainActivity.showCallAndHome(true);
        databaseHandler = new DatabaseHandler(this.getActivity());
        dehaati = v.findViewById(R.id.dehaati);
        name = v.findViewById(R.id.name);
        ename = v.findViewById(R.id.ename);
        editName = v.findViewById(R.id.edit_name);
        node = v.findViewById(R.id.node);
        num = v.findViewById(R.id.mobileNo);
        add = v.findViewById(R.id.address);
        editAdd = v.findViewById(R.id.edit_add);
        logout = v.findViewById(R.id.logout);
        addCrops = v.findViewById(R.id.add_crops);
        addLand = v.findViewById(R.id.add_land);
        changelanguage = v.findViewById(R.id.changelanguage);
        backbutton = v.findViewById(R.id.back);
        imageP = v.findViewById(R.id.image);
        viewSwitcher = v.findViewById(R.id.view_switcher);
        seeOrdersBack = v.findViewById(R.id.see_orders);

        editName.setOnClickListener(this);
        backbutton.setOnClickListener(this);
        editAdd.setOnClickListener(this);
        addCrops.setOnClickListener(this);
        addLand.setOnClickListener(this);
        changelanguage.setOnClickListener(this);
        logout.setOnClickListener(this);
        imageP.setOnClickListener(this);
        dehaati.setOnClickListener(this);
        seeOrdersBack.setOnClickListener(this);

        //display the number and name
        name.setText(AppPreference.getInstance().getName());
        num.setText(AppPreference.getInstance().getMobile());
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        AppUtils.showProgressDialog(getActivity());
        farmerInfoViewModel = ViewModelProviders.of(getActivity()).get(FarmerInfoViewModel.class);
        farmerInfoViewModel.init();
        observeViewModel(farmerInfoViewModel);
        return v;
    }

    private void observeViewModel(FarmerInfoViewModel viewModel) {
        viewModel.getPersonalInfo()
                .observe(this, farmerInfo -> {

                    if (farmerInfo != null) {
                        if (farmerInfo.getFarmer_info() != null)
                            AppUtils.storeFarmerInfo(farmerInfo.getFarmer_info());

                        if (farmerInfo.getNode_info() != null) {
                            AppUtils.storeNodeData(farmerInfo.getNode_info());
                        }
                        displayData();
                    }
                    AppUtils.hideProgressDialog();

                });
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("FarmerProfileEdit", Long.parseLong(AppPreference.getInstance().getMobile()));
        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        firebaseAnalytics.logEvent("FarmerProfileEdit", params);
    }

    private void displayData() {
        // printing the pic
        getPic = AppPreference.getInstance().getIMAGE();
        node.setText(AppPreference.getInstance().getNODE_NAME());

        if (!AppUtils.isNullCase(getPic)) {
            checkPic = databaseHandler.checkImage(getPic);

            if (!checkPic)
                AppUtils.generateAndCheckUrl(getActivity(), getPic);

            picUrlDb = databaseHandler.getFileUrl(getPic);
            if (picUrlDb == null)
                return;
            if (getActivity() == null)
                return;
            final Picasso picasso = Picasso.with(getActivity());
            picasso.setLoggingEnabled(true);
            picasso.load(picUrlDb).into(imageP, new PicassoCallback(getPic) {

                @Override
                public void onErrorPic(URL url) {
                    picasso.load("" + url).error(R.drawable.pic_gender).into(imageP);
                }
            });

        }
        //display the name
        name.setText(AppPreference.getInstance().getName());
        //display the address
        fullAddress = "";
        if (AppPreference.getInstance().getVILLAGE() != null && AppPreference.getInstance().getVILLAGE().size() != 0) {
            String input = AppPreference.getInstance().getVILLAGE().get(1);
            input = input.replace("\n", "");
            input = input.trim();
            if (fullAddress.equals(""))
                fullAddress = input;
            else
                fullAddress = fullAddress + "," + input;
            add.setText(fullAddress);
        }
        if (AppPreference.getInstance().getPANCHAYAT() != null && AppPreference.getInstance().getPANCHAYAT().size() != 0) {
            String input = AppPreference.getInstance().getPANCHAYAT().get(1);
            input = input.replace("\n", "");
            input = input.trim();
            if (fullAddress.equals(""))
                fullAddress = input;
            else
                fullAddress = fullAddress + "," + input;
            add.setText(fullAddress);
        }
        if (AppPreference.getInstance().getBLOCK() != null && AppPreference.getInstance().getBLOCK().size() != 0) {
            String input = AppPreference.getInstance().getBLOCK().get(1);
            input = input.replace("\n", "");
            input = input.trim();
            if (fullAddress.equals(""))
                fullAddress = input;
            else
                fullAddress = fullAddress + "," + input;
            add.setText(fullAddress);
        }
        if (AppPreference.getInstance().getDISTRICT() != null && AppPreference.getInstance().getDISTRICT().size() != 0) {
            String input = AppPreference.getInstance().getDISTRICT().get(1);
            input = input.replace("\n", "");
            input = input.trim();
            if (fullAddress.equals(""))
                fullAddress = input;
            else
                fullAddress = fullAddress + "," + input;
            add.setText(fullAddress);
        }
        if (AppPreference.getInstance().getSTATE() != null && AppPreference.getInstance().getSTATE().size() != 0) {
            String input = AppPreference.getInstance().getSTATE().get(1);
            input = input.replace("\n", "");
            input = input.trim();
            if (fullAddress.equals(""))
                fullAddress = input;
            else
                fullAddress = fullAddress + "," + input;
            add.setText(fullAddress);
        }
        if (!AppUtils.isNullCase("" + AppPreference.getInstance().getPINCODE())) {
            fullAddress = fullAddress + "  " + AppPreference.getInstance().getPINCODE();
            add.setText(fullAddress);
        }
    }

    @Override
    public void onClick(View view) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        int id = view.getId();
        switch (id) {

            case R.id.edit_name:
                if (AppUtils.haveNetworkConnection(getActivity())) {
                    if (editName.getText().equals(getString(R.string.change_name))) {
                        editName.setText(getString(R.string.save_name));
                        viewSwitcher.showNext();
                        ename.setText(AppPreference.getInstance().getName());
                        ename.requestFocus();
                        ename.setSelection(ename.getText().toString().length());
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(ename, InputMethodManager.SHOW_IMPLICIT);
                    } else {
                        String temp = ename.getText().toString();
                        if (!temp.equals("")) {
                            viewSwitcher.showPrevious();
                            editName.setText(getString(R.string.change_name));
                            name.setText(ename.getText().toString());
                            ((InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE))
                                    .hideSoftInputFromWindow(ename.getWindowToken(), 0);
                            sendUpdateInfo(name.getText().toString(), null);
                        } else {
                            editName.setText(getString(R.string.save_name));
                            editName.setError(Html.fromHtml(getString(R.string.your_name)), null);
                            ename.requestFocus();

                        }
                    }
                } else {
                    showInternetDialog();
                }
                break;
            case R.id.edit_add:
                transaction.replace(R.id.frag_container, AddressFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.add_crops:
                transaction.replace(R.id.frag_container, CropFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.add_land:
                transaction.replace(R.id.frag_container, LandInfoFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.logout:
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.want_logout);
                builder.setCancelable(false);

                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        Bundle params = new Bundle();
                        params.putLong("Logout", Long.parseLong(AppPreference.getInstance().getMobile()));
                        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
                        firebaseAnalytics.logEvent("Logout", params);
                        AppPreference.getInstance().setAPP_LOGIN(false);
                        AppPreference.getInstance().clearData();
                        Intent i = new Intent(getActivity(), LoginActivity.class);
                        i.putExtra("logout", "logout");
                        databaseHandler.clearCartTable();
                        databaseHandler.clearIssueStatusSeen();
                        databaseHandler.clearURlsData();
                        Farmer.getInstance().clearApplicationData();
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getActivity().startActivity(i);
                        getActivity().finish();
                        dialog.dismiss();

                    }
                });
                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                android.app.AlertDialog alert = builder.create();
                alert.show();
                break;
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.image:
                if (AppUtils.haveNetworkConnection(getActivity()))
                    AppUtils.imageSelection(this, 1);
                else
                    showInternetDialog();
                break;
            case R.id.dehaati:
                transaction.replace(R.id.frag_container, YourDehaatFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.see_orders:
                transaction.replace(
                        R.id.frag_container,
                        OrderHistoryFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.changelanguage:
                if (AppPreference.getInstance().getLANGUAGE() != null)
                    if (AppPreference.getInstance().getLANGUAGE().equals(UrlConstant.HINDI))
                        AppPreference.getInstance().setLANGUAGE(UrlConstant.ENGLISH);
                    else
                        AppPreference.getInstance().setLANGUAGE(UrlConstant.HINDI);

                Farmer.setLocale(getActivity(), AppPreference.getInstance().getLANGUAGE());
                ((MainActivity) getActivity()).refreshUI();
                getActivity().onBackPressed();// to relaunch the activity
                break;

        }
    }

    private void sendUpdateInfo(final String name, File fileName) {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        int stateId = 0, districtId = 0, blockId = 0, panchayatId = 0, villageId = 0;

        if (AppPreference.getInstance().getSTATE() != null && AppPreference.getInstance().getSTATE().size() != 0)
            stateId = Integer.parseInt(AppPreference.getInstance().getSTATE().get(0));

        if (AppPreference.getInstance().getDISTRICT() != null && AppPreference.getInstance().getDISTRICT().size() != 0)
            districtId = Integer.parseInt(AppPreference.getInstance().getDISTRICT().get(0));

        if (AppPreference.getInstance().getBLOCK() != null && AppPreference.getInstance().getBLOCK().size() != 0)
            blockId = Integer.parseInt(AppPreference.getInstance().getBLOCK().get(0));

        if (AppPreference.getInstance().getPANCHAYAT() != null && AppPreference.getInstance().getPANCHAYAT().size() != 0)
            panchayatId = Integer.parseInt(AppPreference.getInstance().getPANCHAYAT().get(0));

        if (AppPreference.getInstance().getVILLAGE() != null && AppPreference.getInstance().getVILLAGE().size() != 0)
            villageId = Integer.parseInt(AppPreference.getInstance().getVILLAGE().get(0));

        PostFarmerInfo.PostInfo.FarmerInfo farmerInfo = new PostFarmerInfo.PostInfo.FarmerInfo(stateId, districtId, blockId, panchayatId, villageId, name, AppPreference.getInstance().getPINCODE());
        PostFarmerInfo.PostInfo getInfo = new PostFarmerInfo.PostInfo(farmerInfo);
        Call<PostFarmerPersonalInfo> call = client.updateFarmerInfo(fileName, getInfo);
        call.enqueue(new ApiCallback<PostFarmerPersonalInfo>() {
            @Override
            public void onResponse(Response<PostFarmerPersonalInfo> response) {
                AppUtils.hideProgressDialog();
                farmerInfoViewModel.init();
                observeViewModel(farmerInfoViewModel);
            }

            @Override
            public void onResponse401(Response<PostFarmerPersonalInfo> response) throws JSONException {
            }


        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("val", "requestCode ->  " + requestCode + "  resultCode " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (100): {
                if (resultCode == Activity.RESULT_OK) {
                    returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                    storeImagesInList(returnValue);
                }
            }
            break;
        }
    }

    private void storeImagesInList(ArrayList<String> returnValue) {
        AppUtils.showProgressDialog(getActivity());
        imageFileList1.clear();
        for (int i = 0; i < returnValue.size(); i++) {
            File file = new File(returnValue.get(i));
            imageFileList1.add(file);
        }
        Bitmap bitmap = new BitmapDrawable(getActivity().getResources(), imageFileList1.get(0).getAbsolutePath()).getBitmap();
        imageP.setImageBitmap(bitmap);
        sendUpdateInfo(null, imageFileList1.get(0));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AppUtils.imageSelection(this, 5);
                } else {
                }
                return;
            }
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }


    private void showInternetDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.information)
                .setMessage(R.string.info_update_net_req)
                .setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }
}
