package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import app.intspvt.com.farmer.repository.ProductListRepository;
import app.intspvt.com.farmer.rest.response.Products;

public class ProductListViewModel extends ViewModel {
    private MutableLiveData<ArrayList<Products>> productList;
    private ProductListRepository productListRepository;

    public void init(String ids) {
        if (productListRepository == null)
            productListRepository = ProductListRepository.getInstance();
        productList = productListRepository.getProductData(ids);
    }

    public LiveData<ArrayList<Products>> getProductData() {
        return productList;
    }
}
