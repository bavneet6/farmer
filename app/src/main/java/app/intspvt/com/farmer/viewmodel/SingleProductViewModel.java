package app.intspvt.com.farmer.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import app.intspvt.com.farmer.repository.SingleProductRepository;
import app.intspvt.com.farmer.rest.response.Products;

public class SingleProductViewModel extends ViewModel {
    private MutableLiveData<Products> products;
    private SingleProductRepository singleProductRepository;

    public void init(Long id) {
        if (singleProductRepository == null)
            singleProductRepository = SingleProductRepository.getInstance();
        products = singleProductRepository.getSingleProduct(id);
    }

    public LiveData<Products> getSingleProduct() {
        return products;
    }
}
