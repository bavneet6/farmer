package app.intspvt.com.farmer.fragment;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

import app.intspvt.com.farmer.R;
import app.intspvt.com.farmer.activity.MainActivity;
import app.intspvt.com.farmer.adapter.RecyclerAdapterProductList;
import app.intspvt.com.farmer.rest.response.Products;
import app.intspvt.com.farmer.utilities.AppPreference;
import app.intspvt.com.farmer.utilities.AppUtils;
import app.intspvt.com.farmer.utilities.ShowCartCount;
import app.intspvt.com.farmer.utilities.UrlConstant;
import app.intspvt.com.farmer.viewmodel.ProductListViewModel;

/**
 * Created by DELL on 11/15/2017.
 */

public class ProductListFragment extends BaseFragment implements SearchView.OnQueryTextListener, AdapterView.OnItemSelectedListener, View.OnClickListener, ShowCartCount {
    public static final String TAG = ProductListFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    private RecyclerAdapterProductList recyclerAdapterProductList;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Products> sPro = new ArrayList<>();
    private ArrayList<Products> fPro = new ArrayList<>();
    private ArrayList<Products> cPro = new ArrayList<>();
    private ArrayList<Products> data = new ArrayList<>();
    private ArrayList<String> categoriesList = new ArrayList<>();
    private Spinner category;
    private String selected = "";
    private RelativeLayout cart_back;
    private TextView cart_count, order_history;
    private ImageView back;
    private FirebaseAnalytics firebaseAnalytics;
    private SearchView searchView;
    private ProductListViewModel productListViewModel;

    public static ProductListFragment newInstance() {
        return new ProductListFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_product_list, null);
        MainActivity.showCallAndHome(true);
        category = v.findViewById(R.id.category);
        recyclerView = v.findViewById(R.id.products_list);
        recyclerView.setNestedScrollingEnabled(false);
        searchView = v.findViewById(R.id.search);
        cart_back = v.findViewById(R.id.cart_back);
        back = v.findViewById(R.id.back);
        order_history = v.findViewById(R.id.order_history);
        cart_count = v.findViewById(R.id.cart_count);
        searchView.setOnQueryTextListener(this);
        category.setOnItemSelectedListener(this);
        cart_back.setOnClickListener(this);
        back.setOnClickListener(this);
        order_history.setOnClickListener(this);
        categoriesList.clear();
        categoriesList.add(getString(R.string.all));
        categoriesList.add(getString(R.string.seed));
        categoriesList.add(getString(R.string.fertlizer));
        categoriesList.add(getString(R.string.crop_protection));
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        try {
            ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(), R.layout.template_spinner_filter_text, categoriesList);
            adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            category.setAdapter(adp);
        } catch (ActivityNotFoundException ac) {
        }
        searchView.setOnClickListener(v1 -> searchView.setIconified(false));

        showCartCount();
        AppUtils.showProgressDialog(getActivity());
        printData();
        productListViewModel = ViewModelProviders.of(getActivity()).get(ProductListViewModel.class);
        productListViewModel.init(null);
        observeViewModel(productListViewModel);
        return v;

    }

    private void observeViewModel(ProductListViewModel viewModel) {
        viewModel.getProductData()
                .observe(this, productsArrayList -> {
                    sPro = new ArrayList<>();
                    fPro = new ArrayList<>();
                    cPro = new ArrayList<>();
                    AppUtils.hideProgressDialog();
                    data.clear();
                    if (productsArrayList == null)
                        return;
                    data.addAll(productsArrayList);
                    for (int i = 0; i < data.size(); i++) {
                        if (data.get(i).getCategory() != null) {
                            if (data.get(i).getCategory().equals(UrlConstant.SEED))
                                sPro.add(data.get(i));
                            else if (data.get(i).getCategory().equals(UrlConstant.FERT))
                                fPro.add(data.get(i));
                            else if (data.get(i).getCategory().equals(UrlConstant.CROP))
                                cPro.add(data.get(i));
                        }
                    }

                    printData();
                });
    }

    private void printData() {
        if (getActivity() != null && isAdded()) {
            if (selected.equals(""))
                return;
            if (selected.equals(getString(R.string.all))) {
                recyclerAdapterProductList = new RecyclerAdapterProductList(getActivity(), data, this);
            } else if (selected.equals(getString(R.string.seed))) {
                recyclerAdapterProductList = new RecyclerAdapterProductList(getActivity(), sPro, this);

            } else if (selected.equals(getString(R.string.fertlizer))) {
                recyclerAdapterProductList = new RecyclerAdapterProductList(getActivity(), fPro, this);

            } else if (selected.equals(getString(R.string.crop_protection))) {
                recyclerAdapterProductList = new RecyclerAdapterProductList(getActivity(), cPro, this);

            }
            layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(recyclerAdapterProductList);

            if (!searchView.isIconified()) {
                searchView.setQuery("", false);
            }
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("ProductListPage", Long.parseLong(AppPreference.getInstance().getMobile()));
        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getMobile());
        firebaseAnalytics.logEvent("ProductListPage", params);
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        int id = v.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.order_history:
                transaction.replace(R.id.frag_container, OrderHistoryFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
            case R.id.cart_back:
                transaction.replace(R.id.frag_container, CartFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                break;
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {

            case R.id.category:
                selected = parent.getItemAtPosition(position).toString();
                printData();
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (data.size() != 0) {
            recyclerAdapterProductList.getFilter().filter(newText);
        }
        return false;
    }

    public void showCartCount() {
        AppUtils.showCartCount(getActivity(), cart_count, cart_back);
    }

    @Override
    public void showCart(boolean showCart) {
        showCartCount();
    }
}
